/*
Copyright (c) 2003-2011 Alberto Demichelis

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.
*/
#ifndef _VOX_H_
#define _VOX_H_

#include <string>
#include <sstream>
#include <cstring>


/* version and signature information */
#define VOX_VERSION "Vox 1.8.0 experimental"
#define VOX_VERSION_NUMBER 180
#define VOX_COPYRIGHT "Copyright (C) 2003-2011 Beelzebub Software"
#define VOX_AUTHOR  "Beelzebub Software"

/* platform information */
#if defined(__x86_64__) || defined(__ppc64__) || defined(_WIN64)
#   define VOX_PLATFORM_BITS 64
#   define VOX_PLATFORM_BITS_NAME "64"
#else
#   define VOX_PLATFORM_BITS 32
#   define VOX_PLATFORM_BITS_NAME "32"
#endif

/* first the rough check (namely posix) ... */

#if defined(__unix__)
#   include <unistd.h>
#endif

/* now check for details ... */
#if defined(WIN32) || defined(_WIN32) || defined(_WIN64)
#   define VOX_PLATFORM_MSWINDOWS
#   define VOX_PLATFORM_UNTESTED 0
#   define VOX_PLATFORM_NAME "win" VOX_PLATFORM_BITS_NAME
#   define VOX_PLATFORM_GROUP_NAME "nt"
#elif defined(linux) || defined(__linux__)
#   define VOX_PLATFORM_UNIX
#   define VOX_PLATFORM_LINUX 1
#   define VOX_PLATFORM_UNTESTED 0
#   define VOX_PLATFORM_NAME "linux" VOX_PLATFORM_BITS_NAME
#elif defined(unix) || defined(__unix__)
/* TODO: differentiate between *BSD, MacOSX, etc */
#   define VOX_PLATFORM_UNIX
#   define VOX_PLATFORM_UNTESTED 1
#   define VOX_PLATFORM_NAME "unix" VOX_PLATFORM_BITS_NAME
#else
#   ifdef VOX_IGNORE_UNKNOWN_PLATFORM
#       define VOX_PLATFORM_UNKNOWN
#       define VOX_PLATFORM_UNTESTED 1
#       define VOX_PLATFORM_NAME "unknown" VOX_PLATFORM_BITS_NAME
#       define VOX_PLATFORM_GROUP_NAME "unknown"
#   else
#       error "Unknown platform! Define 'VOX_IGNORE_UNKNOWN_PLATFORM' to compile anyway"
#   endif
#endif

/* we deliberately skipped defining VOX_PLATFORM_GROUP_NAME on
   platforms that may define '_POSIX_VERSION'.
   The idea is to overwrite the platform_group for platforms
   that support posix, as defined by the posix standard. */
#if defined(_POSIX_VERSION)
#   if defined(VOX_PLATFORM_GROUP_NAME)
#       undef VOX_PLATFORM_GROUP_NAME
#   endif
#   define VOX_PLATFORM_GROUP_NAME "posix"
#endif

/* if at this point VOX_PLATFORM_GROUP_NAME is still
   undefined, just give it a default value */
#if !defined(VOX_PLATFORM_GROUP_NAME)
#   define VOX_PLATFORM_GROUP_NAME "unknown"
#endif

/* now try to figure out 64bit integer ... */
#if defined(__GNUC__)
#   define VX_LONGINT long long
#elif defined(_MSVC)
#   define VX_LONGINT __int64
#else
    /* just go with <long>... for now. better solution needed, though! */
#   define VX_LONGINT long
#endif

/* stdlib partly depends on boost */
#if defined(VOX_STDLIB_AVAILABLE)
#   undef VOX_STDLIB_AVAILABLE
#endif
#ifdef VOX_HAVEBOOST
#   define VOX_STDLIB_AVAILABLE 1
#endif



#define vox_assert(expr) \
    do \
    { \
        if(bool(expr) == 0) \
        { \
            vox_aux_asserthandler(\
                    #expr, \
                    __FUNCTION__, \
                    __FILE__, \
                    __LINE__); \
        } \
    } while(0);

#define VX_MALLOC(__size) \
    vox_mem_malloc((__size));

#define VX_FREE(__ptr,__size) \
    vox_mem_free((__ptr), (__size));

#define VX_REALLOC(__ptr,__oldsize,__size) \
    vox_mem_realloc((__ptr),(__oldsize),(__size));

/* sanity macros to check errorcodes (see return codes for native closures) */
#define VX_FAILED(res) ((res) == VX_ERROR)
#define VX_SUCCEEDED(res) (!VX_FAILED(res))

/* return codes for native closures */
#define VX_OK (0)
#define VX_ERROR (-1)

/* declaration for extern definitions */
#define VOX_API extern


typedef VX_LONGINT VXInteger;
typedef int VXInt32;
typedef unsigned int VXUnsignedInteger32;
typedef unsigned int VXUnsignedInteger;
typedef unsigned int VXHash;
typedef float VXFloat;
typedef int VXChar;
typedef long VXRawValue;
typedef void* VXUserPointer;


void* vox_mem_malloc(VXUnsignedInteger size);
void* vox_mem_realloc(void* p, VXUnsignedInteger oldsize,VXUnsignedInteger size);
void vox_mem_free(void *p,VXUnsignedInteger size);

struct VXState;
struct VXInstruction;
struct VXWeakRefObj;
struct VXClassObj;
struct VXTableObj;
struct VXArrayObj;
struct VXStringObj;
struct VXForeignClosureObj;
struct VXGeneratorObj;
struct VXNativeClosureObj;
struct VXUserDataObj;
struct VXFuncProtoObj;
struct VXRefCountedObj;
struct VXClassObj;
struct VXInstanceObj;
struct VXDelegableObj;
struct VXOuterObj;
struct VXObject;
struct VXSharedState;


typedef VXInteger (*VXFunction)(VXState*);

typedef VXInteger (*VXReleaseHook)(VXUserPointer,VXInteger size);

typedef void (*VXCompileError)(VXState* /* v */,
                               const char* /* desc */,
                               const char* /* source */,
                               VXInteger /* line */,
                               VXInteger /* column */);

typedef void (*VXPrintFunction)(VXState*,
                                const char * ,...);



typedef VXInteger (*VXWriteFunc)(VXUserPointer,
                                 VXUserPointer,
                                 VXInteger);

typedef VXInteger(*VXReadFunc)(VXUserPointer,
                               VXUserPointer,
                               VXInteger);

typedef VXInteger(*VXPrintFunc)(
        VXState* v,
        bool isend,
        const char* str,
        VXInteger len);

typedef VXInteger(*VXLexReadFunc)(VXUserPointer);

struct VXRegFunction
{
    const char *name;
    VXFunction func;
    VXInteger nparamscheck;
    const char *typemask;
};

struct VXFunctionInfo
{
    VXUserPointer funcid;
    const char *name;
    const char *source;
};

/* vxobject type masks */
enum
{
    VX_RT_MASK          = 0x00FFFFFF,
    VX_RT_NULL          = 0x00000001,
    VX_RT_INTEGER       = 0x00000002,
    VX_RT_FLOAT         = 0x00000004,
    VX_RT_BOOL          = 0x00000008,
    VX_RT_STRING        = 0x00000010,
    VX_RT_TABLE         = 0x00000020,
    VX_RT_ARRAY         = 0x00000040,
    VX_RT_USERDATA      = 0x00000080,
    VX_RT_CLOSURE       = 0x00000100,
    VX_RT_NATIVECLOSURE = 0x00000200,
    VX_RT_GENERATOR     = 0x00000400,
    VX_RT_USERPOINTER   = 0x00000800,
    VX_RT_THREAD        = 0x00001000,
    VX_RT_FUNCPROTO     = 0x00002000,
    VX_RT_CLASS         = 0x00004000,
    VX_RT_INSTANCE      = 0x00008000,
    VX_RT_WEAKREF       = 0x00010000,
    VX_RT_OUTER         = 0x00020000
};

/* object type constants */
enum
{
    VXOBJECT_REF_COUNTED = 0x08000000,
    VXOBJECT_NUMERIC     = 0x04000000,
    VXOBJECT_DELEGABLE   = 0x02000000,
    VXOBJECT_CANBEFALSE  = 0x01000000
};

enum VXMetaMethod
{
    VX_MT_ADD       = 0,
    VX_MT_SUB       = 1,
    VX_MT_MUL       = 2,
    VX_MT_DIV       = 3,
    VX_MT_UNM       = 4,
    VX_MT_MODULO    = 5,
    VX_MT_SET       = 6,
    VX_MT_GET       = 7,
    VX_MT_TYPEOF    = 8,
    VX_MT_NEXTI     = 9,
    VX_MT_CMP       = 10,
    VX_MT_CALL      = 11,
    VX_MT_CLONED    = 12,
    VX_MT_NEWSLOT   = 13,
    VX_MT_DELSLOT   = 14,
    VX_MT_TOSTRING  = 15,
    VX_MT_NEWMEMBER = 16,
    VX_MT_INHERITED = 17,
    VX_MT_LAST      = 18
};

enum VXOType
{
    VX_OT_NULL =
        (VX_RT_NULL | VXOBJECT_CANBEFALSE),
    VX_OT_INTEGER =
        (VX_RT_INTEGER | VXOBJECT_NUMERIC | VXOBJECT_CANBEFALSE),
    VX_OT_FLOAT =
        (VX_RT_FLOAT | VXOBJECT_NUMERIC | VXOBJECT_CANBEFALSE),
    VX_OT_BOOL =
        (VX_RT_BOOL | VXOBJECT_CANBEFALSE),
    VX_OT_STRING =
        (VX_RT_STRING | VXOBJECT_REF_COUNTED),
    VX_OT_TABLE =
        (VX_RT_TABLE | VXOBJECT_REF_COUNTED | VXOBJECT_DELEGABLE),
    VX_OT_ARRAY =
        (VX_RT_ARRAY | VXOBJECT_REF_COUNTED),
    VX_OT_USERDATA =
        (VX_RT_USERDATA | VXOBJECT_REF_COUNTED | VXOBJECT_DELEGABLE),
    VX_OT_CLOSURE =
        (VX_RT_CLOSURE | VXOBJECT_REF_COUNTED),
    VX_OT_NATIVECLOSURE =
        (VX_RT_NATIVECLOSURE | VXOBJECT_REF_COUNTED),
    VX_OT_GENERATOR =
        (VX_RT_GENERATOR | VXOBJECT_REF_COUNTED),
    VX_OT_USERPOINTER =
        (VX_RT_USERPOINTER),
    VX_OT_THREAD =
        (VX_RT_THREAD | VXOBJECT_REF_COUNTED),
    VX_OT_FUNCPROTO =
        (VX_RT_FUNCPROTO | VXOBJECT_REF_COUNTED),
    VX_OT_CLASS =
        (VX_RT_CLASS | VXOBJECT_REF_COUNTED),
    VX_OT_INSTANCE =
        (VX_RT_INSTANCE | VXOBJECT_REF_COUNTED | VXOBJECT_DELEGABLE),
    VX_OT_WEAKREF =
        (VX_RT_WEAKREF | VXOBJECT_REF_COUNTED),
    VX_OT_OUTER =
        (VX_RT_OUTER | VXOBJECT_REF_COUNTED)
};

template<typename Type> class VXVector
{
    private: // variables
        VXUnsignedInteger _size;
        VXUnsignedInteger _allocated;
        Type* _vals;

    private: // functions
        void _do_realloc(VXUnsignedInteger newsize)
        {
            newsize = (newsize > 0) ? newsize : 4;
            _vals = (Type*)VX_REALLOC(_vals,
                        _allocated * sizeof(Type),
                        newsize * sizeof(Type));
            _allocated = newsize;
        }


    public: // functions
        VXVector()
        {
            _vals = NULL;
            _size = 0;
            _allocated = 0;
        }

        VXVector(const VXVector<Type>& v)
        {
            copy(v);
        }

        ~VXVector()
        {
            if(_allocated)
            {
                for(VXUnsignedInteger i = 0; i < _size; i++)
                {
                    _vals[i].~Type();
                }
                VX_FREE(_vals, (_allocated * sizeof(Type)));
            }
        }

        void copy(const VXVector<Type>& v)
        {
            if(_size)
            {
                //destroys all previous stuff
                resize(0);
            }
            if(v._size > _allocated)
            {
                this->_do_realloc(v._size);
            }
            for(VXUnsignedInteger i = 0; i < v._size; i++)
            {
                new ((void *)&_vals[i]) Type(v._vals[i]);
            }
            _size = v._size;
        }

        void clear()
        {
            resize(0);
        }

        void reserve(VXUnsignedInteger newsize)
        {
            this->_do_realloc(newsize);
        }

        void resize(VXUnsignedInteger newsize, const Type& fill = Type())
        {
            if(newsize > _allocated)
            {
                this->_do_realloc(newsize);
            }
            if(newsize > _size)
            {
                while(_size < newsize)
                {
                    new ((void *)&_vals[_size]) Type(fill);
                    _size++;
                }
            }
            else
            {
                for(VXUnsignedInteger i = newsize; i < _size; i++)
                {
                    _vals[i].~Type();
                }
                _size = newsize;
            }
        }

        void shrinktofit()
        {
            if(_size > 4)
            {
                this->_do_realloc(_size);
            }
        }

        Type& top() const
        {
            return _vals[_size - 1];
        }

        inline VXUnsignedInteger size() const
        {
            return _size;
        }

        bool empty() const
        {
            return (_size <= 0);
        }

        inline Type &push_back(const Type& val = Type())
        {
            if(_allocated <= _size)
            {
                this->_do_realloc(_size * 2);
            }
            return *(new ((void *)&_vals[_size++]) Type(val));
        }

        inline void pop_back()
        {
            _size--; _vals[_size].~Type();
        }

        void insert(VXUnsignedInteger idx, const Type& val)
        {
            resize(_size + 1);
            for(VXUnsignedInteger i = _size - 1; i > idx; i--)
            {
                _vals[i] = _vals[i - 1];
            }
            _vals[idx] = val;
        }

        void remove(VXUnsignedInteger idx)
        {
            _vals[idx].~Type();
            if(idx < (_size - 1))
            {
                memcpy(&_vals[idx], &_vals[idx+1], sizeof(Type) * (_size - idx - 1));
            }
            _size--;
        }

        VXUnsignedInteger capacity()
        {
            return _allocated;
        }

        inline Type &back() const
        {
            return _vals[_size - 1];
        }

        inline Type& operator[](VXUnsignedInteger pos) const
        {
            return _vals[pos];
        }

        inline Type& at(VXUnsignedInteger pos) const
        {
            return _vals[pos];
        }

        inline Type* values()
        {
            return this->_vals;
        }
};


struct VXExceptionTrap
{
    VXExceptionTrap()
    {
    }

    VXExceptionTrap(VXInteger ss, VXInteger stackbase,VXInstruction *ip, VXInteger ex_target)
    {
        _stacksize = ss;
        _stackbase = stackbase;
        _ip = ip;
        _extarget = ex_target;
    }

    VXExceptionTrap(const VXExceptionTrap &et)
    {
        (*this) = et;
    }

    VXInteger _stackbase;
    VXInteger _stacksize;
    VXInstruction *_ip;
    VXInteger _extarget;
};


typedef VXVector<VXExceptionTrap> ExceptionsTraps;


struct VXRawObj
{
    union Value
    {
        VXTableObj* pTable;
        VXArrayObj* pArray;
        VXForeignClosureObj *pClosure;
        VXNativeClosureObj *pNativeClosure;
        VXStringObj *pString;
        VXWeakRefObj *pWeakRef;
        VXClassObj *pClass;

        VXFuncProtoObj *pFunctionProto;
        VXUserDataObj *pUserData;
        VXRefCountedObj *pRefCounted;
        VXDelegableObj *pDelegable;
        VXState *pThread;
        VXInstanceObj *pInstance;
        VXOuterObj *pOuter;
        VXGeneratorObj *pGenerator;
        VXRawValue raw;
        VXInteger nInteger;
        VXFloat fFloat;
        VXUserPointer pUserPointer;
    };

    inline VXOType Type() const
    {
        return _type;
    }

    VXOType _type;
    Value _unVal;
};

struct VXObject: public VXRawObj
{
    VXObject();
    VXObject(const VXObject &o);
    VXObject(const VXRawObj &o);
    VXObject(VXTableObj * x);
    VXObject(VXClassObj * x);
    VXObject(VXInstanceObj * x);
    VXObject(VXArrayObj * x);
    VXObject(VXForeignClosureObj * x);
    VXObject(VXNativeClosureObj * x);
    VXObject(VXOuterObj * x);
    VXObject(VXGeneratorObj * x);
    VXObject(VXStringObj * x);
    VXObject(VXUserDataObj * x);
    VXObject(VXWeakRefObj * x);
    VXObject(VXState * x);
    VXObject(VXFuncProtoObj * x);
    VXObject(VXInteger x);
    VXObject(VXFloat x);
    VXObject(VXUserPointer x);
    VXObject(bool bBool);
    ~VXObject();

    VXObject& operator=(VXTableObj *x);
    VXObject& operator=(VXClassObj *x);
    VXObject& operator=(VXInstanceObj *x);
    VXObject& operator=(VXArrayObj *x);
    VXObject& operator=(VXForeignClosureObj *x);
    VXObject& operator=(VXNativeClosureObj *x);
    VXObject& operator=(VXOuterObj *x);
    VXObject& operator=(VXGeneratorObj *x);
    VXObject& operator=(VXStringObj *x);
    VXObject& operator=(VXUserDataObj *x);
    VXObject& operator=(VXWeakRefObj *x);
    VXObject& operator=(VXState *x);
    VXObject& operator=(VXFuncProtoObj *x);
    VXObject& operator=(VXUserPointer x);
    VXObject& operator=(VXInteger x);
    VXObject& operator=(VXFloat x);
    VXObject& operator=(bool b);
    VXObject& operator=(const VXObject& obj);
    VXObject& operator=(const VXRawObj& obj);

    bool Is(VXOType tp) const;
    bool IsNull() const;
    bool IsNumeric() const;
    bool IsTable() const;
    bool IsArray() const;
    bool IsFunction() const;
    bool IsClosure() const;
    bool IsNativeClosure() const;
    bool IsGenerator() const;
    bool IsString() const;
    bool IsInteger() const;
    bool IsFloat() const;
    bool IsUserPointer() const;
    bool IsUserData() const;
    bool IsThread() const;
    bool IsClass() const;
    bool IsInstance() const;
    bool IsBool() const;
    bool IsWeakRef() const;

    bool Boolean() { return Integer(); };
    bool Boolean() const { return Integer(); };
    VXInteger Integer();
    VXInteger Integer() const;
    VXFloat Float();
    VXFloat Float() const;
    VXStringObj* String();
    VXStringObj* String() const;
    void String(const char** dest, VXInteger* len_dest=NULL) const;
    VXTableObj* Table();
    VXTableObj* Table() const;
    VXArrayObj* Array();
    VXArrayObj* Array() const;
    VXGeneratorObj* Generator();
    VXGeneratorObj* Generator() const;
    VXForeignClosureObj* Closure();
    VXForeignClosureObj* Closure() const;
    VXNativeClosureObj* NativeClosure();
    VXNativeClosureObj* NativeClosure() const;
    VXUserDataObj* UserData();
    VXUserDataObj* UserData() const;
    VXUserPointer UserPointer();
    VXUserPointer UserPointer() const;
    VXState* Thread();
    VXState* Thread() const;
    VXFuncProtoObj* FuncProto();
    VXFuncProtoObj* FuncProto() const;
    VXClassObj* Class();
    VXClassObj* Class() const;
    VXInstanceObj* Instance();
    VXInstanceObj* Instance() const;
    VXDelegableObj* Delegable();
    VXDelegableObj* Delegable() const;
    VXWeakRefObj* WeakRef();
    VXWeakRefObj* WeakRef() const;
    VXOuterObj* Outer();
    VXOuterObj* Outer() const;
    VXRefCountedObj* RefCounted();
    VXRefCountedObj* RefCounted() const;
    VXRawValue Raw();
    VXRawValue Raw() const;

    template<typename Type> Type As();

    VXOType Type() const;
    const char* TypeString() const;

    void Null();

    private:
        VXObject(const char *);
};

typedef VXVector<VXObject> VXObjectVec;

struct VXRefCountedObj
{
    VXUnsignedInteger _uiRef;
    struct VXWeakRefObj *_weakref;
    VXRefCountedObj()
    {
        _uiRef = 0;
        _weakref = NULL;
    }
    virtual ~VXRefCountedObj();
    VXWeakRefObj *GetWeakRef(VXOType type);
    virtual void Release()=0;

};

struct VXWeakRefObj : VXRefCountedObj
{
    void Release();
    VXRawObj _obj;
};



struct VXCollectable : public VXRefCountedObj
{
    VXCollectable *_next;
    VXCollectable *_prev;
    VXSharedState *_sharedstate;
    virtual VXOType GetType()=0;
    virtual void Release()=0;
    virtual void Mark(VXCollectable **chain)=0;
    void UnMark();
    virtual void Finalize()=0;
    static void AddToChain(VXCollectable **chain,VXCollectable *c);
    static void RemoveFromChain(VXCollectable **chain,VXCollectable *c);
};

struct VXDelegableObj: public VXCollectable
{
    bool SetDelegate(VXTableObj *m);
    virtual bool GetMetaMethod(VXState *v,VXMetaMethod mm,VXObject &res);
    VXTableObj *_delegate;
};

struct VXStringObj : public VXRefCountedObj
{
    VXStringObj()
    {
    }

    ~VXStringObj()
    {
    }

    public:
        static VXStringObj* Create(VXSharedState* ss, const char* str);

        static VXStringObj* Create(VXSharedState* ss, const char* str, VXInteger len);

        VXInteger Next(const VXObject &refpos, VXObject &outkey, VXObject &outval);

        void Release();

        void Get(const char** dest_str, VXInteger* dest_len);

        inline const char* Value() const
        {
            return this->_val;
        }

        inline VXInteger Length() const
        {
            return this->_len;
        }

        template<typename StrClass>
        void Get(StrClass* dest)
        {
            const char* dest_str;
            VXInteger dest_len;
            Get(&dest_str, &dest_len);
            dest->append(dest_str, dest_len);
        }

        VXSharedState *_sharedstate;

        VXStringObj *_next;

        VXInteger _len;

        VXHash _hash;

        char _val[1];
};

struct VXArrayObj : public VXCollectable
{
    private:
        VXArrayObj(VXSharedState *ss,VXInteger nsize);

        ~VXArrayObj();

    public:
        static VXArrayObj* Create(VXSharedState *ss,VXInteger nInitialSize);
        void Mark(VXCollectable **chain);

        VXOType GetType();

        void Finalize();

        bool Get(const VXInteger nidx,VXObject &val);

        bool Set(const VXInteger nidx,const VXObject &val);

        VXInteger Next(const VXObject &refpos,
                       VXObject &outkey,
                       VXObject &outval);

        VXArrayObj *Clone();

        VXInteger Size() const;

        void Resize(VXInteger size);

        void Resize(VXInteger size,VXObject &fill);

        void Reserve(VXInteger size);

        void Append(const VXObject &o);

        void Extend(const VXArrayObj *a);

        VXObject &Top();

        void Pop();

        bool Insert(VXInteger idx,const VXRawObj &val);

        void ShrinkIfNeeded();

        bool Remove(VXInteger idx);

        void Release();

        VXObjectVec _values;
};

struct VXTableObj: public VXDelegableObj
{
    private:
        struct HashNode
        {
            HashNode()
            {
                next = NULL;
            }

            VXObject val;
            VXObject key;
            HashNode *next;
        };

        HashNode* _firstfree;
        HashNode* _nodes;
        VXInteger _numofnodes;
        VXInteger _usednodes;

        ///////////////////////////
        void AllocNodes(VXInteger nSize);
        void Rehash(bool force);
        void ClearNodes();

        // undefined
        VXTableObj(VXSharedState *ss, VXInteger nInitialSize);


    public:
        static VXTableObj* Create(VXSharedState *ss,VXInteger nInitialSize);

        void Finalize();

        VXTableObj *Clone();

        ~VXTableObj();

        void Mark(VXCollectable **chain);

        VXOType GetType();

        HashNode* GetNode(const VXObject &key,VXHash hash);

        bool Get(const VXObject &key,VXObject &val);

        void Remove(const VXObject &key);

        bool Set(const VXObject &key, const VXObject &val);

        //returns true if a new slot has been created false if it was already present
        bool NewSlot(const VXObject &key,const VXObject &val);

        VXInteger Next(bool getweakrefs,
                       const VXObject &refpos,
                       VXObject &outkey,
                       VXObject &outval);

        VXInteger CountUsed();

        VXInteger Size() const;

        void Clear();

        void Release();

        VXInteger GetBoolValue(const VXObject& keyname, bool* destination);

        VXInteger GetStringValue(const VXObject& keyname, VXStringObj** dest);

        VXInteger GetStringValue(const VXObject& keyname, const char** dest_str, VXInteger* dest_len);

        VXInteger GetIntegerValue(const VXObject& keyname, VXInteger* dest);

        VXInteger GetFloatValue(const VXObject& keyname, VXFloat* dest);

        VXInteger GetArrayValue(const VXObject& keyname, VXArrayObj** dest);

        VXInteger GetTableValue(const VXObject& keyname, VXTableObj** dest);

};

struct VXInstanceObj: public VXDelegableObj
{
    void Init(VXSharedState *ss);
    VXInstanceObj(VXSharedState *ss, VXClassObj *c, VXInteger memsize);
    VXInstanceObj(VXSharedState *ss, VXInstanceObj *c, VXInteger memsize);

    public:
        static VXInstanceObj* Create(VXSharedState *ss, VXClassObj *theclass);

        VXInstanceObj *Clone(VXSharedState *ss);

        ~VXInstanceObj();

        bool Get(const VXObject &key,VXObject &val);

        bool Set(const VXObject &key,const VXObject &val);

        void Release();

        void Finalize();

        void Mark(VXCollectable**);

        VXOType GetType();

        bool InstanceOf(VXClassObj* trg);

        bool GetMetaMethod(VXState *v,VXMetaMethod mm,VXObject &res);

        bool _hasbase;
        VXObject _base;
        VXUserPointer _base_userpointer;
        VXClassObj *_class;
        VXUserPointer _userpointer;
        VXReleaseHook _hook;
        VXInteger _memsize;
        VXObject _values[1];
};

struct VXClassObj: public VXCollectable
{
    struct Member
    {
        VXObject val;
        VXObject attrs;
    };

    typedef VXVector<VXClassObj::Member> ClassMemberVec;

    VXClassObj(VXSharedState *ss, VXClassObj *base);

    public:
        static VXClassObj* Create(VXSharedState *ss, VXClassObj *base);

        ~VXClassObj();

        bool NewSlot(VXSharedState *ss, const VXObject &key, const VXObject &val, bool bstatic);

        bool Get(const VXObject &key,VXObject &val);

        bool GetConstructor(VXObject &ctor);

        bool SetAttributes(const VXObject &key,const VXObject &val);

        bool GetAttributes(const VXObject &key,VXObject &outval);

        void Lock();

        void Release();

        void Finalize();

        void Mark(VXCollectable ** );

        VXOType GetType();

        VXInteger Next(const VXObject &refpos, VXObject &outkey, VXObject &outval);

        VXInstanceObj *CreateInstance();

    public:
        VXSharedState* _ss;

        VXTableObj *_members;

        VXClassObj *_base;

        ClassMemberVec _defaultvalues;

        ClassMemberVec _methods;

        VXObject _metamethods[VX_MT_LAST];

        VXObject _attributes;

        VXUserPointer _typetag;

        VXReleaseHook _hook;

        bool _locked;

        VXInteger _constructoridx;

        VXInteger _udsize;

        VXObject _name;
};


typedef void (*VXDebugHook)(VXState* /* v */,
                            VXInteger /* type */,
                            const char* /* sourcename */,
                            VXInteger /* line */,
                            const char* /*funcname*/);

struct VXMemberHandle
{
    bool _static;
    VXInteger _index;
};

struct VXStackInfos
{
    const char* funcname;
    const char* source;
    VXInteger line;
    VXInteger column;
    bool native;
};


struct VXState: public VXCollectable
{
    enum ExecutionType
    {
        ET_CALL,
        ET_RESUME_GENERATOR,
        ET_RESUME_VM,
        ET_RESUME_THROW_VM
    };

    struct CallInfo
    {
        VXInstruction *_ip;
        VXObject *_literals;
        VXObject _closure;
        VXGeneratorObj *_generator;
        VXInt32 _etraps;
        VXInt32 _prevstkbase;
        VXInt32 _prevtop;
        VXInt32 _target;
        VXInt32 _ncalls;
        bool _root;
    };

    typedef VXVector<CallInfo> CallInfoVec;

    VXState(VXSharedState *ss);

    ~VXState();

    static VXState* Create(VXInteger stacksize);

    static void Destroy(VXState* vm);

    VXInteger GetState();
    VXInteger WakeUp(bool wakeupret,bool retval,bool raiseerror,bool throwerror);

    void DebugHookProxy(VXInteger type,
        const char* sourcename,
        VXInteger line,
        const char* funcname);

    static void _DebugHookProxy(VXState* v,
        VXInteger type,
        const char* sourcename,
        VXInteger line,
        const char* funcname);

    bool Init(VXState *friendvm,
              VXInteger stacksize);

    bool Execute(VXObject &func,
        VXInteger nargs,
        VXInteger stackbase,
        VXObject &outres,
        bool raiseerror,
        ExecutionType et = ET_CALL);

    //starts a native call return when the NATIVE closure returns
    bool CallNative(VXNativeClosureObj* nclosure,
        VXInteger nargs,
        VXInteger newbase,
        VXObject &retval,
        bool &suspend);

    //starts a VOX_ call in the same "Execution loop"
    bool StartCall(VXForeignClosureObj* closure,
                   VXInteger target,
                   VXInteger nargs,
                   VXInteger stackbase,
                   bool tailcall);

    bool CreateClassInstance(VXClassObj* theclass,
                             VXObject& inst,
                             VXObject& constructor);

    //call a generic closure pure VOX_ or NATIVE
    bool Call(VXObject &closure,
        VXInteger nparams,
        VXInteger stackbase,
        VXObject &outres,
        bool raiseerror);

    VXInteger CallSimple(VXObject& closure,
                         VXInteger params,
                         bool retval,
                         bool raiseerror,
                         VXObject& dest);
    VXInteger CallSimple(VXObject& closure, VXInteger params,bool retval,bool raiseerror);
    VXInteger CallStack(VXInteger params,bool retval,bool raiseerror);
    VXInteger CallStack(VXInteger idx, VXInteger params,bool retval,bool raiseerror);

    VXInteger Suspend();

    void CallDebugHook(VXInteger type,
        VXInteger forcedline=0);

    void CallErrorHandler(VXObject &e);

    bool Get(const VXObject &self,
        const VXObject &key,
        VXObject &dest,
        bool raw,
        VXInteger selfidx);

    const char* GetLocal(VXUnsignedInteger level,VXUnsignedInteger idx, VXObject& dest);

    VXInteger FallBackGet(const VXObject &self,
        const VXObject &key,
        VXObject &dest);

    bool InvokeDefaultDelegate(const VXObject &self,
        const VXObject &key,
        VXObject &dest);

    bool Set(const VXObject &self,
             const VXObject &key,
             const VXObject &val,
             VXInteger selfidx);

    VXInteger FallBackSet(const VXObject &self,
                          const VXObject &key,
                          const VXObject &val);

    bool NewSlot(const VXObject &self,
                 const VXObject &key,
                 const VXObject &val,
                 bool bstatic);

    bool DeleteSlot(const VXObject &self, const VXObject &key, VXObject &res);

    bool Clone(const VXObject &self, VXObject &target);

    bool ObjCmp(const VXObject &o1, const VXObject &o2,VXInteger &res);

    bool StringCat(const VXObject &str, const VXObject &obj, VXObject &dest);

    static bool IsEqual(const VXObject &o1,const VXObject &o2,bool &res);

    bool ToString(const VXObject &o,VXObject &res);

    VXStringObj* PrintObjVal(const VXObject &o);

    VXInteger ThrowError();

    VXInteger ThrowError(const char *s, ...);

    VXInteger ThrowError(const VXObject &desc);

    VXInteger Irrecoverable_Error(const char* s, ...);

    VXInteger Raise_IdxError(const VXObject &o,
                             const VXObject& origin,
                             bool is_index=false);

    VXInteger Raise_CompareError(const VXObject& o1, const VXObject& o2);

    VXInteger Raise_ParamTypeError(VXInteger nparam,VXInteger typemask,VXInteger type);

    bool TypeOf(const VXObject &obj1, VXObject &dest);

    void Mark(VXCollectable **chain);

    VXOType GetType();

    void Finalize();

    void GrowCallStack();

    bool EnterFrame(VXInteger newbase, VXInteger newtop, bool tailcall);

    void LeaveFrame();

    void Release();
    ////////////////////////////////////////////////////////////////////////////
    //stack functions for the api
    void Remove(VXInteger n);

    static bool IsFalse(VXObject &o);

    void Pop();

    void Pop(VXInteger n);

    void Push(const VXObject &o);

    void PushString(const char* str, VXInteger len=-1);

    void PushInteger(VXInteger itm);

    void PushFloat(VXFloat itm);

    VXTableObj* RegisterLib(const char* libname,
                     const VXRegFunction* funcs,
                     bool reg_global,
                     VXTableObj* tb=NULL);

    VXClassObj* RegClass(const VXVector<VXRegFunction>& funcs,
                         VXClassObj* base=NULL,
                         VXUserPointer typetag=0);

    VXClassObj* RegClass(const VXRegFunction* funcs,
                         VXClassObj* base=NULL,
                         VXUserPointer typetag=0);

    VXClassObj* NewClass(VXClassObj* baseclass=NULL);

    VXInteger SetParamsCheck(VXObject& o, VXInteger nparams, const char *typemask);

    void PushNull();

    VXObject &Top();

    void SetTop(VXInteger newtop);

    VXInteger GetTop();

    VXObject& PopGet();

    VXObject& GetUp(VXInteger n);

    VXObject& GetAt(VXInteger n);

    VXInteger Repush(VXInteger idx);

    VXInteger StackCall(VXInteger params, bool retval, bool raiseerror);

    //// extras
    void PrintError();

    VXInteger SetDelegate(VXObject& self, const VXObject& mt);

    VXInteger BindEnv(VXObject& o, const VXObject& env, VXObject& ret);

    char* ScratchPad(VXInteger size);

    VXInteger AtExit(const VXObject& ob);

    static void Move(VXState* destination, VXState* source, const VXObject& o);

    static void MoveIndex(VXState* destination, VXState* source, VXInteger idx);

    VXInteger ToStringAt(VXInteger idx, VXObject& dest);

    void SetReleaseHook(VXInteger idx, VXReleaseHook hook);

    VXInteger SetTypeTag(VXObject& o, VXUserPointer typetag);

    VXInteger SetInstanceUp(VXInteger idx, VXUserPointer* p);

    VXInteger GetInstanceUp(VXInteger idx, VXUserPointer *p,VXUserPointer typetag, bool previously_checked_base=false);

    VXInteger RawSetStack(VXInteger idx);

    VXInteger RawGetStack(VXInteger idx);

    VXInteger Raise_InvalidType(VXOType type);

    VXInteger GetSize(const VXObject& o);

    VXInteger GetSizeAt(VXInteger idx);

    VXInteger ClearAt(VXInteger idx);

    VXInteger Clear(VXObject& o);

    void PushLastError();

    VXInteger StackInfos(VXInteger level, VXStackInfos* si);

    void SetPrintFunc(VXPrintFunction printfunc,VXPrintFunction errfunc);

    VXInteger WriteClosure(VXWriteFunc w, VXUserPointer up);

    VXInteger WriteClosureToFile(const char *filename);

    void SetCompilerErrorHandler(VXCompileError f);

    void SetDebugHook(const VXObject& o);

    void SetErrorHandler(const VXObject& o);

    void SetErrorHandlers();

    VXPrintFunction GetErrorFunc();

    void EnableDebugInfo(bool enable);

    VXNativeClosureObj* NewClosure(VXFunction func,
                                   VXUnsignedInteger nfreevars,
                                   const VXObject& name);

    VXNativeClosureObj* NewClosure(VXFunction func,
                                   VXUnsignedInteger nfreevars=0);


    VXStringObj* NewString(const char* str, VXInteger len=-1);

    VXArrayObj* NewArray(VXInteger initial_size=0);

    VXTableObj* NewTable();

    VXState* NewThread(VXState* friendvm, VXInteger initialstacksize);


    VXArrayObj* GetSysArgv();

    VXTableObj* GetRegistryTable();

    VXTableObj* GetRootTable();

    void PushRoot();

    void PopRoot();

    VXSharedState* Shared();

    VXObject& StackGet(VXInteger idx);

    VXInteger ReadClosure(VXReadFunc r, VXUserPointer up, VXObject* dest=NULL);

    VXInteger CompileBuffer(const char* source, VXInteger size, const char* sourcename, bool raiseerror);

    VXInteger LoadFile(const char* filename,
                        bool printerror,
                        bool* failed_to_open=NULL,
                        VXObject* dest=NULL);

    VXInteger DoString(const char* source,
                       VXInteger len,
                       const char* bufname,
                       bool retval,
                       bool printerror);


    VXInteger DoStringFmt(bool globaltable, bool returns,
                          bool raise_error, const char* str_format, ...);

    VXInteger DoFile(const char* path,
                bool retval,
                bool printerror, bool* failed_to_open=NULL);

    void DefineGlobal(const char* name, const VXObject& sym);

    const char* LastError();

    VXInteger GetTypedArg(VXInteger idx, VXOType, VXObject& dest);

    VXInteger GetUserPointer(VXInteger idx, VXUserPointer *p);

    VXInteger GetClosure(VXInteger idx, VXFuncProtoObj** fnproto);

    VXInteger GetFloat(VXInteger idx, VXFloat *f);

    VXInteger GetBool(VXInteger idx,bool *b);

    VXInteger GetInteger(VXInteger idx, VXInteger* dest);

    VXInteger GetString(VXInteger idx, const char** src_dest, VXInteger* len_dest);

    template<typename Type> VXInteger GetString(VXInteger idx, Type* dest)
    {
        const char* src;
        VXInteger len;
        if(VX_SUCCEEDED(this->GetString(idx, &src, &len)))
        {
            dest->append(src, len);
            return VX_OK;
        }
        return VX_ERROR;
    }

    bool CollectGarbage();

    bool ResurrectUnreachable();

    // vars
    VXArrayObj* _system_argv;
    VXObjectVec _stack;

    VXInteger _top;
    VXInteger _stackbase;
    VXOuterObj *_openouters;
    VXObject _roottable;
    VXObject _lasterror;
    VXObject _errorhandler;

    bool _debughook;
    VXDebugHook _debughook_native;
    VXObject _debughook_closure;

    VXObject temp_reg;

    CallInfo* _callsstack;
    VXInteger _callsstacksize;
    VXInteger _alloccallsstacksize;
    VXVector<CallInfo>  _callstackdata;

    ExceptionsTraps _etraps;
    CallInfo *ci;
    void *_foreignptr;
    //VMs sharing the same state
    VXSharedState *_sharedstate;
    VXInteger _nnativecalls;
    VXInteger _nmetamethodscall;
    //suspend infos
    bool _suspended;
    bool _suspended_root;
    VXInteger _suspended_target;
    VXInteger _suspended_traps;

    // atexit stuff
    VXVector<VXObject> _atexit_functions;
};

struct VXRepr
{
    VXState* v;
    VXObject root_ob;
    VXInteger returns;
    std::stringstream strm;

    VXRepr(VXState* _v, const VXObject& ob);

    void do_serialize(const VXObject& o);

    void emit_string(const VXObject& o);

    void emit_table(const VXObject& o);

    void emit_array(const VXObject& o);

    void emit_othertype(const VXObject& o);

    std::string str();

    VXInteger status();
};

VOX_API void* vox_malloc(VXUnsignedInteger size);
VOX_API void* vox_realloc(void* p,VXUnsignedInteger oldsize,VXUnsignedInteger newsize);
VOX_API void vox_free(void* p,VXUnsignedInteger size);

VXInteger vox_aux_invalidtype(VXState* v,VXOType type);
VOX_API void vox_aux_seterrorhandlers(VXState* v);
VOX_API void vox_aux_printcallstack(VXState* v);
VOX_API void vox_aux_asserthandler(const char* expr,
                           const char* funct,
                           const char* file,
                           int line);

VOX_API void vox_defaultprintfunc(VXState* v,const char *s,...);
VOX_API void vox_defaulterrorfunc(VXState* v,const char *s,...);

/* stdlib functions */
/* system, mathlib, iolib and hashlib are part of the core */
VOX_API VXInteger voxstd_register_system(VXState* v);
VOX_API VXInteger voxstd_register_mathlib(VXState* v);
VOX_API VXInteger voxstd_register_iolib(VXState* v);
VOX_API VXInteger voxstd_register_hashlib(VXState* v);
#ifdef VOX_STDLIB_AVAILABLE
    VOX_API VXInteger voxstd_register_importlib(VXState* v);
    VOX_API VXInteger voxstd_register_oslib(VXState* v);
    VOX_API VXInteger voxstd_register_regexlib(VXState* v);
#endif /* VOX_STDLIB_AVAILABLE */


#endif /*_VOX_H_*/
