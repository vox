
### base config for emscriptens clang wrapper
### note that the include flags are added later!
emxx = "em++"
ccflags = ["-Wall", "-Wextra", "-O2"]
outfile = "vox.js"


### core config
### don't touch, here be dragons
rootdir = ".."
thisdir = os.getenv("PWD")
srcdir = os.joinpath(rootdir, "src")
objdir = os.joinpath(thisdir, "objects")
dirs = [os.joinpath(srcdir, "core"), os.joinpath(srcdir, "frontend")]
exts = [".cpp"]
files = []
objects = []

## populate ccflags with include flags
ccflags.extend([
    "-I"+os.joinpath(rootdir, "include"),
    "-I"+os.joinpath(srcdir, "frontend")
])

### populate files array
dirs.map(function(dir) os.listdir(dir).map(function(file)
{
    exts.map(function(ext)
    {
        if(file.endswith(ext))
            files.append(file)
    })
}))

# a really silly function that behaves like execve(), by
# escaping arguments using .quote()
function shellcmd(exe, arguments, just_print=false)
{
    assert(typeof exe == "string")
    local scmd = exe
    arguments.map(function(arg) scmd += " " + arg.quote())
    if(just_print)
    {
        println(scmd)
        return
    }
    local status = os.system(scmd)
    if(status == -1)
    {
        throw ("executing command '%s' failed with status %d".fmt(exe, status))
    }
}



function rmfiles(files)
{
    println("+ [EMS:STEP] removing ...")
    files.map(function(path)
    {
        println("- [RM] %s".fmt(path))
        os.remove(path)
    })
}

function build_object(sourcepath, outpath, basename, rebuild)
{
    local args = clone ccflags
    args.extend([sourcepath, "-o", outpath])
    print("- [EMS:BUILD] %s ... ".fmt(basename)); io.stdout.flush()
    if(os.exists(outpath) && !rebuild)
    {
        print("skipped (exists)")
    }
    else
    {
        shellcmd(emxx, args)
    }
    println()
}

function main()
{
    local rebuild = system.argv.find("rebuild")
    local clean = system.argv.find("clean")
    local distclean = system.argv.find("dist-clean")

    if(clean || distclean)
    {
        local allfiles = []
        os.listdir(objdir).map(function(file) allfiles.append(file))
        if(distclean)
        {
            os.exists(outfile) && allfiles.append(outfile)
        }
        rmfiles(allfiles)
        return
    }

    ## create object directory
    !os.isdir(objdir) && os.mkdir(objdir, 0775)

    ## now build the objects!
    println("+ [EMS:STEP] building objects ... ")
    foreach(file in files)
    {
        local relpath = os.joinpath(objdir, os.basename(file) + ".bc")
        local basename = os.basename(relpath)
        build_object(file, relpath, basename, rebuild)
    }

    ## and now, build vox.js
    println("+ [EMS:STEP] building javascript library ... ")
    if(os.exists(outfile) && !rebuild)
    {
        println("- outfile ('%s') already exists, skipping!".fmt(outfile))
        return
    }
    args = ["-o", outfile]
    args.extend(os.listdir(objdir))
    shellcmd(emxx, args)
}

main()