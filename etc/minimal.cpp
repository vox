
/**
* This is the most minimal version of a standalone interpreter
* of Vox.
* This interpreter will not use the stdlib, so linking
* against libvox_stdlib is not necessary.
* Thus, the only element in the 'std' namespace will be 'system' and 'io',
* which contains implementation defined VM controlling routines.
* 
* TODO: Write some actual documentation
*/

#include <sstream>
#include <stdio.h>
#include <vox.h>


/* here is a simple function callback, iterating through the arguments */
VXInteger sandbox_func(VXState* v)
{
    VXInteger pos;
    VXObject ob;
    fprintf(stderr, "hello from the 'hello' function!\n");
    fprintf(stderr, "iterating through the stack:\n");
    for(pos=2; pos!=v->GetTop()+1; pos++)
    {
        ob = v->StackGet(pos);
        printf("    Stack #%d = (ADDR=%p TYPE=%d) '%s'\n", pos, &ob, ob.Type(), ob.TypeString());
    }
    return 0;
}

/**
* Here is a simple class definition.
* <classdef begin>
*/

struct SimpleClass
{
    bool havename;
    std::string name;
};

// this is the release function for the class, called upon
// destruction of the VM
VXInteger scfn_releasehook(VXUserPointer p, VXInteger size)
{
    SimpleClass* self = (SimpleClass*)p;
    printf("deleting SimpleClass!\n");
    delete self;
    return 1;
}

// this is the constructor for the class, where you might want to check for
// arguments passed to it. as with all native callbacks, the arguments stack
// begins at index 2.
VXInteger scfn_constructor(VXState* v)
{
    SimpleClass* self = new SimpleClass;
    self->havename = false;
    if(VX_SUCCEEDED(v->GetString(2, &(self->name))))
    {
        self->havename = true;
    }
    v->SetInstanceUp(1, (VXUserPointer*)&self);
    v->SetReleaseHook(1, scfn_releasehook);
    // important: the return value of a class constructor is ignored!
    return 0;
}

VXInteger scfn_setname(VXState* v)
{
    std::string name;
    SimpleClass* self;
    v->GetInstanceUp(1, (VXUserPointer*)&self, 0);
    if(VX_FAILED(v->GetString(2, &name)))
    {
        return v->ThrowError("[string] expected!");
    }
    self->name = name;
    self->havename = true;
    return 0;
}

VXInteger scfn_getname(VXState* v)
{
    std::string name;
    SimpleClass* self;
    v->GetInstanceUp(1, (VXUserPointer*)&self, 0);
    if(self->havename)
    {
        v->Push(v->NewString(self->name.c_str(), self->name.length()));
        return 1;
    }
    return v->ThrowError("name not set!");
}

static VXRegFunction scfn_funcs[]=
{
    {"constructor", scfn_constructor,   -1, ".ss"},
    {"setname",     scfn_setname,        2, ".s"},
    {"getname",     scfn_getname,       -1, NULL},
    {0, 0, 0, 0},
};

/**
* <classdef end>
*/

bool getfile(int argc, char** argv, const char** filename)
{
    if(argc > 1)
    {
        (*filename) = argv[1];
        return true;
    }
    return false;
}

void populate_argv(VXState* v, int argc, char** argv, VXInteger start)
{
    VXInteger i;
    for(i=start; i<argc; i++)
    {
        v->GetSysArgv()->Append(v->NewString(argv[i]));
    }
}


int main(int argc, char* argv[])
{
    const char* filename;
    const char* err_message;
    VXState* vm = VXState::Create(1024);
    if(getfile(argc, argv, &filename))
    {
        /* DefineGlobal is just a shortened version of:
                vm->GetRootTable()->NewSlot(
                    vm->NewString("hello"),
                    vm->NewClosure(sandbox_func)
                );
        */
        vm->DefineGlobal("hello", vm->NewClosure(sandbox_func));
        vm->DefineGlobal("SimpleClass", vm->RegClass(scfn_funcs));
        populate_argv(vm, argc, argv, 1);
        if(VX_FAILED(vm->DoFile(filename, false, true)))
        {
            // at this point the script failed to execute.
            // if you want to handle the error yourself, change
            // DoFile(filename, false, true) to DoFile(filename, false, false)
        }
    }
    else
    {
        printf("Usage: %s <filename>\n", argv[0]);
    }
    VXState::Destroy(vm);
}

