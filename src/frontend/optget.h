
/*
* optget.h & optget.c
*
* a commandline parser based on the public domain getopt.c file
* (original: ftp://ftp.es.ele.tue.nl/pub/users/jos/poster/getopt.c)
* ---------
*
* The usage of optget differs from getopt in a few ways;
* However, these differences serve the portability, and
* code-cleanliness, so previous users of getopt should
* feel right at home with the API.
*
* Minimalistic Example:

    int main(int argc, char** argv)
    {
        optget_t opt;
        const char* outfile;
        int verbose;
        int c;
        optget_init(&opt, argc, argv, NULL);
        while((c = optget_main(&opt, "vc:")) != -1)
        {
            case 'v':
                verbose = 1;
                break;
            case 'o':
                outfile = opt.arg;
                break;
            case '?':
                if (opt.opt == 'o')
                {
                    fprintf(stderr, "Option -%c requires an argument.\n", opt.opt);
                }
                else if (isprint(opt.opt))
                {
                    fprintf(stderr, "Unknown option `-%c'.\n", opt.opt);
                }
                return 1;
        }
    }

*
*/

#ifndef __PD_OPTGET_PUBLIC_HEADER_H__
#define __PD_OPTGET_PUBLIC_HEADER_H__

#define OPTGET_BADCH   '?'
#define OPTGET_BADARG  ':'
#define OPTGET_EMSG    ""

#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

typedef void(*optget_errfunc_t)(int,char**,char,const char*);

typedef struct __optget_t {
    /* undocumented error-suppressor */
    int   err;

    /* index into argv vector */
    int   ind;

    /* char checked for validity */
    int   opt;

    /* arg associated with option */
    char* arg;

    /* argc */
    int nargc;

    /* argv*/
    char** nargv;

    /* errfunc */
    optget_errfunc_t errfunc;

} optget_t;


/* {"add",     1,       0,    0}, */
typedef struct optget_option_t
{
    const char *name;
    int has_arg;
    int* flag;
    int val;
} optget_option_t;


void optget_init(optget_t* gto,
                 int nargc,
                 char** nargv,
                 optget_errfunc_t errfunc);

int optget_short(optget_t* gto,
                const char* ostr);


int optget_long(optget_t* opt,
                const char* optstring,
                optget_option_t* longopts,
                int* longindex);

#ifdef __cplusplus
} /* extern "C" */
#endif /* __cplusplus */

#endif /* __PD_OPTGET_PUBLIC_HEADER_H__ */
