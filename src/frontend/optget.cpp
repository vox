
#include <stdio.h>
#include <string.h>
#include "optget.h"

int optget_reset;

static char optget_emsg[] = "";

const char* optget_strchr(const char* dataptr, int search)
{
    char found;
    if(search == 0)
    {
        while(*dataptr++);
        return (dataptr - 1);
    }
    while((found = *dataptr++))
    {
        if(found == search)
        {
            return (dataptr - 1);
        }
    }
    return 0;
}

void optget_tell(int err, char** nargv, char opt, const char* str)
{
    (void)opt;
    if(err)
    {
        fputs(nargv[0], stderr);
        fputs(str, stderr);
        fputc('\n', stderr);
    }
}

void optget_init(optget_t* gto,
                 int nargc,
                 char** nargv,
                 optget_errfunc_t errfunc)
{
    gto->err = 1;
    gto->ind = 1;
    gto->arg = NULL;
    gto->nargc = nargc;
    gto->nargv = nargv;
    gto->errfunc = (errfunc ? errfunc : optget_tell);
}

int optget_short(optget_t* gto, const char* ostr)
{
    const char* oli;
    static char* place = optget_emsg;
    if (!*place)
    {
        if((gto->ind >= gto->nargc) ||
           (*(place = gto->nargv[gto->ind]) != '-') ||
           (!*++place))
        {
            return EOF;
        }
        /* found "--" */
        if (*place == '-')
        {
            (gto->ind)++;
            return EOF;
        }
    }
    /* option letter okay? */
    if (((gto->opt = *place++) == ':') ||
        ((oli = optget_strchr(ostr, gto->opt)) == NULL))
    {
        if (!*place)
        {
            gto->ind++;
        }
        gto->errfunc(gto->err, gto->nargv, gto->opt, ": illegal option -- ");
        goto __optget_Bad;
    }
    /* don't need argument */
    if (*++oli != ':')
    {
        gto->arg = NULL;
        if (!*place)
        {
            (gto->ind)++;
        }
    }
    /* need an argument */
    else
    {
        if (*place)
        {
            /* no white space */
            gto->arg = place;
        }
        else
        {
            if (gto->nargc <= ++(gto->ind))
            {
                place = optget_emsg;
                gto->errfunc(gto->err, gto->nargv, gto->opt,
                        ": option requires an argument -- ");
                goto __optget_Bad;
            }
            else
            {
                /* white space */
                gto->arg = gto->nargv[gto->ind];
            }
        }
        place = optget_emsg;
        (gto->ind)++;
    }
    /* dump back option letter */
    return gto->opt;

    __optget_Bad:
    return OPTGET_BADCH;
}




int optget_long(optget_t* opt,
                const char* optstring,
                optget_option_t* longopts,
                int* longindex)

{
    static char* place = optget_emsg;
    char* oli;
    unsigned int namelen;
    int i;
    if (optget_reset || !*place)
    {
        optget_reset = 0;
        if (opt->ind >= opt->nargc)
        {
            place = optget_emsg;
            return -1;
        }
        place = opt->nargv[opt->ind];
        if (place[0] != '-')
        {
            place = optget_emsg;
            return -1;
        }
        place++;
        if (place[0] && place[0] == '-' && place[1] == '\0')
        {
            ++(opt->ind);
            place = optget_emsg;
            return -1;
        }
        if (place[0] && place[0] == '-' && place[1])
        {
            /* long option */
            place++;
            namelen = strcspn(place, "=");
            for (i = 0; longopts[i].name != NULL; i++)
            {
                if (strlen(longopts[i].name) == namelen
                        && strncmp(place, longopts[i].name, namelen) == 0)
                {
                    if (longopts[i].has_arg)
                    {
                        if (place[namelen] == '=')
                        {
                            opt->arg = place + namelen + 1;
                        }
                        else if (opt->ind < (opt->nargc - 1))
                        {
                            opt->ind++;
                            opt->arg = opt->nargv[opt->ind];
                        }
                        else
                        {
                            if (optstring[0] == ':')
                            {
                                return OPTGET_BADARG;
                            }
                            if (opt->err)
                            {
                                char errbuf[50];
                                sprintf(errbuf,
                                    ": option '--%s' required an argument",
                                    place);
                                opt->errfunc(opt->err, opt->nargv, opt->opt, errbuf);
                            }
                            place = optget_emsg;
                            opt->ind++;
                            return OPTGET_BADCH;
                        }
                    }
                    else
                    {
                        opt->arg = NULL;
                        if (place[namelen] != 0)
                        {
                            /* XXX error? */
                        }
                    }
                    opt->ind++;
                    if (longindex)
                    {
                        *longindex = i;
                    }
                    place = optget_emsg;
                    if (longopts[i].flag == NULL)
                    {
                        return longopts[i].val;
                    }
                    else
                    {
                        *longopts[i].flag = longopts[i].val;
                        return 0;
                    }
                }
            }
            if (opt->err && optstring[0] != ':')
            {
                char errbuf[50];
                sprintf(errbuf, ": illegal option '%s'", place);
                opt->errfunc(opt->err, opt->nargv, opt->opt, errbuf);
            }
            place = optget_emsg;
            opt->ind++;
            return OPTGET_BADCH;
        }
    }
    /* short option */
    opt->opt = (int)*place++;
    oli = strchr((char*)optstring, opt->opt);
    if (!oli)
    {
        if (!*place)
        {
            ++opt->ind;
        }
        if (opt->err && *optstring != ':')
        {
            char errbuf[50];
            sprintf(errbuf, ": illegal option -- %c", opt->opt);
            opt->errfunc(opt->err, opt->nargv, opt->opt, errbuf);
        }
        return OPTGET_BADCH;
    }

    if (oli[1] != ':')
    {
        opt->arg = NULL;
        if (!*place)
        {
            ++opt->ind;
        }
    }
    else
    {
        if (*place)
        {
            opt->arg = place;
        }
        else if (opt->nargc <= (++opt->ind))
        {
            place = optget_emsg;
            if (*optstring == ':')
            {
                    return OPTGET_BADARG;
            }
            if (opt->err)
            {
                char errbuf[50];
                sprintf(errbuf, ": option '-%c' requires an argument",
                    opt->opt);
                opt->errfunc(opt->err, opt->nargv, opt->opt, errbuf);
            }
            return OPTGET_BADCH;
        }
        else
        {
            opt->arg = opt->nargv[opt->ind];
        }
        place = optget_emsg;
        ++opt->ind;
    }
    return opt->opt;
}


static const int i=0;
