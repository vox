
#include <iostream>
#include <vector>
#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <sys/types.h>
#include <sys/stat.h>
#include "optget.h"
#include "vox.h"

#define MAXINPUT 1024


#define USERCFG_FILE "voxrc.vx"
#define USERCFG_ENV_OVERRIDE "VOXRCPATH"
#if defined(VOX_PLATFORM_MSWINDOWS)
#   define USERCFG_ENV_USERHOME "APPDATA"
#   define DELIM '\\'
#else
#   define USERCFG_ENV_USERHOME "HOME"
#   define DELIM '/'
#endif


enum {
    ST_OK = 0,
    ST_ERR = 1
};

static const char* vxf_def_inputprefix = ">> ";
static const char* vxf_def_compiledoutput = "out.vxc";
static int         vxf_def_stacksize = 1024;



bool get_usercfg_path(std::string& dest)
{
    const char* env_value;
    std::stringstream fullpath;
    /* first, check whether the override var is defined ... */
    if((env_value = getenv(USERCFG_ENV_OVERRIDE)) != NULL)
    {
        dest = env_value;
        return true;
    }
    /* technically unlikely to happen, but let's check anyway ... */
    if((env_value = getenv(USERCFG_ENV_USERHOME)) == NULL)
    {
        return false;
    }
    fullpath << env_value << DELIM;
#if !defined(VOX_PLATFORM_MSWINDOWS) && defined(VOX_PLATFORM_UNIX)
    /*note: on Linux and UNIX, go with the freedesktop.org standard of
            putting configuration files into $HOME/.config/ */
    fullpath << ".config" << DELIM;
#endif
    fullpath << USERCFG_FILE;
    dest = fullpath.str();
    return true;
}

bool check_file(const std::string& path)
{
    /* just check whether the file exists (using stat), nothing more */
    struct stat sbuf;
    return stat(path.c_str(), &sbuf) == 0;
}

void load_stdlib(VXState* v)
{
#if VOX_STDLIB_AVAILABLE
    voxstd_register_oslib(v);
    voxstd_register_regexlib(v);
    voxstd_register_importlib(v);
    voxstd_register_hashlib(v);
#else
    (void)v;
#endif
}

class VXFrontend {
    private:
        // our VM
        VXState* vm;

        // status code to be returned in main()
        int exitstatus;

        // arguments passed to this program
        VXVector<std::string> args;

        // argument count
        int argc;

        // argument vector
        char** argv;

        // name of this program
        const char* program;

        // path to the script passed to this program
        std::string scriptfile;

        // contains the script supplied by '-e'
        std::string scriptline;

        // optget instance
        optget_t opt;

        // is true when we have at least one positional argument
        bool havefile;

        // is true when '-e' is being used
        bool is_scriptline;

        // is set to true by the '-c' flag
        bool opt_compileonly;

        // is set to true by the '-d' flag
        bool opt_debuginfo;

        // is set to true by the '-n' flag
        bool opt_ignoreuserconf;

        // is modified by the '-s' flag
        int opt_stacksize;

        // is modified by the '-o' flag
        std::string opt_compiledoutput;

    public:
        VXFrontend(int _argc, char** _argv):
            argc(_argc), argv(_argv)
        {
            int i;
            exitstatus = ST_OK;
            program = argv[0];
            havefile = false;
            is_scriptline = false;
            opt_compiledoutput = vxf_def_compiledoutput;
            opt_compileonly = false;
            opt_debuginfo = false;
            opt_ignoreuserconf = false;
            opt_stacksize = vxf_def_stacksize;
            optget_init(&opt, argc, argv, NULL);
            parse_args();
            if(opt.ind < argc)
            {
                scriptfile = argv[opt.ind];
                havefile = true;
                for(i=opt.ind; i<argc; i++)
                {
                    args.push_back(argv[i]);
                }
            }
            handle_rest();
        }

        void get_err(const char** err)
        {
            (*err) = vm->LastError();
        }

        void populate_argv()
        {
            size_t i;
            std::string arg;
            for(i=0; i<args.size(); i++)
            {
                arg = args.at(i);
                vm->GetSysArgv()->Append(vm->NewString(arg.c_str(), arg.length()));
            }
        }

        void exec_userconf()
        {
            std::string usercfg_path;
            if(get_usercfg_path(usercfg_path))
            {
                if(check_file(usercfg_path))
                {
                    vm->DoFile(usercfg_path.c_str(), true, true);
                }
            }
        }

        bool check_stacksize()
        {
            if(opt_stacksize < 1)
            {
                return false;
            }
            return true;
        }

        void check_vm()
        {
            if(vm == NULL)
            {
                fprintf(stderr, "Failed to create the virtualmachine!\n");
                exit(ST_ERR);
            }
        }

        void start_vm()
        {
            if(!check_stacksize())
            {
                fprintf(stderr, "Stacksize too small to start the VM!\n");
                exitstatus = ST_ERR;
                exit(exitstatus);
            }
            vm = VXState::Create(opt_stacksize);
            check_vm();
            if(opt_debuginfo)
            {
               vm->EnableDebugInfo(true);
            }
            vm->SetErrorHandlers();
            vm->PushRoot();
            load_stdlib(vm);
            populate_argv();
        }

        void stop_vm()
        {
            vm->PopRoot();
            VXState::Destroy(vm);
        }

        void run_file()
        {
            const char* err;
            bool failed_to_open;
            if(VX_FAILED(vm->DoFile(scriptfile.c_str(), false, true, &failed_to_open)))
            {
                get_err(&err);
                exitstatus = ST_ERR;
                if(failed_to_open)
                {
                    fprintf(stderr, "Could not open '%s': %s\n", scriptfile.c_str(), err);
                }
            }
        }

        void run_string()
        {
            const char* err;
            if(VX_FAILED(vm->DoString(scriptline.c_str(),
                                      scriptline.length(),
                                      NULL, false, true)))
            {
                get_err(&err);
                exitstatus = ST_ERR;
            }
        }

        void compile_file()
        {
            const char* err;
            if(VX_SUCCEEDED(vm->LoadFile(scriptfile.c_str(), true)))
            {
                if(VX_FAILED(vm->WriteClosureToFile(opt_compiledoutput.c_str())))
                {
                    get_err(&err);
                    exitstatus = ST_ERR;
                    fprintf(stderr, "Compiling failed: %s\n", err);
                }
            }
        }

        void handle_rest()
        {
            start_vm();
            if(!opt_ignoreuserconf)
            {
                exec_userconf();
            }
            if(is_scriptline)
            {
                run_string();
            }
            else if(havefile)
            {
                if(opt_compileonly)
                {
                    compile_file();
                }
                else
                {
                    run_file();
                }
            }
            else
            {
                enter_interactive();
            }
            stop_vm();
        }

        void interactive_showbanner()
        {
            //VXInt32 bits = sizeof(VXInteger)*8;
            fprintf(stdout, "%s %s (%s)\n",
                VOX_VERSION, VOX_COPYRIGHT, VOX_PLATFORM_NAME);
            fprintf(stdout, "To view the license, type 'license()'\n");
            fprintf(stdout, "To exit, type CTRL+C or CTRL+D\n\n");
            fflush(stdout);
        }

        void enter_interactive()
        {
            char buffer[MAXINPUT];
            char* tmpbuf = NULL;
            bool retval = false;
            VXInteger blocks = 0;
            VXInteger isstring = 0;
            VXInteger done = 0;
            VXInteger cstatus;
            interactive_showbanner();
            while (!done)
            {
                VXInteger i = 0;
                printf("%s", vxf_def_inputprefix);
                fflush(stdout);
                while(true)
                {
                    int c;
                    if(done)
                    {
                        return;
                    }
                    c = getchar();
                    if(c == EOF)
                    {
                        fprintf(stderr, "\nBye\n");
                        return;
                    }
                    else
                    {
                        if (c == '\n')
                        {
                            if (i>0 && buffer[i-1] == '\\')
                            {
                                buffer[i-1] = '\n';
                            }
                            else if(blocks == 0)
                            {
                                break;
                            }
                            buffer[i++] = '\n';
                        }
                        else if (c == '}')
                        {
                            blocks--;
                            buffer[i++] = (char)c;
                        }
                        else if(c == '{' && !isstring)
                        {
                            blocks++;
                            buffer[i++] = (char)c;
                        }
                        else if(c == '"' || c == '\'')
                        {
                            isstring = !isstring;
                            buffer[i++] = (char)c;
                        }
                        else if (i >= (MAXINPUT-1))
                        {
                            fprintf(stderr, "<error> input line too long\n");
                            exit(-1);
                            break;
                        }
                        else
                        {
                            buffer[i++] = (char)c;
                        }
                    }
                }
                buffer[i] = '\0';
                /* allocate tmpbuf, and copy the contents of the buffer chunk, so
                   we can restore it later, if return() failed to compile */
                tmpbuf = new char[MAXINPUT];
                memset(tmpbuf, 0, MAXINPUT);
                memcpy(tmpbuf, buffer, strlen(buffer));
                /* create a code chunk that returns the expression - assuming
                   that the expression can be returned in the first place.
                   that is, the chunk '1 + 1' becomes 'return (1 + 1)', etc.
                   If it fails, the original chunk gets restored */
                sprintf(vm->ScratchPad(MAXINPUT), "return (%s)", &buffer[0]);
                memcpy(buffer, vm->ScratchPad(-1), strlen(vm->ScratchPad(-1)) + 1);
                /* here's where the previously defined chunk gets compiled, to
                   see whether it's valid syntax */
                if(VX_SUCCEEDED(vm->CompileBuffer(buffer, strlen(buffer), "<testcompile>", false)))
                {
                    retval = true;
                    i = strlen(buffer);
                    vm->Pop();
                }
                else
                {
                    /* at this point, returning the value did not work,
                       due to syntax errors (it didn't even compile, so no reason
                       to investigate any further).
                       so restore the old buffer, by copying from tmpbuf */
                    memcpy(buffer, tmpbuf, strlen(tmpbuf) + 1);
                    i = strlen(tmpbuf);
                }
                delete[] tmpbuf;
                if(i > 0)
                {
                    VXInteger oldtop = vm->GetTop();
                    cstatus = vm->CompileBuffer(buffer, i, "<interactive>", true);
                    if(VX_SUCCEEDED(cstatus))
                    {
                        vm->PushRoot();
                        if(VX_SUCCEEDED(vm->CallStack(1, true, true)) && retval)
                        {
#if defined(VOX_INTER_NO_SERIALIZE)
                            /* without serialization, simply ToString()'ify the
                               value, and print to stdout. */
                            VXObject val;
                            vm->ToString(vm->StackGet(-1), val);
                            VXInteger str_len;
                            const char* str_val;
                            val.String()->Get(&str_val, &str_len);
                            fprintf(stdout, "%.*s\n", str_len, str_val);
#else
                            /* using serialization, the data becomes more readable, but
                               might also be a little slower (not tragically, though!) */
                            VXRepr ser(vm, vm->StackGet(-1));
                            const std::string& data = ser.str();
                            fprintf(stdout, "%.*s\n", VXInt32(data.length()), data.c_str());
#endif
                            fflush(stdout);
                            retval = false;
                        }
                    }
                    vm->SetTop(oldtop);
                }
            }
        }

        void print_version()
        {
            printf("%s\n", VOX_VERSION);
            exit(ST_OK);
        }

        void print_help()
        {
            printf(
                "Usage: %s [<options>] [<file> [<arguments>]]\n"
                "Available options:\n"
                "  -c              compiles the file to bytecode (default output: '%s')\n"
                "  -o <name>       specifies output file for the -c option to <name>\n"
                "  -d              enable debug information\n"
                "  -s <value>      sets initial stacksize to <value> (default: %d)\n"
                "  -e <string>     evaluates <string>\n"
                "  -v              displays version\n"
                "  -n              ignore user configuration, even if $VOXRCPATH is defined\n"
                "  -h              prints help\n",
            program, vxf_def_compiledoutput, vxf_def_stacksize);
            exit(ST_OK);
        }

        void parse_args()
        {
            int c;
            while ((c = optget_short(&opt, "hvndco:s:e:")) != -1)
            {
                switch (c)
                {
                    case 'h':
                        print_help();
                        return;
                    case 'v':
                        print_version();
                        return;
                    case 'd':
                        opt_debuginfo = true;
                        break;
                    case 'c':
                        opt_compileonly = true;
                        break;
                    case 'n':
                        opt_ignoreuserconf = true;
                        break;
                    case 'o':
                        opt_compiledoutput = opt.arg;
                        break;
                    case 's':
                        opt_stacksize = atoi(opt.arg);
                        break;
                    case 'e':
                        is_scriptline = true;
                        scriptline = opt.arg;
                        break;
                    case '?':
                        if(opt.opt == 'o' || opt.opt == 's' || opt.opt == 'e')
                        {
                            fprintf(stderr,
                                    "%s: option -%c requires an argument\n",
                                    argv[0], opt.opt);
                        }
                        else if(isprint(opt.opt))
                        {
                            fprintf(stderr,
                                    "%s: unknown option `-%c'\n",
                                    argv[0], opt.opt);
                        }
                        else
                        {
                            fprintf(stderr,
                                    "%s: unknown option character `\\x%x'\n",
                                    argv[0], opt.opt);
                        }
                        exit(1);
                    default:
                        abort();
                }
            }
        }

        int exit_status()
        {
            return exitstatus;
        }
};

int main(int argc, char* argv[])
{
    VXFrontend vxf(argc, argv);
    return vxf.exit_status();
}

