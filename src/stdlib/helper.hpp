
#ifndef __vox_stdlib_helper_hpp
#define __vox_stdlib_helper_hpp

#include <stdexcept>
#include <exception>
#include <vector>
#include <string>
#include <ctime>
#include <cstring>
#include <cstdlib>
#include <cstdio>
#include <cctype>
#include <signal.h>
#include <assert.h>
#include <errno.h>
#include <sys/stat.h>
#include "../core/baselib.hpp"

#if defined(VOX_PLATFORM_UNIX)
#   include <unistd.h>
#   include <sys/utsname.h>
#endif

#if defined(VOX_PLATFORM_MSWINDOWS)
#   include <windows.h>
#   include <direct.h>
#endif

template<typename ReturnT, typename InputT>
ReturnT forcecast(InputT what)
{
    return *((ReturnT*)&what);
}

void oslib_register_libload(VXState* v, VXTableObj* top);

#endif /* __vox_stdlib_helper_hpp */

