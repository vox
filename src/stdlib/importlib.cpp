
#include <ctime>
#include <cstdlib>
#include <cstdio>
#include <boost/filesystem.hpp>
#include "helper.hpp"
#include "vxlibrary.hpp"

#define IMPORTHOOK_NAME "importhook"


#define IMPORT_GET_SELF(__vm__, __var__) \
    VXObject o; \
    if(!__vm__->GetRegistryTable()->Get(v->NewString(IMPORTHOOK_NAME), o)) \
    { \
        return v->ThrowError("importhook wasn't installed correctly!"); \
    } \
    __var__ = (VXImportHook*)o.UserPointer();

namespace fslib = boost::filesystem;
typedef fslib::filesystem_error fslib_error;


//DSO_EXT = kernel.getproperty("DSO_EXT")
//const char* DSO_EXT = "." VOX_PLATFORM_NAME ".vxd";
const char* DSO_EXT = ".vxd";
//VOX_EXT = kernel.getproperty("VOX_EXT")
const char* VOX_EXT = ".vx";

const char* VOX_PLUGIN_FUNCTION_OPEN = "vox_module_open";

void get_err(VXState* v, const char** err)
{
    /*
    vox_pushlasterror(v);
    if(VX_FAILED(vox_getstring(v, -1, err, NULL)))
    {
        *err = "<unknown error>";
    }
    */
    (*err) = v->LastError();
}


struct VXImportError
{
    std::string path;
    std::string message;
    bool is_dll;
    bool dll_failed_to_load;
};

struct VXLibWrap
{
    VXSharedLibrary handle;
};

struct VXImportHook
{
    VXState* v;
    VXVector<std::string> searchpaths;
    VXVector<VXImportError> errvalues;
    bool foundmodule;
    VXInteger status;

    void fill_searchpaths()
    {
        const char* env = getenv("VOX_PATH");
        if(env != NULL)
        {
            searchpaths.push_back(env);
        }
        searchpaths.push_back("./");
        // add installdir before srcdir, so it has higher priority
#if defined(VOX_MODULEPATH_INSTALLDIR)
        searchpaths.push_back(VOX_MODULEPATH_INSTALLDIR);
#endif
#if defined(VOX_MODULEPATH_SRCDIR)
        searchpaths.push_back(VOX_MODULEPATH_SRCDIR);
#endif
    }

    VXImportHook(VXState* _v): v(_v)
    {
        fill_searchpaths();
    }

    void pusherror(const std::string& path,
                   const std::string& message,
                   bool is_dll = false,
                   bool dll_failed_to_load = false)
    {
        VXImportError err;
        err.path = path;
        err.message = message;
        err.is_dll = is_dll;
        err.dll_failed_to_load = dll_failed_to_load;
        errvalues.push_back(err);
    }

    void do_import(const std::string& expr)
    {
        int i;
        char dir_sep = '/';
        char dot = '.';
        std::string relpath;
        std::string abspath;
        std::string searchpath;
        VXImportError pair;
        this->status = 1;
        this->foundmodule = false;
        for(i=0; i<searchpaths.size(); i++)
        {
            searchpath = searchpaths.at(i);
            relpath = expr;
            fslib::path p(searchpath);
            if(p.is_absolute())
            {
                abspath = searchpath + relpath;
            }
            else
            {
                abspath = abspath +  searchpath + relpath;
            }
            this->checkmodule(abspath);
            if(foundmodule == true)
            {
                return;
            }
        }
        if(foundmodule == false)
        {
            // create a little errormessage, explaining what went wrong,
            // and what files we have searched
            // yes, the errormessage is inspired by Lua
            std::string msg;
            msg = "import: Module '"+expr+"' not found:\n";
            for(i=0; i<errvalues.size(); i++)
            {
                pair = errvalues.at(i);
                std::string epath = pair.path;
                std::string eerr = pair.message;
                // might happen in some rare cases
                if(eerr.length() == 0)
                {
                    eerr = "<unknown>";
                }
                //if(pair.is_dll && pair.dll_failed_to_load)
                //{
                //    msg += "  No file '"+epath+"' ("+eerr+")\n";
                //}
                //else
                //{
                    msg += "  No file '"+epath+"'\n";
                //}
            }
            status = v->ThrowError(msg.c_str());
        }
    }


    void checkmodule(const std::string& abspath)
    {
        std::string dso_path;
        std::string vox_path;
        std::string err;
        dso_path = abspath + DSO_EXT;
        vox_path = abspath + VOX_EXT;
        this->check_voxfile(vox_path);
        // only continue searching DSO files when
        // no voxfile could be found, to
        // prevent race conditions
        if(!foundmodule)
        {
            this->check_dsofile(dso_path);
        }
    }

    void check_voxfile(const std::string& path)
    {
        bool retval = true;
        bool ofail;
        const char* err;
        std::string ret_err;
        VXObject closure;
        if(VX_SUCCEEDED(v->LoadFile(path.c_str(), true, &ofail, &closure)))
        {
            v->PushRoot();
            if(VX_SUCCEEDED(v->CallSimple(closure, 1, retval, true)))
            {
                foundmodule = true;
                return;
            }
            else
            {
                get_err(v, &err);
                ret_err = err;
                pusherror(path, ret_err);
                v->Pop(1);
                return;
            }
        }
        else
        {
            get_err(v, &err);
            ret_err = err;
            pusherror(path, ret_err);
            return;
        }
    }

    void check_dsofile(const std::string& path)
    {
        VXFunction func;
        VXLibWrap lib;
        try
        {
            lib.handle.open(path);
            func = lib.handle.resolve<VXFunction>(VOX_PLUGIN_FUNCTION_OPEN);
            func(v);
            foundmodule = true;
            return;
        }
        catch(VXSharedLibrary::LoadError& err)
        {
            lib.handle.close();
            pusherror(path, err.what(), true, true);
        }
        catch(std::runtime_error& err)
        {
            lib.handle.destroy();
            pusherror(path, err.what(), true, true);
        }
    }

    VXInteger finalize()
    {
        this->errvalues.clear();
        return this->status;
    }
};



VXInteger importreg_release(VXState* v)
{
    VXImportHook* self;
    IMPORT_GET_SELF(v, self)
    delete self;
    return 1;
}


VXInteger importlib_import(VXState* v)
{
    std::string expr;
    VXImportHook* self;
    IMPORT_GET_SELF(v, self)
    v->GetString(2, &expr);
    self->do_import(expr);
    return self->finalize();
}

VXInteger package_searchpaths(VXState* v)
{
    std::string path;
    VXInteger i;
    VXImportHook* self;
    IMPORT_GET_SELF(v, self)
    VXArrayObj* arr = v->NewArray();
    for(i=0; i<self->searchpaths.size(); i++)
    {
        path = self->searchpaths.at(i);
        arr->Append(v->NewString(path.c_str(), path.length()));
    }
    v->Push(arr);
    return 1;
}

VXInteger package_addpath(VXState* v)
{
    std::string path;
    VXImportHook* self;
    IMPORT_GET_SELF(v, self)
    v->GetString(2, &path);
    self->searchpaths.push_back(path);
    return VX_OK;
}

static VXRegFunction package_funcs[] =
{
    {"searchpaths",  package_searchpaths,  1, NULL},
    {"addpath",      package_addpath,      2, ".s"},
    {0, 0, 0, 0},
};

static VXRegFunction importlib_funcs[]=
{
    {"import",  importlib_import,  2,  ".s"},
    {0, 0, 0, 0}
};


VXInteger voxstd_register_importlib(VXState* v)
{
    VXImportHook* self = new VXImportHook(v);
    v->GetRegistryTable()->NewSlot(v->NewString(IMPORTHOOK_NAME), (VXUserPointer)self);
    //vox_pushatexitfunc_native(v, importreg_release);
    v->AtExit(v->NewClosure(importreg_release, 0));
    //vox_registerlib(v, "package", package_funcs, true, true);
    v->RegisterLib("package", package_funcs, true);
    //vox_registerlib(v, NULL, importlib_funcs, true, true);
    v->RegisterLib(NULL, importlib_funcs, true);
    return 0;
}
