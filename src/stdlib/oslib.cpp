

#include <boost/filesystem.hpp>
#include "helper.hpp"

namespace fslib = boost::filesystem;
typedef fslib::filesystem_error fslib_error;

struct VXPopen: VXIOBase
{
    FILE* fn_base_open(const char* path, const char* mode)
    {
        return popen(path, mode);
    }

    void fn_base_close(FILE* handle)
    {
        pclose(handle);
    }
};


const char* oshelper_strerror(int errval)
{
#if defined(VOX_PLATFORM_MSWINDOWS)
    LPCSTR tempmessage;
    ::FormatMessage(
            FORMAT_MESSAGE_FROM_SYSTEM |
            FORMAT_MESSAGE_ALLOCATE_BUFFER |
            FORMAT_MESSAGE_IGNORE_INSERTS,
        NULL, errval, MAKELANGID(LANG_NEUTRAL,SUBLANG_DEFAULT),
        (LPTSTR)&tempmessage,0,NULL);
    return (const char*)tempmessage;
#else
    return strerror(errval);
#endif
}

int oshelper_errno()
{
#if defined(VOX_PLATFORM_MSWINDOWS)
    return ::GetLastError();
#else
    return errno;
#endif
}

const char* oshelper_lasterror()
{
    return oshelper_strerror(oshelper_errno());
}

bool oshelper_killpid(int pid, int sign)
{
    bool failed;
#if defined(VOX_PLATFORM_UNIX)
    failed = (kill(pid, sign) == -1);
#elif defined(VOX_PLATFORM_MSWINDOWS)
    int retv;
    HANDLE hnd = OpenProcess(1, 0, pid);
    failed = (0 != TerminateProcess(hnd, 0));
#endif
    return failed;
}

int oshelper_sleep(VXFloat milisec)
{
#ifdef VOX_PLATFORM_MSWINDOWS
    ::Sleep(milisec);
#else
    timespec req = {0, 0};
    time_t sec;
    if (milisec == 0)
    {
        return -1;
    }
    sec = (milisec/1000);
    milisec = milisec - (sec * 1000);
    req.tv_sec = sec;
    req.tv_nsec = milisec*1000000L;
    while(nanosleep(&req, &req) == -1)
    {
        continue;
    }
#endif
    return -1;
}

bool oshelper_chdir(const std::string& path)
{
    int status;
#if defined(VOX_PLATFORM_MSWINDOWS)
    status = _chdir(path.c_str());
#elif defined(VOX_PLATFORM_UNIX)
    status = chdir(path.c_str());
#endif
    return status;
}

bool oshelper_chmod(const std::string& path, unsigned int octal)
{
    int status;
#if defined(VOX_PLATFORM_MSWINDOWS)
    status = _chmod(path.c_str(), octal);
#elif defined(VOX_PLATFORM_UNIX)
    status = chmod(path.c_str(), octal);
#endif
    return (status == 0);
}

bool oshelper_mkdir(const std::string& path, int octal)
{
    int status;
#if defined(VOX_PLATFORM_MSWINDOWS)
    (void)octal;
    status = _mkdir(path.c_str());
#elif defined(VOX_PLATFORM_UNIX)
    status = mkdir(path.c_str(), octal);
#endif
    return (status == -1);
}

template<typename Type>
inline static const char* oshelper_exception_message(Type& err)
{
    return err.code().message().c_str();
}

VXInteger oslib_getenv(VXState* v)
{
    const char* env_name;
    const char* env_value;
    v->GetString(2, &env_name, NULL);
    env_value = getenv(env_name);
    if(env_value != NULL)
    {
        v->Push(v->NewString(env_value));
        return 1;
    }
    return 0;
}

VXInteger oslib_system(VXState* v)
{
    const char* shellcmd;
    if(system(NULL) == 0)
    {
        return v->ThrowError("system() is not available");
    }
    v->GetString(2, &shellcmd, NULL);
    v->Push(VXInteger(system(shellcmd)));
    return 1;
}


VXInteger oslib_clock(VXState* v)
{
    v->Push(VXFloat(clock()) / VXFloat(CLOCKS_PER_SEC));
    return 1;
}

VXInteger oslib_time(VXState* v)
{
    time_t t;
    time(&t);
    v->Push(VXInteger(t));
    return 1;
}

VXInteger oslib_remove(VXState* v)
{
    const char* path;
    v->GetString(2, &path, NULL);
    try
    {
        fslib::remove(path);
    }
    catch(fslib_error& err)
    {
        return v->ThrowError(oshelper_exception_message(err));
    }
    return 0;
}

VXInteger oslib_remove_all(VXState* v)
{
    const char* path;
    v->GetString(2, &path, NULL);
    try
    {
        fslib::remove_all(path);
    }
    catch(fslib_error& err)
    {
        return v->ThrowError(oshelper_exception_message(err));
    }
    return 0;
}

VXInteger oslib_exit(VXState* v)
{
    VXInteger status;
    if(VX_FAILED(v->GetInteger(2, &status)))
    {
        status = -1;
    }
    exit(status);
    return 0;
}


VXInteger oslib_rename(VXState* v)
{
    const char* oldpath;
    const char* newpath;
    v->GetString(2, &oldpath, NULL);
    v->GetString(3, &newpath, NULL);
    try
    {
        fslib::rename(oldpath, newpath);
    }
    catch(fslib_error& err)
    {
        return v->ThrowError(oshelper_exception_message(err));
    }
    return 0;
}

VXInteger oslib_date(VXState* v)
{
    time_t t;
    VXInteger it;
    VXInteger top;
    VXInteger format = 'l';
    if(v->GetTop() > 1)
    {
        v->GetInteger(2,&it);
        t = it;
        if(v->GetTop() > 2)
        {
            v->GetInteger(3, (VXInteger*)&format);
        }
    }
    else
    {
        time(&t);
    }
    tm *date;
    if(format == 'u')
    {
        date = gmtime(&t);
    }
    else
    {
        date = localtime(&t);
    }
    if(!date)
    {
        return v->ThrowError("crt api failure");
    }
    VXTableObj* tb = v->NewTable();
    tb->NewSlot(v->NewString("sec"),   VXInteger(date->tm_sec));
    tb->NewSlot(v->NewString("min"),   VXInteger(date->tm_min));
    tb->NewSlot(v->NewString("hour"),  VXInteger(date->tm_hour));
    tb->NewSlot(v->NewString("day"),   VXInteger(date->tm_mday));
    tb->NewSlot(v->NewString("month"), VXInteger(date->tm_mon));
    tb->NewSlot(v->NewString("year"),  VXInteger(date->tm_year));
    tb->NewSlot(v->NewString("wday"),  VXInteger(date->tm_wday));
    tb->NewSlot(v->NewString("yday"),  VXInteger(date->tm_yday));
    v->Push(tb);
    return 1;
}

VXInteger oslib_listdir(VXState* v)
{
    const char* path;
    std::string s_itm;
    v->GetString(2, &path, NULL);
    try
    {
        fslib::directory_iterator end;
        fslib::directory_iterator iter(path);
        VXArrayObj* arr = v->NewArray();
        for(; iter!=end; iter++)
        {
            s_itm = (*iter).path().string();
            arr->Append(v->NewString(s_itm.c_str(), s_itm.length()));
        }
        v->Push(arr);
    }
    catch(fslib_error& err)
    {
        return v->ThrowError(oshelper_exception_message(err));
    }
    return 1;
}

VXInteger oslib_kill(VXState* v)
{
    VXInteger pid;
    VXInteger sign;
    v->GetInteger(2, &pid);
    if(VX_FAILED(v->GetInteger(3, &sign)))
    {
        sign = SIGINT;
    }
    v->PushInteger(oshelper_killpid(pid, sign));
    return 1;
}

VXInteger oslib_mkdir(VXState* v)
{
    const char* path;
    VXInteger permissions;
    v->GetString(2, &path, NULL);
    if(VX_FAILED(v->GetInteger(3, &permissions)))
    {
        permissions = 0755;
    }
    if(oshelper_mkdir(path, permissions) == false)
    {
        return v->ThrowError(oshelper_lasterror());
    }
    return 0;
}

VXInteger oslib_chmod(VXState* v)
{
    const char* path;
    VXInteger permissions;
    v->GetString(2, &path, NULL);
    v->GetInteger(3, &permissions);
    if(oshelper_chmod(path, permissions) == false)
    {
        return v->ThrowError(oshelper_lasterror());
    }
    return 0;
}

VXInteger oslib_basename(VXState* v)
{
    const char* path;
    std::string newpath;
    v->GetString(2, &path, NULL);
    newpath = fslib::path(path).filename().string();
    v->Push(v->NewString(newpath.c_str(), newpath.length()));
    return 1;
}

VXInteger oslib_dirname(VXState* v)
{
    const char* path;
    std::string newpath;
    v->GetString(2, &path, NULL);
    fslib::path _tmp(path);
    newpath = fslib::path(path).parent_path().string();
    v->Push(v->NewString(newpath.c_str(), newpath.length()));
    return 1;
}

VXInteger oslib_usleep(VXState* v)
{
    VXFloat howlong;
    v->GetFloat(2, &howlong);
    oshelper_sleep(howlong);
    return 0;
}

VXInteger oslib_sleep(VXState* v)
{
    VXInteger howlong;
    v->GetInteger(2, &howlong);
    oshelper_sleep(howlong * 1000);
    return 0;
}

VXInteger oslib_filesize(VXState* v)
{
    const char* path;
    v->GetString(2, &path, NULL);
    try
    {
        v->Push(VXInteger(fslib::file_size(path)));
    }
    catch(fslib_error& err)
    {
        return v->ThrowError(oshelper_exception_message(err));
    }
    return 1;
}

VXInteger oslib_exists(VXState* v)
{
    const char* path;
    v->GetString(2, &path, NULL);
    try
    {
        v->Push(bool(fslib::exists(path)));
    }
    catch(fslib_error& err)
    {
        return v->ThrowError(oshelper_exception_message(err));
    }
    return 1;
}

VXInteger oslib_isfile(VXState* v)
{
    const char* path;
    v->GetString(2, &path, NULL);
    v->Push(bool(fslib::is_regular_file(path)));
    return 1;
}

VXInteger oslib_isdir(VXState* v)
{
    const char* path;
    v->GetString(2, &path, NULL);
    try
    {
        v->Push(bool(fslib::is_directory(path)));
    }
    catch(fslib_error& err)
    {
        return v->ThrowError(oshelper_exception_message(err));
    }
    return 1;
}

VXInteger oslib_stat(VXState* v)
{
    const char* path;
    struct stat buf;
    v->GetString(2, &path, NULL);
    if(stat(path, &buf) == 0)
    {
        VXTableObj* tb = v->NewTable();

        v->Push(tb);
        return 1;
    }
    return v->ThrowError(oshelper_lasterror());
}

/* popen */

VXInteger popenclass_fn_releasehook(VXUserPointer p, VXInteger size)
{
    (void)size;
    VXPopen* self = (VXPopen*)p;
    if(self->isclosed == false)
    {
        self->close();
    }
    delete self;
    return 1;
}

VXInteger popenclass_fn_constructor(VXState* v)
{
    const char* path;
    const char* mode;
    VXPopen* self;
    self = new VXPopen;
    v->GetString(2, &path, NULL);
    if(VX_FAILED(v->GetString(3, &mode, NULL)))
    {
        mode = "r";
    }
    if(!self->open(path, mode))
    {
        return v->ThrowError(oshelper_lasterror());
    }
    v->SetInstanceUp(1, (VXUserPointer*)&self);
    v->SetReleaseHook(1, popenclass_fn_releasehook);
    return 0;
}

VXInteger popenclass_fn_read(VXState* v)
{
    char* buffer;
    VXInteger howmuch;
    VXInteger haveread;
    VXPopen* self;
    v->GetInstanceUp(1, (VXUserPointer*)&self, 0);
    if(VX_FAILED(v->GetInteger(2, &howmuch)))
    {
        howmuch = 1024;
    }
    buffer = new char[howmuch+1];
    haveread = self->readbuf(buffer, howmuch);
    if(haveread > 0)
    {
        buffer[haveread] = 0;
        v->PushString(buffer, haveread);
    }
    else
    {
        v->PushNull();
    }
    delete[] buffer;
    return 1;
}

VXInteger popenclass_fn_flush(VXState* v)
{
    VXPopen* self;
    v->GetInstanceUp(1, (VXUserPointer*)&self, 0);
    v->Push(self->flushstream());
    return 1;
}

VXInteger popenclass_fn_name(VXState* v)
{
    VXPopen* self;
    v->GetInstanceUp(1, (VXUserPointer*)&self, 0);
    v->PushString(self->name);
    return 1;
}

VXInteger popenclass_fn_readline(VXState* v)
{
    VXPopen* self;
    std::string buffer;
    v->GetInstanceUp(1, (VXUserPointer*)&self, 0);
    if(self->eof())
    {
        return 0;
    }
    self->readline(buffer);
    v->Push(v->NewString(buffer.c_str(), buffer.length()));
    return 1;
}

VXInteger popenclass_fn_readchar(VXState* v)
{
    VXPopen* self;
    VXInteger c;
    v->GetInstanceUp(1, (VXUserPointer*)&self, 0);
    c = self->readchar();
    if(c == EOF)
    {
        v->PushNull();
    }
    else
    {
        v->Push(c);
    }
    return 1;
}


VXInteger popenclass_fn_close(VXState* v)
{
    VXPopen* self;
    v->GetInstanceUp(1, (VXUserPointer*)&self, 0);
    v->Push(self->close());
    return 1;
}

VXInteger popenclass_fn_write(VXState* v)
{
    const char* data;
    VXInteger len;
    VXInteger written;
    VXPopen* self;
    v->GetInstanceUp(1, (VXUserPointer*)&self, 0);
    v->GetString(2, &data, &len);
    if(self->write(data, len, &written) == true)
    {
        v->Push(written);
        return 0;
    }
    /* when the file got opened in readonly mode, write()
       will fail, so don't just ignore it, but handle it */
    return v->ThrowError(self->error());
}

VXInteger popenclass_fn_eof(VXState* v)
{
    VXPopen* self;
    v->GetInstanceUp(1, (VXUserPointer*)&self, 0);
    v->Push(bool(self->eof()));
    return 1;
}

static VXRegFunction popen_funcs[]=
{
    {"constructor", popenclass_fn_constructor, -2, ".ss"},
    {"name",        popenclass_fn_name, -1, ".n"},
    {"read",        popenclass_fn_read, -1, ".n"},
    {"readchar",    popenclass_fn_readchar, 1,  NULL},
    {"readline",    popenclass_fn_readline, -1, NULL},
    {"write",       popenclass_fn_write, 2,  ".s"},
    {"close",       popenclass_fn_close, 1,  NULL},
    {"flush",       popenclass_fn_flush, 1,  "x"},
    {"eof",         popenclass_fn_eof, 1,  "x"},
    {0, 0, 0, 0}
};

VXInteger oslib_uname(VXState* v)
{
#if defined(VOX_PLATFORM_UNIX)
    struct utsname buf;
    if(uname(&buf) == 0)
    {
        VXTableObj* tb = v->NewTable();
        tb->NewSlot(v->NewString("sysname"), v->NewString(buf.sysname));
        tb->NewSlot(v->NewString("nodename"), v->NewString(buf.nodename));
        tb->NewSlot(v->NewString("release"), v->NewString(buf.release));
        tb->NewSlot(v->NewString("version"), v->NewString(buf.version));
        tb->NewSlot(v->NewString("machine"), v->NewString(buf.machine));
        v->Push(tb);
        return 1;
    }
    return v->ThrowError(oshelper_lasterror());
#else
    return v->ThrowError("uname() is not available on this platform");
#endif
}



VXInteger oslib_tempname(VXState* v)
{
    char* result; // char*, because it needs to be free()'d
    const char* path;
    const char* tpl;
    if(VX_FAILED(v->GetString(2, &path, NULL)))
        path = NULL;
    if(VX_FAILED(v->GetString(3, &tpl, NULL)))
        tpl = NULL;
    result = tempnam(path, tpl);
    v->Push(v->NewString(result));
    free(result);
    return 1;
}

VXInteger oslib_joinpath(VXState* v)
{
    VXInteger i;
    VXObject strval;
    VXObject ob;
    std::string fullpath;
    fslib::path p;
    for(i=2; i!=(v->GetTop()+1); i++)
    {
        std::string str;
        ob = v->StackGet(i);
        v->ToString(ob, strval);
        strval.String()->Get(&str);
        p /= str;
        ob.Null();
    }
    fullpath = p.string();
    v->Push(v->NewString(fullpath.c_str(), fullpath.length()));
    return 1;
}

VXInteger oslib_abspath(VXState* v)
{
    std::string subject;
    std::string fullpath;
    v->GetString(2, &subject);
    fslib::path p(subject);
    fullpath = p.string();
    v->Push(v->NewString(fullpath.c_str(), fullpath.length()));
    return 1;
}

VXRegFunction oslib_funcs[]=
{
    {"getenv",      oslib_getenv,       2,   ".s"},
    {"system",      oslib_system,       2,   ".s"},
    {"date",        oslib_date,         -1,  ".nn"},
    {"remove",      oslib_remove,       2,   ".s"},
    {"remove_all",  oslib_remove_all,   2,   ".s"},
    {"listdir",     oslib_listdir,      2,   ".s"},
    {"rename",      oslib_rename,       3,   ".ss"},
    {"clock",       oslib_clock,        0,   NULL},
    {"time",        oslib_time,         1,   NULL},
    {"exit",        oslib_exit,         -1,   ".n"},
    {"kill",        oslib_kill,         -1,  ".nn"},
    {"chmod",       oslib_chmod,        3,   ".sn"},
    {"mkdir",       oslib_mkdir,        3,  ".s|.n"},
    {"basename",    oslib_basename,     2,   ".s"},
    {"dirname",     oslib_dirname,      2,   ".s"},
    {"usleep",      oslib_usleep,       2,   ".n"},
    {"sleep",       oslib_sleep,        2,   ".n"},
    {"exists",      oslib_exists,       2,   ".s"},
    {"isdir",       oslib_isdir,        2,   ".s"},
    {"isfile",      oslib_isfile,       2,   ".s"},
    {"abspath",     oslib_abspath,      2,   ".s"},
    {"joinpath",    oslib_joinpath,     -1,  "."},
    {"filesize",    oslib_filesize,     2,   ".s"},
    {"uname",       oslib_uname,        1,   NULL},
    {"tempname",    oslib_tempname,      -1,  ".ss"},
    {0, 0, 0, 0}
};

VXInteger voxstd_register_oslib(VXState* v)
{
    VXTableObj* tb = v->NewTable();
    v->RegisterLib("os", oslib_funcs, true, tb);
    tb->Set(v->NewString("popen"), v->RegClass(popen_funcs));
#ifndef VX_NO_LOADLIB
    oslib_register_libload(v, tb);
#endif
    return 1;
}

