
/* important: don't move boost.xpressive include
   instruction to helper.hpp, otherwise compilation
   speed goes down the drain */
#include <cstdio>
#include <boost/xpressive/xpressive.hpp>
#include "vox.h"

#define SETUP_REX(vm, selfname) \
    RexObj* selfname = NULL; \
    if(VX_FAILED(vm->GetInstanceUp(1, (VXUserPointer *)&selfname,0))) \
    { \
        vm->ThrowError("regex: GetInstanceUp failed!"); \
    }

struct RexObj
{
    boost::xpressive::sregex rex;
};

VXInteger rexhelp_compile(VXState* v, RexObj* self, const std::string& pattern)
{
    try
    {
        self->rex = boost::xpressive::sregex::compile(pattern);
    }
    catch(boost::xpressive::regex_error& err)
    {
        return v->ThrowError(err.what());
    }
    return VX_OK;
}

template<typename Type>
VXArrayObj* rexhelp_array_from_match(VXState* v, Type& what)
{
    VXInteger artop;
    size_t i;
    VXArrayObj* arr = v->NewArray();
    for(i=0; i<what.size(); i++)
    {
        std::string submatch(what[i].first, what[i].second);
        if(submatch.length() > 0)
        {
            arr->Append(v->NewString(submatch.c_str(), submatch.length()));
        }
    }
    return arr;
}

VXArrayObj* rexhelp_findall(VXState* v, RexObj* self, const std::string& subject)
{
    VXArrayObj* arr = v->NewArray();
    std::string::const_iterator begin = subject.begin();
    std::string::const_iterator end = subject.end();
    boost::xpressive::match_results<std::string::const_iterator> what;
    while(boost::xpressive::regex_search(begin, end, what, self->rex))
    {
        VXInteger newtop;
        std::string str;
        str = what.str();
        VXTableObj* tb = v->NewTable();

        /* the primary matched string */
        tb->NewSlot(v->NewString("string"), v->NewString(str.c_str(), str.length()));


        /* matched substrings (eg, from groups) */
        tb->NewSlot(v->NewString("submatches"), rexhelp_array_from_match(v, what));


        /* where the matched string starts */
        tb->NewSlot(v->NewString("position"), VXInteger(what.position()));

        /* absolute length of the matched string */
        tb->NewSlot(v->NewString("length"), VXInteger(what.length()));

        arr->Append(tb);
        begin = what[0].second;
    }
    return arr;
}


VXInteger rexfunc_replace(VXState* v)
{
    std::string pat;
    std::string sub;
    std::string rep;
    std::string result;
    VXInteger status;
    RexObj* self = new RexObj;
    v->GetString(2, &sub);
    v->GetString(3, &pat);
    v->GetString(4, &rep);
    if((status = rexhelp_compile(v, self, pat)) != VX_OK)
    {
        delete self;
        return status;
    }
    else
    {
        try
        {
            result = boost::xpressive::regex_replace(
                sub,
                self->rex,
                rep,
                boost::xpressive::regex_constants::format_all);
            v->Push(v->NewString(result.c_str(), result.length()));
            delete self;
            return 1;
        }
        catch(boost::xpressive::regex_error& err)
        {
            delete self;
            return v->ThrowError(err.what());
        }
    }
    return 0;
}


VXInteger rexfunc_match(VXState* v)
{
    std::string sub;
    std::string pat;
    VXInteger status;
    RexObj* self = new RexObj;
    v->GetString(2, &sub);
    v->GetString(3, &pat);
    if((status = rexhelp_compile(v, self, pat)) != VX_OK)
    {
        delete self;
        return status;
    }
    else
    {
        boost::xpressive::match_results<std::string::const_iterator> what;
        if(boost::xpressive::regex_match(sub, what, self->rex))
        {
            v->Push(rexhelp_array_from_match(v, what));
            delete self;
            return 1;
        }
        return 0;
    }
    return 0;
}

VXInteger rexfunc_search(VXState* v)
{
    std::string sub;
    std::string pat;
    VXInteger status;
    RexObj* self = new RexObj;
    v->GetString(2, &sub);
    v->GetString(3, &pat);
    if((status = rexhelp_compile(v, self, pat)) != VX_OK)
    {
        delete self;
        return status;
    }
    else
    {
        boost::xpressive::match_results<std::string::const_iterator> what;
        if(boost::xpressive::regex_search(sub, what, self->rex))
        {
            v->Push(rexhelp_array_from_match(v, what));
            delete self;
            return 1;
        }
        return 0;
    }
    return 0;
}

static VXInteger rexclass_typeof(VXState* v)
{
    v->Push(v->NewString("regexp"));
    return 1;
}

static VXInteger rexclass_release(VXUserPointer p, VXInteger size)
{
    (void)size;
    RexObj* rex = (RexObj*)p;
    delete rex;
    return 1;
}

VXInteger rexclass_match(VXState* v)
{
    SETUP_REX(v, self)
    std::string subject;
    boost::xpressive::match_results<std::string::const_iterator> what;
    v->GetString(2, &subject);
    if(boost::xpressive::regex_match(subject, what, self->rex))
    {
        v->Push(rexhelp_array_from_match(v, what));
        return 1;
    }
    return 0;
}

VXInteger rexclass_group(VXState* v)
{
    SETUP_REX(v, self)
    std::string subject;
    std::string groupname;
    v->GetString(2, &subject);
    v->GetString(3, &groupname);
    boost::xpressive::smatch what;
    if(regex_search(subject, what, self->rex))
    {
        try
        {
            std::string matched = what[groupname];
            v->Push(v->NewString(matched.c_str(), matched.length()));
        }
        catch(boost::xpressive::regex_error& err)
        {
            return v->ThrowError(err.what());
        }
        return 1;
    }
    return 0;
}

VXInteger rexclass_findall(VXState* v)
{
    SETUP_REX(v, self)
    std::string opt;
    v->GetString(2, &opt);
    v->Push(rexhelp_findall(v, self, opt));
    return 1;
}

static VXInteger rexclass_constructor(VXState* v)
{
    RexObj* self = new RexObj;
    std::string pat;
    VXInteger status;
    v->GetString(2, &pat);
    if((status = rexhelp_compile(v, self, pat)) != VX_OK)
    {
        return status;
    }
    v->SetInstanceUp(1, (VXUserPointer*)&self);
    v->SetReleaseHook(1, rexclass_release);
    return 0;
}

VXRegFunction rexclass_funcs[] =
{
    {"constructor",    rexclass_constructor, 2, ".s"},
    {"findall",        rexclass_findall,     2, ".s"},
    {"match",          rexclass_match,       2, ".s"},
    {"group",          rexclass_group,       3, ".ss"},
    {"__typeof__",     rexclass_typeof,   1, "x"},
    {0, 0, 0, 0},
};


static VXRegFunction regexlib_funcs[]=
{
    {"match",    rexfunc_match,   3, ".ss"},
    {"search",   rexfunc_search,  3, ".ss"},
    {"replace",  rexfunc_replace, 4, ".sss"},
    {0, 0, 0, 0}
};


VXInteger voxstd_register_regexlib(VXState* v)
{
    VXTableObj* tb = v->NewTable();
    tb->NewSlot(v->NewString("compile"), v->RegClass(rexclass_funcs));
    v->RegisterLib("regexp", regexlib_funcs, true, tb);
    return 1;
}
