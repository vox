
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "sharedlib.h"

#if defined(SHAREDLIB_ISWINNT)
#   include <windows.h>
#elif defined(SHAREDLIB_ISUNIX)
#   include <unistd.h>
#   include <dlfcn.h>
#   include <errno.h>
#endif

struct shl_handle_t {
#ifdef SHAREDLIB_ISWINNT
    /* technically, HANDLE is just typedef'd to 'const void*' anyway */
    HANDLE libhandle;
#elif defined(SHAREDLIB_ISUNIX)
    void* libhandle;
#endif
    /* currently not really used */
    int isloaded;
};

shl_handle_t* shl_new()
{
    shl_handle_t* ret;
    ret = (shl_handle_t*)malloc(sizeof(shl_handle_t));
    ret->libhandle = NULL;
    ret->isloaded = 0;
    return ret;
}

shl_handle_t* shl_openlib(const char* path)
{
    shl_handle_t* ret;
    ret = shl_new();
#if defined(SHAREDLIB_ISWINNT)
    ret->libhandle = LoadLibrary(path);
#elif defined(SHAREDLIB_ISUNIX)
    ret->libhandle = dlopen(path, RTLD_LAZY);
#endif
    if(ret->libhandle == NULL)
    {
        shl_destroy(ret);
        return NULL;
    }
    return ret;
}

shl_handle_t* shl_openself()
{
    shl_handle_t* ret;
    ret = shl_new();
#if defined(SHAREDLIB_ISWINNT)
    HANDLE this_process;
    /* GetModuleHandleEx(0,0, &this_process); */
    this_process = GetModuleHandle(0);
    ret->libhandle = this_process;
#elif defined(SHAREDLIB_ISUNIX)
    ret->libhandle = dlopen(NULL, 0);
#endif
    return ret;
}


void shl_destroy(shl_handle_t* shl)
{
    if(shl != NULL)
    {
        free(shl);
        shl = NULL;
    }
}

void shl_closelib(shl_handle_t* shl)
{
    if(shl != NULL)
    {
#if defined(SHAREDLIB_ISWINNT)
        FreeLibrary(shl->libhandle);
#elif defined(SHAREDLIB_ISUNIX)
        dlclose(shl->libhandle);
#endif
    }
    shl_destroy(shl);
}

int shl_havesym(shl_handle_t* shl, const char* sym)
{
    if(shl_resolve(shl, sym) == NULL)
    {
        return 0;
    }
    return 1;
}

void* shl_resolve(shl_handle_t* shl, const char* sym)
{
    void* symaddr;
#if defined(SHAREDLIB_ISWINNT)
    symaddr = (void*)GetProcAddress(shl->libhandle, sym);
#elif defined(SHAREDLIB_ISUNIX)
    symaddr = (void*)dlsym(shl->libhandle, sym);
#endif
    return symaddr;
}

void* shl_loadsym(shl_handle_t* shl, const char* sym)
{
    if(shl_havesym(shl, sym) == 0)
    {
        return NULL;
    }
    return shl_resolve(shl, sym);
}

int shl_geterrcode(shl_handle_t* shl)
{
    (void)shl;
#if defined(SHAREDLIB_ISWINNT)
    /* the interface for loading/unloading shared objects on
       windows uses the global errorcode system */
    return GetLastError();
#elif defined(SHAREDLIB_ISUNIX)
    return errno;
#endif
}

const char* shl_geterrstr(shl_handle_t* shl)
{
#if defined(SHAREDLIB_ISWINNT)
    int errval = shl_geterrcode(shl);
    /*
        sometimes, in localized versions of Windows XP (and newer),
        the output returned by FormatMessage() contains "\r\n" ...
        so far I have not found a 'sane' way to fix this.
        also, it's not like i'm the one who should fix this in the first place.
        YES MICROSOFT I AM TALKING ABOUT YOU FIX YOUR SHIT FOR FUCKS SAKE
    */
    LPCSTR tempmessage;
    FormatMessage(
            FORMAT_MESSAGE_FROM_SYSTEM |
            FORMAT_MESSAGE_ALLOCATE_BUFFER |
            FORMAT_MESSAGE_IGNORE_INSERTS,
        NULL, errval, MAKELANGID(LANG_NEUTRAL,SUBLANG_DEFAULT),
        (LPTSTR)& tempmessage, 0,NULL);
    return tempmessage;
#elif defined(SHAREDLIB_ISUNIX)
    const char* errstr;
    /* sometimes dlerror() returns NULL, and in that case,
       we just use strerror() instead */
    if((errstr = dlerror()) == NULL)
    {
        errstr = strerror(shl_geterrcode(shl));
    }
    return errstr;
#endif
}

int shl_preload(const char* path)
{
    shl_handle_t* handle;
    if((handle = shl_openlib(path)) == NULL)
    {
        return 0;
    }
    shl_closelib(handle);
    return 1;

}
