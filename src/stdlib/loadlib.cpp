
#include "helper.hpp"

#ifndef VX_NO_LOADLIB
#include "vxlibrary.hpp"

VXInteger oslib_loadlib_releasehook(VXUserPointer p, VXInteger size)
{
    (void)size;
    VXSharedLibrary* self = (VXSharedLibrary*)p;
    delete self;
    return 1;
}

VXInteger oslib_loadlib_constructor(VXState* v)
{
    VXInteger ret = 0;
    const char* path;
    VXSharedLibrary* self = NULL;
    v->GetString(2, &path, NULL);
    try
    {
        self = new VXSharedLibrary(path);
    }
    catch(VXSharedLibrary::LoadError& err)
    {
        ret = v->ThrowError(err.what());
    }
    v->SetInstanceUp(1, (VXUserPointer*)&self);
    v->SetReleaseHook(1, oslib_loadlib_releasehook);
    return ret;
}

VXInteger oslib_loadlib_resolve(VXState* v)
{
    const char* symname;
    char* errbuf;
    VXFunction func;
    VXSharedLibrary* self;
    v->GetInstanceUp(1, (VXUserPointer*)&self, 0);
    v->GetString(2, &symname, NULL);
    try
    {
        func = self->resolve<VXFunction>(symname);
        v->Push(v->NewClosure(func, 0));
        return 1;
    }
    catch(VXSharedLibrary::SymbolError& err)
    {
        return v->ThrowError("Could not resolve symbol '%s'", symname);
    }
    return 0;
}

VXInteger oslib_loadlib_close(VXState* v)
{
    VXSharedLibrary *self;
    v->GetInstanceUp(1, (VXUserPointer*)&self, 0);
    if(self == NULL)
    {
        return v->ThrowError("attempt to close an invalid LoadLib object");
    }
    self->close();
    return 0;
}

static VXInteger oslib_loadlib_typeof(VXState* v)
{
    v->PushString("os.loadlib", -1);
    return 1;
}

static VXRegFunction oslib_libload_funcs[]=
{
    {"constructor",  oslib_loadlib_constructor,  2,  ".s"},
    {"resolve",      oslib_loadlib_resolve,      2,  ".s"},
    {"close",        oslib_loadlib_close,        1,  NULL},
    {"_typeof",      oslib_loadlib_typeof,       1,  "x"},
    {0, 0, 0, 0}
};

void oslib_register_libload(VXState* v, VXTableObj* tb)
{
    //vox_regclass(v, "loadlib", oslib_libload_funcs);
    //vox_rawset(v, top);
    tb->NewSlot(v->NewString("loadlib"), v->RegClass(oslib_libload_funcs));
}

#endif

