
/*
* Copyright (C) 2008-2011 Beelzebub Inc.  All rights reserved.
*
* Permission is hereby granted, free of charge, to any person obtaining
* a copy of this software and associated documentation files (the
* "Software"), to deal in the Software without restriction, including
* without limitation the rights to use, copy, modify, merge, publish,
* distribute, sublicense, and/or sell copies of the Software, and to
* permit persons to whom the Software is furnished to do so, subject to
* the following conditions:
*
* The above copyright notice and this permission notice shall be
* included in all copies or substantial portions of the Software.
*
* THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
* EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
* MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
* IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY
* CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT,
* TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE
* SOFTWARE OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
*/

#ifndef __sharedlib_header_h__
#define __sharedlib_header_h__

#if defined(_WIN32) || defined(_WIN32)
#   define SHAREDLIB_ISWINNT 1
#elif defined(__unix__) || defined(__linux__)
#   define SHAREDLIB_ISUNIX
#else
#   error "Unknown or unsupported Operating system?"
#endif



#ifdef __cplusplus
extern "C" {
#endif /* __cplusplus */

typedef struct shl_handle_t shl_handle_t;

shl_handle_t* shl_new();
shl_handle_t* shl_openlib(const char* path);
shl_handle_t* shl_openself();
void shl_destroy(shl_handle_t* shl);
void shl_closelib(shl_handle_t* shl);
int shl_havesym(shl_handle_t* shl, const char* sym);
void* shl_resolve(shl_handle_t* shl, const char* sym);
void* shl_loadsym(shl_handle_t* shl, const char* sym);
int shl_geterrcode(shl_handle_t* shl);
const char* shl_geterrstr(shl_handle_t* shl);
int shl_preload(const char* path);

#ifdef __cplusplus
}
#endif /*__cplusplus */

#endif /* __sharedlib_header_h__ */
