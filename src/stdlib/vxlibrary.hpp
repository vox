
#include "helper.hpp"
#include "sharedlib.h"


class VXSharedLibrary {
    public:
        class Error: public std::runtime_error {
            public:
                Error():
                    std::runtime_error("VXSharedLibrary::Error")
                {}

                Error(const std::string& msg):
                    std::runtime_error(msg)
                {}

                virtual ~Error() throw()
                {}
        };

        class LoadError: public VXSharedLibrary::Error {
            public:
                LoadError(const std::string& msg):
                    VXSharedLibrary::Error(msg.c_str())
                {
                }
        };

        class SymbolError: public VXSharedLibrary::Error {
            public:
                SymbolError(const std::string& msg):
                    VXSharedLibrary::Error(msg.c_str())
                {
                }
        };

        class CloseError: public VXSharedLibrary::Error {
            public:
                CloseError(const std::string& msg):
                    VXSharedLibrary::Error(msg.c_str())
                {
                }
        };

        class NotOpenError: public VXSharedLibrary::Error {
            public:
                NotOpenError(const std::string& msg):
                    VXSharedLibrary::Error(msg.c_str())
                {
                }
        };

    public:
        static VXSharedLibrary openSelf()
        {
            VXSharedLibrary lib(shl_openself());
            return lib;
        }


    private:
        shl_handle_t* m_handle;
        std::string errormessage;
        std::string message;
        bool m_isclosed;

    private:
        std::string lasterr()
        {
            const char* err = shl_geterrstr(m_handle);
            if(err == NULL)
            {
                return strerror(errno);
            }
            return err;
        }

        void setup()
        {
            m_isclosed = false;
        }

    public:
        VXSharedLibrary()
        {
        }

        VXSharedLibrary(shl_handle_t* ctx)
        {
            this->setup();
            this->m_handle = ctx;
        }

        VXSharedLibrary(const std::string& path)
        {
            this->setup();
            this->open(path);
        }

        ~VXSharedLibrary()
        {}

        void open(const std::string& path)
        {
            this->setup();
            this->m_handle = shl_openlib(path.c_str());
            if(this->m_handle == NULL)
            {
                this->destroy();
                throw VXSharedLibrary::LoadError(this->lasterr());
            }
        }

        void* resolve(const std::string& funcname)
        {
            void* ptr;
            ptr = shl_resolve(this->m_handle, funcname.c_str());
            if(ptr == NULL)
            {
                throw VXSharedLibrary::SymbolError(this->lasterr());
            }
            return ptr;
        }

        template<typename func_t> func_t resolve(const std::string& name)
        {
            return forcecast<func_t>(this->resolve(name));
        }

        void destroy()
        {
            shl_destroy(this->m_handle);
        }

        void close()
        {
            if(this->m_handle != NULL)
            {
                if(!m_isclosed)
                {
                    shl_closelib(this->m_handle);
                    m_isclosed = true;
                }
                else
                {
                    shl_destroy(this->m_handle);
                }
            }
            else
            {
                shl_destroy(this->m_handle);
            }
        }
};
