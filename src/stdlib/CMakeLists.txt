
SET(vox_stdlib_sourcefiles
    "loadlib.cpp"
    ## 3rdparty yatta yatta
    "sharedlib.c"

)

IF(Boost_FOUND)
    MESSAGE(STATUS "Found boost, 'std.os', 'std.re' and 'import' will be supported!")
    SET(vox_stdlib_sourcefiles
        ${vox_stdlib_sourcefiles}
        "oslib.cpp"
        "regexp.cpp"
        "importlib.cpp"
    )
ENDIF()

INCLUDE_DIRECTORIES("extra-include")

IF(BUILD_SHAREDLIBS)
    ADD_LIBRARY(${vox_stdlib} SHARED ${vox_stdlib_sourcefiles})
    TARGET_LINK_LIBRARIES(${vox_stdlib} ${vox_corelib})
    SET_TARGET_PROPERTIES(${vox_stdlib}
        PROPERTIES
            BUILD_WITH_INSTALL_RPATH    TRUE
            INSTALL_RPATH_USE_LINK_PATH TRUE
            INSTALL_RPATH               "${vox_rpath_paths}"
    )
    IF(WIN32)
        SET_TARGET_PROPERTIES(${vox_stdlib}
            PROPERTIES
                PREFIX ""
        )
    ENDIF(WIN32)
ELSE(BUILD_SHAREDLIBS)
    ADD_LIBRARY(${vox_stdlib} STATIC ${vox_stdlib_sourcefiles})
ENDIF(BUILD_SHAREDLIBS)
SET_TARGET_PROPERTIES(${vox_stdlib}
    PROPERTIES
        OUTPUT_NAME                 "${vox_stdlib_name}"
)
INSTALL(TARGETS ${vox_stdlib} DESTINATION lib)
