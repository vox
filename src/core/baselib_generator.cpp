
#include "baselib.hpp"
#include "names.hpp"

VXInteger generator_getstatus(VXState* v)
{
    VXRawObj &o=stack_get(v,1);
    switch(_generator(o)->_state)
    {
        case VXGeneratorObj::eSuspended:
            v->Push(VXStringObj::Create(_ss(v), GENERATOR_STATUS_SUSPENDED));
            break;
        case VXGeneratorObj::eRunning:
            v->Push(VXStringObj::Create(_ss(v), GENERATOR_STATUS_RUNNING));
            break;
        case VXGeneratorObj::eDead:
            v->Push(VXStringObj::Create(_ss(v), GENERATOR_STATUS_DEAD));
            break;
    }
    return 1;
}

VXRegFunction VXSharedState::_generator_default_delegate_funcz[]=
{
    {"getstatus",  generator_getstatus,       1, "g"},
    {"weakref",    obj_delegate_weakref,      1, NULL },
    {"tostring",   default_delegate_tostring, -1, ".b"},
    {"repr",       default_delegate_repr,     1,  "."},
    {0, 0, 0, 0}
};





