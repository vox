
#ifndef _VXState_H_
#define _VXState_H_

#include "opcodes.hpp"
#include "closure.hpp"
#include "funcproto.hpp"
#include "object.hpp"

#define MAX_NATIVE_CALLS 100
#define MIN_STACK_OVERHEAD 15

#define VX_SUSPEND_FLAG -666
#define DONT_FALL_BACK 666

#define _INLINE

#define STK(a) _stack.values()[_stackbase+(a)]
#define TARGET _stack.values()[_stackbase+arg0]



#define PUSH_CALLINFO(v,nci) \
    { \
        VXInteger css = v->_callsstacksize; \
        if(css == v->_alloccallsstacksize) \
        { \
            v->GrowCallStack(); \
        } \
        v->ci = &v->_callsstack[css]; \
        *(v->ci) = nci; \
        v->_callsstacksize++; \
    }

#define POP_CALLINFO(v) \
    { \
        VXInteger css = --v->_callsstacksize; \
        v->ci->_closure.Null(); \
        v->ci = css?&v->_callsstack[css-1]:NULL; \
    }

struct AutoDec
{
    VXInteger *_n;
    AutoDec(VXInteger *n)
    {
        _n = n;
    }
    ~AutoDec()
    {
        (*_n)--;
    }
};


inline VXObject &stack_get(VXState* v,VXInteger idx)
{
    return v->StackGet(idx);
}


#endif                           //_VXState_H_
