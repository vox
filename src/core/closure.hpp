
#ifndef _VXCLOSURE_H_
#define _VXCLOSURE_H_

#include "pcheader.hpp"
#include "funcproto.hpp"
#include "object.hpp"
#include "state.hpp"

#define _CALC_CLOSURE_SIZE(func) \
    (sizeof(VXForeignClosureObj) + (func->_noutervalues*sizeof(VXObject)) + (func->_ndefaultparams*sizeof(VXObject)))

//struct VXFuncProtoObj;
//struct VXClassObj;

struct VXForeignClosureObj : public VXCollectable
{
    private:
        VXForeignClosureObj(VXSharedState *ss,VXFuncProtoObj *func)
        {
            _function = func;
            __ObjAddRef(_function);
            _base = NULL;
            INIT_CHAIN();
            ADD_TO_CHAIN(&_ss(this)->_gc_chain,this);
            _env = NULL;
        }

    public:
        static VXForeignClosureObj* Create(VXSharedState *ss,VXFuncProtoObj *func)
        {
            VXInteger size = _CALC_CLOSURE_SIZE(func);
            VXForeignClosureObj *nc=(VXForeignClosureObj*)VX_MALLOC(size);
            new (nc) VXForeignClosureObj(ss,func);
            nc->_outervalues = (VXObject *)(nc + 1);
            nc->_defaultparams = &nc->_outervalues[func->_noutervalues];
            VX_CONSTRUCT_VECTOR(VXObject,func->_noutervalues,nc->_outervalues);
            VX_CONSTRUCT_VECTOR(VXObject,func->_ndefaultparams,nc->_defaultparams);
            return nc;
        }
        void Release()
        {
            VXFuncProtoObj *f = _function;
            VXInteger size = _CALC_CLOSURE_SIZE(f);
            VX_DESTRUCT_VECTOR(VXObject,f->_noutervalues,_outervalues);
            VX_DESTRUCT_VECTOR(VXObject,f->_ndefaultparams,_defaultparams);
            __ObjRelease(_function);
            this->~VXForeignClosureObj();
            vox_mem_free(this,size);
        }

        VXForeignClosureObj *Clone()
        {
            VXFuncProtoObj *f = _function;
            VXForeignClosureObj * ret = VXForeignClosureObj::Create(_opt_ss(this),f);
            ret->_env = _env;
            if(ret->_env) __ObjAddRef(ret->_env);
            VX_COPY_VECTOR(ret->_outervalues,_outervalues,f->_noutervalues);
            VX_COPY_VECTOR(ret->_defaultparams,_defaultparams,f->_ndefaultparams);
            return ret;
        }
        ~VXForeignClosureObj();

        bool Save(VXState *v,VXUserPointer up,VXWriteFunc write);
        static bool Load(VXState *v,VXUserPointer up,VXReadFunc read,VXObject &ret);
    #ifndef NO_GARBAGE_COLLECTOR
        void Mark(VXCollectable **chain);
        void Finalize()
        {
            VXFuncProtoObj *f = _function;
            _NULL_VXOBJECT_VECTOR(_outervalues,f->_noutervalues);
            _NULL_VXOBJECT_VECTOR(_defaultparams,f->_ndefaultparams);
        }
        VXOType GetType() {return VX_OT_CLOSURE;}
    #endif
        VXWeakRefObj *_env;
        VXClassObj *_base;
        VXFuncProtoObj *_function;
        VXObject *_outervalues;
        VXObject *_defaultparams;
};

//////////////////////////////////////////////
struct VXOuterObj : public CHAINABLE_OBJ
{

    private:
        VXOuterObj(VXSharedState *ss, VXObject *outer){_valptr = outer; _next = NULL; INIT_CHAIN(); ADD_TO_CHAIN(&_ss(this)->_gc_chain,this); }

    public:
        static VXOuterObj *Create(VXSharedState *ss, VXObject *outer)
        {
            VXOuterObj *nc  = (VXOuterObj*)VX_MALLOC(sizeof(VXOuterObj));
            new (nc) VXOuterObj(ss, outer);
            return nc;
        }
        ~VXOuterObj() { REMOVE_FROM_CHAIN(&_ss(this)->_gc_chain,this); }

        void Release()
        {
            this->~VXOuterObj();
            vox_mem_free(this,sizeof(VXOuterObj));
        }

    #ifndef NO_GARBAGE_COLLECTOR
        void Mark(VXCollectable **chain);
        void Finalize() { _value.Null(); }
        VXOType GetType() {return VX_OT_OUTER;}
    #endif

        VXObject *_valptr;    /* pointer to value on stack, or _value below */
        VXInteger    _idx;       /* idx in stack array, for relocation */
        VXObject  _value;     /* value of outer after stack frame is closed */
        VXOuterObj     *_next;      /* pointer to next outer when frame is open   */
};

//////////////////////////////////////////////
struct VXGeneratorObj : public CHAINABLE_OBJ
{
    enum VXGeneratorObjState
    {
        eRunning,
        eSuspended,
        eDead
    };

    private:
        VXGeneratorObj(VXSharedState *ss,VXForeignClosureObj *closure)
        {
            _closure = closure;
            _state = eRunning;
            _ci._generator = NULL;
            INIT_CHAIN();
            ADD_TO_CHAIN(&_ss(this)->_gc_chain,this);
        }

    public:
        static VXGeneratorObj *Create(VXSharedState *ss,VXForeignClosureObj *closure)
        {
            VXGeneratorObj *nc=(VXGeneratorObj*)VX_MALLOC(sizeof(VXGeneratorObj));
            new (nc) VXGeneratorObj(ss,closure);
            return nc;
        }

        ~VXGeneratorObj()
        {
            REMOVE_FROM_CHAIN(&_ss(this)->_gc_chain,this);
        }

        void Kill()
        {
            _state=eDead;
            _stack.resize(0);
            _closure.Null();
        }

        void Release()
        {
            vox_delete(this,VXGeneratorObj);
        }

        bool Yield(VXState *v,VXInteger target);

        bool Resume(VXState *v,VXObject &dest);

        void Mark(VXCollectable **chain);

        void Finalize()
        {
            _stack.resize(0);
            _closure.Null();
        }

        VXOType GetType()
        {
            return VX_OT_GENERATOR;
        }

        VXObject _closure;
        VXObjectVec _stack;
        VXState::CallInfo _ci;
        ExceptionsTraps _etraps;
        VXGeneratorObjState _state;
};

#define _CALC_NATVIVECLOSURE_SIZE(noutervalues) (sizeof(VXNativeClosureObj) + (noutervalues*sizeof(VXObject)))

struct VXNativeClosureObj : public CHAINABLE_OBJ
{
    private:
        VXNativeClosureObj(VXSharedState *ss,VXFunction func)
        {
            _function=func;
            INIT_CHAIN();
            ADD_TO_CHAIN(&_ss(this)->_gc_chain,this);
            _env = NULL;
        }

    public:
        static VXNativeClosureObj* Create(
            VXSharedState *ss,
            VXFunction func,
            VXInteger nouters,
            const VXObject& name)
        {
            VXInteger size = _CALC_NATVIVECLOSURE_SIZE(nouters);
            VXNativeClosureObj *nc=(VXNativeClosureObj*)VX_MALLOC(size);
            new (nc) VXNativeClosureObj(ss,func);
            nc->_outervalues = (VXObject *)(nc + 1);
            nc->_noutervalues = nouters;
            nc->_name = name;
            VX_CONSTRUCT_VECTOR(VXObject,nc->_noutervalues,nc->_outervalues);
            return nc;
        }

        VXNativeClosureObj *Clone()
        {
            VXNativeClosureObj * ret = VXNativeClosureObj::Create(_opt_ss(this),_function,_noutervalues, _name);
            ret->_env = _env;
            if(ret->_env)
            {
                __ObjAddRef(ret->_env);
            }
            ret->_name = _name;
            VX_COPY_VECTOR(ret->_outervalues,_outervalues,_noutervalues);
            ret->_typecheck.copy(_typecheck);
            ret->_nparamscheck = _nparamscheck;
            return ret;
        }

        ~VXNativeClosureObj()
        {
            __ObjRelease(_env);
            REMOVE_FROM_CHAIN(&_ss(this)->_gc_chain,this);
        }

        void Release()
        {
            VXInteger size = _CALC_NATVIVECLOSURE_SIZE(_noutervalues);
            VX_DESTRUCT_VECTOR(VXObject,_noutervalues,_outervalues);
            this->~VXNativeClosureObj();
            vox_free(this,size);
        }

        void Mark(VXCollectable **chain);

        void Finalize()
        {
            _NULL_VXOBJECT_VECTOR(_outervalues,_noutervalues);
        }

        VXOType GetType()
        {
            return VX_OT_NATIVECLOSURE;
        }

        VXInteger _nparamscheck;
        VXIntVec _typecheck;
        VXObject *_outervalues;
        VXUnsignedInteger _noutervalues;
        VXWeakRefObj *_env;
        VXFunction _function;
        VXObject _name;
};
#endif                           //_VXCLOSURE_H_
