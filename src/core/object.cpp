
#include "pcheader.hpp"
#include "vm.hpp"
#include "string.hpp"
#include "table.hpp"
#include "userdata.hpp"
#include "funcproto.hpp"
#include "class.hpp"
#include "closure.hpp"




VXWeakRefObj *VXRefCountedObj::GetWeakRef(VXOType type)
{
    if(!_weakref)
    {
        vox_new(_weakref,VXWeakRefObj);
        _weakref->_obj._type = type;
        _weakref->_obj._unVal.pRefCounted = this;
    }
    return _weakref;
}

VXRefCountedObj::~VXRefCountedObj()
{
    if(_weakref)
    {
        _weakref->_obj._type = VX_OT_NULL;
        _weakref->_obj._unVal.pRefCounted = NULL;
    }
}

void VXWeakRefObj::Release()
{
    if(ISREFCOUNTED(_obj._type))
    {
        _obj._unVal.pRefCounted->_weakref = NULL;
    }
    vox_delete(this,VXWeakRefObj);
}

bool VXDelegableObj::GetMetaMethod(VXState *v,VXMetaMethod mm,VXObject &res)
{
    if(_delegate)
    {
        return _delegate->Get((*_ss(v)->_metamethods)[mm],res);
    }
    return false;
}

bool VXDelegableObj::SetDelegate(VXTableObj *mt)
{
    VXTableObj *temp = mt;
    if(temp == this)
    {
        return false;
    }
    while(temp)
    {
        // loop detected
        if(temp->_delegate == this)
        {
            return false;
        }
        temp = temp->_delegate;
    }
    if(mt)
    {
        __ObjAddRef(mt);
    }
    __ObjRelease(_delegate);
    this->_delegate = mt;
    return true;
}

bool VXGeneratorObj::Yield(VXState *v,VXInteger target)
{
    if(_state==eSuspended)
    {
        v->ThrowError("internal vm error, yielding dead generator");
        return false;
    }
    if(_state==eDead)
    {
        v->ThrowError("internal vm error, yielding a dead generator");
        return false;
    }
    VXInteger size = ((v->_top) - (v->_stackbase));
    _stack.resize(size);
    VXRawObj _this = v->_stack[v->_stackbase];
    _stack.values()[0] = (
        ISREFCOUNTED(type(_this)) ?
        VXObject(_refcounted(_this)->GetWeakRef(type(_this))) :
        _this);
    for(VXInteger n =1; n<target; n++)
    {
        _stack.values()[n] = v->_stack[v->_stackbase+n];
    }
    for(VXInteger j =0; j < size; j++)
    {
        v->_stack[v->_stackbase+j].Null();
    }

    _ci = *v->ci;
    _ci._generator=NULL;
    for(VXInteger i=0;i<_ci._etraps;i++)
    {
        _etraps.push_back(v->_etraps.top());
        v->_etraps.pop_back();
    }
    _state=eSuspended;
    return true;
}

bool VXGeneratorObj::Resume(VXState *v,VXObject &dest)
{
    if(_state==eDead)
    {
        v->ThrowError("resuming dead generator");
        return false;
    }
    if(_state==eRunning)
    {
        v->ThrowError("resuming active generator");
        return false;
    }
    VXInteger size = _stack.size();
    VXInteger target = &dest - &(v->_stack.values()[v->_stackbase]);
    vox_assert(target>=0 && target<=255);
    if(!v->EnterFrame(v->_top, v->_top + size, false))
    {
        return false;
    }
    v->ci->_generator   = this;
    v->ci->_target      = (VXInt32)target;
    v->ci->_closure     = _ci._closure;
    v->ci->_ip          = _ci._ip;
    v->ci->_literals    = _ci._literals;
    v->ci->_ncalls      = _ci._ncalls;
    v->ci->_etraps      = _ci._etraps;
    v->ci->_root        = _ci._root;
    for(VXInteger i=0;i<_ci._etraps;i++)
    {
        v->_etraps.push_back(_etraps.top());
        _etraps.pop_back();
    }
    VXRawObj _this = _stack.values()[0];
    v->_stack[v->_stackbase] = type(_this) == VX_OT_WEAKREF ? _weakref(_this)->_obj : _this;

    for(VXInteger n = 1; n<size; n++)
    {
        v->_stack[v->_stackbase+n] = _stack.values()[n];
        _stack.values()[n].Null();
    }

    _state=eRunning;
    if (v->_debughook)
    {
        v->CallDebugHook('c');
    }
    return true;
}


void VXInstanceObj::Mark(VXCollectable **chain)
{
    START_MARK()
        _class->Mark(chain);
    VXUnsignedInteger nvalues = _class->_defaultvalues.size();
    for(VXUnsignedInteger i =0; i< nvalues; i++)
    {
        VXSharedState::MarkObject(_values[i], chain);
    }
    END_MARK()
}

void VXGeneratorObj::Mark(VXCollectable **chain)
{
    START_MARK()
    for(VXUnsignedInteger i = 0; i < _stack.size(); i++)
    {
        VXSharedState::MarkObject(_stack[i], chain);
    }
    VXSharedState::MarkObject(_closure, chain);
    END_MARK()
}



void VXOuterObj::Mark(VXCollectable **chain)
{
    START_MARK()
    /* If the valptr points to a closed value, that value is alive */
    if(_valptr == &_value)
    {
        VXSharedState::MarkObject(_value, chain);
    }
    END_MARK()
}

void VXUserDataObj::Mark(VXCollectable **chain)
{
    START_MARK()
    if(_delegate)
    {
        _delegate->Mark(chain);
    }
    END_MARK()
}

void VXCollectable::UnMark()
{
    _uiRef&=~MARK_FLAG;
}



void VXObject::Null()
{
    __Release(_type ,_unVal);
    _type = VX_OT_NULL;
    _unVal.raw = (VXRawValue)NULL;
}

bool VXObject::IsNull() const
{
    return (_unVal.raw == (VXRawValue)NULL);
}

bool VXObject::IsNumeric() const
{
    return (Type() & VXOBJECT_NUMERIC);
}


/**
*    Object instance access methods
*/

VXInteger VXObject::Integer()
{
    return _unVal.nInteger;
}

VXInteger VXObject::Integer() const
{
    return _unVal.nInteger;
}

VXFloat VXObject::Float()
{
    return _unVal.fFloat;
}

VXFloat VXObject::Float() const
{
    return _unVal.fFloat;
}

VXStringObj* VXObject::String()
{
    return _unVal.pString;
}


VXStringObj* VXObject::String() const
{
    return _unVal.pString;
}

void VXObject::String(const char** dest, VXInteger* len_dest) const
{
    VXStringObj* ob = this->String();
    (*dest) = ob->_val;
    if(len_dest != NULL)
    {
        (*len_dest) = ob->_len;
    }
}

VXTableObj* VXObject::Table()
{
    return _unVal.pTable;
}

VXTableObj* VXObject::Table() const
{
    return _unVal.pTable;
}

VXArrayObj* VXObject::Array()
{
    return _unVal.pArray;
}

VXArrayObj* VXObject::Array() const
{
    return _unVal.pArray;
}
VXForeignClosureObj* VXObject::Closure()
{
    return _unVal.pClosure;
}

VXGeneratorObj* VXObject::Generator()
{
    return _unVal.pGenerator;
}

VXNativeClosureObj* VXObject::NativeClosure()
{
    return _unVal.pNativeClosure;
}

VXUserDataObj* VXObject::UserData()
{
    return _unVal.pUserData;
}

VXUserPointer VXObject::UserPointer()
{
    return _unVal.pUserPointer;
}

VXState* VXObject::Thread()
{
    return _unVal.pThread;
}

VXFuncProtoObj* VXObject::FuncProto()
{
    return _unVal.pFunctionProto;
}

VXClassObj* VXObject::Class()
{
    return _unVal.pClass;
}

VXClassObj* VXObject::Class() const
{
    return _unVal.pClass;
}

VXInstanceObj* VXObject::Instance()
{
    return _unVal.pInstance;
}

VXDelegableObj* VXObject::Delegable()
{
    return (VXDelegableObj*)(_unVal.pDelegable);
}

VXWeakRefObj* VXObject::WeakRef()
{
    return _unVal.pWeakRef;
}

VXOuterObj* VXObject::Outer()
{
    return _unVal.pOuter;
}

VXRefCountedObj* VXObject::RefCounted()
{
    return _unVal.pRefCounted;
}

VXRefCountedObj* VXObject::RefCounted() const
{
    return _unVal.pRefCounted;
}

VXRawValue VXObject::Raw()
{
    return _unVal.raw;
}


template<> VXFloat VXObject::As<VXFloat>()
{
    return Float();
}

template<> VXInteger VXObject::As<VXInteger>()
{
    return Integer();
}

/**
*   Type Information
*/

VXOType VXObject::Type() const
{
    return _type;
}

const char* VXObject::TypeString() const
{
    return IdType2Name(this->Type());
}

bool VXObject::Is(VXOType tp) const
{
    return Type() == tp;
}

bool VXObject::IsTable() const
{
    return Is(VX_OT_TABLE);
}

bool VXObject::IsArray() const
{
    return Is(VX_OT_ARRAY);
}

bool VXObject::IsFunction() const
{
    return Is(VX_OT_FUNCPROTO);
}
bool VXObject::IsClosure() const
{
    return Is(VX_OT_CLOSURE);
}

bool VXObject::IsNativeClosure() const
{
    return Is(VX_OT_NATIVECLOSURE);
}

bool VXObject::IsGenerator() const
{
    return Is(VX_OT_GENERATOR);
}

bool VXObject::IsString() const
{
    return Is(VX_OT_STRING);
}

bool VXObject::IsInteger() const
{
    return Is(VX_OT_INTEGER);
}

bool VXObject::IsFloat() const
{
    return Is(VX_OT_FLOAT);
}

bool VXObject::IsUserPointer() const
{
    return Is(VX_OT_USERPOINTER);
}

bool VXObject::IsUserData() const
{
    return Is(VX_OT_USERDATA);
}

bool VXObject::IsThread() const
{
    return Is(VX_OT_THREAD);
}

bool VXObject::IsClass() const
{
    return Is(VX_OT_CLASS);
}

bool VXObject::IsInstance() const
{
    return Is(VX_OT_INSTANCE);
}

bool VXObject::IsBool() const
{
    return Is(VX_OT_BOOL);
}

bool VXObject::IsWeakRef() const
{
    return Is(VX_OT_WEAKREF);
}

/**
*   Constructors
*/

VXObject::~VXObject()
{
    __Release(_type,_unVal);
}

VXObject::VXObject()
{
    VX_OBJECT_RAWINIT(this)
    _type=VX_OT_NULL;
    _unVal.pUserPointer=NULL;
}
VXObject::VXObject(const VXObject &o)
{
    _type=o._type;
    _unVal=o._unVal;
    VX_ADDREF(_type,_unVal);
}

VXObject::VXObject(const VXRawObj &o)
{
    _type=o._type;
    _unVal=o._unVal;
    VX_ADDREF(_type,_unVal);
}


VXObject::VXObject(bool bBool)
{
    VX_OBJECT_RAWINIT(this)
    _type = VX_OT_BOOL;
    _unVal.nInteger = bBool?1:0;
}

VXObject::VXObject (VXTableObj * x)
{
    VX_OBJECT_RAWINIT(this)
    _type = VX_OT_TABLE;
    _unVal.pTable = x;
    vox_assert (_unVal.pTable);
    _unVal.pRefCounted->_uiRef++;
}

VXObject::VXObject (VXClassObj * x)
{
    VX_OBJECT_RAWINIT(this)
    _type = VX_OT_CLASS;
    _unVal.pClass = x;
    vox_assert (_unVal.pTable);
    _unVal.pRefCounted->_uiRef++;
}

VXObject & VXObject::operator= (VXClassObj * x)
{
    VXOType tOldType;
    VXRawObj::Value unOldVal;
    tOldType = _type;
    unOldVal = _unVal;
    _type = VX_OT_CLASS;
    VX_REFOBJECT_INIT (this)
    _unVal.pClass = x;
    _unVal.pRefCounted->_uiRef++;
    __Release (tOldType, unOldVal);
    return *this;
}

VXObject::VXObject (VXInstanceObj * x)
{
    VX_OBJECT_RAWINIT(this)
    _type = VX_OT_INSTANCE;
    _unVal.pInstance = x;
    vox_assert (_unVal.pTable);
    _unVal.pRefCounted->_uiRef++;
}

VXObject::VXObject(VXArrayObj * x)
{
    VX_OBJECT_RAWINIT(this)
    _type = VX_OT_ARRAY;
    _unVal.pArray = x;
    vox_assert(_unVal.pTable);
    _unVal.pRefCounted->_uiRef++;
}



VXObject::VXObject (VXForeignClosureObj * x)
{
    VX_OBJECT_RAWINIT(this)
    _type = VX_OT_CLOSURE;
    _unVal.pClosure = x;
    vox_assert (_unVal.pTable);
    _unVal.pRefCounted->_uiRef++;
}



VXObject::VXObject (VXNativeClosureObj * x)
{
    VX_OBJECT_RAWINIT(this)
    _type = VX_OT_NATIVECLOSURE;
    _unVal.pNativeClosure = x;
    vox_assert (_unVal.pTable);
    _unVal.pRefCounted->_uiRef++;
}



VXObject::VXObject (VXOuterObj * x)
{
    VX_OBJECT_RAWINIT(this)
    _type = VX_OT_OUTER;
    _unVal.pOuter = x;
    vox_assert (_unVal.pTable);
    _unVal.pRefCounted->_uiRef++;
}


VXObject::VXObject (VXGeneratorObj * x)
{
    VX_OBJECT_RAWINIT(this)
    _type = VX_OT_GENERATOR;
    _unVal.pGenerator = x;
    vox_assert (_unVal.pTable);
    _unVal.pRefCounted->_uiRef++;
}

VXObject::VXObject (VXStringObj * x)
{
    VX_OBJECT_RAWINIT(this)
    _type = VX_OT_STRING;
    _unVal.pString = x;
    vox_assert (_unVal.pTable);
    _unVal.pRefCounted->_uiRef++;
}

VXObject::VXObject (VXUserDataObj * x)
{
    VX_OBJECT_RAWINIT(this)
    _type = VX_OT_USERDATA;
    _unVal.pUserData = x;
    vox_assert (_unVal.pTable);
    _unVal.pRefCounted->_uiRef++;
}


VXObject::VXObject (VXWeakRefObj * x)
{
    VX_OBJECT_RAWINIT(this)
    _type = VX_OT_WEAKREF;
    _unVal.pWeakRef = x;
    vox_assert (_unVal.pTable);
    _unVal.pRefCounted->_uiRef++;
}


VXObject::VXObject (VXState * x)
{
    VX_OBJECT_RAWINIT(this)
    _type = VX_OT_THREAD;
    _unVal.pThread = x;
    vox_assert (_unVal.pTable);
    _unVal.pRefCounted->_uiRef++;
}


VXObject::VXObject (VXFuncProtoObj * x)
{
    VX_OBJECT_RAWINIT(this)
    _unVal.pFunctionProto = x;
    _type = VX_OT_FUNCPROTO;
    vox_assert (_unVal.pTable);
    _unVal.pRefCounted->_uiRef++;
}

VXObject::VXObject(VXInteger x)
{
    VX_OBJECT_RAWINIT(this)
    _type = VX_OT_INTEGER;
    _unVal.nInteger = x;
}

VXObject::VXObject (VXFloat x)
{
    VX_OBJECT_RAWINIT(this)
    _type = VX_OT_FLOAT;
    _unVal.fFloat = x;
}

VXObject::VXObject (VXUserPointer x)
{
    VX_OBJECT_RAWINIT(this)
    _type = VX_OT_USERPOINTER;
    _unVal.pUserPointer = x;
}

/**
*    operator=() methods
*/

VXObject & VXObject::operator= (VXGeneratorObj * x)
{
    VXOType tOldType;
    VXRawObj::Value unOldVal;
    tOldType = _type;
    unOldVal = _unVal;
    _type = VX_OT_GENERATOR;
    VX_REFOBJECT_INIT (this)
    _unVal.pGenerator = x;
    _unVal.pRefCounted->_uiRef++;
    __Release (tOldType, unOldVal);
    return *this;
}

VXObject & VXObject::operator= (VXOuterObj * x)
{
    VXOType tOldType;
    VXRawObj::Value unOldVal;
    tOldType = _type;
    unOldVal = _unVal;
    _type = VX_OT_OUTER;
    VX_REFOBJECT_INIT (this)
    _unVal.pOuter = x;
    _unVal.pRefCounted->_uiRef++;
    __Release (tOldType, unOldVal);
    return *this;
}


VXObject & VXObject::operator= (VXNativeClosureObj * x)
{
    VXOType tOldType;
    VXRawObj::Value unOldVal;
    tOldType = _type;
    unOldVal = _unVal;
    _type = VX_OT_NATIVECLOSURE;
    VX_REFOBJECT_INIT (this)
    _unVal.pNativeClosure = x;
    _unVal.pRefCounted->_uiRef++;
    __Release (tOldType, unOldVal);
    return *this;
}

VXObject & VXObject::operator= (VXForeignClosureObj * x)
{
    VXOType tOldType;
    VXRawObj::Value unOldVal;
    tOldType = _type;
    unOldVal = _unVal;
    _type = VX_OT_CLOSURE;
    VX_REFOBJECT_INIT (this)
    _unVal.pClosure = x;
    _unVal.pRefCounted->_uiRef++;
    __Release (tOldType, unOldVal);
    return *this;
}

VXObject & VXObject::operator= (VXArrayObj * x)
{
    VXOType tOldType;
    VXRawObj::Value unOldVal;
    tOldType = _type;
    unOldVal = _unVal;
    _type = VX_OT_ARRAY;
    VX_REFOBJECT_INIT(this)
    _unVal.pArray = x;
    _unVal.pRefCounted->_uiRef++;
    __Release (tOldType, unOldVal);
    return *this;
}

VXObject & VXObject::operator= (VXInstanceObj * x)
{
    VXOType tOldType;
    VXRawObj::Value unOldVal;
    tOldType = _type;
    unOldVal = _unVal;
    _type = VX_OT_INSTANCE;
    VX_REFOBJECT_INIT (this)
    _unVal.pInstance = x;
    _unVal.pRefCounted->_uiRef++;
    __Release (tOldType, unOldVal);
    return *this;
}

VXObject & VXObject::operator= (VXTableObj * x)
{
    VXOType tOldType;
    VXRawObj::Value unOldVal;
    tOldType = _type;
    unOldVal = _unVal;
    _type = VX_OT_TABLE;
    VX_REFOBJECT_INIT(this)
    _unVal.pTable = x;
    _unVal.pRefCounted->_uiRef++;
    __Release (tOldType, unOldVal);
    return *this;
}

VXObject& VXObject::operator=(const VXObject& obj)
{
    VXOType tOldType;
    VXRawObj::Value unOldVal;
    tOldType=_type;
    unOldVal=_unVal;
    _unVal = obj._unVal;
    _type = obj._type;
    VX_ADDREF(_type,_unVal);
    __Release(tOldType,unOldVal);
    return *this;
}

VXObject& VXObject::operator=(const VXRawObj& obj)
{
    VXOType tOldType;
    VXRawObj::Value unOldVal;
    tOldType=_type;
    unOldVal=_unVal;
    _unVal = obj._unVal;
    _type = obj._type;
    VX_ADDREF(_type,_unVal);
    __Release(tOldType,unOldVal);
    return *this;
}

VXObject& VXObject::operator=(bool b)
{
    __Release(_type,_unVal);
    VX_OBJECT_RAWINIT(this)
    _type = VX_OT_BOOL;
    _unVal.nInteger = b?1:0;
    return *this;
}

VXObject & VXObject::operator= (VXStringObj * x)
{
    VXOType tOldType;
    VXRawObj::Value unOldVal;
    tOldType = _type;
    unOldVal = _unVal;
    _type = VX_OT_STRING;
    VX_REFOBJECT_INIT (this)
    _unVal.pString = x;
    _unVal.pRefCounted->_uiRef++;
    __Release (tOldType, unOldVal);
    return *this;
}

VXObject & VXObject::operator= (VXUserDataObj * x)
{
    VXOType tOldType;
    VXRawObj::Value unOldVal;
    tOldType = _type;
    unOldVal = _unVal;
    _type = VX_OT_USERDATA;
    VX_REFOBJECT_INIT (this)
    _unVal.pUserData = x;
    _unVal.pRefCounted->_uiRef++;
    __Release (tOldType, unOldVal);
    return *this;
}

VXObject & VXObject::operator= (VXWeakRefObj * x)
{
    VXOType tOldType;
    VXRawObj::Value unOldVal;
    tOldType = _type;
    unOldVal = _unVal;
    _type = VX_OT_WEAKREF;
    VX_REFOBJECT_INIT (this)
    _unVal.pWeakRef = x;
    _unVal.pRefCounted->_uiRef++;
    __Release (tOldType, unOldVal);
    return *this;
}

VXObject & VXObject::operator= (VXState * x)
{
    VXOType tOldType;
    VXRawObj::Value unOldVal;
    tOldType = _type;
    unOldVal = _unVal;
    _type = VX_OT_THREAD;
    VX_REFOBJECT_INIT (this)
    _unVal.pThread = x;
    _unVal.pRefCounted->_uiRef++;
    __Release (tOldType, unOldVal);
    return *this;
}

VXObject & VXObject::operator= (VXFuncProtoObj * x)
{
    VXOType tOldType;
    VXRawObj::Value unOldVal;
    tOldType = _type;
    unOldVal = _unVal;
    _type = VX_OT_FUNCPROTO;
    VX_REFOBJECT_INIT (this)
    _unVal.pFunctionProto = x;
    _unVal.pRefCounted->_uiRef++;
    __Release (tOldType, unOldVal);
    return *this;
}


VXObject & VXObject::operator= (VXFloat x)
{
    __Release (_type, _unVal);
    _type = VX_OT_FLOAT;
    VX_OBJECT_RAWINIT(this)
    _unVal.fFloat = x;
    return *this;
}

VXObject & VXObject::operator= (VXInteger x)
{
    __Release (_type, _unVal);
    _type = VX_OT_INTEGER;
    VX_OBJECT_RAWINIT(this)
    _unVal.nInteger = x;
    return *this;
}

VXObject & VXObject::operator= (VXUserPointer x)
{
    __Release (_type, _unVal);
    _type = VX_OT_USERPOINTER;
    VX_OBJECT_RAWINIT(this)
    _unVal.pUserPointer = x;
    return *this;
}


