
#include "pcheader.hpp"
#include "vm.hpp"
#include "table.hpp"
#include "funcproto.hpp"
#include "closure.hpp"

VXTableObj::VXTableObj(VXSharedState *ss,VXInteger nInitialSize)
{
    VXInteger pow2size = MINPOWER2;
    while(nInitialSize > pow2size)
    {
        pow2size = pow2size << 1;
    }
    AllocNodes(pow2size);
    _usednodes = 0;
    _delegate = NULL;
    INIT_CHAIN();
    ADD_TO_CHAIN(&_sharedstate->_gc_chain,this);
}

void VXTableObj::Remove(const VXObject &key)
{
    HashNode *n = GetNode(key, HashObj(key) & (_numofnodes - 1));
    if (n)
    {
        n->val.Null();
        n->key.Null();
        _usednodes--;
        Rehash(false);
    }
}

void VXTableObj::AllocNodes(VXInteger nSize)
{
    HashNode *nodes=(HashNode *)VX_MALLOC(sizeof(HashNode)*nSize);
    for(VXInteger i=0;i<nSize;i++)
    {
        HashNode &n = nodes[i];
        new (&n) HashNode;
        n.next=NULL;
    }
    _numofnodes=nSize;
    _nodes=nodes;
    _firstfree=&_nodes[_numofnodes-1];
}

void VXTableObj::Rehash(bool force)
{
    VXInteger oldsize=_numofnodes;
    //prevent problems with the integer division
    if(oldsize<4)oldsize=4;
    HashNode *nold=_nodes;
    VXInteger nelems=CountUsed();

    /* using more than 3/4? */
    if (nelems >= oldsize-oldsize/4)
    {
        AllocNodes(oldsize*2);
    }

    /* less than 1/4? */
    else if (nelems <= oldsize/4 && oldsize > MINPOWER2)
    {
        AllocNodes(oldsize/2);
    }

    else if(force)
        AllocNodes(oldsize);
    else
        return;
    _usednodes = 0;
    for (VXInteger i=0; i<oldsize; i++)
    {
        HashNode *old = nold+i;
        if (type(old->key) != VX_OT_NULL)
            NewSlot(old->key,old->val);
    }
    for(VXInteger k=0;k<oldsize;k++)
        nold[k].~HashNode();
    VX_FREE(nold,oldsize*sizeof(HashNode));
}


/* don't define _FAST_CLONE; may occasionally fail (assertion errors)
   YOU HAVE BEEN WARNED!
*/
VXTableObj *VXTableObj::Clone()
{
    VXTableObj *nt=Create(_opt_ss(this),_numofnodes);
#ifdef _FAST_CLONE
    HashNode *basesrc = _nodes;
    HashNode *basedst = nt->_nodes;
    HashNode *src = _nodes;
    HashNode *dst = nt->_nodes;
    VXInteger n = 0;
    for(n=0; n<_numofnodes; n++)
    {
        dst->key = src->key;
        dst->val = src->val;
        if(src->next)
        {
            vox_assert(src->next > basesrc);
            dst->next = basedst + (src->next - basesrc);
            vox_assert(dst != dst->next);
        }
        dst++;
        src++;
    }
    vox_assert(_firstfree > basesrc);
    vox_assert(_firstfree != NULL);
    nt->_firstfree = basedst + (_firstfree - basesrc);
#else
    VXInteger ridx=0;
    VXObject key,val;
    while((ridx=Next(true,ridx,key,val))!=-1)
    {
        nt->NewSlot(key,val);
    }
#endif
    nt->SetDelegate(_delegate);
    return nt;
}

bool VXTableObj::Get(const VXObject &key,VXObject &val)
{
    HashNode* n;
    if(key.Type() == VX_OT_NULL)
    {
        return false;
    }
    n = GetNode(key, HashObj(key) & (_numofnodes - 1));
    if (n)
    {
        val = VX_REALVAL(n->val);
        return true;
    }
    return false;
}
bool VXTableObj::NewSlot(const VXObject &key,const VXObject &val)
{
    VXHash h;
    HashNode* n;
    vox_assert(key.Type() != VX_OT_NULL);
    h = HashObj(key) & (_numofnodes - 1);
    n = GetNode(key, h);
    if(n)
    {
        n->val = val;
        return true;
    }
    HashNode *mp = &_nodes[h];
    n = mp;
    //key not found I'll insert it
    //main pos is not free
    if(mp->key.Type() != VX_OT_NULL)
    {
        /* get a free place */
        n = _firstfree;
        VXHash mph = HashObj(mp->key) & (_numofnodes - 1);
        /* main position of colliding node */
        HashNode *othern;
        if((mp > n) && ((othern = &_nodes[mph]) != mp))
        {
            /* yes; move colliding node into free position */
            while(othern->next != mp)
            {
                vox_assert(othern->next != NULL);
                /* find previous */
                othern = othern->next;
            }
            /* redo the chain with `n' in place of `mp' */
            othern->next = n;
            n->key = mp->key;
            /* copy colliding node into free pos. (mp->next also goes) */
            n->val = mp->val;
            n->next = mp->next;
            mp->key.Null();
            mp->val.Null();
            /* now `mp' is free */
            mp->next = NULL;
        }
        else
        {
            /* new node will go into free position */
            /* chain new position */
            n->next = mp->next;
            mp->next = n;
            mp = n;
        }
    }
    mp->key = key;
    /* correct `firstfree' */
    for (;;)
    {
        if ((_firstfree->key.Type() == VX_OT_NULL) && (_firstfree->next == NULL))
        {
            mp->val = val;
            _usednodes++;
            /* OK; table still has a free place */
            return true;
        }
        else if(_firstfree == _nodes)
        {
            /* cannot decrement from here */
            break;
        }
        else
        {
            (_firstfree)--;
        }
    }
    Rehash(true);
    return NewSlot(key, val);
}


bool VXTableObj::Set(const VXObject &key, const VXObject &val)
{
    // technically pointless to check here, since NewSlot already
    // checks...
    /*
    HashNode *n = GetNode(key, HashObj(key) & (_numofnodes - 1));
    if(n)
    {
        n->val = val;
        return true;
    }
    */
    return NewSlot(key, val);
}


VXInteger VXTableObj::Next(bool getweakrefs,
                           const VXObject &refpos,
                           VXObject &outkey,
                           VXObject &outval)
{
    VXInteger idx = (VXInteger)TranslateIndex(refpos);
    while(idx < _numofnodes)
    {
        HashNode &n = _nodes[idx];
        if(type(n.key) != VX_OT_NULL)
        {
            //first found
            outkey = n.key;
            outval = getweakrefs?(VXRawObj)n.val:VX_REALVAL(n.val);
            //return idx for the next iteration
            return ++idx;
        }

        ++idx;
    }
    //nothing to iterate anymore
    return -1;
}


void VXTableObj::ClearNodes()
{
    VXInteger i;
    for(i=0; i<_numofnodes; i++)
    {
        HashNode &n = _nodes[i];
        n.key.Null();
        n.val.Null();
    }
}

void VXTableObj::Finalize()
{
    ClearNodes();
    SetDelegate(NULL);
}

VXInteger VXTableObj::Size() const
{
    return _usednodes;

}

void VXTableObj::Clear()
{
    ClearNodes();
    _usednodes = 0;
    Rehash(true);
}


VXTableObj* VXTableObj::Create(VXSharedState *ss,VXInteger nInitialSize)
{
    VXTableObj *newtable = (VXTableObj*)VX_MALLOC(sizeof(VXTableObj));
    new (newtable) VXTableObj(ss, nInitialSize);
    newtable->_delegate = NULL;
    return newtable;
}

VXTableObj::~VXTableObj()
{
    VXInteger i;
    SetDelegate(NULL);
    REMOVE_FROM_CHAIN(&_sharedstate->_gc_chain, this);
    for(i=0; i<_numofnodes; i++)
    {
        _nodes[i].~HashNode();
    }
    VX_FREE(_nodes, _numofnodes * sizeof(HashNode));
}


VXOType VXTableObj::GetType()
{
    return VX_OT_TABLE;
}

VXTableObj::HashNode* VXTableObj::GetNode(const VXObject &key,VXHash hash)
{
    VXTableObj::HashNode* n = &_nodes[hash];
    do
    {
        if(_rawval(n->key) == _rawval(key) && (n->key.Type() == key.Type()))
        {
            return n;
        }
    } while((n = n->next));
    return NULL;
}


VXInteger VXTableObj::CountUsed()
{
    return _usednodes;
}

void VXTableObj::Release()
{
    vox_delete(this, VXTableObj);
}

void VXTableObj::Mark(VXCollectable **chain)
{
    START_MARK()
    if(_delegate)
    {
        _delegate->Mark(chain);
    }
    VXInteger len = _numofnodes;
    for(VXInteger i = 0; i < len; i++)
    {
        VXSharedState::MarkObject(_nodes[i].key, chain);
        VXSharedState::MarkObject(_nodes[i].val, chain);
    }
    END_MARK()
}


#define get_object(keyvar, destvar, expected_type) \
    if(this->Get(keyvar, destvar) == false) \
    { \
        return VX_ERROR; \
    } \
    if(destvar.Type() != expected_type) \
    { \
        return VX_ERROR; \
    } \

VXInteger VXTableObj::GetBoolValue(const VXObject& keyname, bool* destination)
{
    VXObject ob;
    get_object(keyname, ob, VX_OT_BOOL)
    (*destination) = ob.Boolean();
    return VX_OK;
}

VXInteger VXTableObj::GetStringValue(const VXObject& keyname, VXStringObj** dest)
{
    VXObject ob;
    get_object(keyname, ob, VX_OT_STRING)
    (*dest) = ob.String();
    return VX_OK;
}

VXInteger VXTableObj::GetStringValue(const VXObject& keyname, const char** dest_str, VXInteger* dest_len)
{
    VXObject ob;
    get_object(keyname, ob, VX_OT_STRING)
    ob.String()->Get(dest_str, dest_len);
    return VX_OK;
}

VXInteger VXTableObj::GetIntegerValue(const VXObject& keyname, VXInteger* dest)
{
    VXObject ob;
    get_object(keyname, ob, VX_OT_INTEGER)
    (*dest) = ob.Integer();
    return VX_OK;
}

VXInteger VXTableObj::GetFloatValue(const VXObject& keyname, VXFloat* dest)
{
    VXObject ob;
    get_object(keyname, ob, VX_OT_FLOAT)
    (*dest) = ob.Float();
    return VX_OK;
}

VXInteger VXTableObj::GetArrayValue(const VXObject& keyname, VXArrayObj** dest)
{
    VXObject ob;
    get_object(keyname, ob, VX_OT_ARRAY)
    (*dest) = ob.Array();
    return VX_OK;
}

VXInteger VXTableObj::GetTableValue(const VXObject& keyname, VXTableObj** dest)
{
    VXObject ob;
    get_object(keyname, ob, VX_OT_TABLE)
    (*dest) = ob.Table();
    return VX_OK;
}
