
#ifndef _VXFUNCSTATE_H_
#define _VXFUNCSTATE_H_

#include "utils.hpp"

struct VXFuncState
{
    VXFuncState(VXSharedState *ss,VXFuncState *parent,CompilerErrorFunc efunc,void *ed);
    ~VXFuncState();
    #ifdef _DEBUG_DUMP
    void Dump(VXFuncProtoObj *func);
    #endif
    void Error(const char *err);
    VXFuncState *PushChildState(VXSharedState *ss);
    void PopChildState();
    void AddInstruction(VXOpcode _op,VXInteger arg0=0,VXInteger arg1=0,VXInteger arg2=0,VXInteger arg3=0)
    {
        VXInstruction i(_op,arg0,arg1,arg2,arg3);
        AddInstruction(i);
    }
    void AddInstruction(VXInstruction &i);
    void SetIntructionParams(VXInteger pos,VXInteger arg0,VXInteger arg1,VXInteger arg2=0,VXInteger arg3=0);
    void SetIntructionParam(VXInteger pos,VXInteger arg,VXInteger val);
    VXInstruction &GetInstruction(VXInteger pos){return _instructions[pos];}
    void PopInstructions(VXInteger size){for(VXInteger i=0;i<size;i++)_instructions.pop_back();}
    void SetStackSize(VXInteger n);
    VXInteger CountOuters(VXInteger stacksize);
    void SnoozeOpt(){_optimization=false;}
    void AddDefaultParam(VXInteger trg) { _defaultparams.push_back(trg); }
    VXInteger GetDefaultParamCount() { return _defaultparams.size(); }
    VXInteger GetCurrentPos(){return _instructions.size()-1;}
    VXInteger GetNumericConstant(const VXInteger cons);
    VXInteger GetNumericConstant(const VXFloat cons);
    VXInteger PushLocalVariable(const VXRawObj &name);
    void AddParameter(const VXRawObj &name);
    //void AddOuterValue(const VXRawObj &name);
    VXInteger GetLocalVariable(const VXRawObj &name);
    void MarkLocalAsOuter(VXInteger pos);
    VXInteger GetOuterVariable(const VXRawObj &name);
    VXInteger GenerateCode();
    VXInteger GetStackSize();
    VXInteger CalcStackFrameSize();
    void AddLineInfos(VXInteger line,bool lineop,bool force=false);
    VXFuncProtoObj *BuildProto();
    VXInteger AllocStackPos();
    VXInteger PushTarget(VXInteger n=-1);
    VXInteger PopTarget();
    VXInteger TopTarget();
    VXInteger GetUpTarget(VXInteger n);
    void DiscardTarget();
    bool IsLocal(VXUnsignedInteger stkpos);
    VXRawObj CreateString(const char *s,VXInteger len = -1);
    VXRawObj CreateTable();
    bool IsConstant(const VXRawObj &name,VXRawObj &e);
    VXInteger _returnexp;
    VXLocalVarInfoVec _vlocals;
    VXIntVec _targetstack;
    VXInteger _stacksize;
    bool _varparams;
    bool _bgenerator;
    VXIntVec _unresolvedbreaks;
    VXIntVec _unresolvedcontinues;
    VXObjectVec _functions;
    VXObjectVec _parameters;
    VXOuterObjVarVec _outervalues;
    VXInstructionVec _instructions;
    VXLocalVarInfoVec _localvarinfos;
    VXObject _literals;
    VXObject _strings;
    VXObject _name;
    VXObject _sourcename;
    VXInteger _nliterals;
    VXLineInfoVec _lineinfos;
    VXFuncState *_parent;
    VXIntVec _scope_blocks;
    VXIntVec _breaktargets;
    VXIntVec _continuetargets;
    VXIntVec _defaultparams;
    VXInteger _lastline;
    VXInteger _traps;            //contains number of nested exception traps
    VXInteger _outers;
    bool _optimization;
    VXSharedState *_sharedstate;
    VXVector<VXFuncState*> _childstates;
    VXInteger GetConstant(const VXRawObj &cons);
    private:
        CompilerErrorFunc _errfunc;
        void *_errtarget;
        VXSharedState *_ss;
};
#endif                           //_VXFUNCSTATE_H_
