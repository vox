
#ifndef _VXTABLE_H_
#define _VXTABLE_H_
/*
 * The following code is based on Lua 4.0 (Copyright 1994-2002 Tecgraf, PUC-Rio.)
 * http://www.lua.org/copyright.html#4
 * http://www.lua.org/source/4.0.1/src_ltable.c.html
 */

#include "string.hpp"

//#define hashptr(p)  ((VXHash)(((VXInteger)p) >> 3))
#define hashptr(p) (((*reinterpret_cast<VXInteger*>(p)) >> 3))

inline VXHash HashObj(const VXObject &key)
{
    switch(type(key))
    {
        case VX_OT_STRING:
            return _string(key)->_hash;
        case VX_OT_FLOAT:
            return (VXHash)((VXInteger)_float(key));
        case VX_OT_BOOL:
        case VX_OT_INTEGER:
            return (VXHash)((VXInteger)_integer(key));
        default:
            return hashptr(key._unVal.pRefCounted);
    }
}


#endif //_VXTABLE_H_
