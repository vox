
#include "baselib.hpp"

/////////////////////////////////////////////////////////////////
//TABLE DEFAULT DELEGATE

VXInteger table_rawdelete(VXState* v)
{
    //if(VX_FAILED(vox_rawdeleteslot(v,1,true)))
    //{
    //    return VX_ERROR;
    //}
    //return 1;
    (void)v;
    return 0;
}

VXInteger table_setdelegate(VXState* v)
{
    VXObject& self = v->StackGet(1);
    VXObject& mt = v->StackGet(2);
    if(VX_SUCCEEDED(v->SetDelegate(self, mt)))
    {
        v->Push(self);
        return 1;
    }
    return VX_ERROR;
}

VXInteger table_getdelegate(VXState* v)
{
    //return VX_SUCCEEDED(vox_getdelegate(v,-1))?1:VX_ERROR;
    (void)v;
    return 0;
}

VXInteger table_get(VXState* v)
{
    VXObject dest;
    VXObject key;
    VXObject self = v->StackGet(1);
    VXTableObj* tb = self.Table();
    key = v->StackGet(2);
    if(tb->Get(key, dest))
    {
        v->Push(dest);
        return 1;
    }
    //return VX_ERROR;
    return v->Raise_IdxError(key, tb);
}

VXInteger table_each(VXState* v)
{
    VXTableObj* self = v->StackGet(1).Table();
    VXObject& closure = v->StackGet(2);
    VXInteger ridx;
    VXObject key;
    VXObject val;
    while((ridx = self->Next(true, ridx, key, val)) != -1)
    {
        v->PushRoot();
        v->Push(key);
        v->Push(val);


        //v->Push(closure);
        //v->Repush(-2);
        //if(VX_FAILED(v->CallSimple(3, false, true)))
        if(VX_FAILED(v->CallSimple(closure, 3, false, true)))
        {
            return VX_ERROR;
        }
    }
    return VX_OK;
}

VXRegFunction VXSharedState::_table_default_delegate_funcz[]=
{
    {"len",         default_delegate_len,      1, "t"},
    {"tostring",    default_delegate_tostring, -1, ".b"},
    {"weakref",     obj_delegate_weakref,      1, NULL},
    {"clear",       obj_clear,                 1, "."},
    {"rawget",      container_rawget,          2, "t"},
    {"rawset",      container_rawset,          3, "t"},
    {"rawdelete",   table_rawdelete,           2, "t"},
    {"each",        table_each,                2, ".c"},
    {"get",         table_get,                 2, NULL},
    {"setdelegate", table_setdelegate,         2, ".t|o"},
    {"getdelegate", table_getdelegate,         1, "."},
    {"repr",        default_delegate_repr,     1,  "."},
    {0, 0, 0, 0}
};

