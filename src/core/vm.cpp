
#include <math.h>
#include <stdlib.h>

#include "pcheader.hpp"
#include "opcodes.hpp"
#include "vm.hpp"
#include "funcproto.hpp"
#include "closure.hpp"
#include "string.hpp"
#include "table.hpp"
#include "userdata.hpp"
#include "class.hpp"
#include "names.hpp"
#include "compiler.hpp"
#include "debug.hpp"

#define TOP() (_stack.values()[_top-1])

#define arg0 (_i_._arg0)
#define sarg0 ((VXInteger)*((signed char *)&_i_._arg0))
#define arg1 (_i_._arg1)
#define sarg1 (*((VXInteger*)&_i_._arg1))
#define arg2 (_i_._arg2)
#define arg3 (_i_._arg3)
#define sarg3 ((VXInteger)*((signed char *)&_i_._arg3))

#define _ARITH_(op,trg,o1,o2) \
{ \
    VXInteger tmask = type(o1)|type(o2); \
    switch(tmask) \
    { \
        case VX_OT_INTEGER: trg = _integer(o1) op _integer(o2);break; \
        case (VX_OT_FLOAT|VX_OT_INTEGER): \
        case (VX_OT_FLOAT): trg = tofloat(o1) op tofloat(o2); break;\
        default: _GUARD(ARITH_OP(this, (#op)[0],trg,o1,o2)); break;\
    } \
}

#define _ARITH_NOZERO(op,trg,o1,o2,err) \
{ \
    VXInteger tmask = type(o1)|type(o2); \
    switch(tmask) { \
        case VX_OT_INTEGER: \
            { \
                VXInteger i2 = _integer(o2); \
                if(i2 == 0) \
                { \
                    ThrowError(err); \
                    VX_THROW(); \
                } \
                trg = _integer(o1) op i2; \
            } \
            break;\
        case (VX_OT_FLOAT|VX_OT_INTEGER): \
        case (VX_OT_FLOAT): \
            trg = tofloat(o1) op tofloat(o2); \
            break;\
        default: \
            _GUARD(ARITH_OP(this, (#op)[0],trg,o1,o2)); \
            break;\
    } \
}

#define COND_LITERAL (arg3!=0?ci->_literals[arg1]:STK(arg1))

#define VX_THROW() \
    { \
        goto exception_trap; \
    }

#define _GUARD(exp) \
    { \
        if(!(exp)) \
        { \
            VX_THROW(); \
        } \
    }

#define _RET_SUCCEED(exp) \
    { \
        result = (exp); \
        return true; \
    }

#define FALLBACK_OK         0
#define FALLBACK_NO_MATCH   1
#define FALLBACK_ERROR      2



#undef _GETSAFE_OBJ

#define _GETSAFE_OBJ(v,idx,type,o) \
    { \
        if(VX_FAILED(v->GetTypedArg(v,idx,type,&o))) \
        { \
            return VX_ERROR; \
        } \
    }


#define _GETSAFE_OBJ_REF(v, idx, type, o) \
    { \
        if(VX_FAILED(v->GetTypedArg(idx, type, o))) \
        { \
            return VX_ERROR; \
        } \
    }

void vox_defaultprintfunc(VXState* v,const char *s,...)
{
    (void)v;
    va_list vl;
    va_start(vl, s);
    vfprintf(stdout, s, vl);
    va_end(vl);
}

void vox_defaulterrorfunc(VXState* v,const char *s,...)
{
    (void)v;
    va_list vl;
    va_start(vl, s);
    vfprintf(stderr, s, vl);
    va_end(vl);
}



template<typename Type, typename ClosureType>
Type WrongNumberOfArguments(VXState* vm,
                            Type retval,
                            bool isnative,
                            ClosureType func)
{
    const char* funcname;
    const char* prefix = isnative ? " native" : "";
    if(func->_name.Type() == VX_OT_STRING)
    {
        funcname = func->_name.String()->Value();
    }
    else
    {
        funcname = NAMES_ANONFUNC;
    }
    vm->ThrowError("wrong number of parameters to%s function '%s'", prefix, funcname);
    return retval;
}

VXUnsignedInteger TranslateIndex(const VXObject &idx)
{
    switch(type(idx))
    {
        case VX_OT_NULL:
            return 0;
        case VX_OT_INTEGER:
            return (VXUnsignedInteger)_integer(idx);
        default: vox_assert(0); break;
    }
    return 0;
}

const char *IdType2Name(VXOType type)
{
    switch(VX_RAW_TYPE(type))
    {
        case VX_RT_NULL:          return "null";
        case VX_RT_INTEGER:       return "integer";
        case VX_RT_FLOAT:         return "float";
        case VX_RT_BOOL:          return "bool";
        case VX_RT_STRING:        return "string";
        case VX_RT_TABLE:         return "table";
        case VX_RT_ARRAY:         return "array";
        case VX_RT_GENERATOR:     return "generator";
        case VX_RT_CLOSURE:
        case VX_RT_NATIVECLOSURE: return "function";
        case VX_RT_USERDATA:
        case VX_RT_USERPOINTER:   return "userdata";
        case VX_RT_THREAD:        return "thread";
        case VX_RT_FUNCPROTO:     return "function";
        case VX_RT_CLASS:         return "class";
        case VX_RT_INSTANCE:      return "instance";
        case VX_RT_WEAKREF:       return "weakref";
        case VX_RT_OUTER:         return "outer";
    }
    return "<unknown>";
}

const char *GetTypeName(const VXObject &obj1)
{
    return IdType2Name(type(obj1));
}


VXInteger VXState::GetState()
{
    if(this->_suspended)
    {
        return VX_VMSTATE_SUSPENDED;
    }
    if(this->_callsstacksize != 0)
    {
        return VX_VMSTATE_RUNNING;
    }
    return VX_VMSTATE_IDLE;
}

void VXState::PrintError()
{
    vox_aux_printerror(this);
}

VXInteger VXState::WakeUp(bool wakeupret,bool retval,bool raiseerror,bool throwerror)
{
    VXObject ret;
    if(!this->_suspended)
    {
        return this->ThrowError("cannot resume a vm that is not running any code");
    }
    VXInteger target = this->_suspended_target;
    if(wakeupret)
    {
        if(target != -1)
        {
                                 //retval
            this->GetAt(this->_stackbase + this->_suspended_target) = this->GetUp(-1);
        }
        this->Pop();
    }
    else if(target != -1)
    {
        this->GetAt(this->_stackbase + this->_suspended_target).Null();
    }
    VXObject dummy;
    VXState::ExecutionType extype;
    extype = throwerror ? VXState::ET_RESUME_THROW_VM : VXState::ET_RESUME_VM;
    if(!this->Execute(dummy, -1, -1, ret, raiseerror, extype))
    {
        return VX_ERROR;
    }
    if(retval)
    {
        this->Push(ret);
    }
    return VX_OK;
}

VXInteger VXState::SetDelegate(VXObject& self, const VXObject& mt)
{
    VXOType type = self.Type();
    switch(type)
    {
        case VX_OT_TABLE:
            if(mt.Type() == VX_OT_TABLE)
            {
                if(!self.Table()->SetDelegate(_table(mt)))
                {
                    return ThrowError("delegate cycle");
                }
            }
            else if(mt.Type() == VX_OT_NULL)
            {
                self.Table()->SetDelegate(NULL);
            }
            else
            {
                return vox_aux_invalidtype(this, type);
            }
            break;
        case VX_OT_USERDATA:
            if(mt.Type() == VX_OT_TABLE)
            {
                _userdata(self)->SetDelegate(_table(mt));
            }
            else if(type(mt)==VX_OT_NULL)
            {
                _userdata(self)->SetDelegate(NULL);
            }
            else return vox_aux_invalidtype(this, type);
            break;
        default:
            return vox_aux_invalidtype(this, type);
            break;
    }
    return VX_OK;
}

bool SafeWrite(VXState* v,
               VXWriteFunc write,
               VXUserPointer up,
               VXUserPointer dest,
               VXInteger size)
{
    if(write(up,dest,size) != size)
    {
        v->ThrowError("io error (write function failure)");
        return false;
    }
    return true;
}

bool SafeRead(VXState* v,VXWriteFunc read,VXUserPointer up,VXUserPointer dest,VXInteger size)
{
    if(size && read(up,dest,size) != size)
    {
        v->ThrowError("io error, read function failure, the origin stream could be corrupted/trucated");
        return false;
    }
    return true;
}

bool WriteTag(VXState* v,VXWriteFunc write,VXUserPointer up,VXUnsignedInteger32 tag)
{
    return SafeWrite(v,write,up,&tag,sizeof(tag));
}

bool CheckTag(VXState* v,VXWriteFunc read,VXUserPointer up,VXUnsignedInteger32 tag)
{
    VXUnsignedInteger32 t;
    _CHECK_IO(SafeRead(v,read,up,&t,sizeof(t)));
    if(t != tag)
    {
        v->ThrowError("invalid or corrupted closure stream");
        return false;
    }
    return true;
}

bool WriteObject(VXState* v,VXUserPointer up,VXWriteFunc write,VXObject &o)
{
    VXUnsignedInteger32 _type = (VXUnsignedInteger32)type(o);
    _CHECK_IO(SafeWrite(v, write, up, &_type,sizeof(_type)));
    switch(type(o))
    {
        case VX_OT_STRING:
            _CHECK_IO(SafeWrite(v,write,up,&_string(o)->_len,sizeof(VXInteger)));
            _CHECK_IO(SafeWrite(v,
                write, up,
                VXUserPointer(o.String()->Value()), o.String()->Length()));
            break;
        case VX_OT_INTEGER:
            _CHECK_IO(SafeWrite(v,write,up,&_integer(o),sizeof(VXInteger)));break;
        case VX_OT_FLOAT:
            _CHECK_IO(SafeWrite(v,write,up,&_float(o),sizeof(VXFloat)));break;
        case VX_OT_NULL:
            break;
        default:
            v->ThrowError("cannot serialize a %s",GetTypeName(o));
            return false;
    }
    return true;
}

bool ReadObject(VXState* v,VXUserPointer up,VXReadFunc read,VXObject &o)
{
    VXUnsignedInteger32 _type;
    _CHECK_IO(SafeRead(v,read,up,&_type,sizeof(_type)));
    VXOType t = (VXOType)_type;
    switch(t)
    {
        case VX_OT_STRING:
        {
            VXInteger len;
            _CHECK_IO(SafeRead(v,read,up,&len,sizeof(VXInteger)));
            _CHECK_IO(SafeRead(v,read,up,v->ScratchPad(len),len));
            o = v->NewString(v->ScratchPad(-1), len);
        }
        break;
        case VX_OT_INTEGER:
        {
            VXInteger i;
            _CHECK_IO(SafeRead(v,read,up,&i,sizeof(VXInteger))); o = i; break;
        }
        case VX_OT_FLOAT:
        {
            VXFloat f;
            _CHECK_IO(SafeRead(v,read,up,&f,sizeof(VXFloat))); o = f; break;
        }
        case VX_OT_NULL:
            o.Null();
            break;
        default:
            v->ThrowError("cannot serialize a %s",IdType2Name(t));
            return false;
    }
    return true;
}



char* VXState::ScratchPad(VXInteger size)
{
    return Shared()->GetScratchPad(size);
}

void VXState::SetReleaseHook(VXInteger idx, VXReleaseHook hook)
{
    if(GetTop() >= 1)
    {
        VXObject& ud = StackGet(idx);
        switch(type(ud))
        {
            case VX_OT_USERDATA:
                _userdata(ud)->_hook = hook;
                break;
            case VX_OT_INSTANCE:
                _instance(ud)->_hook = hook;
                break;
            case VX_OT_CLASS:
                _class(ud)->_hook = hook;
                break;
            default:
                break;
        }
    }
}

VXInteger VXState::SetTypeTag(VXObject& o, VXUserPointer typetag)
{
    switch(o.Type())
    {
        case VX_OT_USERDATA:
            _userdata(o)->_typetag = typetag;
            break;
        case VX_OT_CLASS:
            _class(o)->_typetag = typetag;
            break;
        default:
            return ThrowError("invalid object type");
    }
    return VX_OK;
}

VXInteger VXState::GetInstanceUp(VXInteger idx, VXUserPointer *p,
                                 VXUserPointer typetag,
                                 bool previously_checked_base)
{
    (void)previously_checked_base;
    VXObject& o = StackGet(idx);
    if(o.Type() != VX_OT_INSTANCE)
    {
        return ThrowError("the object is not a class instance");
    }
    (*p) = _instance(o)->_userpointer;
    if(typetag != 0)
    {
        VXClassObj *cl = _instance(o)->_class;
        do
        {
            if(cl->_typetag == typetag)
            {
                return VX_OK;
            }
            cl = cl->_base;
        } while(cl != NULL);
        return this->ThrowError("invalid type tag");
    }
    return VX_OK;
}


VXInteger VXState::SetInstanceUp(VXInteger idx, VXUserPointer* p)
{
    VXObject& o = StackGet(idx);
    if(type(o) != VX_OT_INSTANCE)
    {
        return ThrowError("the object is not a class instance");
    }
    _instance(o)->_userpointer = (*p);
    Push(o);
    return VX_OK;
}


void VXState::PushString(const char* str, VXInteger len)
{
    this->Push(this->NewString(str, len));
}

void VXState::PushInteger(VXInteger itm)
{
    this->Push(VXObject(itm));
}

void VXState::PushFloat(VXFloat itm)
{
    this->Push(VXObject(itm));
}

void VXState::SetTop(VXInteger newtop)
{
    VXInteger top = this->GetTop();
    if(top > newtop)
    {
        this->Pop(top - newtop);
    }
    else
    {
        while(top++ < newtop)
        {
            this->PushNull();
        }
    }
}

VXInteger VXState::GetTop()
{
    return ((this->_top) - (this->_stackbase));
}

VXInteger VXState::WriteClosure(VXWriteFunc w, VXUserPointer up)
{
    VXObject o;
    _GETSAFE_OBJ_REF(this, -1, VX_OT_CLOSURE,o);
    unsigned short tag = VX_BYTECODE_STREAM_TAG;
    if(w(up, &tag, 2) != 2)
    {
        return this->ThrowError("io error");
    }
    if(!_closure(o)->Save(this, up, w))
    {
        return VX_ERROR;
    }
    return VX_OK;
}


VXInteger VXState::WriteClosureToFile(const char *filename)
{
    FILE* file = fopen(filename, "wb+");
    if(!file)
    {
        return ThrowError("Cannot open '%s' for writing: %s", filename, strerror(errno));
    }
    else
    {
        if(VX_SUCCEEDED(this->WriteClosure(file_write, file)))
        {
            fclose(file);
            return VX_OK;
        }
        fclose(file);
    }
    return VX_ERROR;             //forward the error
}

VXInteger VXState::BindEnv(VXObject& o, const VXObject& env, VXObject& ret)
{
    VXWeakRefObj* weak;
    VXForeignClosureObj* fclos;
    VXNativeClosureObj* nclos;
    if(!o.IsNativeClosure() && !o.IsClosure())
    {
        return this->ThrowError("the target is not a closure");
    }
    if(!env.IsTable() && !env.IsClass() && !env.IsInstance())
    {
        return this->ThrowError("invalid environment");
    }
    weak = env.RefCounted()->GetWeakRef(env.Type());
    if(o.IsClosure())
    {
        fclos = o.Closure()->Clone();
        __ObjRelease(fclos->_env);
        fclos->_env = weak;
        __ObjAddRef(fclos->_env);
        if(_closure(o)->_base)
        {
            fclos->_base = o.Closure()->_base;
            __ObjAddRef(fclos->_base);
        }
        ret = fclos;
    }
    else                         //then must be a native closure
    {
        nclos = o.NativeClosure()->Clone();
        __ObjRelease(nclos->_env);
        nclos->_env = weak;
        __ObjAddRef(nclos->_env);
        ret = nclos;
    }
    return VX_OK;
}

void VXState::SetCompilerErrorHandler(VXCompileError f)
{
    Shared()->_compilererrorhandler = f;
}

void VXState::SetDebugHook(const VXObject& o)
{
    if(o.IsClosure() || o.IsNativeClosure() || o.IsNull())
    {
        this->_debughook_closure = o;
        this->_debughook_native = NULL;
        this->_debughook = !o.IsNull();
    }
}

void VXState::SetErrorHandler(const VXObject& o)
{
    if(o.IsClosure() || o.IsNativeClosure() || o.IsNull())
    {
        this->_errorhandler = o;
    }
}


void VXState::SetErrorHandlers()
{
    this->SetCompilerErrorHandler(vox_aux_compiler_error);
    VXObject fn = VXNativeClosureObj::Create(this->Shared(), vox_aux_printerror, 0, VXObject());
    if(fn.IsClosure() || fn.IsNativeClosure() || fn.IsNull())
    {
        this->_errorhandler = fn;
    }
}

void VXState::EnableDebugInfo(bool enable)
{
    _ss(this)->_debuginfo = enable;
}

void VXState::PushRoot()
{
    this->Push(this->_roottable);
}

void VXState::PopRoot()
{
    this->Pop(1);
}

VXInteger VXState::GetUserPointer(VXInteger idx, VXUserPointer *p)
{
    VXObject o;
    _GETSAFE_OBJ_REF(this, idx, VX_OT_USERPOINTER, o);
    (*p) = _userpointerval(o);
    return VX_OK;
}

VXInteger VXState::GetString(VXInteger idx, const char** destination, VXInteger* len_dest)
{
    VXObject&  o = this->StackGet(idx);
    if(o.Type() == VX_OT_STRING)
    {
        (*destination) = o.String()->Value();
        if(len_dest != NULL)
        {
            (*len_dest) = o.String()->Length();
        }
        return VX_OK;
    }
    return VX_ERROR;

    /*
    VXObject objptr;
    _GETSAFE_OBJ_REF(this, idx, VX_OT_STRING, objptr);
    *destination = _stringval(objptr);
    if(len_dest != NULL)
    {
        *len_dest = _stringlen(objptr);
    }
    return VX_OK;
    */
}

VXInteger VXState::GetInteger(VXInteger idx, VXInteger* dest)
{
    VXObject& o = this->StackGet(idx);
    if(o.IsNumeric())
    {
        *dest = o.Integer();
        return VX_OK;
    }
    return VX_ERROR;
}


VXInteger VXState::GetFloat(VXInteger idx, VXFloat* dest)
{
    VXObject& o = this->StackGet(idx);
    if(o.IsNumeric())
    {
        *dest = o.Float();
        return VX_OK;
    }
    return VX_ERROR;
}

VXInteger VXState::GetBool(VXInteger idx, bool* dest)
{
    VXObject& o = this->StackGet(idx);
    if(o.IsBool())
    {
        *dest = o.Integer();
        return VX_OK;
    }
    return VX_ERROR;
}

VXInteger VXState::GetClosure(VXInteger idx, VXFuncProtoObj** fnproto)
{
    VXRawObj o = this->StackGet(idx);
    if(type(o) == VX_OT_CLOSURE)
    {
        (*fnproto) = _closure(o)->_function;
        return VX_OK;
    }
    return VX_ERROR;
}

VXInteger VXState::GetTypedArg(VXInteger idx, VXOType expected_type, VXObject& dest)
{
    dest = this->StackGet(idx);
    if(type(dest) != expected_type)
    {
        VXObject oval = this->PrintObjVal(dest);
        this->ThrowError(
            "wrong argument type, expected '%s' got '%.50s'",
            IdType2Name(expected_type),
            _stringval(oval));
        return VX_ERROR;
    }
    return VX_OK;
}

bool VXState::CollectGarbage()
{
    return this->Shared()->CollectGarbage(this);
}

bool VXState::ResurrectUnreachable()
{
    return this->Shared()->ResurrectUnreachable(this);
}

VXInteger VXState::AtExit(const VXObject& ob)
{
    if(ob.Type() ==  VX_OT_NATIVECLOSURE || ob.Type() ==  VX_OT_CLOSURE)
    {
        this->_atexit_functions.push_back(ob);
        return VX_OK;
    }
    return ThrowError("cannot use type '%s' in atexit", ob.TypeString());

}

void VXState::Move(VXState* destination, VXState* source, const VXObject& o)
{
    (void)source;
    destination->Push(o);
}

void VXState::MoveIndex(VXState* destination, VXState* source, VXInteger idx)
{
    VXState::Move(destination, source, source->StackGet(idx));
}

VXInteger DoCompile(VXState* v, VXLexReadFunc read,
                    VXUserPointer p, const char *sourcename,
                    bool raiseerror, VXObject* dest=NULL)
{
    VXObject o;
    #ifndef VOX_NO_COMPILER
    if(Compile(v, read, p, sourcename, o, raiseerror, _ss(v)->_debuginfo))
    {
        VXForeignClosureObj* closure = VXForeignClosureObj::Create(_ss(v), _funcproto(o));
        if(dest != NULL)
        {
            (*dest) = closure;
        }
        else
        {
            v->Push(closure);
        }
        return VX_OK;
    }
    return VX_ERROR;
    #else
    return v->ThrowError("this is a no compiler build");
    #endif
}


VXInteger VXState::CompileBuffer(const char* source, VXInteger size,
                                 const char* sourcename, bool raiseerror)
{
    BufState buf;
    buf.buf = source;
    buf.size = size;
    buf.ptr = 0;
    return DoCompile(this, buf_lexfeed, &buf, sourcename, raiseerror);
}

VXInteger VXState::DoStringFmt(bool globaltable, bool returns,
                               bool raise_error, const char* str_format, ...)
{
    VXInteger retvalue;
    va_list arguments;
    va_start(arguments, str_format);
    char* buffptr;
    size_t realsize;
    size_t needed = 0;
    size_t initial_bufsize = 1;
    buffptr = new char[initial_bufsize+1];
    needed = 1 + vsnprintf(buffptr, initial_bufsize, str_format, arguments);
    va_end(arguments);
    if(needed > initial_bufsize)
    {
        delete[] buffptr;
        buffptr = new char[needed+1];
        va_start(arguments, str_format);
        realsize = vsnprintf(buffptr, needed, str_format, arguments);
        va_end(arguments);
    }
    else
    {
        realsize = needed;
    }
    buffptr[realsize] = 0;
    if(globaltable)
    {
        this->PushRoot();
    }
    retvalue = this->DoString(buffptr, realsize, "<buffer>", returns, raise_error);
    if(globaltable)
    {
        this->Pop(1);
    }
    delete[] buffptr;
    return retvalue;
}


VXInteger VXState::DoString(const char* str, VXInteger len,
                            const char* bufname, bool retval, bool printerror)
{
    len = (len == -1) ? strlen(str) : len;
    if(bufname == NULL)
    {
        bufname = "<buffer>";//DOSTRING_DEFAULTBUFFERNAME;
    }
    if(VX_FAILED(this->CompileBuffer(str, len, bufname, printerror)))
    {
        return VX_ERROR;
    }
    else
    {
        Repush(-2);
        if(VX_FAILED(StackCall(1, retval, printerror)))
        {
            return VX_ERROR;
        }
        return VX_OK;
    }
    return ThrowError("call failed");
}

VXInteger VXState::ReadClosure(VXReadFunc r, VXUserPointer up, VXObject* dest)
{
    VXObject closure;
    unsigned short tag;
    if(r(up, &tag, 2) != 2)
    {
        return ThrowError("io error");
    }
    if(tag != VX_BYTECODE_STREAM_TAG)
    {
        return ThrowError("invalid stream");
    }
    if(!VXForeignClosureObj::Load(this, up, r, closure))
    {
        return VX_ERROR;
    }
    if(dest)
    {
        (*dest) = closure;
    }
    else
    {
        this->Push(closure);
    }
    return VX_OK;
}

VXInteger VXState::LoadFile(const char* filename, bool printerror,
                            bool* failed_to_open, VXObject* dest)
{

    FILE* file = fopen(filename, "rb");
    VXInteger ret;
    unsigned short us;
    unsigned char uc;
    VXLexReadFunc func = _io_file_lexfeed_PLAIN;
    if(file)
    {
        if(failed_to_open != NULL)
        {
            (*failed_to_open) = false;
        }
        ret = fread(&us,1,2,file);
        if(ret != 2)
        {
            //probably an empty file
            us = 0;
        }
        //BYTECODE
        if(us == VX_BYTECODE_STREAM_TAG)
        {
            VXInteger st;
            fseek(file, 0, SEEK_SET);
            if(VX_SUCCEEDED((st = this->ReadClosure(file_read, file, dest))))
            {
                fclose(file);
                return st;
            }
            return st;
        }
        else                     //SCRIPT
        {
            switch(us)
            {
                //gotta swap the next 2 lines on BIG endian machines
                //UTF-16 little endian
                case 0xFFFE:
                    func = _io_file_lexfeed_UCS2_BE;
                    break;

                    // UTF-16 big endian
                case 0xFEFF:
                    func = _io_file_lexfeed_UCS2_LE;
                    break;

                    // UTF-8
                case 0xBBEF:
                    if(fread(&uc, 1, sizeof(uc), file) == 0)
                    {
                        fclose(file);
                        return ThrowError("io error");
                    }
                    if(uc != 0xBF)
                    {
                        fclose(file);
                        return ThrowError("Unrecognozed encoding");
                    }
                    func = _io_file_lexfeed_PLAIN;
                    break;

                    // ascii
                default:
                    fseek(file, 0, SEEK_SET);
                    break;
            }
            if(VX_SUCCEEDED(DoCompile(this, func,file,filename,printerror, dest)))
            {
                fclose(file);
                return VX_OK;
            }
        }
        fclose(file);
        return VX_ERROR;
    }
    if(failed_to_open != NULL)
    {
        (*failed_to_open) = true;
    }
    return ThrowError("Cannot open '%s' for reading: %s", filename, strerror(errno));
}

VXInteger VXState::CallStack(VXInteger idx, VXInteger params,bool retval,bool raiseerror)
{
    return this->CallSimple(this->GetUp(idx), params, retval, raiseerror);
}

VXInteger VXState::CallStack(VXInteger params,bool retval,bool raiseerror)
{
    return this->CallStack(-(params+1), params, retval, raiseerror);
}

VXInteger VXState::CallSimple(VXObject& closure,
                              VXInteger params,
                              bool retval,
                              bool raiseerror,
                              VXObject& dest)
{
    if(this->Call(closure, params, this->_top-params, dest, raiseerror))
    {
        if(!this->_suspended)
        {
            //pop closure and args
            //this->Pop(params);
        }
        if(retval)
        {
            //this->Push(res);
            return VX_OK;
        }
        return VX_OK;
    }
    else
    {
        //this->Pop(params);
        return VX_ERROR;
    }
    if(!this->_suspended)
    {
        //this->Pop(params);
    }
    return this->ThrowError("call failed");
}

VXInteger VXState::CallSimple(VXObject& closure, VXInteger params,bool retval,bool raiseerror)
{
    VXObject res;
    VXInteger st;
    if(VX_SUCCEEDED(st = CallSimple(closure, params, retval, raiseerror, res)))
    {
        Push(res);
    }
    return st;
}


VXInteger VXState::Repush(VXInteger idx)
{
    this->Push(this->StackGet(idx));
    return VX_OK;
}

VXInteger VXState::StackCall(VXInteger params, bool retval, bool raiseerror)
{
    VXObject res;
    if(this->Call(this->GetUp(-(params+1)), params, this->_top - params, res, raiseerror))
    {
        if(!this->_suspended)
        {
            //pop closure and args
            this->Pop(params);
        }
        if(retval)
        {
            this->Push(res);
            return VX_OK;
        }
        return VX_OK;
    }
    else
    {
        this->Pop(params);
        return VX_ERROR;
    }
    if(!this->_suspended)
    {
        this->Pop(params);
    }
    return ThrowError("call failed");
}

VXInteger VXState::DoFile(const char* filename, bool retval, bool printerror, bool* failed_to_open)
{
    if(VX_SUCCEEDED(this->LoadFile(filename, printerror, failed_to_open)))
    {
        this->Repush(-2);
        if(VX_SUCCEEDED(this->StackCall(1, retval, printerror)))
        {
            this->Remove(retval ? -2 : -1);
            return VX_OK;
        }
        this->Pop(1);
        return VX_ERROR;
    }
    return VX_ERROR;
}



const char* VXState::GetLocal(VXUnsignedInteger level,VXUnsignedInteger idx, VXObject& dest)
{
    VXUnsignedInteger cstksize = this->_callsstacksize;
    VXUnsignedInteger lvl = (cstksize-level)-1;
    VXInteger stackbase = this->_stackbase;
    if(lvl < cstksize)
    {
        for(VXUnsignedInteger i=0; i<level; i++)
        {
            VXState::CallInfo &ci = this->_callsstack[(cstksize-i)-1];
            stackbase -= ci._prevstkbase;
        }
        VXState::CallInfo &ci = this->_callsstack[lvl];
        if(type(ci._closure) != VX_OT_CLOSURE)
            return NULL;
        VXForeignClosureObj *c = _closure(ci._closure);
        VXFuncProtoObj *func = c->_function;
        if(func->_noutervalues > (VXInteger)idx)
        {
            //v->Push(*_outer(c->_outervalues[idx])->_valptr);
            dest = *_outer(c->_outervalues[idx])->_valptr;
            return _stringval(func->_outervalues[idx]._name);
        }
        idx -= func->_noutervalues;
        return func->GetLocal(this,
                        stackbase, idx,
                        (VXInteger)(ci._ip-func->_instructions)-1, dest);
    }
    return NULL;
}



void VXState::DefineGlobal(const char* name, const VXObject& sym)
{
    GetRootTable()->NewSlot(NewString(name), sym);
}

void VXState::SetPrintFunc(VXPrintFunction printfunc,VXPrintFunction errfunc)
{
    this->Shared()->_printfunc = printfunc;
    this->Shared()->_errorfunc = errfunc;
}

void VXState::PushLastError()
{
    this->Push(this->_lasterror);
}

const char* VXState::LastError()
{
    const char* result;
    VXInteger tp;
    VXObject ob;
    ob = this->_lasterror;
    tp = ob.Type();
    if(tp == VX_OT_INSTANCE)
    {
        const char* clname;
        const char* tmpstr;
        VXObject res;
        VXObject whatfn;
        VXInstanceObj* inst = ob.Instance();
        clname = inst->_class->_name.String()->Value();
        if(inst->Get(NewString("what"), whatfn))
        {
            Push(inst);
            if(VX_SUCCEEDED(CallSimple(whatfn, 1, true, false, res)))
            {
                if(res.Type() == VX_OT_STRING)
                {
                    tmpstr = res.String()->Value();
                }
                else
                {
                    tmpstr = "[unknown message]";
                }
                sprintf(ScratchPad(res.String()->Length() +
                        inst->_class->_name.String()->Length() + 1024),
                    "%s: %s", clname, tmpstr);
                return ScratchPad(-1);
            }
        }
    }
    else if(tp == VX_OT_STRING)
    {
        return ob.String()->Value();
    }
    return "[unknown error]";




    /*
    const char* result;
    this->PushLastError();
    if(VX_FAILED(this->GetString(-1, &result, NULL)))
    {
        result = "[unknown error]";
    }
    return result;
    */
}

void VXState::Mark(VXCollectable **chain)
{
    START_MARK()
    VXSharedState::MarkObject(_lasterror,chain);
    VXSharedState::MarkObject(_errorhandler,chain);
    VXSharedState::MarkObject(_debughook_closure,chain);
    VXSharedState::MarkObject(_roottable, chain);
    VXSharedState::MarkObject(temp_reg, chain);
    for(VXUnsignedInteger i = 0; i < _stack.size(); i++)
    {
        VXSharedState::MarkObject(_stack[i], chain);
    }
    for(VXInteger k = 0; k < _callsstacksize; k++)
    {
        VXSharedState::MarkObject(_callsstack[k]._closure, chain);
    }
    END_MARK()
}

void VXState::Release()
{
    vox_delete(this,VXState);
}

VXOType VXState::GetType()
{
    return VX_OT_THREAD;
}

void VXState::GrowCallStack()
{
    VXInteger newsize = _alloccallsstacksize*2;
    _callstackdata.resize(newsize);
    _callsstack = &_callstackdata[0];
    _alloccallsstacksize = newsize;
}

VXObject& VXState::StackGet(VXInteger idx)
{
    //return ((idx>=0) ? (this->GetAt(idx + ((this->_stackbase)-1))) : (this->GetUp(idx)));
    if(idx >= 0)
    {
        return this->GetAt(idx + ((this->_stackbase)-1));
    }
    return this->GetUp(idx);
}

VXInteger VXState::SetParamsCheck(VXObject& o, VXInteger nparams, const char *typemask)
{
    //VXObject o = stack_get(v, -1);
    if(!o.IsNativeClosure())
    {
        return ThrowError("native closure expected");
    }
    VXNativeClosureObj *nc = _nativeclosure(o);
    nc->_nparamscheck = nparams;
    if(typemask)
    {
        VXIntVec res;
        if(!CompileTypemask(res, typemask))
        {
            return ThrowError("invalid typemask");
        }
        nc->_typecheck.copy(res);
    }
    else
    {
        nc->_typecheck.resize(0);
    }
    if(nparams == VX_MATCHTYPEMASKSTRING)
    {
        nc->_nparamscheck = nc->_typecheck.size();
    }
    return VX_OK;
}


void RegisterLib_IterReg(VXState* self, const VXRegFunction* funcs, VXTableObj* dest)
{
    VXInteger i;
    for(i=0; funcs[i].name; i++)
    {
        VXRegFunction reg = funcs[i];
        VXObject name = self->NewString(reg.name);
        VXObject fn = self->NewClosure(reg.func, 0, self->NewString(reg.name));
        self->SetParamsCheck(fn, reg.nparamscheck, reg.typemask);
        dest->NewSlot(name, fn);
    }
}

VXTableObj* VXState::RegisterLib(const char* libname, const VXRegFunction* funcs,
                                 bool reg_global, VXTableObj* tb)
{

    if(reg_global && (libname == NULL))
    {
        // in this case, each function of $funcs is supposed to be
        // a separate item in the root table
        RegisterLib_IterReg(this, funcs, GetRootTable());
    }
    else
    {
        // otherwise, a separate table with each function of $funcs will be
        // created and newslot'd to the root table
        if(tb == NULL)
        {
            tb = this->NewTable();
        }
        RegisterLib_IterReg(this, funcs, tb);
        if(reg_global)
        {
            vox_assert(libname != NULL);
            GetRootTable()->NewSlot(NewString(libname), tb);
        }
    }
    return tb;
}

VXClassObj* VXState::RegClass(const VXVector<VXRegFunction>& funcs,
                              VXClassObj* base,
                              VXUserPointer typetag)
{
    VXUnsignedInteger i;
    VXClassObj* cl;
    cl = this->NewClass(base);
    for(i=0; i<funcs.size(); i++)
    {
        VXRegFunction f = funcs[i];
        VXObject fn = this->NewClosure(f.func, 0, this->NewString(f.name));
        VXObject name = this->NewString(f.name);
        this->SetParamsCheck(fn, f.nparamscheck, f.typemask);
        cl->NewSlot(this->Shared(), name, fn, false);
    }
    if(typetag != 0)
    {
        cl->_typetag = typetag;
    }
    return cl;

}

VXClassObj* VXState::RegClass(const VXRegFunction* funcs,
                              VXClassObj* base,
                              VXUserPointer typetag)
{
    VXInteger i;
    VXVector<VXRegFunction> vecfuncs;
    for(i=0; funcs[i].name; i++)
    {
        vecfuncs.push_back(funcs[i]);
    }
    return this->RegClass(vecfuncs, base, typetag);
}


VXClassObj* VXState::NewClass(VXClassObj* baseclass)
{
    VXClassObj* ret;
    ret = VXClassObj::Create(this->Shared(), baseclass);
    return ret;
}

VXNativeClosureObj* VXState::NewClosure(VXFunction func,
                                        VXUnsignedInteger nfreevars,
                                        const VXObject& name)
{
    VXNativeClosureObj *nc;
    nc = VXNativeClosureObj::Create(this->Shared(), func, nfreevars, name);
    nc->_nparamscheck = 0;
    for(VXUnsignedInteger i=0; i<nfreevars; i++)
    {
        nc->_outervalues[i] = this->Top();
        this->Pop();
    }
    return nc;
}

VXNativeClosureObj* VXState::NewClosure(VXFunction func,
                                        VXUnsignedInteger nfreevars)
{
    return this->NewClosure(func, nfreevars, VXObject());
}

VXStringObj* VXState::NewString(const char* str, VXInteger len)
{
    return VXStringObj::Create(_sharedstate, str, len);
}

VXArrayObj* VXState::NewArray(VXInteger initial_size)
{
    return VXArrayObj::Create(_sharedstate, initial_size);
}

VXTableObj* VXState::NewTable()
{
    return VXTableObj::Create(_sharedstate, 0);
}


VXState* VXState::NewThread(VXState* friendvm, VXInteger initialstacksize)
{
    VXSharedState *ss;
    VXState *v;
    ss = _ss(friendvm);
    v = (VXState *)VX_MALLOC(sizeof(VXState));
    new (v) VXState(ss);

    if(v->Init(friendvm, initialstacksize))
    {
        friendvm->Push(v);
        return v;
    }
    else
    {
        vox_delete(v, VXState);
        return NULL;
    }
}

VXArrayObj* VXState::GetSysArgv()
{
    return _system_argv;
}

VXTableObj* VXState::GetRegistryTable()
{
    return Shared()->_registry.Table();
}

VXTableObj* VXState::GetRootTable()
{
    return _roottable.Table();
}

VXInteger VXState::ThrowError()
{
    return ThrowError(_lasterror);
}

VXInteger VXState::ThrowError(const VXObject &desc)
{
    _lasterror = desc;
    return VX_ERROR;
}

VXInteger VXState::ThrowError(const char *s, ...)
{
    va_list vl;
    va_start(vl, s);
    vsprintf(ScratchPad((VXInteger)strlen(s)+(NUMBER_MAX_CHAR*2)), s, vl);
    va_end(vl);
    return this->ThrowError(VXStringObj::Create(_ss(this),ScratchPad(-1),-1));
}

VXInteger VXState::Irrecoverable_Error(const char* s, ...)
{
    va_list vl;
    va_start(vl, s);
    vsprintf(ScratchPad((VXInteger)strlen(s)+(NUMBER_MAX_CHAR*2)), s, vl);
    va_end(vl);
    fprintf(stderr, "Irrecoverable Error: %s\n", ScratchPad(-1));
    exit(-1);
    return VX_ERROR; // never reached
}


VXStringObj* VXState::PrintObjVal(const VXObject &o)
{
    switch(type(o))
    {
        case VX_OT_STRING:
            return _string(o);
        case VX_OT_INTEGER:
            sprintf(ScratchPad(NUMBER_MAX_CHAR+1), _PRINT_INT_FMT, _integer(o));
            return VXStringObj::Create(_ss(this), ScratchPad(-1));
            break;
        case VX_OT_FLOAT:
            sprintf(ScratchPad(NUMBER_MAX_CHAR+1), "%.14g", _float(o));
            return VXStringObj::Create(_ss(this), ScratchPad(-1));
            break;
        default:
            return VXStringObj::Create(_ss(this), GetTypeName(o));
    }
}

VXInteger VXState::Raise_IdxError(const VXObject &o,
                                  const VXObject& origin,
                                  bool is_index)
{
    VXInteger len;
    VXOType otp = origin.Type();
    VXObject idx = PrintObjVal(o);
    bool is_func = (otp == VX_OT_CLOSURE || otp == VX_OT_NATIVECLOSURE);
    const char* idx_s = idx.String()->Value();
    if(origin.Type() == VX_OT_TABLE)
    {
        return ThrowError("table key '%.50s' does not exist", idx_s);
    }
    if(((otp == VX_OT_CLASS || otp == VX_OT_INSTANCE) ||
       (otp & VX_OT_INSTANCE) || is_func) && !is_index)
    {
        return ThrowError("%s member '%.50s' does not exist",
            origin.TypeString(), idx_s);
    }
    if(otp == VX_OT_STRING || otp == VX_OT_ARRAY)
    {
        len = GetSize(origin);
        return ThrowError("%s index '"_PRINT_INT_FMT"' out of range (%s size: "_PRINT_INT_FMT")",
            origin.TypeString(), o.Integer(), origin.TypeString(), len);
    }
    return ThrowError("%s index '%.50s' does not exist",
        origin.TypeString(), otp);
}


VXInteger VXState::Raise_CompareError(const VXObject &o1, const VXObject &o2)
{
    VXObject oval1 = PrintObjVal(o1);
    VXObject oval2 = PrintObjVal(o2);
    ThrowError("comparison between '%s' and '%s'", o1.TypeString(), o2.TypeString());
    return VX_ERROR;
}

VXInteger VXState::Raise_ParamTypeError(VXInteger nparam,VXInteger typemask,VXInteger type)
{
    VXObject exptypes = VXStringObj::Create(_ss(this), "", -1);
    VXInteger found = 0;
    for(VXInteger i=0; i<16; i++)
    {
        VXInteger mask = 0x00000001 << i;
        if(typemask & (mask))
        {
            if(found > 0)
            {
                StringCat(exptypes,VXStringObj::Create(_ss(this), "|", -1), exptypes);
            }
            found++;
            StringCat(exptypes,VXStringObj::Create(_ss(this),
                IdType2Name((VXOType)mask), -1), exptypes);
        }
    }
    return ThrowError("parameter %d has an invalid type '%s' ; expected: '%s'",
        nparam,
        IdType2Name((VXOType)type),
        _stringval(exptypes)
    );
}


VXSharedState* VXState::Shared()
{
    return _sharedstate;
}

VXState* VXState::Create(VXInteger stacksize)
{
    VXSharedState *ss;
    VXState *v;
    vox_new(ss, VXSharedState);
    ss->Init();
    v = (VXState *)VX_MALLOC(sizeof(VXState));
    new (v) VXState(ss);
    ss->_root_vm = v;
    if(v->Init(NULL, stacksize) == true)
    {
        v->_system_argv = v->NewArray();
        voxstd_register_system(v);
        voxstd_register_mathlib(v);
        voxstd_register_iolib(v);
        v->SetErrorHandlers();
        v->SetPrintFunc(vox_defaultprintfunc, vox_defaulterrorfunc);
        return v;
    }
    else
    {
        vox_delete(v, VXState);
        return NULL;
    }
    return NULL;
}

void VXState::Destroy(VXState* vm)
{
    VXUnsignedInteger i;
    VXSharedState* ss = _ss(vm);
    for(i=0; i<(vm->_atexit_functions.size()); i++)
    {
        VXObject res;
        if(!vm->Call(vm->_atexit_functions[i], 1, vm->GetTop()-1, res, true))
        {
            //fprintf(stderr, "Call() failed?\n");
        }
    }
    _thread(ss->_root_vm)->Finalize();
    vox_delete(ss, VXSharedState);
}


VXState::VXState(VXSharedState *ss)
{
    _sharedstate=ss;
    _suspended = false;
    _suspended_target = -1;
    _suspended_root = false;
    _suspended_traps = -1;
    _foreignptr = NULL;
    _nnativecalls = 0;
    _nmetamethodscall = 0;
    _lasterror.Null();
    _errorhandler.Null();
    _debughook = false;
    _debughook_native = NULL;
    _debughook_closure.Null();
    _openouters = NULL;
    ci = NULL;
    INIT_CHAIN();
    ADD_TO_CHAIN(&_ss(this)->_gc_chain,this);
}

void VXState::Finalize()
{
    if(_openouters)
    {
        CloseOuters(this, &_stack.values()[0]);
    }
    _roottable.Null();
    _lasterror.Null();
    _errorhandler.Null();
    _debughook = false;
    _debughook_native = NULL;
    _debughook_closure.Null();
    temp_reg.Null();
    _callstackdata.resize(0);
    VXInteger size=_stack.size();
    for(VXInteger i=0;i<size;i++)
    {
        _stack[i].Null();
    }
}

VXState::~VXState()
{
    Finalize();
    REMOVE_FROM_CHAIN(&_ss(this)->_gc_chain,this);
}




bool VXState::ObjCmp(const VXObject &o1,
                     const VXObject &o2,
                     VXInteger &result)
{
    VXObject res;
    VXOType t1 = type(o1);
    VXOType t2 = type(o2);
    if(t1 == t2)
    {
        if(_rawval(o1) == _rawval(o2))
        {
            _RET_SUCCEED(0);
        }
        switch(t1)
        {
            case VX_OT_STRING:
                _RET_SUCCEED(strcmp(_stringval(o1), _stringval(o2)));
            case VX_OT_INTEGER:
                _RET_SUCCEED(_integer(o1)-_integer(o2));
            case VX_OT_FLOAT:
                _RET_SUCCEED((_float(o1)<_float(o2))?-1:1);
            case VX_OT_TABLE:
            case VX_OT_USERDATA:
            case VX_OT_INSTANCE:
                if(_delegable(o1)->_delegate)
                {
                    VXObject closure;
                    if(_delegable(o1)->GetMetaMethod(this, VX_MT_CMP, closure))
                    {
                        Push(o1);Push(o2);
                        if(!CallMetaMethod(this, closure,VX_MT_CMP,2,res))
                        {
                            return false;
                        }
                    }
                    break;
                }
                //continues through (no break needed)
            default:
                _RET_SUCCEED( _userpointerval(o1) < _userpointerval(o2)?-1:1 );
        }
        if(type(res)!=VX_OT_INTEGER)
        {
            Raise_CompareError(o1,o2);
            return false;
        }
        _RET_SUCCEED(_integer(res));

    }
    else
    {
        if(o1.IsNumeric() && o2.IsNumeric())
        {
            if((t1==VX_OT_INTEGER) && (t2==VX_OT_FLOAT))
            {
                if(_integer(o1) == _float(o2) )
                {
                    _RET_SUCCEED(0);
                }
                else if(_integer(o1)<_float(o2))
                {
                    _RET_SUCCEED(-1);
                }
                _RET_SUCCEED(1);
            }
            else
            {
                if(_float(o1)==_integer(o2))
                {
                    _RET_SUCCEED(0);
                }
                else if(_float(o1) < _integer(o2))
                {
                    _RET_SUCCEED(-1);
                }
                _RET_SUCCEED(1);
            }
        }
        else if(t1==VX_OT_NULL)
        {
            _RET_SUCCEED(-1);
        }
        else if(t2==VX_OT_NULL)
        {
            _RET_SUCCEED(1);
        }
        else
        {
            Raise_CompareError(o1,o2);
            return false;
        }
    }
    vox_assert(0);
    _RET_SUCCEED(0); //cannot happen
}


bool VXState::ToString(const VXObject &o, VXObject &res)
{
    VXObject temp;
    switch(type(o))
    {
        case VX_OT_STRING:
            res = o;
            return true;
        case VX_OT_FLOAT:
            sprintf(ScratchPad(NUMBER_MAX_CHAR+1),"%g",_float(o));
            break;
        case VX_OT_INTEGER:
            sprintf(ScratchPad(NUMBER_MAX_CHAR+1),_PRINT_INT_FMT,_integer(o));
            break;
        case VX_OT_BOOL:
            sprintf(ScratchPad(6),_integer(o)?"true":"false");
            break;
        case VX_OT_TABLE:
        case VX_OT_USERDATA:
        case VX_OT_INSTANCE:
            if(_delegable(o)->_delegate)
            {
                VXObject closure;
                if(_delegable(o)->GetMetaMethod(this, VX_MT_TOSTRING, closure))
                {
                    Push(o);
                    if(CallMetaMethod(this, closure,VX_MT_TOSTRING,1,res))
                    {
                        if(type(res) == VX_OT_STRING)
                        {
                            return true;
                        }
                    }
                    //else
                    //{
                    //    return false;
                    //}
                }
            }
        default:
            if(type(o) != VX_OT_NULL)
            {
                sprintf(ScratchPad(2048),
                        "<%s at %p>",
                        GetTypeName(o),(void*)_rawval(o));
            }
            else
            {
                sprintf(ScratchPad(9), "<null>");
            }
    }
    //res = VXStringObj::Create(_ss(this), ScratchPad(-1));
    res = NewString(ScratchPad(-1));
    return true;
}


bool VXState::StringCat(const VXObject &str,
                        const VXObject &obj,
                        VXObject &dest)
{
    VXObject a, b;
    if(!ToString(str, a))
    {
        return false;
    }
    if(!ToString(obj, b))
    {
        return false;
    }
    VXInteger l = _string(a)->_len , ol = _string(b)->_len;
    char *s = ScratchPad(l + ol + 1);
    memcpy(s, _stringval(a), l);
    memcpy(s + l, _stringval(b), ol);
    dest = VXStringObj::Create(_ss(this), ScratchPad(-1), l + ol);
    return true;
}

bool VXState::TypeOf(const VXObject &obj1,
                     VXObject &dest)
{
    if(is_delegable(obj1) && _delegable(obj1)->_delegate)
    {
        VXObject closure;
        if(_delegable(obj1)->GetMetaMethod(this, VX_MT_TYPEOF, closure))
        {
            Push(obj1);
            return CallMetaMethod(this, closure,VX_MT_TYPEOF,1,dest);
        }
    }
    dest = VXStringObj::Create(_ss(this),GetTypeName(obj1));
    return true;
}

bool VXState::Init(VXState *friendvm, VXInteger stacksize)
{
    _stack.resize(stacksize);
    _alloccallsstacksize = 4;
    _callstackdata.resize(_alloccallsstacksize);
    _callsstacksize = 0;
    _callsstack = &_callstackdata[0];
    _stackbase = 0;
    _top = 0;
    if(!friendvm)
    {
        _roottable = NewTable();
    }
    else
    {
        _roottable = friendvm->_roottable;
        _errorhandler = friendvm->_errorhandler;
        _debughook = friendvm->_debughook;
        _debughook_native = friendvm->_debughook_native;
        _debughook_closure = friendvm->_debughook_closure;
    }
    return true;

}


bool VXState::StartCall(VXForeignClosureObj *closure,
                        VXInteger target,
                        VXInteger args,
                        VXInteger stackbase,
                        bool tailcall)
{
    VXFuncProtoObj *func = closure->_function;

    VXInteger paramssize = func->_nparameters;
    const VXInteger newtop = stackbase + func->_stacksize;
    VXInteger nargs = args;
    const char* funcname = _stringval(func->_name);
    if(func->_name.IsNull())
    {
        funcname = NAMES_ANONFUNC;
    }
    if(func->_varparams)
    {
        paramssize--;
        if(nargs < paramssize)
        {
            return WrongNumberOfArguments(this, false, false, func);
        }
        VXInteger nvargs = nargs - paramssize;
        VXArrayObj *arr = VXArrayObj::Create(_ss(this),nvargs);
        VXInteger pbase = stackbase+paramssize;
        for(VXInteger n = 0; n < nvargs; n++)
        {
            arr->_values[n] = _stack.values()[pbase];
            _stack.values()[pbase].Null();
            pbase++;
        }
        _stack.values()[stackbase+paramssize] = arr;
    }
    else if (paramssize != nargs)
    {
        VXInteger ndef = func->_ndefaultparams;
        VXInteger diff;
        if(ndef && (nargs < paramssize) && (diff = (paramssize - nargs)) <= ndef)
        {
            for(VXInteger n = ndef - diff; n < ndef; n++)
            {
                _stack.values()[stackbase + (nargs++)] = closure->_defaultparams[n];
            }
        }
        else
        {
            return WrongNumberOfArguments(this, false, false, func);
        }
    }
    if(closure->_env)
    {
        _stack.values()[stackbase] = closure->_env->_obj;
    }
    if(!EnterFrame(stackbase, newtop, tailcall))
    {
        return false;
    }
    ci->_closure  = closure;
    ci->_literals = func->_literals;
    ci->_ip       = func->_instructions;
    ci->_target   = (VXInt32)target;
    if (_debughook)
    {
        CallDebugHook('c');
    }
    if (closure->_function->_bgenerator)
    {
        VXFuncProtoObj *f = closure->_function;
        VXGeneratorObj *gen = VXGeneratorObj::Create(_ss(this), closure);
        if(!gen->Yield(this,f->_stacksize))
            return false;
        VXObject temp;
        Return(this, 1, target, temp);
        STK(target) = gen;
    }
    return true;
}



VXInteger VXState::RawGetStack(VXInteger idx)
{
    VXObject& self = StackGet(idx);
    switch(type(self))
    {
        case VX_OT_TABLE:
            if(_table(self)->Get(GetUp(-1), GetUp(-1)))
                return VX_OK;
            break;
        case VX_OT_CLASS:
            if(_class(self)->Get(GetUp(-1), GetUp(-1)))
                return VX_OK;
            break;
        case VX_OT_INSTANCE:
            if(_instance(self)->Get(GetUp(-1), GetUp(-1)))
                return VX_OK;
            break;
        case VX_OT_ARRAY:
            {
                VXObject& key = GetUp(-1);
                if(key.IsNumeric())
                {
                    if(_array(self)->Get(tointeger(key), GetUp(-1)))
                    {
                        return VX_OK;
                    }
                }
                else
                {
                    Pop();
                    return ThrowError("invalid index type for an array");
                }
            }
            break;
        default:
            Pop();
            return ThrowError("rawget works only on array/table/instance and class");
    }
    Pop();
    return ThrowError("the index doesn't exist");
}

VXInteger VXState::RawSetStack(VXInteger idx)
{
    VXObject &self = this->StackGet(idx);
    if(type(this->GetUp(-2)) == VX_OT_NULL)
    {
        return this->ThrowError("null key");
    }
    switch(type(self))
    {
        case VX_OT_TABLE:
            _table(self)->NewSlot(GetUp(-2), GetUp(-1));
            Pop(2);
            return VX_OK;
            break;
        case VX_OT_CLASS:
            _class(self)->NewSlot(Shared(), GetUp(-2), GetUp(-1),false);
            Pop(2);
            return VX_OK;
            break;
        case VX_OT_INSTANCE:
            if(_instance(self)->Set(GetUp(-2), GetUp(-1)))
            {
                Pop(2);
                return VX_OK;
            }
            break;
        case VX_OT_ARRAY:
            if(Set(self, GetUp(-2), GetUp(-1),false))
            {
                Pop(2);
                return VX_OK;
            }
            break;
        default:
            Pop(2);
            return ThrowError("rawset works only on array/table/class and instance");
    }
    return Raise_IdxError(GetUp(-2), self);
}

VXInteger VXState::ToStringAt(VXInteger idx, VXObject& dest)
{
    VXObject &o = this->StackGet(idx);
    return this->ToString(o, dest);
}

VXInteger VXState::Raise_InvalidType(VXOType type)
{
    return ThrowError("unexpected type %s", IdType2Name(type));
}


VXInteger VXState::GetSize(const VXObject& o)
{
    VXOType type = type(o);
    switch(type)
    {
        case VX_OT_STRING:
            return _string(o)->_len;
        case VX_OT_TABLE:
            return _table(o)->CountUsed();
        case VX_OT_ARRAY:
            return _array(o)->Size();
        case VX_OT_USERDATA:
            return _userdata(o)->_size;
        case VX_OT_INSTANCE:
            return _instance(o)->_class->_udsize;
        case VX_OT_CLASS:
            return _class(o)->_udsize;
        default:
            return this->Raise_InvalidType(type);
    }
}

VXInteger VXState::GetSizeAt(VXInteger idx)
{
    VXObject &o = StackGet(idx);
    return this->GetSize(o);
}

VXInteger VXState::Clear(VXObject& o)
{
    switch(type(o))
    {
        case VX_OT_TABLE:
            _table(o)->Clear();
            break;
        case VX_OT_ARRAY:
            _array(o)->Resize(0);
            break;
        default:
            return this->ThrowError("clear only works on table and array");
            break;
    }
    return VX_OK;
}


VXInteger VXState::ClearAt(VXInteger idx)
{
    return this->Clear(this->StackGet(idx));
}


VXInteger VXState::StackInfos(VXInteger level, VXStackInfos* si)
{
    VXInteger cssize = this->_callsstacksize;
    if (cssize > level)
    {
        memset(si, 0, sizeof(VXStackInfos));
        VXState::CallInfo &ci = this->_callsstack[cssize-level-1];
        switch(type(ci._closure))
        {
            case VX_OT_CLOSURE:
            {
                VXFuncProtoObj *func = _closure(ci._closure)->_function;
                si->native = false;
                if (type(func->_name) == VX_OT_STRING)
                {
                    si->funcname = _stringval(func->_name);
                }
                if (type(func->_sourcename) == VX_OT_STRING)
                {
                    si->source = _stringval(func->_sourcename);
                }
                si->line = func->GetLine(ci._ip);
            }
            break;
            case VX_OT_NATIVECLOSURE:
                {
                    si->native = true;
                    si->source = "<native>";
                    si->funcname = "<closure>";
                    if(type(_nativeclosure(ci._closure)->_name) == VX_OT_STRING)
                    {
                        si->funcname = _stringval(_nativeclosure(ci._closure)->_name);
                    }
                    si->line = -1;
                }
                break;
            default:
                break;
        }
        return VX_OK;
    }
    return VX_ERROR;
}

VXPrintFunction VXState::GetErrorFunc()
{
    return Shared()->_errorfunc;
}

VXInteger VXState::Suspend()
{
    if (_suspended)
    {
        return this->ThrowError("cannot suspend an already suspended vm");
    }
    if (_nnativecalls!=2)
    {
        return this->ThrowError("cannot suspend through native calls/metamethods");
    }
    return VX_SUSPEND_FLAG;
}

bool VXState::IsEqual(const VXObject &o1,
                      const VXObject &o2,
                      bool &res)
{
    if(type(o1) == type(o2))
    {
        res = (_rawval(o1) == _rawval(o2));
    }
    else
    {
        if(o1.IsNumeric() && o2.IsNumeric())
        {
            res = (tofloat(o1) == tofloat(o2));
        }
        else
        {
            res = false;
        }
    }
    return true;
}

bool VXState::IsFalse(VXObject &o)
{
    if(((type(o) & VXOBJECT_CANBEFALSE)
        && ( ((type(o) == VX_OT_FLOAT) && (_float(o) == VXFloat(0.0))) ))
        || (_integer(o) == 0) )  //VX_OT_NULL|OT_INTEGER|OT_BOOL
    {
        return true;
    }
    return false;
}

bool VXState::Execute(VXObject &closure,
                      VXInteger nargs,
                      VXInteger stackbase,
                      VXObject &outres,
                      bool raiseerror,
                      ExecutionType et)
{
    if ((_nnativecalls + 1) > MAX_NATIVE_CALLS)
    {
        ThrowError("native stack overflow");
        return false;
    }
    _nnativecalls++;
    AutoDec ad(&_nnativecalls);
    VXInteger traps = 0;
    CallInfo *prevci = ci;
    switch(et)
    {
        case ET_CALL:
            {
                temp_reg = closure;
                if(!StartCall(_closure(temp_reg),
                              _top - nargs,
                              nargs, stackbase, false))
                {
                    // call the handler if there are no calls in the
                    // stack, if not relies on the previous node
                    if(ci == NULL)
                    {
                        CallErrorHandler(_lasterror);
                    }
                    return false;
                }
                if(ci == prevci)
                {
                    outres = STK(_top-nargs);
                    return true;
                }
                ci->_root = true;
            }
            break;
        case ET_RESUME_GENERATOR:
            _generator(closure)->Resume(this, outres);
            ci->_root = true;
            traps += ci->_etraps;
            break;
        case ET_RESUME_VM:
        case ET_RESUME_THROW_VM:
            traps = _suspended_traps;
            ci->_root = _suspended_root;
            _suspended = false;
            if(et == ET_RESUME_THROW_VM)
            {
                VX_THROW();
            }
            break;
    }

exception_restore:
    //
    {
        for(;;)
        {
            const VXInstruction &_i_ = *ci->_ip++;
            switch(_i_.op)
            {
                case _OP_LINE:
                    if (_debughook)
                    {
                        CallDebugHook('l',arg1);
                    }
                    continue;

                case _OP_LOAD:
                    TARGET = ci->_literals[arg1];
                    continue;

                case _OP_LOADINT:
                    TARGET = (VXInteger)arg1;
                    continue;

                case _OP_LOADFLOAT:
                    TARGET = *((VXFloat *)&arg1);
                    continue;

                case _OP_DLOAD:
                    TARGET = ci->_literals[arg1];
                    STK(arg2) = ci->_literals[arg3];
                    continue;

                case _OP_TAILCALL:
                    {
                        VXInteger i;
                        VXObject &t = STK(arg1);
                        if (type(t) == VX_OT_CLOSURE
                            && (!_closure(t)->_function->_bgenerator))
                        {
                            VXObject clo = t;
                            if(_openouters)
                            {
                                CloseOuters(this, &(_stack.values()[_stackbase]));
                            }
                            for(i=0; i<arg3; i++)
                            {
                                STK(i) = STK(arg2 + i);
                            }
                            _GUARD(StartCall(_closure(clo),
                                ci->_target, arg3, _stackbase, true));
                            continue;
                        }
                    }

                case _OP_CALL:
                    {
                        VXObject clo = STK(arg1);
                        switch (type(clo))
                        {
                            case VX_OT_CLOSURE:
                                _GUARD(StartCall(_closure(clo),
                                    sarg0, arg3,
                                    _stackbase+arg2, false));
                                continue;
                            case VX_OT_NATIVECLOSURE:
                                {
                                    bool suspend;
                                    _GUARD(CallNative(
                                        _nativeclosure(clo),
                                        arg3, _stackbase+arg2,
                                        clo,suspend));
                                    if(suspend)
                                    {
                                        _suspended = true;
                                        _suspended_target = sarg0;
                                        _suspended_root = ci->_root;
                                        _suspended_traps = traps;
                                        outres = clo;
                                        return true;
                                    }
                                    if(sarg0 != -1)
                                    {
                                        STK(arg0) = clo;
                                    }
                                }
                                continue;
                            case VX_OT_CLASS:
                                {
                                    VXInteger stkbase;
                                    VXObject inst;
                                    _GUARD(CreateClassInstance(
                                        _class(clo),inst,clo));
                                    if(sarg0 != -1)
                                    {
                                        STK(arg0) = inst;
                                    }
                                    switch(type(clo))
                                    {
                                        case VX_OT_CLOSURE:
                                            stkbase = _stackbase+arg2;
                                            _stack.values()[stkbase] = inst;
                                            _GUARD(StartCall(
                                                _closure(clo), -1, arg3,
                                                stkbase, false));
                                            break;
                                        case VX_OT_NATIVECLOSURE:
                                            bool suspend;
                                            stkbase = _stackbase+arg2;
                                            _stack.values()[stkbase] = inst;

                                            _GUARD(CallNative(
                                                _nativeclosure(clo), arg3,
                                                stkbase, clo, suspend));


                                            break;
                                        default:
                                            break; //shutup GCC 4.x
                                    }

     

                                }
                                break;
                            case VX_OT_TABLE:
                            case VX_OT_USERDATA:
                            case VX_OT_INSTANCE:
                            {
                                VXObject closure;
                                if(_delegable(clo)->_delegate &&
                                   _delegable(clo)->GetMetaMethod(this,VX_MT_CALL,closure))
                                {
                                    Push(clo);
                                    for (VXInteger i=0; i<arg3; i++)
                                    {
                                        Push(STK(arg2 + i));
                                    }
                                    if(!CallMetaMethod(this, closure, VX_MT_CALL, arg3+1, clo))
                                    {
                                        VX_THROW();
                                    }
                                    if(sarg0 != -1)
                                    {
                                        STK(arg0) = clo;
                                    }
                                    break;
                                }
                            }
                            default:
                                ThrowError("attempt to call '%s'",
                                    GetTypeName(clo));
                                VX_THROW();
                        }
                    }
                    continue;

                case _OP_PREPCALL:
                case _OP_PREPCALLK:
                    {
                        VXObject &key =
                            (_i_.op == _OP_PREPCALLK) ?
                            (ci->_literals)[arg1] : STK(arg1);
                        VXObject &o = STK(arg2);
                        if (!Get(o, key, temp_reg,false,arg2))
                        {
                            VX_THROW();
                        }
                        STK(arg3) = o;
                        _Swap(TARGET,temp_reg);//TARGET = temp_reg;
                    }
                    continue;

                case _OP_GETK:
                    if (!Get(STK(arg2), ci->_literals[arg1],
                            temp_reg, false,arg2))
                    {
                        VX_THROW();
                    }
                    _Swap(TARGET,temp_reg);//TARGET = temp_reg;
                    continue;

                case _OP_MOVE:
                    TARGET = STK(arg1);
                    continue;

                case _OP_NEWSLOT:
                    _GUARD(NewSlot(STK(arg1), STK(arg2),
                            STK(arg3),false));
                    if(arg0 != 0xFF)
                    {
                        TARGET = STK(arg3);
                    }
                    continue;

                case _OP_DELETE:
                    _GUARD(DeleteSlot(STK(arg1), STK(arg2), TARGET));
                    continue;

                case _OP_SET:
                    if (!Set(STK(arg1), STK(arg2), STK(arg3),arg1))
                    {
                        VX_THROW();
                    }
                    if (arg0 != 0xFF)
                    {
                        TARGET = STK(arg3);
                    }
                    continue;

                case _OP_GET:
                    if (!Get(STK(arg1), STK(arg2), temp_reg, false,arg1))
                    {
                        VX_THROW();
                    }
                    _Swap(TARGET,temp_reg);//TARGET = temp_reg;
                    continue;

                case _OP_EQ:
                    {
                        bool res;
                        if(!IsEqual(STK(arg2),COND_LITERAL,res))
                        {
                            VX_THROW();
                        }
                        TARGET = res ? true : false;
                    }
                    continue;

                case _OP_NE:
                    {
                        bool res;
                        if(!IsEqual(STK(arg2),COND_LITERAL,res))
                        {
                            VX_THROW();
                        }
                        TARGET = (!res) ? true : false;
                    }
                    continue;

                case _OP_ADD:
                    _ARITH_(+,TARGET,STK(arg2),STK(arg1));
                    continue;

                case _OP_SUB:
                    _ARITH_(-,TARGET,STK(arg2),STK(arg1));
                    continue;

                case _OP_MUL:
                    _ARITH_(*,TARGET,STK(arg2),STK(arg1));
                    continue;

                case _OP_DIV:
                    _ARITH_NOZERO(/,TARGET,STK(arg2),
                        STK(arg1), "division by zero");
                    continue;

                case _OP_MOD:
                    _GUARD(ARITH_OP(this, '%',TARGET,STK(arg2),STK(arg1)));
                    continue;

                case _OP_BITW:
                    _GUARD(BW_OP(this, arg3,TARGET,STK(arg2),STK(arg1)));
                    continue;

                case _OP_RETURN:
                    if((ci)->_generator)
                    {
                        (ci)->_generator->Kill();
                    }
                    if(Return(this, arg0, arg1, temp_reg))
                    {
                        vox_assert(traps == 0);
                        //outres = temp_reg;
                        _Swap(outres,temp_reg);
                        return true;
                    }
                    continue;

                case _OP_LOADNULLS:
                    {
                        for(VXInt32 n=0; n < arg1; n++)
                        {
                            STK(arg0+n).Null();
                        }
                    }
                    continue;

                case _OP_LOADROOT:
                    TARGET = _roottable;
                    continue;

                case _OP_LOADBOOL:
                    TARGET = arg1?true:false;
                    continue;

                case _OP_DMOVE:
                    STK(arg0) = STK(arg1);
                    STK(arg2) = STK(arg3);
                    continue;

                case _OP_JMP:
                    ci->_ip += (sarg1);
                    continue;

                case _OP_JCMP:
                    _GUARD(CMP_OP(this, (CmpOP)arg3,STK(arg2),
                            STK(arg0),temp_reg));
                    if(IsFalse(temp_reg))
                    {
                        ci->_ip+=(sarg1);
                    }
                    continue;

                case _OP_JZ:
                    if(IsFalse(STK(arg0)))
                    {
                        ci->_ip+=(sarg1);
                    }
                    continue;

                case _OP_GETOUTER:
                    {
                        VXForeignClosureObj *cur_cls = _closure(ci->_closure);
                        VXOuterObj *otr = _outer(cur_cls->_outervalues[arg1]);
                        TARGET = *(otr->_valptr);
                    }
                    continue;

                case _OP_SETOUTER:
                    {
                        VXForeignClosureObj *cur_cls = _closure(ci->_closure);
                        VXOuterObj   *otr = _outer(cur_cls->_outervalues[arg1]);
                        *(otr->_valptr) = STK(arg2);
                        if(arg0 != 0xFF)
                        {
                            TARGET = STK(arg2);
                        }
                    }
                    continue;

                case _OP_NEWOBJ:
                    switch(arg3)
                    {
                        case NVX_OT_TABLE:
                            TARGET = VXTableObj::Create(_ss(this), arg1);
                            continue;
                        case NVX_OT_ARRAY:
                            TARGET = VXArrayObj::Create(_ss(this), 0);
                            _array(TARGET)->Reserve(arg1);
                            continue;

                        case NVX_OT_CLASS:
                            _GUARD(CLASS_OP(this, TARGET,arg1,arg2, arg3));
                            continue;

                        default:
                            vox_assert(0);
                            continue;
                    }
                case _OP_APPENDARRAY:
                    {
                        VXRawObj val;
                        val._unVal.raw = 0;
                        switch(arg2)
                        {
                            case AAT_STACK:
                                val = STK(arg1);
                                break;
                            case AAT_LITERAL:
                                val = ci->_literals[arg1];
                                break;
                            case AAT_INT:
                                val._type = VX_OT_INTEGER;
                                val._unVal.nInteger = (VXInteger)arg1;
                                break;
                            case AAT_FLOAT:
                                val._type = VX_OT_FLOAT;
                                val._unVal.fFloat = *((VXFloat *)&arg1);
                                break;
                            case AAT_BOOL:
                                val._type = VX_OT_BOOL;
                                val._unVal.nInteger = arg1;
                                break;
                            default:
                                vox_assert(0);
                                break;

                        }
                        _array(STK(arg0))->Append(val);
                        continue;
                    }

                case _OP_COMPARITH:
                    {
                        VXInteger selfidx =
                            (((VXUnsignedInteger)arg1&0xFFFF0000)>>16);
                        _GUARD(DerefInc(this, arg3, TARGET,
                            STK(selfidx), STK(arg2),
                            STK(arg1&0x0000FFFF), false, selfidx));
                    }
                    continue;

                case _OP_INC:
                    {
                        VXObject o(sarg3);
                        _GUARD(DerefInc(this, '+',TARGET, STK(arg1),
                                STK(arg2), o, false, arg1));
                    }
                    continue;

                case _OP_INCL:
                    {
                        VXObject &a = STK(arg1);
                        if(type(a) == VX_OT_INTEGER)
                        {
                            a._unVal.nInteger = _integer(a) + sarg3;
                        }
                        else
                        {
                            VXObject o(sarg3);
                            _ARITH_(+,a,a,o);
                        }
                    }
                    continue;

                case _OP_PINC:
                    {
                        VXObject o(sarg3);
                        _GUARD(DerefInc(this, '+',TARGET,
                            STK(arg1), STK(arg2), o, true, arg1));
                    }
                    continue;

                case _OP_PINCL:
                    {
                        VXObject &a = STK(arg1);
                        if(type(a) == VX_OT_INTEGER)
                        {
                            TARGET = a;
                            a._unVal.nInteger = _integer(a) + sarg3;
                        }
                        else
                        {
                            VXObject o(sarg3);
                            _GUARD(PLOCAL_INC(this, '+',TARGET, STK(arg1), o));
                        }
                    }
                    continue;

                case _OP_CMP:
                    _GUARD(CMP_OP(this, (CmpOP)arg3,
                            STK(arg2),STK(arg1),TARGET));
                    continue;

                case _OP_EXISTS:
                    TARGET =
                        Get(STK(arg1), STK(arg2),
                            temp_reg, true, DONT_FALL_BACK)
                        ? true : false;
                    continue;

                case _OP_INSTANCEOF:
                    if(type(STK(arg1)) != VX_OT_CLASS)
                    {
                        ThrowError(
                            "cannot apply instanceof between a %s and a %s",
                            GetTypeName(STK(arg1)),
                            GetTypeName(STK(arg2)));
                        VX_THROW();
                    }
                    TARGET =
                        (type(STK(arg2)) == VX_OT_INSTANCE) ?
                        (_instance(STK(arg2))->InstanceOf(_class(STK(arg1)))
                            ? true : false) : false;
                    continue;

                case _OP_AND:
                    if(IsFalse(STK(arg2)))
                    {
                        TARGET = STK(arg2);
                        ci->_ip += (sarg1);
                    }
                    continue;

                case _OP_OR:
                    if(!IsFalse(STK(arg2)))
                    {
                        TARGET = STK(arg2);
                        ci->_ip += (sarg1);
                    }
                    continue;

                case _OP_NEG:
                    _GUARD(NEG_OP(this, TARGET,STK(arg1)));
                    continue;

                case _OP_NOT:
                    TARGET = IsFalse(STK(arg1));
                    continue;

                case _OP_BWNOT:
                    if(type(STK(arg1)) == VX_OT_INTEGER)
                    {
                        VXInteger t = _integer(STK(arg1));
                        TARGET = VXInteger(~t);
                        continue;
                    }
                    ThrowError(
                        "attempt to perform a bitwise op on a %s",
                        GetTypeName(STK(arg1)));
                    VX_THROW();

                case _OP_CLOSURE:
                {
                    VXForeignClosureObj *c = ci->_closure._unVal.pClosure;
                    VXFuncProtoObj *fp = c->_function;
                    if(!CLOSURE_OP(this, TARGET,
                        fp->_functions[arg1]._unVal.pFunctionProto))
                    {
                        VX_THROW();
                    }
                    continue;
                }
                case _OP_YIELD:
                    {
                        if(ci->_generator)
                        {
                            if(sarg1 != MAX_FUNC_STACKSIZE)
                                temp_reg = STK(arg1);
                            _GUARD(ci->_generator->Yield(this,arg2));
                            traps -= ci->_etraps;
                            if(sarg1 != MAX_FUNC_STACKSIZE)
                                _Swap(STK(arg1),temp_reg);
                        }
                        else
                        {
                            ThrowError(
                                "trying to yield a '%s',"
                                "only genenerator can be yielded",
                                GetTypeName(ci->_generator));
                            VX_THROW();
                        }
                        if(Return(this, arg0, arg1, temp_reg))
                        {
                            vox_assert(traps == 0);
                            outres = temp_reg;
                            return true;
                        }

                    }
                    continue;

                case _OP_RESUME:
                    if(type(STK(arg1)) != VX_OT_GENERATOR)
                    {
                        ThrowError(
                            "trying to resume a '%s',"
                            " only a genenerator can be resumed",
                            GetTypeName(STK(arg1)));
                            VX_THROW();
                    }
                    _GUARD(_generator(STK(arg1))->Resume(this, TARGET));
                    traps += ci->_etraps;
                    continue;

                case _OP_FOREACH:
                    {
                        int tojump;
                        _GUARD(FOREACH_OP(this, STK(arg0),STK(arg2),
                                STK(arg2+1),STK(arg2+2),arg2,
                                sarg1,tojump));
                        ci->_ip += tojump;
                    }
                    continue;

                case _OP_POSTFOREACH:
                    vox_assert(type(STK(arg0)) == VX_OT_GENERATOR);
                    if(_generator(STK(arg0))->_state == VXGeneratorObj::eDead)
                        ci->_ip += (sarg1 - 1);
                    continue;

                case _OP_CLONE:
                    _GUARD(Clone(STK(arg1), TARGET));
                    continue;

                case _OP_TYPEOF:
                    _GUARD(TypeOf(STK(arg1), TARGET));
                    continue;

                case _OP_PUSHTRAP:
                    {
                        VXInstruction *_iv =
                            _closure(ci->_closure)->_function->_instructions;
                        _etraps.push_back(VXExceptionTrap(
                            _top,_stackbase,
                            &_iv[(ci->_ip-_iv)+arg1], arg0));
                        traps++;
                        ci->_etraps++;
                    }
                    continue;

                case _OP_POPTRAP:
                    {
                        for(VXInteger i=0; i<arg0; i++)
                        {
                            _etraps.pop_back(); traps--;
                            ci->_etraps--;
                        }
                    }
                    continue;

                case _OP_THROW:
                    ThrowError(TARGET);
                    VX_THROW();
                    continue;

                case _OP_NEWSLOTA:
                    {
                        bool bstatic =
                            (arg0&NEW_SLVX_OT_STATIC_FLAG)?true:false;
                        if(type(STK(arg1)) == VX_OT_CLASS)
                        {
                            if(type(_class(STK(arg1))->
                                    _metamethods[VX_MT_NEWMEMBER]) != VX_OT_NULL)
                            {
                                Push(STK(arg1));
                                Push(STK(arg2));
                                Push(STK(arg3));
                                Push((arg0&NEW_SLVX_OT_ATTRIBUTES_FLAG)?
                                    STK(arg2-1) : VXObject());
                                Push(bstatic);
                                int nparams = 5;
                                if(Call(_class(STK(arg1))->
                                    _metamethods[VX_MT_NEWMEMBER],
                                    nparams, _top - nparams,
                                    temp_reg,false))
                                {
                                    Pop(nparams);
                                    continue;
                                }
                                else
                                {
                                    VX_THROW();
                                }
                            }
                        }
                        _GUARD(NewSlot(STK(arg1), STK(arg2),
                                STK(arg3),bstatic));
                        if((arg0&NEW_SLVX_OT_ATTRIBUTES_FLAG))
                        {
                            _class(STK(arg1))->SetAttributes(
                                STK(arg2), STK(arg2-1));
                        }
                    }
                    continue;

                case _OP_GETBASE:
                    {
                        VXForeignClosureObj *clo = _closure(ci->_closure);
                        if(clo->_base)
                        {
                            TARGET = clo->_base;
                        }
                        else
                        {
                            TARGET.Null();
                        }
                        continue;
                    }

                case _OP_CLOSE:
                    if(_openouters)
                    {
                        CloseOuters(this, &(STK(arg1)));
                    }
                    continue;
            }
        }
    }

exception_trap:
    {
        VXObject currerror = _lasterror;
        VXInteger last_top = _top;
        if(_ss(this)->_notifyallexceptions || (!traps && raiseerror))
        {
            CallErrorHandler(currerror);
        }
        while(ci)
        {
            if(ci->_etraps > 0)
            {
                VXExceptionTrap &et = _etraps.top();
                ci->_ip = et._ip;
                _top = et._stacksize;
                _stackbase = et._stackbase;
                _stack.values()[_stackbase + et._extarget] = currerror;
                _etraps.pop_back(); traps--; ci->_etraps--;
                while(last_top >= _top)
                {
                    _stack.values()[last_top--].Null();
                }
                goto exception_restore;
            }
            else if (_debughook)
            {
                //notify debugger of a "return"
                //even if it really an exception unwinding the stack
                for(VXInteger i=0; i<(ci->_ncalls); i++)
                {
                    CallDebugHook('r');
                }
            }
            if(ci->_generator)
            {
                ci->_generator->Kill();
            }
            bool mustbreak = ci && ci->_root;
            LeaveFrame();
            if(mustbreak)
            {
                break;
            }
        }
        _lasterror = currerror;
        return false;
    }
    vox_assert(0);
}

bool VXState::CreateClassInstance(VXClassObj *theclass,
                                VXObject &inst,
                                VXObject &constructor)
{
    inst = theclass->CreateInstance();
    if(!theclass->GetConstructor(constructor))
    {
        constructor.Null();
    }
    return true;
}

void VXState::CallErrorHandler(VXObject &error)
{
    if(type(_errorhandler) != VX_OT_NULL)
    {
        VXObject out;
        if(_errorhandler.Type() == VX_OT_NATIVECLOSURE)
        {
            _errorhandler.NativeClosure()->_nparamscheck = 2;
        }
        Push(_roottable);
        Push(error);
        if(!Call(_errorhandler, 2, _top-2, out, false))
        {
            // the irony is killing me!
            Irrecoverable_Error("failed to call errorhandler!");
        }
        Pop(2);
    }
}


void VXState::CallDebugHook(VXInteger type,
                            VXInteger forcedline)
{
    VXFuncProtoObj* func;
    VXInteger line;
    const char* src;
    const char* fname;
    _debughook = false;
    func =_closure(ci->_closure)->_function;
    if(_debughook_native)
    {
        src = type(func->_sourcename) == VX_OT_STRING ?
                _stringval(func->_sourcename) : NULL;
        fname = type(func->_name) == VX_OT_STRING ?
                    _stringval(func->_name) : NULL;
        line = forcedline ? forcedline : func->GetLine(ci->_ip);
        _debughook_native(this,type,src,line,fname);
    }
    else
    {
        VXObject temp_reg;
        VXInteger nparams=5;
        Push(_roottable);
        Push(VXInteger(type));
        Push(func->_sourcename);
        Push(VXInteger(forcedline?forcedline:func->GetLine(ci->_ip)));
        Push(func->_name);
        Call(_debughook_closure,nparams,_top-nparams,temp_reg,false);
        Pop(nparams);
    }
    _debughook = true;
}

bool VXState::CallNative(VXNativeClosureObj *nclosure,
                         VXInteger nargs,
                         VXInteger newbase,
                         VXObject &retval,
                         bool &suspend)
{
    VXInteger nparamscheck = nclosure->_nparamscheck;
    VXInteger newtop = newbase + nargs + nclosure->_noutervalues;
    if(_nnativecalls + 1 > MAX_NATIVE_CALLS)
    {
        ThrowError("native stack overflow");
        return false;
    }
    if(nparamscheck && (((nparamscheck > 0) && (nparamscheck != nargs)) ||
       ((nparamscheck < 0) && (nargs < (-nparamscheck)))))
    {
        return WrongNumberOfArguments(this, false, true, nclosure);
    }
    VXInteger tcs;
    VXIntVec &tc = nclosure->_typecheck;
    if((tcs = tc.size()))
    {
        for(VXInteger i = 0; i < nargs && i < tcs; i++)
        {
            if((tc.values()[i] != -1) && !(type(_stack.values()[newbase+i]) & tc.values()[i]))
            {
                Raise_ParamTypeError(i,tc.values()[i],type(_stack.values()[newbase+i]));
                return false;
            }
        }
    }
    if(!EnterFrame(newbase, newtop, false))
    {
        return false;
    }
    ci->_closure  = nclosure;
    VXInteger outers = nclosure->_noutervalues;
    for (VXInteger i = 0; i < outers; i++)
    {
        _stack.values()[newbase+nargs+i] = nclosure->_outervalues[i];
    }
    if(nclosure->_env)
    {
        _stack.values()[newbase] = nclosure->_env->_obj;
    }
    _nnativecalls++;
    VXInteger ret = (nclosure->_function)(this);
    _nnativecalls--;
    suspend = false;
    if (ret == VX_SUSPEND_FLAG)
    {
        suspend = true;
    }
    else if (ret < 0)
    {
        LeaveFrame();
        ThrowError(_lasterror);
        return false;
    }
    if(ret)
    {
        retval = _stack.values()[_top-1];
    }
    else
    {
        retval.Null();
    }
    //retval = ret ? _stack.values()[_top-1] : _null_;
    LeaveFrame();
    return true;
}



bool VXState::Get(const VXObject &self,
                  const VXObject &key,
                  VXObject &dest,
                  bool raw,
                  VXInteger selfidx)
{
    switch(type(self))
    {
        case VX_OT_TABLE:
            if(_table(self)->Get(key,dest))
            {
                return true;
            }
            break;
        case VX_OT_ARRAY:
            if(key.IsNumeric())
            {
                if(self.Array()->Get(key.Integer(), dest))
                {
                    return true;
                }
                Raise_IdxError(key, self, true);
                return false;
            }
            break;
        case VX_OT_INSTANCE:
            if(_instance(self)->Get(key,dest))
            {
                return true;
            }
            break;
        case VX_OT_CLASS:
            if(_class(self)->Get(key,dest))
            {
                return true;
            }
            break;
        case VX_OT_STRING:
            if(key.IsNumeric())
            {
                VXInteger n = tointeger(key);
                if(abs((int)n) < _string(self)->_len)
                {
                    if(n < 0)
                    {
                        n = ((_string(self)->_len) - n);
                    }
                    dest = VXInteger(_stringval(self)[n]);
                    return true;
                }
                Raise_IdxError(key, self, true);
                return false;
            }
            break;
        default:
            break; //shut up compiler
    }
    if(!raw)
    {
        switch(FallBackGet(self,key,dest))
        {
            case FALLBACK_OK:
                return true; //okie
            case FALLBACK_NO_MATCH:
                break; //keep falling back
            case FALLBACK_ERROR:
                return false; // the metamethod failed
        }
        if(InvokeDefaultDelegate(self,key,dest))
        {
            return true;
        }
    }
    if(selfidx == 0)
    {
        if(_table(_roottable)->Get(key,dest))
        {
            return true;
        }
    }
    Raise_IdxError(key, self);
    return false;
}

bool VXState::InvokeDefaultDelegate(const VXObject &self,
                                    const VXObject &key,
                                    VXObject &dest)
{
    VXTableObj *ddel = NULL;
    switch(type(self))
    {
        case VX_OT_CLASS:
            ddel = _class_ddel;
            break;
        case VX_OT_TABLE:
            ddel = _table_ddel;
            break;
        case VX_OT_ARRAY:
            ddel = _array_ddel;
            break;
        case VX_OT_STRING:
            ddel = _string_ddel;
            break;
        case VX_OT_INSTANCE:
            ddel = _instance_ddel;
            break;
        case VX_OT_INTEGER:
        case VX_OT_FLOAT:
        case VX_OT_BOOL:
            ddel = _number_ddel;
            break;
        case VX_OT_GENERATOR:
            ddel = _generator_ddel;
            break;
        case VX_OT_CLOSURE:
        case VX_OT_NATIVECLOSURE:
            ddel = _closure_ddel;
            break;
        case VX_OT_THREAD:
            ddel = _thread_ddel;
            break;
        case VX_OT_WEAKREF:
            ddel = _weakref_ddel;
            break;
        default:
            return false;
    }
    return  ddel->Get(key,dest);
}


VXInteger VXState::FallBackGet(const VXObject &self,
                               const VXObject &key,
                               VXObject &dest)
{
    switch(type(self))
    {
        case VX_OT_TABLE:
        case VX_OT_USERDATA:
            //delegation
            if(_delegable(self)->_delegate)
            {
                if(Get(VXObject(_delegable(self)->_delegate),
                       key,dest,false,DONT_FALL_BACK))
                {
                    return FALLBACK_OK;
                }
            }
            else
            {
                return FALLBACK_NO_MATCH;
            }
            //go through
        case VX_OT_INSTANCE:
            {
                VXObject closure;
                if(_delegable(self)->GetMetaMethod(this, VX_MT_GET, closure))
                {
                    Push(self);
                    Push(key);
                    _nmetamethodscall++;
                    AutoDec ad(&_nmetamethodscall);
                    if(Call(closure, 2, _top - 2, dest, false))
                    {
                        Pop(2);
                        return FALLBACK_OK;
                    }
                    else
                    {
                        //NULL means "clean failure" (not found)
                        if(type(_lasterror) != VX_OT_NULL)
                        {
                            //error
                            Pop(2);
                            return FALLBACK_ERROR;
                        }
                    }
                }
            }
            break;
        default:
            break;//shutup GCC 4.x
    }
    // no metamethod or no fallback type
    return FALLBACK_NO_MATCH;
}

bool VXState::Set(const VXObject &self,
                  const VXObject &key,
                  const VXObject &val,
                  VXInteger selfidx)
{
    switch(type(self))
    {
        case VX_OT_TABLE:
            if(_table(self)->Set(key,val))
            {
                return true;
            }
            break;
        case VX_OT_CLASS:
            if(self.Class()->NewSlot(Shared(), key, val, false))
            {
                return true;
            }
            break;
        case VX_OT_INSTANCE:
            if(_instance(self)->Set(key,val))
            {
                return true;
            }
            break;
        case VX_OT_ARRAY:
            if(!key.IsNumeric())
            {
                ThrowError("cannot index %s with type %s",
                    GetTypeName(self), GetTypeName(key));
                return false;
            }
            if(!_array(self)->Set(tointeger(key), val))
            {
                Raise_IdxError(key, self);
                return false;
            }
            return true;
        default:
            ThrowError("type '%s' does not support index assignment", GetTypeName(self));
            return false;
    }
    switch(FallBackSet(self,key,val))
    {
        case FALLBACK_OK:
            return true; //okie
        case FALLBACK_NO_MATCH:
            break; //keep falling back
        case FALLBACK_ERROR:
            return false; // the metamethod failed
    }
    if(selfidx == 0)
    {
        if(_table(_roottable)->Set(key,val))
        {
            return true;
        }
    }
    Raise_IdxError(key, self);
    return false;
}

VXInteger VXState::FallBackSet(const VXObject &self,
                               const VXObject &key,
                               const VXObject &val)
{
    switch(type(self))
    {
        case VX_OT_TABLE:
            if(_table(self)->_delegate)
            {
                if(Set(_table(self)->_delegate,key,val,DONT_FALL_BACK))
                {
                    return FALLBACK_OK;
                }
            }
            //keps on going
        case VX_OT_INSTANCE:
        case VX_OT_USERDATA:
            {
                VXObject closure;
                VXObject t;
                if(_delegable(self)->GetMetaMethod(this, VX_MT_SET, closure))
                {
                    Push(self);
                    Push(key);
                    Push(val);
                    _nmetamethodscall++;
                    AutoDec ad(&_nmetamethodscall);
                    if(Call(closure, 3, _top - 3, t, false))
                    {
                        Pop(3);
                        return FALLBACK_OK;
                    }
                    else
                    {
                        //NULL means "clean failure" (not found)
                        if(type(_lasterror) != VX_OT_NULL)
                        {
                            //error
                            Pop(3);
                            return FALLBACK_ERROR;
                        }
                    }
                }
            }
            break;
        default:
            break;//shutup GCC 4.x
    }
    // no metamethod or no fallback type
    return FALLBACK_NO_MATCH;
}

bool VXState::Clone(const VXObject& self,
                    VXObject &target)
{
    VXObject temp_reg;
    VXObject newobj;
    switch(type(self))
    {
        case VX_OT_TABLE:
            newobj = _table(self)->Clone();
            goto cloned_mt;
        case VX_OT_INSTANCE:
            {
                newobj = _instance(self)->Clone(_ss(this));
cloned_mt:
                VXObject closure;
                if(_delegable(newobj)->_delegate &&
                   _delegable(newobj)->GetMetaMethod(this,VX_MT_CLONED,closure))
                {
                    Push(newobj);
                    Push(self);
                    if(!CallMetaMethod(this, closure,VX_MT_CLONED,2,temp_reg))
                    {
                        return false;
                    }
                }
            }
            target = newobj;
            return true;
        case VX_OT_ARRAY:
            target = _array(self)->Clone();
            return true;
        default:
            ThrowError("cannot clone a '%s'", GetTypeName(self));
            return false;
    }
}

bool VXState::NewSlot(const VXObject &self,
                      const VXObject &key,
                      const VXObject &val,
                      bool bstatic)
{
    if(key.Type() == VX_OT_NULL)
    {
        ThrowError("null cannot be used as index");
        return false;
    }
    switch(type(self))
    {
        case VX_OT_TABLE:
        {
            bool rawcall = true;
            if(self.Table()->_delegate)
            {
                VXObject res;
                if(!self.Table()->Get(key,res))
                {
                    VXObject closure;
                    if(_delegable(self)->_delegate &&
                       _delegable(self)->GetMetaMethod(this,VX_MT_NEWSLOT,closure))
                    {
                        Push(self);
                        Push(key);
                        Push(val);
                        if(!CallMetaMethod(this, closure,VX_MT_NEWSLOT,3,res))
                        {
                            return false;
                        }
                        rawcall = false;
                    }
                    else
                    {
                        rawcall = true;
                    }
                }
            }
            if(rawcall)
            {
                _table(self)->NewSlot(key,val); //cannot fail
            }
            break;
        }
        case VX_OT_INSTANCE:
            {
                VXObject res;
                VXObject closure;
                if(_delegable(self)->_delegate &&
                   _delegable(self)->GetMetaMethod(this, VX_MT_NEWSLOT, closure))
                {
                    Push(self);
                    Push(key);
                    Push(val);
                    if(!CallMetaMethod(this, closure, VX_MT_NEWSLOT, 3,res))
                    {
                        return false;
                    }
                    break;
                }
                //ThrowError("class instances do not support the new slot operator");
                ThrowError("cannot create slots in class instances; use 'local' instead!");
                return false;
                break;
            }
        case VX_OT_CLASS:
            if(!_class(self)->NewSlot(Shared(), key, val, bstatic))
            {
                if(_class(self)->_locked)
                {
                    ThrowError("trying to modify a class that has already been instantiated");
                    return false;
                }
                else
                {
                    VXObject oval = PrintObjVal(key);
                    ThrowError("property '%s' already exists",_stringval(oval));
                    return false;
                }
            }
            break;
        default:
            ThrowError("indexing %s with %s", GetTypeName(self),GetTypeName(key));
            return false;
            break;
    }
    return true;
}



bool VXState::DeleteSlot(const VXObject &self,
                         const VXObject &key,
                         VXObject &res)
{
    switch(type(self))
    {
        case VX_OT_TABLE:
        case VX_OT_INSTANCE:
        case VX_OT_USERDATA:
            {
                VXObject t;
                //bool handled = false;
                VXObject closure;
                if(_delegable(self)->_delegate &&
                   _delegable(self)->GetMetaMethod(this,VX_MT_DELSLOT,closure))
                {
                    Push(self);
                    Push(key);
                    return CallMetaMethod(this, closure,VX_MT_DELSLOT,2,res);
                }
                else
                {
                    if(type(self) == VX_OT_TABLE)
                    {
                        if(_table(self)->Get(key,t))
                        {
                            _table(self)->Remove(key);
                        }
                        else
                        {
                            Raise_IdxError((VXRawObj &)key, self);
                            return false;
                        }
                    }
                    else
                    {
                        ThrowError("cannot delete a slot from %s",GetTypeName(self));
                        return false;
                    }
                }
                res = t;
            }
            break;
        default:
            ThrowError("attempt to delete a slot from a %s",GetTypeName(self));
            return false;
    }
    return true;
}

bool VXState::Call(VXObject &closure,
                   VXInteger nparams,
                   VXInteger stackbase,
                   VXObject &outres,
                   bool raiseerror)
{
    switch(type(closure))
    {
        case VX_OT_CLOSURE:
            return Execute(closure, nparams, stackbase, outres, raiseerror);
            break;
        case VX_OT_NATIVECLOSURE:
            {
                bool suspend;
                return CallNative(_nativeclosure(closure), nparams, stackbase, outres,suspend);
            }
            break;
        case VX_OT_CLASS:
            {
                VXObject constr;
                VXObject temp;
                CreateClassInstance(_class(closure),outres,constr);
                if(type(constr) != VX_OT_NULL)
                {
                    _stack[stackbase] = outres;
                    return Call(constr,nparams,stackbase,temp,raiseerror);
                }
                return true;
            }
            break;
        default:
            // populate LastError() with a meaningful message
            ThrowError("Call(): not a closure (type: %s)", closure.TypeString());
            return false;
    }
    return true;
}


bool VXState::EnterFrame(VXInteger newbase, VXInteger newtop, bool tailcall)
{
    if( !tailcall )
    {
        if( _callsstacksize == _alloccallsstacksize )
        {
            GrowCallStack();
        }
        ci = &_callsstack[_callsstacksize++];
        ci->_prevstkbase = (VXInt32)(newbase - _stackbase);
        ci->_prevtop = (VXInt32)(_top - _stackbase);
        ci->_etraps = 0;
        ci->_ncalls = 1;
        ci->_generator = NULL;
        ci->_root = false;
    }
    else
    {
        ci->_ncalls++;
    }
    _stackbase = newbase;
    _top = newtop;
    if(newtop + MIN_STACK_OVERHEAD > (VXInteger)_stack.size())
    {
        if(_nmetamethodscall)
        {
            ThrowError("stack overflow, cannot resize stack while in a metamethod");
            return false;
        }
        _stack.resize(_stack.size() + (MIN_STACK_OVERHEAD << 2));
        RelocateOuters(this);
    }
    return true;
}

void VXState::LeaveFrame()
{
    VXInteger last_top = _top;
    VXInteger last_stackbase = _stackbase;
    VXInteger css = --_callsstacksize;
    /* First clean out the call stack frame */
    ci->_closure.Null();
    _stackbase -= ci->_prevstkbase;
    _top = _stackbase + ci->_prevtop;
    ci = (css) ? &_callsstack[css-1] : NULL;
    if(_openouters)
    {
        CloseOuters(this, &(_stack.values()[last_stackbase]));
    }
    while (last_top >= _top)
    {
        _stack.values()[last_top--].Null();
    }
}

void VXState::Remove(VXInteger n)
{
    n = (n >= 0)?n + _stackbase - 1:_top + n;
    for(VXInteger i = n; i < _top; i++)
    {
        _stack[i] = _stack[i+1];
    }
    _stack[_top].Null();
    _top--;
}

void VXState::Pop()
{
    _stack[--_top].Null();
}

void VXState::Pop(VXInteger n)
{
    for(VXInteger i = 0; i < n; i++)
    {
        _stack[--_top].Null();
    }
}

void VXState::PushNull()
{
    _stack[_top++].Null();
}

void VXState::Push(const VXObject &o)
{
    _stack[_top++] = o;
}

VXObject& VXState::Top()
{
    return _stack[_top-1];
}

VXObject& VXState::PopGet()
{
    return _stack[--_top];
}

VXObject& VXState::GetUp(VXInteger n)
{
    return _stack[_top+n];
}

VXObject& VXState::GetAt(VXInteger n)
{
    return _stack[n];
}
