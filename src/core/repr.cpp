
#include "baselib.hpp"
#include "string.hpp"
#include <sstream>

#define Err(...) \
    { \
        this->returns = v->ThrowError(__VA_ARGS__); \
        return; \
    }

VXRepr::VXRepr(VXState* _v, const VXObject& ob): v(_v), root_ob(ob)
{
    returns = VX_OK;
    do_serialize(root_ob);
}

void VXRepr::do_serialize(const VXObject& o)
{
    switch(o.Type())
    {
        case VX_OT_NULL:
            strm << "null";
            break;
        case VX_OT_CLOSURE:
        case VX_OT_NATIVECLOSURE:
            /* TODO: this is where the function proto should be emitted */
        case VX_OT_INSTANCE:
        case VX_OT_CLASS:
        case VX_OT_WEAKREF:
            emit_othertype(o);
            break;
        case VX_OT_STRING:
            emit_string(o);
            break;
        case VX_OT_ARRAY:
            emit_array(o);
            break;
        case VX_OT_TABLE:
            emit_table(o);
            break;
        case VX_OT_INTEGER:
            strm << o.Integer();
            break;
        case VX_OT_FLOAT:
            strm << o.Float();
            break;
        case VX_OT_BOOL:
            strm << (o.Boolean() ? "true" : "false");
            break;
        default:
            Err("cannot serialize type '%s'", o.TypeString());
            break;
    }
}

void VXRepr::emit_othertype(const VXObject& o)
{
    //const VXUserPointer o_addr = VXUserPointer(&o);
    //strm << "<" << o.TypeString() << " at " << o_addr << ">";
    VXObject val;
    v->ToString(o, val);
    strm.write(val.String()->Value(), val.String()->Length());
}

void VXRepr::emit_string(const VXObject& o)
{
    std::string tmp;
    const char* str_p;
    VXInteger len_p;
    o.String(&str_p, &len_p);
    tmp.append(str_p, len_p);
    strm << strhelp_quote(tmp);
}

void VXRepr::emit_table(const VXObject& o)
{
    VXInteger size;
    VXInteger count = 0;
    VXInteger ridx = 0; // start at first index
    VXObject outkey;
    VXObject outval;
    VXTableObj* tb = o.Table();
    size = tb->Size();
    strm << "{";
    while((ridx = tb->Next(false, ridx, outkey, outval)) != -1)
    {
        do_serialize(outkey);
        strm << ": ";
        do_serialize(outval);
        if((count+1) != size)
        {
            strm << ", ";
        }
        count++;
    }
    strm << "}";
}

void VXRepr::emit_array(const VXObject& o)
{
    VXInteger size;
    VXInteger ridx = 0; // start at first index
    VXObject outkey;
    VXObject outval;
    VXArrayObj* arr = o.Array();
    size = arr->Size();
    strm << "[";
    while((ridx = arr->Next(ridx, outkey, outval)) != -1)
    {
        do_serialize(outval);
        if(ridx != size)
        {
            strm << ", ";
        }
    }
    strm << "]";
}

std::string VXRepr::str()
{
    return this->strm.str();
}

VXInteger VXRepr::status()
{
    return returns;
}
