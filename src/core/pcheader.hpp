
#ifndef _VXPCHEADER_H_
#define _VXPCHEADER_H_

#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>
#include <string.h>
#include <new>
#include <errno.h>
#include "errors.hpp"
#include "vox.h"

#define VX_VMSTATE_IDLE   0
#define VX_VMSTATE_RUNNING  1
#define VX_VMSTATE_SUSPENDED 2

#define VOX_EOB 0

#define VX_MATCHTYPEMASKSTRING (-99999)
#define VX_RAW_TYPE(type) ((type) & VX_RT_MASK)

#define ISREFCOUNTED(t) (t & VXOBJECT_REF_COUNTED)

#define _PRINT_INT_FMT "%lld"

#define VOX_MAX_CHAR 0xFF

// VX_ALIGNMENT shall be less than or equal to VX_MALLOC alignments, and
// its value shall be power of 2.
#define VX_ALIGNMENT 4

#define VX_BYTECODE_STREAM_TAG 0xFAFA

#define DOSTRING_DEFAULTBUFFERNAME "<dostring>"

#define vox_new(__ptr, __type) \
    { \
        __ptr = (__type *)vox_mem_malloc(sizeof(__type)); \
        new(__ptr) __type; \
    }

#define vox_delete(__ptr,__type) \
    { \
        __ptr->~__type(); \
        vox_mem_free(__ptr, sizeof(__type)); \
    }

#define vox_aligning(v) (((size_t)(v) + (VX_ALIGNMENT-1)) & (~(VX_ALIGNMENT-1)))

#endif                           //_VXPCHEADER_H_
