
#include "baselib.hpp"

VXInteger class_getattributes(VXState* v)
{
    VXObject& o = v->StackGet(1);
    VXObject &key = v->StackGet(2);
    VXObject attrs;
    if(o.Class()->GetAttributes(key,attrs))
    {
        v->Push(attrs);
        return 1;
    }
    return v->ThrowError("wrong index");
}


VXInteger class_setattributes(VXState* v)
{
    VXObject& o = v->StackGet(1);
    VXObject& key = v->StackGet(2);
    VXObject& val = v->StackGet(3);
    VXObject attrs;
    if(key.Type() == VX_OT_NULL)
    {
        attrs = o.Class()->_attributes;
        o.Class()->_attributes = val;
        v->Push(attrs);
        return 1;
    }
    else if(o.Class()->GetAttributes(key,attrs))
    {
        o.Class()->SetAttributes(key, val);
        v->Push(attrs);
        return 1;
    }
    return v->ThrowError("wrong index");
}

VXInteger class_instance(VXState* v)
{
    VXObject& o = v->StackGet(1);
    v->Push(o.Class()->CreateInstance());
    return 1;
}

VXInteger class_getbase(VXState* v)
{
    VXObject& o = v->StackGet(1);
    if(o.Class()->_base)
    {
        v->Push(o.Class()->_base);
        return 1;
    }
    return 0;
}

VXInteger instance_getclass(VXState* v)
{
    VXObject& o = v->StackGet(1);
    v->Push(o.Instance()->_class);
    return 1;
}


VXRegFunction VXSharedState::_class_default_delegate_funcz[] =
{
    {"weakref",       obj_delegate_weakref,         1, NULL },
    {"tostring",      default_delegate_tostring,   -1, ".b"},
    {"rawget",        container_rawget,             2, "y"},
    {"rawset",        container_rawset,             3, "y"},
    {"instance",      class_instance,               1, "y"},
    {"getbase",       class_getbase,                1, "y"},
    {"getattributes", class_getattributes,          2, "y."},
    {"setattributes", class_setattributes,          3, "y.."},
    {"repr",          default_delegate_repr,        1, "."},
    {0, 0, 0, 0}
};


VXRegFunction VXSharedState::_instance_default_delegate_funcz[] =
{
    {"weakref",   obj_delegate_weakref,      1,  NULL },
    {"tostring",  default_delegate_tostring, -1, ".b"},
    {"rawget",    container_rawget,          2,  "x"},
    {"rawset",    container_rawset,          3,  "x"},
    {"getclass",  instance_getclass,         1,  "x"},
    {"repr",      default_delegate_repr,     1,  "."},
    {0, 0, 0, 0}
};

