
#ifndef _VXOBJECT_H_
#define _VXOBJECT_H_

#include "utils.hpp"

#define UINT_MINUS_ONE (0xFFFFFFFF)

#define VX_OBJECT_RAWINIT(selfptr) { selfptr->_unVal.raw = 0; }
#define VX_REFOBJECT_INIT(selfptr) VX_OBJECT_RAWINIT(selfptr)

/*
#define VX_CLOSURESTREAM_HEAD (('L' << 24) | ('X' << 16) | ('O' << 8) | 'V')
#define VX_CLOSURESTREAM_PART (('P' << 24) | ('A' << 16) |('R' << 8) | 'T')
#define VX_CLOSURESTREAM_TAIL (('T' << 24) | ('A' << 16) |('I' << 8) | 'L')
*/

#define VX_CLOSURESTREAM_HEAD \
    (('!' << 24) | ('X' << 16) | ('O' << 8) | 'V')

#define VX_CLOSURESTREAM_PART \
    (('T' << 24) | (10 << 16) | ('P' << 8) | 100)

#define VX_CLOSURESTREAM_TAIL \
    (('L' << 24) | (10 << 16) | ('T' << 8) | 200)

#define _CHECK_IO(exp)  { if(!exp)return false; }

#define VX_MM_ADD       "__add__"
#define VX_MM_SUB       "__sub__"
#define VX_MM_MUL       "__mul__"
#define VX_MM_DIV       "__div__"
#define VX_MM_UNM       "__unm__"
#define VX_MM_MODULO    "__modulo__"
#define VX_MM_SET       "__set__"
#define VX_MM_GET       "__get__"
#define VX_MM_TYPEOF    "__typeof__"
#define VX_MM_NEXTI     "__nexti__"
#define VX_MM_CMP       "__cmp__"
#define VX_MM_CALL      "__call__"
#define VX_MM_CLONED    "__cloned__"
#define VX_MM_NEWSLOT   "__newslot__"
#define VX_MM_DELSLOT   "__delslot__"
#define VX_MM_TOSTRING  "__tostring__"
#define VX_MM_NEWMEMBER "__newmember__"
#define VX_MM_INHERITED "__inherited__"


#define VX_CONSTRUCT_VECTOR(type,size,ptr) \
{ \
    for(VXInteger n = 0; n < ((VXInteger)size); n++) { \
            new (&ptr[n]) type(); \
        } \
}

#define VX_DESTRUCT_VECTOR(type,size,ptr) { \
    for(VXInteger nl = 0; nl < ((VXInteger)size); nl++) { \
            ptr[nl].~type(); \
    } \
}

#define VX_COPY_VECTOR(dest,src,size) { \
    for(VXInteger _n_ = 0; _n_ < ((VXInteger)size); _n_++) { \
        dest[_n_] = src[_n_]; \
    } \
}


#define VX_REALVAL(o) \
    (type((o)) != VX_OT_WEAKREF?(VXRawObj)o:_weakref(o)->_obj)


#define VX_ADDREF(type,unval) if(ISREFCOUNTED(type)) \
        { \
            unval.pRefCounted->_uiRef++; \
        }

#define __Release(type,unval) \
        if(ISREFCOUNTED(type) && ((--unval.pRefCounted->_uiRef)==0))  \
        {   \
            unval.pRefCounted->Release();   \
        }

#define __ObjRelease(obj) { \
    if((obj)) { \
        (obj)->_uiRef--; \
        if((obj)->_uiRef == 0) \
            (obj)->Release(); \
        (obj) = NULL;   \
    } \
}

#define __ObjAddRef(obj) { \
    (obj)->_uiRef++; \
}

#define type(obj) ((obj)._type)
#define is_delegable(t) (t.Type() & VXOBJECT_DELEGABLE)

#define _integer(obj) ((obj)._unVal.nInteger)
#define _float(obj) ((obj)._unVal.fFloat)
#define _string(obj) ((obj)._unVal.pString)
#define _table(obj) ((obj)._unVal.pTable)
#define _array(obj) ((obj)._unVal.pArray)
#define _closure(obj) ((obj)._unVal.pClosure)
#define _generator(obj) ((obj)._unVal.pGenerator)
#define _nativeclosure(obj) ((obj)._unVal.pNativeClosure)
#define _userdata(obj) ((obj)._unVal.pUserData)
#define _userpointerval(obj) ((obj)._unVal.pUserPointer)
#define _thread(obj) ((obj)._unVal.pThread)
#define _funcproto(obj) ((obj)._unVal.pFunctionProto)
#define _class(obj) ((obj)._unVal.pClass)
#define _instance(obj) ((obj)._unVal.pInstance)
#define _delegable(obj) ((VXDelegableObj *)(obj)._unVal.pDelegable)
#define _weakref(obj) ((obj)._unVal.pWeakRef)
#define _outer(obj) ((obj)._unVal.pOuter)
#define _refcounted(obj) ((obj)._unVal.pRefCounted)
#define _rawval(obj) ((obj)._unVal.raw)

#define _stringval(obj) (((obj)._unVal).pString->Value())
#define _stringlen(obj) (((obj)._unVal).pString->Length())
#define _userdataval(obj) ((VXUserPointer)vox_aligning((obj)._unVal.pUserData + 1))

#define tofloat(num) ((type(num)==VX_OT_INTEGER)?(VXFloat)_integer(num):_float(num))
#define tointeger(num) ((type(num)==VX_OT_FLOAT)?(VXInteger)_float(num):_integer(num))

#define MINPOWER2 4

#define START_MARK()    \
    if(!(_uiRef & MARK_FLAG))\
    { \
        _uiRef |= MARK_FLAG;

#define END_MARK() \
    this->RemoveFromChain(&_sharedstate->_gc_chain, this); \
    this->AddToChain(chain, this); \
    }


#define MARK_FLAG 0x80000000
#define ADD_TO_CHAIN(chain,obj) AddToChain(chain,obj)
#define REMOVE_FROM_CHAIN(chain,obj) {if(!(_uiRef&MARK_FLAG)) this->RemoveFromChain(chain,obj);}
#define CHAINABLE_OBJ VXCollectable
#define INIT_CHAIN() {_next=NULL;_prev=NULL;_sharedstate=ss;}



struct VXSharedState;
struct VXObject;
typedef VXVector<VXInteger> VXIntVec;

inline void _Swap(VXRawObj &a,VXRawObj &b)
{
    VXOType tOldType = a._type;
    VXRawObj::Value unOldVal = a._unVal;
    a._type = b._type;
    a._unVal = b._unVal;
    b._type = tOldType;
    b._unVal = unOldVal;
}

template<typename Type> void _NULL_VXOBJECT_VECTOR(Type& vec, VXInteger size)
{
    VXInteger n;
    for(n= 0; n< size; n++)
    {
        vec[n].Null();
    }
}


VXUnsignedInteger TranslateIndex(const VXObject &idx);


const char *GetTypeName(const VXObject &obj1);
const char *IdType2Name(VXOType type);

bool SafeWrite(VXState* v,
               VXWriteFunc write,
               VXUserPointer up,
               VXUserPointer dest,
               VXInteger size);
bool SafeRead(VXState* v,VXWriteFunc read,VXUserPointer up,VXUserPointer dest,VXInteger size);
bool WriteTag(VXState* v,VXWriteFunc write,VXUserPointer up,VXUnsignedInteger32 tag);
bool CheckTag(VXState* v,VXWriteFunc read,VXUserPointer up,VXUnsignedInteger32 tag);
bool WriteObject(VXState* v,VXUserPointer up,VXWriteFunc write,VXObject &o);
bool ReadObject(VXState* v,VXUserPointer up,VXReadFunc read,VXObject &o);


#endif //_VXOBJECT_H_
