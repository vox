
#ifndef _VXLEXER_H_
#define _VXLEXER_H_

typedef unsigned char LexChar;
struct VXLexer
{
    VXLexer();
    ~VXLexer();
    void Init(VXSharedState *ss,VXLexReadFunc rg,VXUserPointer up,CompilerErrorFunc efunc,void *ed);
    void Error(const char *err);
    VXInteger Lex();
    const char *Tok2Str(VXInteger tok);
    private:
        VXInteger GetIDType(char *s);
        void      Parse_Escapes(bool);
        VXInteger ReadString(VXInteger ndelim,bool verbatim);
        VXInteger ReadNumber();
        void LexBlockComment();
        void LexLineComment();
        VXInteger ReadID();
        void Next();
        VXInteger _curtoken;
        VXTableObj *_keywords;
        bool _reached_eof;
    public:
        VXInteger _prevtoken;
        VXInteger _currentline;
        VXInteger _lasttokenline;
        VXInteger _currentcolumn;
        const char *_svalue;
        VXInteger _nvalue;
        VXFloat _fvalue;
        VXLexReadFunc _readf;
        VXUserPointer _up;
        LexChar _currdata;
        VXSharedState *_sharedstate;
        VXVector<char> _longstr;
        CompilerErrorFunc _errfunc;
        void *_errtarget;
};
#endif
