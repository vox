
#ifndef _VXUSERDATA_H_
#define _VXUSERDATA_H_

struct VXUserDataObj : VXDelegableObj
{
    VXUserDataObj(VXSharedState *ss){ _delegate = 0; _hook = NULL; INIT_CHAIN(); ADD_TO_CHAIN(&_ss(this)->_gc_chain, this); }
    ~VXUserDataObj()
    {
        REMOVE_FROM_CHAIN(&_ss(this)->_gc_chain, this);
        SetDelegate(NULL);
    }
    static VXUserDataObj* Create(VXSharedState *ss, VXInteger size)
    {
        VXUserDataObj* ud = (VXUserDataObj*)VX_MALLOC(vox_aligning(sizeof(VXUserDataObj))+size);
        new (ud) VXUserDataObj(ss);
        ud->_size = size;
        ud->_typetag = 0;
        return ud;
    }
    #ifndef NO_GARBAGE_COLLECTOR
    void Mark(VXCollectable **chain);
    void Finalize(){SetDelegate(NULL);}
    VXOType GetType(){ return VX_OT_USERDATA;}
    #endif
    void Release()
    {
        if (_hook) _hook((VXUserPointer)vox_aligning(this + 1),_size);
        VXInteger tsize = _size;
        this->~VXUserDataObj();
        VX_FREE(this, vox_aligning(sizeof(VXUserDataObj)) + tsize);
    }

    VXInteger _size;
    VXReleaseHook _hook;
    VXUserPointer _typetag;
    //char _val[1];
};
#endif                           //_VXUSERDATA_H_
