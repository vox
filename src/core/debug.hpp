
#ifndef __vox_debug_hpp__
#define __vox_debug_hpp__

#include "names.hpp"

template<typename Type> const char* get_funcname(Type& si)
{
    if(si.funcname)
    {
        return si.funcname;
    }
    return NAMES_ANONFUNC;
}

template<typename Type> const char* get_sourcename(Type& si)
{
    if(si.source)
    {
        return si.source;
    }
    return NAMES_ANONFILE;
}


void vox_aux_compiler_error(VXState* v,
                            const char *sErr,
                            const char *sSource,
                            VXInteger line,
                            VXInteger column);

VXInteger vox_aux_printerror(VXState* v);
#endif /* __vox_debug_hpp__ */
