
#include "pcheader.hpp"
#include "vm.hpp"
#include "table.hpp"
#include "class.hpp"
#include "funcproto.hpp"
#include "closure.hpp"

VXClassObj* VXClassObj::Create(VXSharedState *ss, VXClassObj *base)
{
    VXClassObj *newclass = (VXClassObj *)VX_MALLOC(sizeof(VXClassObj));
    new (newclass) VXClassObj(ss, base);
    return newclass;
}

bool VXClassObj::Get(const VXObject &key,VXObject &val)
{
    if(_members->Get(key,val))
    {
        if(_isfield(val))
        {
            VXObject &o = _defaultvalues[_member_idx(val)].val;
            val = VX_REALVAL(o);
        }
        else
        {
            val = _methods[_member_idx(val)].val;
        }
        return true;
    }
    return false;
}

bool VXClassObj::GetConstructor(VXObject &dest)
{
    if(_constructoridx != -1)
    {
        dest = _methods[_constructoridx].val;
        return true;
    }
    return false;
}


void VXClassObj::Lock()
{
    _locked = true;
    if(_base)
    {
        _base->Lock();
    }
}

void VXClassObj::Release()
{
    if (_hook)
    {
        _hook(_typetag,0);
    }
    vox_delete(this, VXClassObj);
}


VXOType VXClassObj::GetType()
{
    return VX_OT_CLASS;
}


VXClassObj::VXClassObj(VXSharedState *ss,VXClassObj *base)
{
    _base = base;
    _typetag = 0;
    _hook = NULL;
    _udsize = 0;
    _locked = false;
    _constructoridx = -1;
    _ss = ss;
    //_name = NULL;
    if(_base)
    {
        _constructoridx = _base->_constructoridx;
        _udsize = _base->_udsize;
        _defaultvalues.copy(base->_defaultvalues);
        _methods.copy(base->_methods);
        VX_COPY_VECTOR(_metamethods,base->_metamethods,VX_MT_LAST);
        __ObjAddRef(_base);
    }
    _members = base?base->_members->Clone() : VXTableObj::Create(ss,0);
    __ObjAddRef(_members);

    INIT_CHAIN();
    ADD_TO_CHAIN(&_sharedstate->_gc_chain, this);
}

void VXClassObj::Finalize()
{
    _attributes.Null();
    _defaultvalues.resize(0);
    _methods.resize(0);
    _NULL_VXOBJECT_VECTOR(_metamethods,VX_MT_LAST);
    __ObjRelease(_members);
    if(_base)
    {
        __ObjRelease(_base);
    }
}

VXClassObj::~VXClassObj()
{
    REMOVE_FROM_CHAIN(&_sharedstate->_gc_chain, this);
    Finalize();
}

bool VXClassObj::NewSlot(VXSharedState *ss, const VXObject &key, const VXObject &val, bool bstatic)
{
    VXObject temp;
    bool belongs_to_static_table = (
        type(val) == VX_OT_CLOSURE || type(val) == VX_OT_NATIVECLOSURE || bstatic);
    if(_locked && !belongs_to_static_table)
    {
        return false;            //the class already has an instance so cannot be modified
                                 //overrides the default value
    }
    if(_members->Get(key,temp) && _isfield(temp))
    {
        _defaultvalues[_member_idx(temp)].val = val;
        return true;
    }
    if(belongs_to_static_table)
    {
        VXInteger mmidx;
        if((type(val) == VX_OT_CLOSURE || type(val) == VX_OT_NATIVECLOSURE) &&
            (mmidx = ss->GetMetaMethodIdxByName(key)) != -1)
        {
            _metamethods[mmidx] = val;
        }
        else
        {
            VXObject theval = val;
            if(_base && type(val) == VX_OT_CLOSURE)
            {
                theval = _closure(val)->Clone();
                _closure(theval)->_base = _base;
                __ObjAddRef(_base); //ref for the closure
            }
            if(type(temp) == VX_OT_NULL)
            {
                bool isconstructor;
                VXState::IsEqual(ss->_constructoridx, key, isconstructor);
                if(isconstructor)
                {
                    _constructoridx = (VXInteger)_methods.size();
                }
                VXClassObj::Member m;
                m.val = theval;
                _members->NewSlot(key,VXObject(_make_method_idx(_methods.size())));
                _methods.push_back(m);
            }
            else
            {
                _methods[_member_idx(temp)].val = theval;
            }
        }
        return true;
    }
    VXClassObj::Member m;
    m.val = val;
    _members->NewSlot(key,VXObject(_make_field_idx(_defaultvalues.size())));
    _defaultvalues.push_back(m);
    return true;
}

VXInstanceObj* VXClassObj::CreateInstance()
{
    if(!_locked)
    {
        Lock();
    }
    return VXInstanceObj::Create(_opt_ss(this),this);
}

VXInteger VXClassObj::Next(const VXObject &refpos, VXObject &outkey, VXObject &outval)
{
    VXObject oval;
    VXInteger idx = _members->Next(false,refpos,outkey,oval);
    if(idx != -1)
    {
        if(_ismethod(oval))
        {
            outval = _methods[_member_idx(oval)].val;
        }
        else
        {
            VXObject &o = _defaultvalues[_member_idx(oval)].val;
            outval = VX_REALVAL(o);
        }
    }
    return idx;
}

bool VXClassObj::SetAttributes(const VXObject &key,const VXObject &val)
{
    VXObject idx;
    if(_members->Get(key,idx))
    {
        if(_isfield(idx))
            _defaultvalues[_member_idx(idx)].attrs = val;
        else
            _methods[_member_idx(idx)].attrs = val;
        return true;
    }
    return false;
}

bool VXClassObj::GetAttributes(const VXObject &key,VXObject &outval)
{
    VXObject idx;
    if(_members->Get(key,idx))
    {
        //outval = (_isfield(idx)?_defaultvalues[_member_idx(idx)].attrs:_methods[_member_idx(idx)].attrs);
        if(_isfield(idx))
        {
            outval = _defaultvalues[_member_idx(idx)].attrs;
        }
        else
        {
            outval = _methods[_member_idx(idx)].attrs;
        }
        return true;
    }
    return false;
}

///////////////////////////////////////////////////////////////////////

VXInstanceObj* VXInstanceObj::Create(VXSharedState *ss,VXClassObj *theclass)
{

    VXInteger size = calcinstancesize(theclass);
    VXInstanceObj *newinst = (VXInstanceObj *)VX_MALLOC(size);
    new (newinst) VXInstanceObj(ss, theclass,size);
    if(theclass->_udsize)
    {
        newinst->_userpointer = ((unsigned char *)newinst) + (size - theclass->_udsize);
    }
    return newinst;
}

VXInstanceObj* VXInstanceObj::Clone(VXSharedState *ss)
{
    VXInteger size = calcinstancesize(_class);
    VXInstanceObj *newinst = (VXInstanceObj *)VX_MALLOC(size);
    new (newinst) VXInstanceObj(ss, this,size);
    if(_class->_udsize)
    {
        newinst->_userpointer = ((unsigned char *)newinst) + (size - _class->_udsize);
    }
    return newinst;
}


bool VXInstanceObj::Get(const VXObject &key,VXObject &val)
{
    if(_class->_members->Get(key,val))
    {
        if(_isfield(val))
        {
            VXObject &o = _values[_member_idx(val)];
            val = VX_REALVAL(o);
        }
        else
        {
            val = _class->_methods[_member_idx(val)].val;
        }
        return true;
    }
    return false;
}

bool VXInstanceObj::Set(const VXObject &key,const VXObject &val)
{
    VXObject idx;
    if(_class->_members->Get(key,idx) && _isfield(idx))
    {
        _values[_member_idx(idx)] = val;
        return true;
    }
    return false;
}

void VXInstanceObj::Release()
{
    _uiRef++;
    if (_hook)
    {
        _hook(_userpointer, 0);
    }
    _uiRef--;
    if(_uiRef > 0)
    {
        return;
    }
    VXInteger size = _memsize;
    this->~VXInstanceObj();
    VX_FREE(this, size);
}

VXOType VXInstanceObj::GetType()
{
    return VX_OT_INSTANCE;
}

void VXInstanceObj::Init(VXSharedState *ss)
{
    _userpointer = NULL;
    _hook = NULL;
    __ObjAddRef(_class);
    _delegate = _class->_members;
    INIT_CHAIN();
    ADD_TO_CHAIN(&_sharedstate->_gc_chain, this);
}

VXInstanceObj::VXInstanceObj(VXSharedState *ss, VXClassObj *c, VXInteger memsize)
{
    _memsize = memsize;
    _class = c;
    VXUnsignedInteger nvalues = _class->_defaultvalues.size();
    for(VXUnsignedInteger n = 0; n < nvalues; n++)
    {
        new (&_values[n]) VXObject(_class->_defaultvalues[n].val);
    }
    Init(ss);
}

VXInstanceObj::VXInstanceObj(VXSharedState *ss, VXInstanceObj *i, VXInteger memsize)
{
    _memsize = memsize;
    _class = i->_class;
    VXUnsignedInteger nvalues = _class->_defaultvalues.size();
    for(VXUnsignedInteger n = 0; n < nvalues; n++)
    {
        new (&_values[n]) VXObject(i->_values[n]);
    }
    Init(ss);
}

void VXInstanceObj::Finalize()
{
    VXUnsignedInteger nvalues = _class->_defaultvalues.size();
    __ObjRelease(_class);
    _NULL_VXOBJECT_VECTOR(_values,nvalues);
}

VXInstanceObj::~VXInstanceObj()
{
    REMOVE_FROM_CHAIN(&_sharedstate->_gc_chain, this);
    if(_class)                   //if _class is null it was already finalized by the GC
    {
        Finalize();
    }
}

bool VXInstanceObj::GetMetaMethod(VXState *v,VXMetaMethod mm,VXObject &res)
{
    (void)v;
    if(type(_class->_metamethods[mm]) != VX_OT_NULL)
    {
        res = _class->_metamethods[mm];
        return true;
    }
    return false;
}

bool VXInstanceObj::InstanceOf(VXClassObj *trg)
{
    VXClassObj *parent = _class;
    while(parent != NULL)
    {
        if(parent == trg)
        {
            return true;
        }
        parent = parent->_base;
    }
    return false;
}



void VXClassObj::Mark(VXCollectable **chain)
{
    START_MARK()
    _members->Mark(chain);
    if(_base)
    {
        _base->Mark(chain);
    }
    VXSharedState::MarkObject(_attributes, chain);
    for(VXUnsignedInteger i =0; i< _defaultvalues.size(); i++)
    {
        VXSharedState::MarkObject(_defaultvalues[i].val, chain);
        VXSharedState::MarkObject(_defaultvalues[i].attrs, chain);
    }
    for(VXUnsignedInteger j =0; j< _methods.size(); j++)
    {
        VXSharedState::MarkObject(_methods[j].val, chain);
        VXSharedState::MarkObject(_methods[j].attrs, chain);
    }
    for(VXUnsignedInteger k =0; k< VX_MT_LAST; k++)
    {
        VXSharedState::MarkObject(_metamethods[k], chain);
    }
    END_MARK()
}
