
#include "baselib.hpp"
#include "hash.hpp"

template<typename Type> struct VXHashProxy
{
    Type* instance;

    VXHashProxy()
    {
        this->instance = new Type;
    }

    ~VXHashProxy()
    {
        delete this->instance;
    }
};


template<typename Type> VXInteger hashclass_releasehook(VXUserPointer p, VXInteger)
{
    VXHashProxy<Type>* self = (VXHashProxy<Type>*)p;
    delete self;
    return 1;
}

template<typename Type> VXInteger hashclass_constructor(VXState* v)
{
    VXHashProxy<Type>* self;
    self = new VXHashProxy<Type>;
    v->SetInstanceUp(1, (VXUserPointer*)&self);
    v->SetReleaseHook(1, hashclass_releasehook<Type>);
    return 1;
}

VXInteger hashclass_update(VXState* v)
{
    VXHashProxy<Hash::Object>* self;
    std::string value;
    v->GetInstanceUp(1, (VXUserPointer*)&self, 0);
    v->GetString(2, &value);
    self->instance->update(value);
    return VX_OK;
}

VXInteger hashclass_update_char(VXState* v)
{
    VXHashProxy<Hash::Object>* self;
    VXInteger value;
    v->GetInstanceUp(1, (VXUserPointer*)&self, 0);
    v->GetInteger(2, &value);
    self->instance->update(value);
    return VX_OK;
}

VXInteger hashclass_update_file(VXState* v)
{
    VXHashProxy<Hash::Object>* self;
    std::string path;
    v->GetInstanceUp(1, (VXUserPointer*)&self, 0);
    v->GetString(2, &path);
    self->instance->updateFromFile(path);
    return VX_OK;
}

VXInteger hashclass_finalize(VXState* v)
{
    VXHashProxy<Hash::Object>* self;
    v->GetInstanceUp(1, (VXUserPointer*)&self, 0);
    self->instance->finalize();
    return VX_OK;
}

VXInteger hashclass_hexdigest(VXState* v)
{
    VXHashProxy<Hash::Object>* self;
    std::string hd;
    v->GetInstanceUp(1, (VXUserPointer*)&self, 0);
    try
    {
        hd = self->instance->hexDigest();
        v->Push(v->NewString(hd.c_str(), hd.length()));
        return 1;
    }
    catch(Hash::Error& err)
    {
        return v->ThrowError(err.what());
    }
    return VX_ERROR;
}

template<typename Type> VXRegFunction* hashfuncs()
{
    static VXRegFunction funcs[] =
    {
        {"constructor", hashclass_constructor<Type>, 0, "x"},
        {"update",      hashclass_update,            2, ".s"},
        {"update_char", hashclass_update_char,       2, ".n"},
        {"update_file", hashclass_update_file,       2, ".s"},
        {"finalize",    hashclass_finalize,          0, "x"},
        {"hexdigest",   hashclass_hexdigest,         0, "x"},
        {0, 0, 0, 0},
    };
    return funcs;
}

VXInteger voxstd_register_hashlib(VXState* v)
{
    VXTableObj* tb;
    tb = v->NewTable();
    tb->NewSlot(v->NewString("SHA1"), v->RegClass(hashfuncs<Hash::SHA1>()));
    tb->NewSlot(v->NewString("MD5"),  v->RegClass(hashfuncs<Hash::MD5>()));
    v->GetRootTable()->NewSlot(v->NewString("hashlib"), tb);
    return VX_OK;
}

