
// continue: line 720


#ifndef VOX_NO_COMPILER
#include <stdarg.h>
#include <setjmp.h>

#include "opcodes.hpp"
#include "string.hpp"
#include "funcproto.hpp"
#include "compiler.hpp"
#include "funcstate.hpp"
#include "lexer.hpp"
#include "vm.hpp"
#include "table.hpp"
#include "names.hpp"

#define EXPR   1
#define OBJECT 2
#define BASE   3
#define LOCAL  4
#define OUTER  5

struct VXExpState {
  VXInteger  etype;       /* expr. type; one of EXPR, OBJECT, BASE, OUTER or LOCAL */
  VXInteger  epos;        /* expr. location on stack; -1 for OBJECT and BASE */
  bool       donot_get;   /* signal not to deref the next value */
};

struct VXScope {
    VXInteger outers;
    VXInteger stacksize;
};

#define BEGIN_VXCOPE() \
    VXScope __oldscope__ = _scope; \
    _scope.outers = _fs->_outers; \
    _scope.stacksize = _fs->GetStackSize();

#define RESOLVE_OUTERS() \
    if(_fs->GetStackSize() != _scope.stacksize) \
    { \
        if(_fs->CountOuters(_scope.stacksize)) \
        { \
            _fs->AddInstruction(_OP_CLOSE,0,_scope.stacksize); \
        } \
    }

#define END_VXCOPE_NO_CLOSE() \
    { \
        if(_fs->GetStackSize() != _scope.stacksize) \
        { \
            _fs->SetStackSize(_scope.stacksize); \
        } \
        _scope = __oldscope__; \
    }

#define END_VXCOPE() \
    { \
        VXInteger oldouters = _fs->_outers; \
        if(_fs->GetStackSize() != _scope.stacksize) \
        { \
            _fs->SetStackSize(_scope.stacksize); \
            if(oldouters != _fs->_outers) \
            { \
                _fs->AddInstruction(_OP_CLOSE,0,_scope.stacksize); \
            } \
        } \
        _scope = __oldscope__; \
    }

#define BEGIN_BREAKBLE_BLOCK() \
        VXInteger __nbreaks__ = _fs->_unresolvedbreaks.size(); \
        VXInteger __ncontinues__ = _fs->_unresolvedcontinues.size(); \
        _fs->_breaktargets.push_back(0); \
        _fs->_continuetargets.push_back(0);

#define END_BREAKBLE_BLOCK(continue_target) \
    { \
        __nbreaks__ = _fs->_unresolvedbreaks.size() - __nbreaks__; \
        __ncontinues__ = _fs->_unresolvedcontinues.size() - __ncontinues__; \
        if(__ncontinues__>0)\
        { \
            ResolveContinues(_fs,__ncontinues__,continue_target); \
        } \
        if(__nbreaks__>0) \
        { \
            ResolveBreaks(_fs,__nbreaks__); \
        } \
        _fs->_breaktargets.pop_back(); \
        _fs->_continuetargets.pop_back(); \
    }

class VXCompiler
{
    public:
        VXCompiler(VXState *v,
                   VXLexReadFunc rg,
                   VXUserPointer up,
                   const char* sourcename,
                   bool raiseerror,
                   bool lineinfo)
        {
            _vm = v;
            _lex.Init(_ss(v), rg, up, HandleError,this);
            _sourcename = VXStringObj::Create(_ss(v), sourcename);
            _lineinfo = lineinfo;_raiseerror = raiseerror;
            _scope.outers = 0;
            _scope.stacksize = 0;
            compilererror = NULL;
        }

        static void HandleError(void *ud, const char *s)
        {
            VXCompiler *c = (VXCompiler *)ud;
            c->Error(s);
        }

        void Error(const char *s, ...)
        {
            static char temp[256];
            va_list vl;
            va_start(vl, s);
            vsprintf(temp, s, vl);
            va_end(vl);
            compilererror = temp;
            longjmp(_errorjmp,1);
        }

        void Lex()
        {
            _token = _lex.Lex();
        }

        VXRawObj Expect(VXInteger tok)
        {
            if(_token != tok)
            {
                if(_token == TK_CONSTRUCTOR && tok == TK_IDENTIFIER)
                {
                    //do nothing
                }
                else
                {
                    const char *etypename;
                    if(tok > 255)
                    {
                        switch(tok)
                        {
                            case TK_IDENTIFIER:
                                etypename = "IDENTIFIER";
                                break;
                            case TK_STRING_LITERAL:
                                etypename = "STRING_LITERAL";
                                break;
                            case TK_INTEGER:
                                etypename = "INTEGER";
                                break;
                            case TK_FLOAT:
                                etypename = "FLOAT";
                                break;
                            default:
                                etypename = _lex.Tok2Str(tok);
                        }
                        Error("expected '%s'", etypename);
                    }
                    Error("expected '%c'", tok);
                }
            }
            VXObject ret;
            switch(tok)
            {
                case TK_IDENTIFIER:
                    ret = _fs->CreateString(_lex._svalue);
                    break;
                case TK_STRING_LITERAL:
                    ret = _fs->CreateString(_lex._svalue,_lex._longstr.size()-1);
                    break;
                case TK_INTEGER:
                    ret = VXObject(_lex._nvalue);
                    break;
                case TK_FLOAT:
                    ret = VXObject(_lex._fvalue);
                    break;
            }
            Lex();
            return ret;
        }
        bool IsEndOfStatement()
        {
            return (
                (_lex._prevtoken == '\n')  ||
                (_token          == VOX_EOB)    ||
                (_token          == '}')   ||
                (_token          == ';')
            );
        }
        void OptionalSemicolon()
        {
            if(_token == ';')
            {
                Lex();
                return;
            }
            if(!IsEndOfStatement())
            {
                Error("end of statement expected (';' or EOL)");
            }
        }
        void MoveIfCurrentTargetIsLocal()
        {
            VXInteger trg = _fs->TopTarget();
            if(_fs->IsLocal(trg))
            {
                trg = _fs->PopTarget(); //no pops the target and move it
                _fs->AddInstruction(_OP_MOVE, _fs->PushTarget(), trg);
            }
        }
        bool Compile(VXObject &o)
        {
            _debugline = 1;
            _debugop = 0;

            VXFuncState funcstate(_ss(_vm), NULL, HandleError,this);
            funcstate._name = VXStringObj::Create(_ss(_vm), NAMES_MAINFUNC);
            _fs = &funcstate;
            _fs->AddParameter(_fs->CreateString(NAMES_TK_THISVAR));
            _fs->AddParameter(_fs->CreateString(NAMES_VARGV));
            _fs->_varparams = true;
            _fs->_sourcename = _sourcename;
            VXInteger stacksize = _fs->GetStackSize();
            if(setjmp(_errorjmp) == 0)
            {
                Lex();
                while(_token > 0)
                {
                    Statement();
                    if(_lex._prevtoken != '}' && _lex._prevtoken != ';')
                    {
                        OptionalSemicolon();
                    }
                }
                _fs->SetStackSize(stacksize);
                _fs->AddLineInfos(_lex._currentline, _lineinfo, true);
                _fs->AddInstruction(_OP_RETURN, 0xFF);
                _fs->SetStackSize(0);
                o =_fs->BuildProto();
#ifdef _DEBUG_DUMP
                _fs->Dump(_funcproto(o));
#endif
            }
            else
            {
                if(_raiseerror && _ss(_vm)->_compilererrorhandler)
                {
                    _ss(_vm)->_compilererrorhandler(
                        _vm,
                        compilererror,
                        type(_sourcename)==VX_OT_STRING?_stringval(_sourcename):"unknown",
                        _lex._currentline,
                        _lex._currentcolumn);
                }
                _vm->_lasterror = VXStringObj::Create(_ss(_vm), compilererror, -1);
                return false;
            }
            return true;
        }

        void Statements()
        {
            while(_token != '}' && _token != TK_DEFAULT && _token != TK_CASE)
            {
                Statement();
                if(_lex._prevtoken != '}' && _lex._prevtoken != ';')
                {
                    OptionalSemicolon();
                }
            }
        }

        void Statement(bool closeframe = true)
        {
            _fs->AddLineInfos(_lex._currentline, _lineinfo);
            switch(_token)
            {
                case ';':
                    Lex();
                    break;

                case TK_IF:
                    IfStatement();
                    break;

                case TK_WHILE:
                    WhileStatement();
                    break;

                case TK_DO:
                    DoWhileStatement();
                    break;

                case TK_FOR:
                    ForStatement();
                    break;

                case TK_FOREACH:
                    ForEachStatement();
                    break;

                case TK_SWITCH:
                    SwitchStatement();
                    break;

                case TK_LOCAL:
                    LocalDeclStatement();
                    break;

                case TK_RETURN:
                case TK_YIELD:
                    {
                        VXOpcode op;
                        if(_token == TK_RETURN)
                        {
                            op = _OP_RETURN;
                        }
                        else
                        {
                            op = _OP_YIELD;
                            _fs->_bgenerator = true;
                        }
                        Lex();
                        if(!IsEndOfStatement())
                        {
                            VXInteger retexp = _fs->GetCurrentPos()+1;
                            CommaExpr();
                            if(op == _OP_RETURN && _fs->_traps > 0)
                            {
                                _fs->AddInstruction(_OP_POPTRAP, _fs->_traps, 0);
                            }
                            _fs->_returnexp = retexp;
                            _fs->AddInstruction(op, 1, _fs->PopTarget(),_fs->GetStackSize());
                        }
                        else
                        {
                            if(op == _OP_RETURN && _fs->_traps > 0)
                                _fs->AddInstruction(_OP_POPTRAP, _fs->_traps ,0);
                            _fs->_returnexp = -1;
                            _fs->AddInstruction(op, 0xFF,0,_fs->GetStackSize());
                        }
                        break;
                    }

                case TK_BREAK:
                    if(_fs->_breaktargets.size() <= 0)
                    {
                        Error("'break' has to be in a loop block");
                    }
                    if(_fs->_breaktargets.top() > 0)
                    {
                        _fs->AddInstruction(_OP_POPTRAP, _fs->_breaktargets.top(), 0);
                    }
                    RESOLVE_OUTERS();
                    _fs->AddInstruction(_OP_JMP, 0, -1234);
                    _fs->_unresolvedbreaks.push_back(_fs->GetCurrentPos());
                    Lex();
                    break;

                case TK_CONTINUE:
                    if(_fs->_continuetargets.size() <= 0)
                    {
                        Error("'continue' has to be in a loop block");
                    }
                    if(_fs->_continuetargets.top() > 0)
                    {
                        _fs->AddInstruction(_OP_POPTRAP, _fs->_continuetargets.top(), 0);
                    }
                    RESOLVE_OUTERS();
                    _fs->AddInstruction(_OP_JMP, 0, -1234);
                    _fs->_unresolvedcontinues.push_back(_fs->GetCurrentPos());
                    Lex();
                    break;

                case TK_FUNCTION:
                    FunctionStatement();
                    break;

                case TK_CLASS:
                    ClassStatement();
                    break;

                case TK_ENUM:
                    EnumStatement();
                    break;

                case '{':
                    {
                        BEGIN_VXCOPE();
                        Lex();
                        Statements();
                        Expect('}');
                        if(closeframe)
                        {
                            END_VXCOPE();
                        }
                        else
                        {
                            END_VXCOPE_NO_CLOSE();
                        }
                    }
                    break;

                case TK_TRY:
                    TryCatchStatement();
                    break;

                case TK_THROW:
                    Lex();
                    CommaExpr();
                    _fs->AddInstruction(_OP_THROW, _fs->PopTarget());
                    break;

                case TK_CONST:
                    {
                        Lex();
                        VXRawObj id = Expect(TK_IDENTIFIER);
                        Expect('=');
                        VXRawObj val = ExpectScalar();
                        OptionalSemicolon();
                        VXTableObj *enums = _table(_ss(_vm)->_consts);
                        VXObject strongid = id;
                        enums->NewSlot(strongid,VXObject(val));
                        strongid.Null();
                    }
                    break;

                default:
                    CommaExpr();
                    _fs->DiscardTarget();
                    //_fs->PopTarget();
                    break;
            }
            _fs->SnoozeOpt();
        }

        void EmitDerefOp(VXOpcode op)
        {
            VXInteger val = _fs->PopTarget();
            VXInteger key = _fs->PopTarget();
            VXInteger src = _fs->PopTarget();
            _fs->AddInstruction(op,_fs->PushTarget(),src,key,val);
        }

        void Emit2ArgsOP(VXOpcode op, VXInteger p3 = 0)
        {
            VXInteger p2 = _fs->PopTarget(); //src in OP_GET
            VXInteger p1 = _fs->PopTarget(); //key in OP_GET
            _fs->AddInstruction(op,_fs->PushTarget(), p1, p2, p3);
        }

        void EmitCompoundArith(VXInteger tok, VXInteger etype, VXInteger pos)
        {
            /* Generate code depending on the expression type */
            switch(etype)
            {
                case LOCAL:
                    {
                        VXInteger p2 = _fs->PopTarget(); //src in OP_GET
                        VXInteger p1 = _fs->PopTarget(); //key in OP_GET
                        _fs->PushTarget(p1);
                        //EmitCompArithLocal(tok, p1, p1, p2);
                        _fs->AddInstruction(ChooseArithOpByToken(tok),p1, p2, p1, 0);
                    }
                    break;
                case OBJECT:
                case BASE:
                    {
                        VXInteger val = _fs->PopTarget();
                        VXInteger key = _fs->PopTarget();
                        VXInteger src = _fs->PopTarget();
                        /* _OP_COMPARITH mixes dest obj and source val in the arg1 */
                        _fs->AddInstruction(
                            _OP_COMPARITH,
                            _fs->PushTarget(),
                            (src<<16)|val,
                            key,
                            ChooseCompArithCharByToken(tok));
                    }
                    break;
                case OUTER:
                    {
                        VXInteger val = _fs->TopTarget();
                        VXInteger tmp = _fs->PushTarget();
                        _fs->AddInstruction(_OP_GETOUTER,   tmp, pos);
                        _fs->AddInstruction(ChooseArithOpByToken(tok), tmp, val, tmp, 0);
                        _fs->AddInstruction(_OP_SETOUTER, tmp, pos, tmp);
                    }
                    break;
            }
        }

        void CommaExpr()
        {
            for(Expression();_token == ',';_fs->PopTarget(), Lex(), CommaExpr());
        }

        void Expression()
        {
            VXExpState es = _es;
            _es.etype     = EXPR;
            _es.epos      = -1;
            _es.donot_get = false;
            LogicalOrExp();
            switch(_token)
            {
                case '=':
                case TK_NEWSLOT:
                case TK_MINUSEQ:
                case TK_PLUSEQ:
                case TK_MULEQ:
                case TK_DIVEQ:
                case TK_MODEQ:
                    {
                        VXInteger op = _token;
                        VXInteger ds = _es.etype;
                        VXInteger pos = _es.epos;
                        if(ds == EXPR)
                        {
                            Error("can't assign expression");
                        }
                        Lex();
                        Expression();
                        switch(op)
                        {
                            case TK_NEWSLOT:
                                if(ds == OBJECT || ds == BASE)
                                {
                                    EmitDerefOp(_OP_NEWSLOT);
                                }
                                //if _derefstate != DEREF_NO_DEREF &&
                                // DEREF_FIELD so is the index of a local
                                else
                                {
                                    //Error("can't \"create\" a local slot");
                                    Error("can't use variable as slot that was previously declared local");
                                }
                                break;
                            case '=': //ASSIGN
                                switch(ds)
                                {
                                    case LOCAL:
                                        {
                                            VXInteger src = _fs->PopTarget();
                                            VXInteger dst = _fs->TopTarget();
                                            _fs->AddInstruction(_OP_MOVE, dst, src);
                                        }
                                        break;
                                    case OBJECT:
                                    case BASE:
                                        EmitDerefOp(_OP_SET);
                                        break;
                                    case OUTER:
                                        {
                                            VXInteger src = _fs->PopTarget();
                                            VXInteger dst = _fs->PushTarget();
                                            _fs->AddInstruction(_OP_SETOUTER, dst, pos, src);
                                        }
                                }
                                break;
                            case TK_MINUSEQ:
                            case TK_PLUSEQ:
                            case TK_MULEQ:
                            case TK_DIVEQ:
                            case TK_MODEQ:
                                EmitCompoundArith(op, ds, pos);
                                break;
                        }
                    }
                    break;
                case '?':
                    {
                        Lex();
                        _fs->AddInstruction(_OP_JZ, _fs->PopTarget());
                        VXInteger jzpos = _fs->GetCurrentPos();
                        VXInteger trg = _fs->PushTarget();
                        Expression();
                        VXInteger first_exp = _fs->PopTarget();
                        if(trg != first_exp)
                        {
                            _fs->AddInstruction(_OP_MOVE, trg, first_exp);
                        }
                        VXInteger endfirstexp = _fs->GetCurrentPos();
                        _fs->AddInstruction(_OP_JMP, 0, 0);
                        Expect(':');
                        VXInteger jmppos = _fs->GetCurrentPos();
                        Expression();
                        VXInteger second_exp = _fs->PopTarget();
                        if(trg != second_exp)
                        {
                            _fs->AddInstruction(_OP_MOVE, trg, second_exp);
                        }
                        _fs->SetIntructionParam(jmppos, 1, _fs->GetCurrentPos() - jmppos);
                        _fs->SetIntructionParam(jzpos, 1, endfirstexp - jzpos + 1);
                        _fs->SnoozeOpt();
                    }
                    break;
            }
            _es = es;
        }

        template<typename T> void BIN_EXP(VXOpcode op, T f,VXInteger op3 = 0)
        {
            Lex();
            (this->*f)();
            VXInteger op1 = _fs->PopTarget();
            VXInteger op2 = _fs->PopTarget();
            _fs->AddInstruction(op, _fs->PushTarget(), op1, op2, op3);
        }

        void LogicalOrExp()
        {
            LogicalAndExp();
            for(;;)
            {
                if(_token == TK_OR)
                {
                    VXInteger first_exp = _fs->PopTarget();
                    VXInteger trg = _fs->PushTarget();
                    _fs->AddInstruction(_OP_OR, trg, 0, first_exp, 0);
                    VXInteger jpos = _fs->GetCurrentPos();
                    if(trg != first_exp)
                    {
                        _fs->AddInstruction(_OP_MOVE, trg, first_exp);
                    }
                    Lex();
                    LogicalOrExp();
                    _fs->SnoozeOpt();
                    VXInteger second_exp = _fs->PopTarget();
                    if(trg != second_exp)
                    {
                        _fs->AddInstruction(_OP_MOVE, trg, second_exp);
                    }
                    _fs->SnoozeOpt();
                    _fs->SetIntructionParam(jpos, 1, (_fs->GetCurrentPos() - jpos));
                    break;
                }
                else
                {
                    return;
                }
            }
        }
        void LogicalAndExp()
        {
            BitwiseOrExp();
            for(;;)
            {
                switch(_token)
                {
                    case TK_AND:
                        {
                            VXInteger first_exp = _fs->PopTarget();
                            VXInteger trg = _fs->PushTarget();
                            _fs->AddInstruction(_OP_AND, trg, 0, first_exp, 0);
                            VXInteger jpos = _fs->GetCurrentPos();
                            if(trg != first_exp)
                            {
                                _fs->AddInstruction(_OP_MOVE, trg, first_exp);
                            }
                            Lex();
                            LogicalAndExp();
                            _fs->SnoozeOpt();
                            VXInteger second_exp = _fs->PopTarget();
                            if(trg != second_exp)
                            {
                                _fs->AddInstruction(_OP_MOVE, trg, second_exp);
                            }
                            _fs->SnoozeOpt();
                            _fs->SetIntructionParam(jpos, 1, (_fs->GetCurrentPos() - jpos));
                            break;
                        }
                    case TK_IN:
                        BIN_EXP(_OP_EXISTS, &VXCompiler::BitwiseOrExp);
                        break;
                    case TK_INSTANCEOF:
                        BIN_EXP(_OP_INSTANCEOF, &VXCompiler::BitwiseOrExp);
                        break;
                    default:
                        return;
                }
            }
        }
        void BitwiseOrExp()
        {
            BitwiseXorExp();
            for(;;)
            {
                if(_token == '|')
                {
                    BIN_EXP(_OP_BITW, &VXCompiler::BitwiseXorExp,BW_OR);
                }
                else
                {
                    return;
                }
            }
        }
        void BitwiseXorExp()
        {
            BitwiseAndExp();
            for(;;)
            {
                if(_token == '^')
                {
                    BIN_EXP(_OP_BITW, &VXCompiler::BitwiseAndExp,BW_XOR);
                }
                else
                {
                    return;
                }
            }
        }
        void BitwiseAndExp()
        {
            EqExp();
            for(;;)
            {
                if(_token == '&')
                {
                    BIN_EXP(_OP_BITW, &VXCompiler::EqExp,BW_AND);
                }
                else
                {
                    return;
                }
            }
        }
        void EqExp()
        {
            CompExp();
            for(;;)
            {
                switch(_token)
                {
                    case TK_EQ:
                        BIN_EXP(_OP_EQ, &VXCompiler::CompExp);
                        break;
                    case TK_NE:
                        BIN_EXP(_OP_NE, &VXCompiler::CompExp);
                        break;
                    case TK_3WAYSCMP:
                        BIN_EXP(_OP_CMP, &VXCompiler::CompExp,CMP_3W);
                        break;
                    default:
                        return;
                }
            }
        }
        void CompExp()
        {
            ShiftExp();
            for(;;)
            {
                switch(_token)
                {
                    case '>':
                        BIN_EXP(_OP_CMP, &VXCompiler::ShiftExp,CMP_G);
                        break;
                    case '<':
                        BIN_EXP(_OP_CMP, &VXCompiler::ShiftExp,CMP_L);
                        break;
                    case TK_GE:
                        BIN_EXP(_OP_CMP, &VXCompiler::ShiftExp,CMP_GE);
                        break;
                    case TK_LE:
                        BIN_EXP(_OP_CMP, &VXCompiler::ShiftExp,CMP_LE);
                        break;
                    default:
                        return;
                }
            }
        }

        void ShiftExp()
        {
            PlusExp();
            for(;;)
            {
                switch(_token)
                {
                    case TK_USHIFTR:
                        BIN_EXP(_OP_BITW, &VXCompiler::PlusExp,BW_USHIFTR);
                        break;
                    case TK_SHIFTL:
                        BIN_EXP(_OP_BITW, &VXCompiler::PlusExp,BW_SHIFTL);
                        break;
                    case TK_SHIFTR:
                        BIN_EXP(_OP_BITW, &VXCompiler::PlusExp,BW_SHIFTR);
                        break;
                    default: return;
                }
            }
        }

        VXOpcode ChooseArithOpByToken(VXInteger tok)
        {
            switch(tok)
            {
                case TK_PLUSEQ:
                case '+':
                    return _OP_ADD;
                case TK_MINUSEQ:
                case '-':
                    return _OP_SUB;
                case TK_MULEQ:
                    case '*':
                    return _OP_MUL;
                case TK_DIVEQ:
                case '/':
                    return _OP_DIV;
                case TK_MODEQ:
                case '%':
                    return _OP_MOD;
                default:
                    vox_assert(0);
            }
            return _OP_ADD;
        }

        VXInteger ChooseCompArithCharByToken(VXInteger tok)
        {
            VXInteger oper;
            switch(tok)
            {
                case TK_MINUSEQ:
                    oper = '-';
                    break;
                case TK_PLUSEQ:
                    oper = '+'; break;
                case TK_MULEQ:
                    oper = '*'; break;
                case TK_DIVEQ:
                    oper = '/'; break;
                case TK_MODEQ:
                    oper = '%'; break;
                default:
                    oper = 0; //shut up compiler
                    vox_assert(0);
                    break;
            };
            return oper;
        }

        void PlusExp()
        {
            MultExp();
            for(;;)
            {
                switch(_token)
                {
                    case '+':
                    case '-':
                        BIN_EXP(ChooseArithOpByToken(_token), &VXCompiler::MultExp);
                        break;
                    default:
                        return;
                }
            }
        }

        void MultExp()
        {
            PrefixedExpr();
            for(;;)
            {
                switch(_token)
                {
                    case '*':
                    case '/':
                    case '%':
                        BIN_EXP(ChooseArithOpByToken(_token), &VXCompiler::PrefixedExpr);
                        break;
                    default:
                        return;
                }
            }
        }

        //if 'pos' != -1 the previous variable is a local variable
        void PrefixedExpr()
        {
            VXInteger pos = Factor();
            for(;;)
            {
                switch(_token)
                {
                    case '.':
                        pos = -1;
                        Lex();
                        _fs->AddInstruction(
                            _OP_LOAD,
                            _fs->PushTarget(),
                            _fs->GetConstant(Expect(TK_IDENTIFIER)));
                        if(_es.etype==BASE)
                        {
                            Emit2ArgsOP(_OP_GET);
                            pos = _fs->TopTarget();
                            _es.etype = EXPR;
                            _es.epos   = pos;
                        }
                        else
                        {
                            if(NeedGet())
                            {
                                Emit2ArgsOP(_OP_GET);
                            }
                            _es.etype = OBJECT;
                        }
                        break;
                    case '[':
                        if(_lex._prevtoken == '\n')
                        {
                            Error("cannot brake deref/or comma needed after [exp]=exp slot declaration");
                        }
                        Lex();
                        Expression();
                        Expect(']');
                        pos = -1;
                        if(_es.etype==BASE)
                        {
                            Emit2ArgsOP(_OP_GET);
                            pos = _fs->TopTarget();
                            _es.etype = EXPR;
                            _es.epos   = pos;
                        }
                        else
                        {
                            if(NeedGet())
                            {
                                Emit2ArgsOP(_OP_GET);
                            }
                            _es.etype = OBJECT;
                        }
                        break;
                    case TK_MINUSMINUS:
                    case TK_PLUSPLUS:
                        {
                            if(IsEndOfStatement())
                                return;
                            VXInteger diff = (_token==TK_MINUSMINUS) ? -1 : 1;
                            Lex();
                            switch(_es.etype)
                            {
                                case EXPR:
                                    Error("can't '++' or '--' an expression");
                                    break;
                                case OBJECT:
                                case BASE:
                                    Emit2ArgsOP(_OP_PINC, diff);
                                    break;
                                case LOCAL:
                                    {
                                        VXInteger src = _fs->PopTarget();
                                        _fs->AddInstruction(
                                            _OP_PINCL,
                                            _fs->PushTarget(),
                                            src, 0, diff);
                                    }
                                    break;
                                case OUTER:
                                    {
                                        VXInteger tmp1 = _fs->PushTarget();
                                        VXInteger tmp2 = _fs->PushTarget();
                                        _fs->AddInstruction(_OP_GETOUTER,
                                            tmp2, _es.epos);
                                        _fs->AddInstruction(_OP_PINCL,
                                            tmp1, tmp2, 0, diff);
                                        _fs->AddInstruction(_OP_SETOUTER,
                                            tmp2, _es.epos, tmp2);
                                        _fs->PopTarget();
                                    }
                            }
                        }
                        return;
                        break;
                    case '(':
                        switch(_es.etype)
                        {
                            case OBJECT:
                            {
                                /* location of the key */
                                VXInteger key = _fs->PopTarget();
                                /* location of the object */
                                VXInteger table   = _fs->PopTarget();
                                /* location for the closure */
                                VXInteger closure = _fs->PushTarget();
                                /* location for 'this' pointer */
                                VXInteger ttarget = _fs->PushTarget();
                                _fs->AddInstruction(_OP_PREPCALL,
                                        closure, key, table, ttarget);
                                }
                                break;
                            case BASE:
                                //Emit2ArgsOP(_OP_GET);
                                _fs->AddInstruction(_OP_MOVE,
                                    _fs->PushTarget(), 0);
                                break;
                            case OUTER:
                                _fs->AddInstruction(_OP_GETOUTER,
                                    _fs->PushTarget(), _es.epos);
                                _fs->AddInstruction(_OP_MOVE,
                                    _fs->PushTarget(), 0);
                                break;
                            default:
                                _fs->AddInstruction(_OP_MOVE,
                                    _fs->PushTarget(), 0);
                        }
                        _es.etype = EXPR;
                        Lex();
                        FunctionCallArgs();
                        break;
                    default:
                        return;
                }
            }
        }
        VXInteger Factor()
        {
            _es.etype = EXPR;
            switch(_token)
            {
                case '{':
                    _fs->AddInstruction(_OP_NEWOBJ,
                                        _fs->PushTarget(),
                                        0, NVX_OT_TABLE);
                    Lex();
                    ParseTableOrClass(',', '}');
                    break;
                case '[':
                {
                        _fs->AddInstruction(_OP_NEWOBJ,
                                            _fs->PushTarget(),0,0,
                                            NVX_OT_ARRAY);
                        VXInteger apos = _fs->GetCurrentPos();
                        VXInteger key = 0;
                        Lex();
                        while(_token != ']')
                        {
                            Expression();
                            if(_token == ',')
                            {
                                Lex();
                            }
                            VXInteger val = _fs->PopTarget();
                            VXInteger array = _fs->TopTarget();
                            _fs->AddInstruction(_OP_APPENDARRAY,
                                                array,
                                                val,
                                                AAT_STACK);
                            key++;
                        }
                        _fs->SetIntructionParam(apos, 1, key);
                        Lex();
                    }
                    break;
                case TK_FUNCTION:
                    FunctionExp(_token);
                    break;
                case '@':
                    FunctionExp(_token,true);
                    break;
                case TK_CLASS:
                    Lex();
                    ClassExp();
                    break;
                case '-':
                    Lex();
                    switch(_token)
                    {
                        case TK_INTEGER:
                            EmitLoadConstInt(-_lex._nvalue,-1);
                            Lex();
                            break;
                        case TK_FLOAT:
                            EmitLoadConstFloat(-_lex._fvalue,-1);
                            Lex();
                            break;
                        default:
                            UnaryOP(_OP_NEG);
                    }
                    break;
                case '!':
                    Lex();
                    UnaryOP(_OP_NOT);
                    break;
                case '~':
                    Lex();
                    if(_token == TK_INTEGER)
                    {
                        EmitLoadConstInt(~_lex._nvalue,-1);
                        Lex();
                        break;
                    }
                    UnaryOP(_OP_BWNOT);
                    break;
                case TK_STRING_LITERAL:
                    _fs->AddInstruction(_OP_LOAD,
                        _fs->PushTarget(),
                        _fs->GetConstant(
                            _fs->CreateString(_lex._svalue,
                                              _lex._longstr.size()-1)));
                    Lex();
                    break;
                case TK_BASE:
                    Lex();
                    _fs->AddInstruction(_OP_GETBASE,
                                        _fs->PushTarget());
                    _es.etype = BASE;
                    _es.epos = _fs->TopTarget();
                    return (_es.epos);
                    break;
                case TK_IDENTIFIER:
                case TK_CONSTRUCTOR:
                case TK_THIS:
                    {
                        VXRawObj id;
                        VXRawObj constant;

                        switch(_token)
                        {
                            case TK_IDENTIFIER:
                                id = _fs->CreateString(_lex._svalue);
                                break;
                            case TK_THIS:
                                id = _fs->CreateString(NAMES_TK_THISVAR);
                                break;
                            case TK_CONSTRUCTOR:
                                id = _fs->CreateString(NAMES_TK_CTOR);
                                break;
                        }

                        VXInteger pos = -1;
                        Lex();
                        if((pos = _fs->GetLocalVariable(id)) != -1)
                        {
                            /* Handle a local variable (includes 'this') */
                            _fs->PushTarget(pos);
                            _es.etype  = LOCAL;
                            _es.epos   = pos;
                        }

                        else if((pos = _fs->GetOuterVariable(id)) != -1)
                        {
                            /* Handle a free var */
                            if(NeedGet())
                            {
                                _es.epos  = _fs->PushTarget();
                                _fs->AddInstruction(_OP_GETOUTER, _es.epos, pos);
                                /* _es.etype = EXPR; already default value */
                            }
                            else
                            {
                                _es.etype = OUTER;
                                _es.epos  = pos;
                            }
                        }

                        else if(_fs->IsConstant(id, constant))
                        {
                            /* Handle named constant */
                            VXObject constval;
                            VXRawObj    constid;
                            if(type(constant) == VX_OT_TABLE)
                            {
                                Expect('.');
                                constid = Expect(TK_IDENTIFIER);
                                if(!_table(constant)->Get(constid, constval))
                                {
                                    constval.Null();
                                    Error("invalid constant [%s.%s]",
                                          _stringval(id),
                                          _stringval(constid));
                                }
                            }
                            else
                            {
                                constval = constant;
                            }
                            _es.epos = _fs->PushTarget();

                            /* generate direct or literal function depending on size */
                            VXOType ctype = type(constval);
                            switch(ctype)
                            {
                                case VX_OT_INTEGER:
                                    EmitLoadConstInt(_integer(constval),
                                                     _es.epos);
                                    break;
                                case VX_OT_FLOAT:
                                    EmitLoadConstFloat(_float(constval),
                                                       _es.epos);
                                    break;
                                default:
                                    _fs->AddInstruction(
                                        _OP_LOAD,
                                        _es.epos,
                                        _fs->GetConstant(constval));
                                    break;
                            }
                            _es.etype = EXPR;
                        }
                        else
                        {
                            /*
                            * Handle a non-local variable, aka a field.
                            * Push the 'this' pointer on
                            * the virtual stack (always found in offset 0, so
                            * no instruction needs to
                            * be generated), and push the key next. Generate an
                            * _OP_LOAD instruction
                            * for the latter. If we are not using the variable as
                            * a dref expr, generate
                            * the _OP_GET instruction.
                            */
                            _fs->PushTarget(0);
                            _fs->AddInstruction(
                                _OP_LOAD,
                                _fs->PushTarget(),
                                _fs->GetConstant(id));
                            if(NeedGet())
                            {
                                Emit2ArgsOP(_OP_GET);
                            }
                            _es.etype = OBJECT;
                        }
                        return _es.epos;
                    }
                    break;
                case TK_DOUBLE_COLON:  // "::"
                    _fs->AddInstruction(_OP_LOADROOT, _fs->PushTarget());
                    _es.etype = OBJECT;
                    _token = '.'; /* hack: drop into PrefixExpr, case '.'*/
                    _es.epos = -1;
                    return _es.epos;
                    break;
                case TK_NULL:
                    _fs->AddInstruction(_OP_LOADNULLS,
                                        _fs->PushTarget(),1);
                    Lex();
                    break;
                case TK_INTEGER:
                    EmitLoadConstInt(_lex._nvalue,-1);
                    Lex();
                    break;
                case TK_FLOAT:
                    EmitLoadConstFloat(_lex._fvalue,-1);
                    Lex();
                    break;
                case TK_TRUE:
                case TK_FALSE:
                    _fs->AddInstruction(
                        _OP_LOADBOOL,
                        _fs->PushTarget(),
                        _token == TK_TRUE ? 1 : 0);
                    Lex();
                    break;
                case TK_TYPEOF:
                    Lex();
                    UnaryOP(_OP_TYPEOF);
                    break;
                case TK_RESUME:
                    Lex();
                    UnaryOP(_OP_RESUME);
                    break;
                case TK_CLONE:
                    Lex();
                    UnaryOP(_OP_CLONE);
                    break;
                case TK_MINUSMINUS:
                case TK_PLUSPLUS:
                    PrefixIncDec(_token);
                    break;
                case TK_DELETE:
                    DeleteExpr();
                    break;
                case '(':
                    Lex();
                    CommaExpr();
                    Expect(')');
                    break;
                default:
                    Error("expression expected");
            }
            return -1;
        }

        void EmitLoadConstInt(VXInteger value,VXInteger target)
        {
            if(target < 0)
            {
                target = _fs->PushTarget();
            }
            //does it fit in 32 bits?
            if((value & (~((VXInteger)0xFFFFFFFF))) == 0)
            {
                _fs->AddInstruction(_OP_LOADINT, target,value);
            }
            else
            {
                _fs->AddInstruction(_OP_LOAD,
                                    target,
                                    _fs->GetNumericConstant(value));
            }
        }
        void EmitLoadConstFloat(VXFloat value,VXInteger target)
        {
            if(target < 0)
            {
                target = _fs->PushTarget();
            }
            if(sizeof(VXFloat) == sizeof(VXInt32))
            {
                _fs->AddInstruction(_OP_LOADFLOAT, target,*((VXInt32 *)&value));
            }
            else
            {
                _fs->AddInstruction(_OP_LOAD,
                                    target,
                                    _fs->GetNumericConstant(value));
            }
        }

        void UnaryOP(VXOpcode op)
        {
            PrefixedExpr();
            VXInteger src = _fs->PopTarget();
            _fs->AddInstruction(op, _fs->PushTarget(), src);
        }

        bool NeedGet()
        {
            switch(_token)
            {
                case '=':
                case '(':
                case TK_NEWSLOT:
                case TK_MODEQ:
                case TK_MULEQ:
                case TK_DIVEQ:
                case TK_MINUSEQ:
                case TK_PLUSEQ:
                case TK_PLUSPLUS:
                case TK_MINUSMINUS:
                    return false;
            }
            return (!_es.donot_get ||
                    (_es.donot_get &&
                     (_token == '.' || _token == '[')));
        }
        void FunctionCallArgs()
        {
            VXInteger nargs = 1;//this
            while(_token != ')')
            {
                Expression();
                MoveIfCurrentTargetIsLocal();
                nargs++;
                if(_token == ',')
                {
                    Lex();
                    if(_token == ')')
                    {
                        Error("expression expected, found ')'");
                    }
                }
            }
            Lex();
            for(VXInteger i = 0; i < (nargs - 1); i++)
            {
                _fs->PopTarget();
            }
            VXInteger stackbase = _fs->PopTarget();
            VXInteger closure = _fs->PopTarget();
            _fs->AddInstruction(_OP_CALL,
                                _fs->PushTarget(), closure,
                                stackbase, nargs);
        }

        void ParseTableOrClass(VXInteger separator,VXInteger terminator)
        {
            VXInteger tpos = _fs->GetCurrentPos();
            VXInteger nkeys = 0;
            while(_token != terminator)
            {
                bool hasattrs = false;
                bool isstatic = false;
                //check if is an attribute
                if(separator == ';')
                {
                    if(_token == TK_ATTR_OPEN)
                    {
                        _fs->AddInstruction(_OP_NEWOBJ,
                                            _fs->PushTarget(),0,
                                            NVX_OT_TABLE);
                        Lex();
                        ParseTableOrClass(',', TK_ATTR_CLOSE);
                        hasattrs = true;
                    }
                    if(_token == TK_STATIC)
                    {
                        isstatic = true;
                        Lex();
                    }
                }
                switch(_token)
                {
                    case TK_FUNCTION:
                    case TK_CONSTRUCTOR:
                        {
                            VXInteger tk = _token;
                            Lex();
                            VXRawObj id = (tk == TK_FUNCTION ?
                                           Expect(TK_IDENTIFIER) :
                                           _fs->CreateString("constructor"));
                            Expect('(');
                            _fs->AddInstruction(_OP_LOAD,
                                                _fs->PushTarget(),
                                                _fs->GetConstant(id));
                            CreateFunction(id);
                            _fs->AddInstruction(_OP_CLOSURE,
                                                _fs->PushTarget(),
                                                _fs->_functions.size() - 1, 0);
                        }
                        break;
                    case '[':
                        Lex();
                        CommaExpr();
                        Expect(']');
                        Expect('=');
                        Expression();
                        break;
                    //case TK_IDENTIFIER:
                    case TK_STRING_LITERAL: //JSON
                        //only works for tables
                        if(separator == ',')
                        {
                            _fs->AddInstruction(
                                _OP_LOAD,
                                _fs->PushTarget(),
                                _fs->GetConstant(Expect(TK_STRING_LITERAL)));
                            Expect(':');
                            Expression();
                            break;
                        }
                    default:
                        _fs->AddInstruction(
                            _OP_LOAD,
                            _fs->PushTarget(),
                            _fs->GetConstant(Expect(TK_IDENTIFIER)));
                        Expect('=');
                        Expression();
                }
                if(_token == separator)
                {
                    Lex();//optional comma/semicolon
                }
                nkeys++;
                VXInteger val = _fs->PopTarget();
                VXInteger key = _fs->PopTarget();
                VXInteger attrs = hasattrs ? _fs->PopTarget():-1;
                vox_assert((hasattrs && (attrs == key-1)) || !hasattrs);
                unsigned char flags = (
                    (hasattrs ? NEW_SLVX_OT_ATTRIBUTES_FLAG : 0) |
                    (isstatic ? NEW_SLVX_OT_STATIC_FLAG : 0));
                // vvv BECAUSE OF THIS NO COMMON EMIT FUNC IS POSSIBLE
                VXInteger table = _fs->TopTarget();
                //hack recognizes a table from the separator
                if(separator == ',')
                {
                    _fs->AddInstruction(_OP_NEWSLOT, 0xFF, table, key, val);
                }
                else
                {
                    //this for classes only as it invokes _newmember
                    _fs->AddInstruction(_OP_NEWSLOTA, flags, table, key, val);
                }
            }
            if(separator == ',') //hack recognizes a table from the separator
            {
                _fs->SetIntructionParam(tpos, 1, nkeys);
            }
            Lex();
        }
        void LocalDeclStatement()
        {
            VXRawObj varname;
            Lex();
            if( _token == TK_FUNCTION)
            {
                Lex();
                varname = Expect(TK_IDENTIFIER);
                Expect('(');
                CreateFunction(varname, false);
                _fs->AddInstruction(_OP_CLOSURE,
                                    _fs->PushTarget(),
                                    _fs->_functions.size() - 1, 0);
                _fs->PopTarget();
                _fs->PushLocalVariable(varname);
                return;
            }
            do
            {
                varname = Expect(TK_IDENTIFIER);
                if(_token == '=')
                {
                    Lex();
                    Expression();
                    VXInteger src = _fs->PopTarget();
                    VXInteger dest = _fs->PushTarget();
                    if(dest != src)
                    {
                        _fs->AddInstruction(_OP_MOVE, dest, src);
                    }
                }
                else
                {
                    _fs->AddInstruction(_OP_LOADNULLS, _fs->PushTarget(),1);
                }
                _fs->PopTarget();
                _fs->PushLocalVariable(varname);
                if(_token == ',')
                {
                    Lex();
                }
                else
                {
                    break;
                }
            } while(1);
        }

        void IfStatement()
        {
            VXInteger jmppos;
            bool haselse = false;
            Lex();
            Expect('(');
            CommaExpr();
            Expect(')');
            _fs->AddInstruction(_OP_JZ, _fs->PopTarget());
            VXInteger jnepos = _fs->GetCurrentPos();
            BEGIN_VXCOPE();
            Statement();
            //
            if(_token != '}' && _token != TK_ELSE)
            {
                OptionalSemicolon();
            }
            END_VXCOPE();
            VXInteger endifblock = _fs->GetCurrentPos();
            if(_token == TK_ELSE)
            {
                haselse = true;
                BEGIN_VXCOPE();
                _fs->AddInstruction(_OP_JMP);
                jmppos = _fs->GetCurrentPos();
                Lex();
                Statement();
                OptionalSemicolon();
                END_VXCOPE();
                _fs->SetIntructionParam(jmppos, 1,
                                        _fs->GetCurrentPos() - jmppos);
            }
            _fs->SetIntructionParam(jnepos, 1,
                                    endifblock - jnepos + (haselse?1:0));
        }

        void WhileStatement()
        {
            VXInteger jzpos, jmppos;
            jmppos = _fs->GetCurrentPos();
            Lex();
            Expect('(');
            CommaExpr();
            Expect(')');
            BEGIN_BREAKBLE_BLOCK();
            _fs->AddInstruction(_OP_JZ, _fs->PopTarget());
            jzpos = _fs->GetCurrentPos();
            BEGIN_VXCOPE();
            Statement();
            END_VXCOPE();
            _fs->AddInstruction(_OP_JMP, 0, jmppos - _fs->GetCurrentPos() - 1);
            _fs->SetIntructionParam(jzpos, 1, _fs->GetCurrentPos() - jzpos);
            END_BREAKBLE_BLOCK(jmppos);
        }

        void DoWhileStatement()
        {
            Lex();
            VXInteger jmptrg = _fs->GetCurrentPos();
            BEGIN_BREAKBLE_BLOCK()
            BEGIN_VXCOPE();
            Statement();
            END_VXCOPE();
            Expect(TK_WHILE);
            VXInteger continuetrg = _fs->GetCurrentPos();
            Expect('(');
            CommaExpr();
            Expect(')');
            _fs->AddInstruction(_OP_JZ, _fs->PopTarget(), 1);
            _fs->AddInstruction(_OP_JMP, 0, jmptrg - _fs->GetCurrentPos() - 1);
            END_BREAKBLE_BLOCK(continuetrg);
        }

        void ForStatement()
        {
            Lex();
            BEGIN_VXCOPE();
            Expect('(');
            if(_token == TK_LOCAL)
            {
                LocalDeclStatement();
            }
            else if(_token != ';')
            {
                CommaExpr();
                _fs->PopTarget();
            }
            Expect(';');
            _fs->SnoozeOpt();
            VXInteger jmppos = _fs->GetCurrentPos();
            VXInteger jzpos = -1;
            if(_token != ';')
            {
                CommaExpr();
                _fs->AddInstruction(_OP_JZ, _fs->PopTarget());
                jzpos = _fs->GetCurrentPos();
            }
            Expect(';');
            _fs->SnoozeOpt();
            VXInteger expstart = _fs->GetCurrentPos() + 1;
            if(_token != ')')
            {
                CommaExpr();
                _fs->PopTarget();
            }
            Expect(')');
            _fs->SnoozeOpt();
            VXInteger expend = _fs->GetCurrentPos();
            VXInteger expsize = (expend - expstart) + 1;
            VXInstructionVec exp;
            if(expsize > 0)
            {
                for(VXInteger i = 0; i < expsize; i++)
                {
                    exp.push_back(_fs->GetInstruction(expstart + i));
                }
                _fs->PopInstructions(expsize);
            }
            BEGIN_BREAKBLE_BLOCK()
            Statement();
            VXInteger continuetrg = _fs->GetCurrentPos();
            if(expsize > 0)
            {
                for(VXInteger i = 0; i < expsize; i++)
                {
                    _fs->AddInstruction(exp[i]);
                }
            }
            _fs->AddInstruction(_OP_JMP, 0, jmppos - _fs->GetCurrentPos() - 1, 0);
            if(jzpos>  0)
            {
                _fs->SetIntructionParam(jzpos, 1, _fs->GetCurrentPos() - jzpos);
            }
            END_VXCOPE();
            END_BREAKBLE_BLOCK(continuetrg);
        }

        void ForEachStatement()
        {
            VXRawObj idxname, valname;
            Lex();
            Expect('(');
            valname = Expect(TK_IDENTIFIER);
            if(_token == ',')
            {
                idxname = valname;
                Lex();
                valname = Expect(TK_IDENTIFIER);
            }
            else
            {
                idxname = _fs->CreateString("@INDEX@");
            }
            Expect(TK_IN);

            //save the stack size
            BEGIN_VXCOPE();
            //put the table in the stack(evaluate the table expression)
            Expression();
            Expect(')');
            VXInteger container = _fs->TopTarget();
            //push the index local var
            VXInteger indexpos = _fs->PushLocalVariable(idxname);
            _fs->AddInstruction(_OP_LOADNULLS, indexpos,1);
            //push the value local var
            VXInteger valuepos = _fs->PushLocalVariable(valname);
            _fs->AddInstruction(_OP_LOADNULLS, valuepos,1);
            //push reference index
            //use invalid id to make it inaccessible
            VXInteger itrpos = _fs->PushLocalVariable(_fs->CreateString("@ITERATOR@"));
            _fs->AddInstruction(_OP_LOADNULLS, itrpos,1);
            VXInteger jmppos = _fs->GetCurrentPos();
            _fs->AddInstruction(_OP_FOREACH, container, 0, indexpos);
            VXInteger foreachpos = _fs->GetCurrentPos();
            _fs->AddInstruction(_OP_POSTFOREACH, container, 0, indexpos);
            //generate the statement code
            BEGIN_BREAKBLE_BLOCK()
            Statement();
            _fs->AddInstruction(_OP_JMP, 0, jmppos - _fs->GetCurrentPos() - 1);
            _fs->SetIntructionParam(foreachpos, 1, _fs->GetCurrentPos() - foreachpos);
            _fs->SetIntructionParam(foreachpos + 1, 1, _fs->GetCurrentPos() - foreachpos);
            END_BREAKBLE_BLOCK(foreachpos - 1);
            //restore the local variable stack(remove index,val and ref idx)
            END_VXCOPE();
        }

        void SwitchStatement()
        {
            Lex();
            Expect('(');
            CommaExpr();
            Expect(')');
            Expect('{');
            VXInteger expr = _fs->TopTarget();
            bool bfirst = true;
            VXInteger tonextcondjmp = -1;
            VXInteger skipcondjmp = -1;
            VXInteger __nbreaks__ = _fs->_unresolvedbreaks.size();
            _fs->_breaktargets.push_back(0);
            while(_token == TK_CASE)
            {
                if(!bfirst)
                {
                    _fs->AddInstruction(_OP_JMP, 0, 0);
                    skipcondjmp = _fs->GetCurrentPos();
                    _fs->SetIntructionParam(tonextcondjmp, 1, _fs->GetCurrentPos() - tonextcondjmp);
                }
                //condition
                Lex();
                Expression();
                Expect(':');
                VXInteger trg = _fs->PopTarget();
                _fs->AddInstruction(_OP_EQ, trg, trg, expr);
                _fs->AddInstruction(_OP_JZ, trg, 0);
                //end condition
                if(skipcondjmp != -1)
                {
                    _fs->SetIntructionParam(skipcondjmp, 1,
                                            (_fs->GetCurrentPos()-skipcondjmp));
                }
                tonextcondjmp = _fs->GetCurrentPos();
                BEGIN_VXCOPE();
                Statements();
                END_VXCOPE();
                bfirst = false;
            }
            if(tonextcondjmp != -1)
            {
                _fs->SetIntructionParam(tonextcondjmp, 1,
                                        _fs->GetCurrentPos()-tonextcondjmp);
            }
            if(_token == TK_DEFAULT)
            {
                Lex();
                Expect(':');
                BEGIN_VXCOPE();
                Statements();
                END_VXCOPE();
            }
            Expect('}');
            _fs->PopTarget();
            __nbreaks__ = _fs->_unresolvedbreaks.size() - __nbreaks__;
            if(__nbreaks__ > 0)
            {
                ResolveBreaks(_fs, __nbreaks__);
            }
            _fs->_breaktargets.pop_back();
        }

        void FunctionStatement()
        {
            VXRawObj id;
            Lex(); id = Expect(TK_IDENTIFIER);
            _fs->PushTarget(0);
            _fs->AddInstruction(_OP_LOAD, _fs->PushTarget(), _fs->GetConstant(id));
            if(_token == TK_DOUBLE_COLON)
            {
                Emit2ArgsOP(_OP_GET);
            }
            while(_token == TK_DOUBLE_COLON)
            {
                Lex();
                id = Expect(TK_IDENTIFIER);
                _fs->AddInstruction(_OP_LOAD,
                                    _fs->PushTarget(),
                                    _fs->GetConstant(id));
                if(_token == TK_DOUBLE_COLON)
                {
                    Emit2ArgsOP(_OP_GET);
                }
            }
            Expect('(');
            CreateFunction(id);
            _fs->AddInstruction(_OP_CLOSURE,
                                _fs->PushTarget(),
                                _fs->_functions.size() - 1, 0);
            EmitDerefOp(_OP_NEWSLOT);
            _fs->PopTarget();
        }

        void ClassStatement()
        {
            VXExpState es;
            Lex();
            es = _es;
            _es.donot_get = true;
            PrefixedExpr();
            if(_es.etype == EXPR)
            {
                Error("invalid class name");
            }
            else if(_es.etype == OBJECT || _es.etype == BASE)
            {
                ClassExp();
                EmitDerefOp(_OP_NEWSLOT);
                _fs->PopTarget();
            }
            else
            {
                Error("cannot create a class in a local with the syntax(class <local>)");
            }
            _es = es;
        }

        VXRawObj ExpectScalar()
        {
            VXRawObj val;
            val._type = VX_OT_NULL; val._unVal.nInteger = 0; //shut up GCC 4.x
            switch(_token)
            {
                case TK_INTEGER:
                    val._type = VX_OT_INTEGER;
                    val._unVal.nInteger = _lex._nvalue;
                    break;
                case TK_FLOAT:
                    val._type = VX_OT_FLOAT;
                    val._unVal.fFloat = _lex._fvalue;
                    break;
                case TK_STRING_LITERAL:
                    val = _fs->CreateString(_lex._svalue,_lex._longstr.size()-1);
                    break;
                case '-':
                    Lex();
                    switch(_token)
                    {
                    case TK_INTEGER:
                        val._type = VX_OT_INTEGER;
                        val._unVal.nInteger = -_lex._nvalue;
                    break;
                    case TK_FLOAT:
                        val._type = VX_OT_FLOAT;
                        val._unVal.fFloat = -_lex._fvalue;
                    break;
                    default:
                        Error("scalar expected: integer, float");
                    }
                    break;
                default:
                    Error("scalar expected: integer, float or string");
            }
            Lex();
            return val;
        }
        void EnumStatement()
        {
            Lex();
            VXRawObj id = Expect(TK_IDENTIFIER);
            Expect('{');
            VXRawObj table = _fs->CreateTable();
            VXInteger nval = 0;
            while(_token != '}')
            {
                VXRawObj key = Expect(TK_IDENTIFIER);
                VXRawObj val;
                if(_token == '=')
                {
                    Lex();
                    val = ExpectScalar();
                }
                else
                {
                    val._type = VX_OT_INTEGER;
                    val._unVal.nInteger = nval++;
                }
                _table(table)->NewSlot(VXObject(key),VXObject(val));
                if(_token == ',')
                {
                    Lex();
                }
            }
            VXTableObj *enums = _table(_ss(_vm)->_consts);
            VXObject strongid = id;
            enums->NewSlot(VXObject(strongid),VXObject(table));
            strongid.Null();
            Lex();
        }

        void TryCatchStatement()
        {
            VXRawObj exid;
            Lex();
            _fs->AddInstruction(_OP_PUSHTRAP,0,0);
            _fs->_traps++;
            if(_fs->_breaktargets.size())
            {
                _fs->_breaktargets.top()++;
            }

            if(_fs->_continuetargets.size())
            {
                _fs->_continuetargets.top()++;
            }

            VXInteger trappos = _fs->GetCurrentPos();
            {
                BEGIN_VXCOPE();
                Statement();
                END_VXCOPE();
            }
            _fs->_traps--;
            _fs->AddInstruction(_OP_POPTRAP, 1, 0);
            if(_fs->_breaktargets.size())
            {
                _fs->_breaktargets.top()--;
            }
            if(_fs->_continuetargets.size())
            {
                _fs->_continuetargets.top()--;
            }
            _fs->AddInstruction(_OP_JMP, 0, 0);
            VXInteger jmppos = _fs->GetCurrentPos();
            _fs->SetIntructionParam(trappos, 1, (_fs->GetCurrentPos() - trappos));
            Expect(TK_CATCH);
            Expect('(');
            exid = Expect(TK_IDENTIFIER);
            Expect(')');
            {
                BEGIN_VXCOPE();
                VXInteger ex_target = _fs->PushLocalVariable(exid);
                _fs->SetIntructionParam(trappos, 0, ex_target);
                Statement();
                _fs->SetIntructionParams(jmppos, 0, (_fs->GetCurrentPos() - jmppos), 0);
                END_VXCOPE();
            }
        }

        void FunctionExp(VXInteger ftype,bool lambda = false)
        {
            Lex();
            Expect('(');
            VXObject dummy;
            CreateFunction(dummy,lambda);
            _fs->AddInstruction(_OP_CLOSURE,
                                _fs->PushTarget(),
                                _fs->_functions.size() - 1,
                                ftype == TK_FUNCTION?0:1);
        }

        void ClassExp()
        {
            VXInteger base = -1;
            VXInteger attrs = -1;
            if(_token == TK_EXTENDS)
            {
                Lex();
                Expression();
                base = _fs->TopTarget();
            }

            if(_token == TK_ATTR_OPEN)
            {
                Lex();
                _fs->AddInstruction(_OP_NEWOBJ, _fs->PushTarget(),0,NVX_OT_TABLE);
                ParseTableOrClass(',',TK_ATTR_CLOSE);
                attrs = _fs->TopTarget();
            }
            Expect('{');
            if(attrs != -1)
            {
                _fs->PopTarget();
            }
            if(base != -1)
            {
                _fs->PopTarget();
            }
            _fs->AddInstruction(_OP_NEWOBJ,
                                _fs->PushTarget(),
                                base, attrs, NVX_OT_CLASS);
            ParseTableOrClass(';', '}');
        }

        void DeleteExpr()
        {
            VXExpState es;
            Lex();
            es = _es;
            _es.donot_get = true;
            PrefixedExpr();
            if(_es.etype==EXPR)
            {
                Error("can't delete an expression");
            }
            if(_es.etype==OBJECT || _es.etype==BASE)
            {
                Emit2ArgsOP(_OP_DELETE);
            }
            else
            {
                Error("cannot delete an (outer) local");
            }
            _es = es;
        }

        void PrefixIncDec(VXInteger token)
        {
            VXExpState  es;
            VXInteger diff = (token==TK_MINUSMINUS) ? -1 : 1;
            Lex();
            es = _es;
            _es.donot_get = true;
            PrefixedExpr();
            if(_es.etype==EXPR)
            {
                Error("can't '++' or '--' an expression");
            }
            else if(_es.etype==OBJECT || _es.etype==BASE)
            {
                Emit2ArgsOP(_OP_INC, diff);
            }
            else if(_es.etype==LOCAL)
            {
                VXInteger src = _fs->TopTarget();
                _fs->AddInstruction(_OP_INCL, src, src, 0, diff);
            }
            else if(_es.etype==OUTER)
            {
                VXInteger tmp = _fs->PushTarget();
                _fs->AddInstruction(_OP_GETOUTER, tmp, _es.epos);
                _fs->AddInstruction(_OP_INCL,     tmp, tmp, 0, diff);
                _fs->AddInstruction(_OP_SETOUTER, tmp, _es.epos, tmp);
            }
            _es = es;
        }

        void CreateFunction(VXRawObj &name,bool lambda = false)
        {
            VXFuncState *funcstate = _fs->PushChildState(_ss(_vm));
            funcstate->_name = name;
            VXRawObj paramname;
            funcstate->AddParameter(_fs->CreateString(NAMES_TK_THISVAR));
            funcstate->_sourcename = _sourcename;
            VXInteger defparams = 0;
            while(_token!=')')
            {
                if(_token == TK_VARPARAMS)
                {
                    if(defparams > 0)
                    {
                        Error("function with default parameters cannot"
                              " have variable number of parameters");
                    }
                    funcstate->AddParameter(_fs->CreateString(NAMES_VARGV));
                    funcstate->_varparams = true;
                    Lex();
                    if(_token != ')')
                    {
                        Error("expected ')'");
                    }
                    break;
                }
                else
                {
                    paramname = Expect(TK_IDENTIFIER);
                    funcstate->AddParameter(paramname);
                    if(_token == '=')
                    {
                        Lex();
                        Expression();
                        funcstate->AddDefaultParam(_fs->TopTarget());
                        defparams++;
                    }
                    else
                    {
                        if(defparams > 0)
                        {
                            Error("expected '='");
                        }
                    }
                    if(_token == ',')
                    {
                        Lex();
                    }
                    else if(_token != ')')
                    {
                        Error("expected ')' or ','");
                    }
                }
            }
            Expect(')');
            for(VXInteger n = 0; n < defparams; n++)
            {
                _fs->PopTarget();
            }

            VXFuncState *currchunk = _fs;
            _fs = funcstate;
            if(lambda)
            {
                Expression();
                _fs->AddInstruction(_OP_RETURN, 1, _fs->PopTarget());
            }
            else
            {
                Statement(false);
            }
            funcstate->AddLineInfos(
                (_lex._prevtoken == '\n') ?
                _lex._lasttokenline : _lex._currentline,
                _lineinfo, true);
            funcstate->AddInstruction(_OP_RETURN, -1);
            funcstate->SetStackSize(0);

            VXFuncProtoObj *func = funcstate->BuildProto();
    #ifdef _DEBUG_DUMP
            funcstate->Dump(func);
    #endif
            _fs = currchunk;
            _fs->_functions.push_back(func);
            _fs->PopChildState();
        }

        void ResolveBreaks(VXFuncState *funcstate, VXInteger ntoresolve)
        {
            while(ntoresolve > 0)
            {
                VXInteger pos = funcstate->_unresolvedbreaks.back();
                funcstate->_unresolvedbreaks.pop_back();
                //set the jmp instruction
                funcstate->SetIntructionParams(pos, 0, funcstate->GetCurrentPos() - pos, 0);
                ntoresolve--;
            }
        }

        void ResolveContinues(VXFuncState *funcstate,
                              VXInteger ntoresolve,
                              VXInteger targetpos)
        {
            while(ntoresolve > 0)
            {
                VXInteger pos = funcstate->_unresolvedcontinues.back();
                funcstate->_unresolvedcontinues.pop_back();
                //set the jmp instruction
                funcstate->SetIntructionParams(pos, 0, targetpos - pos, 0);
                ntoresolve--;
            }
        }

    private:
        VXInteger _token;
        VXFuncState *_fs;
        VXObject _sourcename;
        VXLexer _lex;
        bool _lineinfo;
        bool _raiseerror;
        VXInteger _debugline;
        VXInteger _debugop;
        VXExpState   _es;
        VXScope _scope;
        char *compilererror;
        jmp_buf _errorjmp;
        VXState *_vm;
};

bool Compile(VXState *vm,VXLexReadFunc rg,
             VXUserPointer up,
             const char *sourcename,
             VXObject &out,
             bool raiseerror,
             bool lineinfo)
{
    VXCompiler p(vm, rg, up, sourcename, raiseerror, lineinfo);
    return p.Compile(out);
}

#endif
