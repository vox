
#include "pcheader.hpp"
#ifndef VOX_NO_COMPILER
#include "compiler.hpp"
#include "string.hpp"
#include "funcproto.hpp"
#include "table.hpp"
#include "opcodes.hpp"
#include "funcstate.hpp"

#ifdef _DEBUG_DUMP
VXInstructionDesc g_InstrDesc[]=
{
    {"_OP_LINE"},
    {"_OP_LOAD"},
    {"_OP_LOADINT"},
    {"_OP_LOADFLOAT"},
    {"_OP_DLOAD"},
    {"_OP_TAILCALL"},
    {"_OP_CALL"},
    {"_OP_PREPCALL"},
    {"_OP_PREPCALLK"},
    {"_OP_GETK"},
    {"_OP_MOVE"},
    {"_OP_NEWSLOT"},
    {"_OP_DELETE"},
    {"_OP_SET"},
    {"_OP_GET"},
    {"_OP_EQ"},
    {"_OP_NE"},
    {"_OP_ADD"},
    {"_OP_SUB"},
    {"_OP_MUL"},
    {"_OP_DIV"},
    {"_OP_MOD"},
    {"_OP_BITW"},
    {"_OP_RETURN"},
    {"_OP_LOADNULLS"},
    {"_OP_LOADROOT"},
    {"_OP_LOADBOOL"},
    {"_OP_DMOVE"},
    {"_OP_JMP"},
    {"_OP_JCMP"},
    {"_OP_JZ"},
    {"_OP_SETOUTER"},
    {"_OP_GETOUTER"},
    {"_OP_NEWOBJ"},
    {"_OP_APPENDARRAY"},
    {"_OP_COMPARITH"},
    {"_OP_INC"},
    {"_OP_INCL"},
    {"_OP_PINC"},
    {"_OP_PINCL"},
    {"_OP_CMP"},
    {"_OP_EXISTS"},
    {"_OP_INSTANCEOF"},
    {"_OP_AND"},
    {"_OP_OR"},
    {"_OP_NEG"},
    {"_OP_NOT"},
    {"_OP_BWNOT"},
    {"_OP_CLOSURE"},
    {"_OP_YIELD"},
    {"_OP_RESUME"},
    {"_OP_FOREACH"},
    {"_OP_POSTFOREACH"},
    {"_OP_CLONE"},
    {"_OP_TYPEOF"},
    {"_OP_PUSHTRAP"},
    {"_OP_POPTRAP"},
    {"_OP_THROW"},
    {"_OP_NEWSLOTA"},
    {"_OP_GETBASE"},
    {"_OP_CLOSE"},
    {"_OP_JCMP"}
};
#endif
void DumpLiteral(VXObject &o)
{
    switch(type(o))
    {
        case VX_OT_STRING:
            printf("\"%s\"", _stringval(o));
            break;
        case VX_OT_FLOAT:
            printf("{%f}", _float(o));
            break;
        case VX_OT_INTEGER:
            printf("{" _PRINT_INT_FMT "}", _integer(o));
            break;
        case VX_OT_BOOL:
            printf("%s", _integer(o) ? "true" : "false");
            break;
        default:
            printf("(%s %p)", GetTypeName(o), &_rawval(o));
            break;
            break;               //shut up compiler
    }
}

VXFuncState::VXFuncState(VXSharedState *ss,VXFuncState *parent,CompilerErrorFunc efunc,void *ed)
{
    _nliterals = 0;
    _literals = VXTableObj::Create(ss,0);
    _strings =  VXTableObj::Create(ss,0);
    _sharedstate = ss;
    _lastline = 0;
    _optimization = true;
    _parent = parent;
    _stacksize = 0;
    _traps = 0;
    _returnexp = 0;
    _varparams = false;
    _errfunc = efunc;
    _errtarget = ed;
    _bgenerator = false;
    _outers = 0;
    _ss = ss;

}

void VXFuncState::Error(const char *err)
{
    _errfunc(_errtarget,err);
}

#ifdef _DEBUG_DUMP
void VXFuncState::Dump(VXFuncProtoObj *func)
{
    VXUnsignedInteger n=0,i;
    VXInteger si;
    printf("VXInstruction sizeof %d\n",sizeof(VXInstruction));
    printf("VXRawObj sizeof %d\n",sizeof(VXRawObj));
    printf("-----------------------------------------------------\n");
    printf("*****FUNCTION [%s]\n",
        type(func->_name)==VX_OT_STRING ? _stringval(func->_name):"unknown");
    printf("-----LITERALS\n");
    VXObject refidx,key,val;
    VXInteger idx;
    VXObjectVec templiterals;
    templiterals.resize(_nliterals);
    while((idx=_table(_literals)->Next(false,refidx,key,val))!=-1)
    {
        refidx=idx;
        templiterals[_integer(val)]=key;
    }
    for(i=0;i<templiterals.size();i++)
    {
        printf("[%d] ",n);
        DumpLiteral(templiterals[i]);
        printf("\n");
        n++;
    }
    printf("-----PARAMS\n");
    if(_varparams)
        printf("<<VARPARAMS>>\n");
    n=0;
    for(i=0;i<_parameters.size();i++)
    {
        printf("[%d] ",n);
        DumpLiteral(_parameters[i]);
        printf("\n");
        n++;
    }
    printf("-----LOCALS\n");
    for(si=0;si<func->_nlocalvarinfos;si++)
    {
        VXLocalVarInfo lvi=func->_localvarinfos[si];
        printf("[%d] %s \t%d %d\n",
            lvi._pos,_stringval(lvi._name),lvi._start_op,lvi._end_op);
        n++;
    }
    printf("-----LINE INFO\n");
    for(i=0;i<_lineinfos.size();i++)
    {
        VXLineInfo li=_lineinfos[i];
        printf("op [%d] line [%d] \n",li._op,li._line);
        n++;
    }
    printf("-----dump\n");
    n=0;
    for(i=0;i<_instructions.size();i++)
    {
        VXInstruction &inst=_instructions[i];
        if(inst.op==_OP_LOAD || inst.op==_OP_DLOAD || inst.op==_OP_PREPCALLK || inst.op==_OP_GETK )
        {

            VXInteger lidx = inst._arg1;
            printf("[%03d] %15s %d ",n,g_InstrDesc[inst.op].name,inst._arg0);
            if(lidx >= 0xFFFFFFFF)
                printf("null");
            else
            {
                VXInteger refidx;
                VXObject val,key,refo;
                while(((refidx=_table(_literals)->Next(false,refo,key,val))!= -1) && (_integer(val) != lidx))
                {
                    refo = refidx;
                }
                DumpLiteral(key);
            }
            if(inst.op != _OP_DLOAD)
            {
                printf(" %d %d \n",inst._arg2,inst._arg3);
            }
            else
            {
                printf(" %d ",inst._arg2);
                lidx = inst._arg3;
                if(lidx >= 0xFFFFFFFF)
                    printf("null");
                else
                {
                    VXInteger refidx;
                    VXObject val,key,refo;
                    while(((refidx=_table(_literals)->Next(false,refo,key,val))!= -1) && (_integer(val) != lidx))
                    {
                        refo = refidx;
                    }
                    DumpLiteral(key);
                    printf("\n");
                }
            }
        }
        else if(inst.op==_OP_LOADFLOAT)
        {
            printf("[%03d] %15s %d %f %d %d\n",
                n,g_InstrDesc[inst.op].name,
                inst._arg0,
                *((VXFloat*)&inst._arg1),
                inst._arg2,
                inst._arg3);
        }
        /*  else if(inst.op==_OP_ARITH){
                printf("[%03d] %15s %d %d %d %c\n",
                    n,g_InstrDesc[inst.op].name,
                    inst._arg0,
                    inst._arg1,
                    inst._arg2,
                    inst._arg3);
            }*/
        else
        {
            printf("[%03d] %15s %d %d %d %d\n",
                n,g_InstrDesc[inst.op].name,
                inst._arg0,
                inst._arg1,
                inst._arg2,
                inst._arg3);
        }
        n++;
    }
    printf("-----\n");
    printf("stack size[%d]\n",func->_stacksize);
    printf("---------------------------------------------------\n\n");
}
#endif

VXInteger VXFuncState::GetNumericConstant(const VXInteger cons)
{
    return GetConstant(VXObject(cons));
}

VXInteger VXFuncState::GetNumericConstant(const VXFloat cons)
{
    return GetConstant(VXObject(cons));
}

VXInteger VXFuncState::GetConstant(const VXRawObj &cons)
{
    VXObject val;
    if(!_table(_literals)->Get(cons,val))
    {
        val = _nliterals;
        _table(_literals)->NewSlot(cons,val);
        _nliterals++;
        if(_nliterals > MAX_LITERALS)
        {
            val.Null();
            Error("internal compiler error: too many literals");
        }
    }
    return _integer(val);
}

void VXFuncState::SetIntructionParams(VXInteger pos,VXInteger arg0,VXInteger arg1,VXInteger arg2,VXInteger arg3)
{
    _instructions[pos]._arg0=(unsigned char)*((VXUnsignedInteger *)&arg0);
    _instructions[pos]._arg1=(VXInt32)*((VXUnsignedInteger *)&arg1);
    _instructions[pos]._arg2=(unsigned char)*((VXUnsignedInteger *)&arg2);
    _instructions[pos]._arg3=(unsigned char)*((VXUnsignedInteger *)&arg3);
}

void VXFuncState::SetIntructionParam(VXInteger pos,VXInteger arg,VXInteger val)
{
    switch(arg)
    {
        case 0:_instructions[pos]._arg0=(unsigned char)*((VXUnsignedInteger *)&val);break;
        case 1:case 4:_instructions[pos]._arg1=(VXInt32)*((VXUnsignedInteger *)&val);break;
        case 2:_instructions[pos]._arg2=(unsigned char)*((VXUnsignedInteger *)&val);break;
        case 3:_instructions[pos]._arg3=(unsigned char)*((VXUnsignedInteger *)&val);break;
    };
}

VXInteger VXFuncState::AllocStackPos()
{
    VXInteger npos=_vlocals.size();
    _vlocals.push_back(VXLocalVarInfo());
    if(_vlocals.size()>((VXUnsignedInteger)_stacksize))
    {
        if(_stacksize>MAX_FUNC_STACKSIZE)
        {
            Error("internal compiler error: too many locals");
        }
        _stacksize=_vlocals.size();
    }
    return npos;
}

VXInteger VXFuncState::PushTarget(VXInteger n)
{
    if(n!=-1)
    {
        _targetstack.push_back(n);
        return n;
    }
    n=AllocStackPos();
    _targetstack.push_back(n);
    return n;
}

VXInteger VXFuncState::GetUpTarget(VXInteger n)
{
    return _targetstack[((_targetstack.size()-1)-n)];
}

VXInteger VXFuncState::TopTarget()
{
    return _targetstack.back();
}

VXInteger VXFuncState::PopTarget()
{
    VXInteger npos=_targetstack.back();
    VXLocalVarInfo &t=_vlocals[_targetstack.back()];
    if(type(t._name)==VX_OT_NULL)
    {
        _vlocals.pop_back();
    }
    _targetstack.pop_back();
    return npos;
}

VXInteger VXFuncState::GetStackSize()
{
    return _vlocals.size();
}

VXInteger VXFuncState::CountOuters(VXInteger stacksize)
{
    VXInteger outers = 0;
    VXInteger k = _vlocals.size() - 1;
    while(k >= stacksize)
    {
        VXLocalVarInfo &lvi = _vlocals[k];
        k--;
                                 //this means is an outer
        if(lvi._end_op == UINT_MINUS_ONE)
        {
            outers++;
        }
    }
    return outers;
}

void VXFuncState::SetStackSize(VXInteger n)
{
    VXInteger size=_vlocals.size();
    while(size>n)
    {
        size--;
        VXLocalVarInfo lvi = _vlocals.back();
        if(type(lvi._name)!=VX_OT_NULL)
        {
                                 //this means is an outer
            if(lvi._end_op == UINT_MINUS_ONE)
            {
                _outers--;
            }
            lvi._end_op = GetCurrentPos();
            _localvarinfos.push_back(lvi);
        }
        _vlocals.pop_back();
    }
}

bool VXFuncState::IsConstant(const VXRawObj &name,VXRawObj &e)
{
    VXObject val;
    if(_table(_sharedstate->_consts)->Get(name,val))
    {
        e = val;
        return true;
    }
    return false;
}

bool VXFuncState::IsLocal(VXUnsignedInteger stkpos)
{
    if(stkpos>=_vlocals.size())return false;
    else if(type(_vlocals[stkpos]._name)!=VX_OT_NULL)return true;
    return false;
}

VXInteger VXFuncState::PushLocalVariable(const VXRawObj &name)
{
    VXInteger pos=_vlocals.size();
    VXLocalVarInfo lvi;
    lvi._name=name;
    lvi._start_op=GetCurrentPos()+1;
    lvi._pos=_vlocals.size();
    _vlocals.push_back(lvi);
    if(_vlocals.size()>((VXUnsignedInteger)_stacksize))_stacksize=_vlocals.size();
    return pos;
}

VXInteger VXFuncState::GetLocalVariable(const VXRawObj &name)
{
    VXInteger locals=_vlocals.size();
    while(locals>=1)
    {
        VXLocalVarInfo &lvi = _vlocals[locals-1];
        if(type(lvi._name)==VX_OT_STRING && _string(lvi._name)==_string(name))
        {
            return locals-1;
        }
        locals--;
    }
    return -1;
}

void VXFuncState::MarkLocalAsOuter(VXInteger pos)
{
    VXLocalVarInfo &lvi = _vlocals[pos];
    lvi._end_op = UINT_MINUS_ONE;
    _outers++;
}

VXInteger VXFuncState::GetOuterVariable(const VXRawObj &name)
{
    VXInteger outers = _outervalues.size();
    for(VXInteger i = 0; i<outers; i++)
    {
        if(_string(_outervalues[i]._name) == _string(name))
            return i;
    }
    VXInteger pos=-1;
    if(_parent)
    {
        pos = _parent->GetLocalVariable(name);
        if(pos == -1)
        {
            pos = _parent->GetOuterVariable(name);
            if(pos != -1)
            {
                                 //local
                _outervalues.push_back(VXOuterObjVar(name,VXObject(VXInteger(pos)),otOUTER));
                return _outervalues.size() - 1;
            }
        }
        else
        {
            _parent->MarkLocalAsOuter(pos);
                                 //local
            _outervalues.push_back(VXOuterObjVar(name,VXObject(VXInteger(pos)),otLOCAL));
            return _outervalues.size() - 1;

        }
    }
    return -1;
}

void VXFuncState::AddParameter(const VXRawObj &name)
{
    PushLocalVariable(name);
    _parameters.push_back(name);
}

void VXFuncState::AddLineInfos(VXInteger line,bool lineop,bool force)
{
    if(_lastline!=line || force)
    {
        VXLineInfo li;
        li._line=line;li._op=(GetCurrentPos()+1);
        if(lineop)AddInstruction(_OP_LINE,0,line);
        if(_lastline!=line)
        {
            _lineinfos.push_back(li);
        }
        _lastline=line;
    }
}

void VXFuncState::DiscardTarget()
{
    VXInteger discardedtarget = PopTarget();
    VXInteger size = _instructions.size();
    if(size > 0 && _optimization)
    {
                                 //previous instruction
        VXInstruction &pi = _instructions[size-1];
        switch(pi.op)
        {
            case _OP_SET:case _OP_NEWSLOT:case _OP_SETOUTER:case _OP_CALL:
                if(pi._arg0 == discardedtarget)
                {
                    pi._arg0 = 0xFF;
                }
        }
    }
}

void VXFuncState::AddInstruction(VXInstruction &i)
{
    VXInteger size = _instructions.size();
    if(size > 0 && _optimization)//simple optimizer
    {
                                 //previous instruction
        VXInstruction &pi = _instructions[size-1];
        switch(i.op)
        {
            case _OP_JZ:
                if( pi.op == _OP_CMP && pi._arg1 < 0xFF)
                {
                    pi.op = _OP_JCMP;
                    pi._arg0 = (unsigned char)pi._arg1;
                    pi._arg1 = i._arg1;
                    return;
                }
            case _OP_SET:
            case _OP_NEWSLOT:
                if(i._arg0 == i._arg3)
                {
                    i._arg0 = 0xFF;
                }
                break;
            case _OP_SETOUTER:
                if(i._arg0 == i._arg2)
                {
                    i._arg0 = 0xFF;
                }
                break;
            case _OP_RETURN:
                if(_parent &&
                    (i._arg0 != MAX_FUNC_STACKSIZE) &&
                    (pi.op == _OP_CALL) &&
                    (_returnexp < (size-1)))
                {
                    pi.op = _OP_TAILCALL;
                }
                else if(pi.op == _OP_CLOSE)
                {
                    pi = i;
                    return;
                }
                break;
            case _OP_GET:
                if(pi.op == _OP_LOAD && pi._arg0 == i._arg2 && (!IsLocal(pi._arg0)))
                {
                    pi._arg1 = pi._arg1;
                    pi._arg2 = (unsigned char)i._arg1;
                    pi.op = _OP_GETK;
                    pi._arg0 = i._arg0;
                    return;
                }
                break;
            case _OP_PREPCALL:
                if( pi.op == _OP_LOAD  && pi._arg0 == i._arg1 && (!IsLocal(pi._arg0)))
                {
                    pi.op = _OP_PREPCALLK;
                    pi._arg0 = i._arg0;
                    pi._arg1 = pi._arg1;
                    pi._arg2 = i._arg2;
                    pi._arg3 = i._arg3;
                    return;
                }
                break;
            case _OP_APPENDARRAY:
            {
                VXInteger aat = -1;
                switch(pi.op)
                {
                    case _OP_LOAD: aat = AAT_LITERAL; break;
                    case _OP_LOADINT: aat = AAT_INT; break;
                    case _OP_LOADBOOL: aat = AAT_BOOL; break;
                    case _OP_LOADFLOAT: aat = AAT_FLOAT; break;
                    default: break;
                }
                if(aat != -1 && pi._arg0 == i._arg1 && (!IsLocal(pi._arg0)))
                {
                    pi.op = _OP_APPENDARRAY;
                    pi._arg0 = i._arg0;
                    pi._arg1 = pi._arg1;
                    pi._arg2 = (unsigned char)aat;
                    pi._arg3 = MAX_FUNC_STACKSIZE;
                    return;
                }
            }
            break;
            case _OP_MOVE:
                switch(pi.op)
                {
                    case _OP_GET:
                    case _OP_ADD:
                    case _OP_SUB:
                    case _OP_MUL:
                    case _OP_DIV:
                    case _OP_MOD:
                    case _OP_BITW:
                    case _OP_LOADINT:
                    case _OP_LOADFLOAT:
                    case _OP_LOADBOOL:
                    case _OP_LOAD:
                        if(pi._arg0 == i._arg1)
                        {
                            pi._arg0 = i._arg0;
                            _optimization = false;
                            //_result_elimination = false;
                            return;
                        }
                }

                if(pi.op == _OP_MOVE)
                {
                    pi.op = _OP_DMOVE;
                    pi._arg2 = i._arg0;
                    pi._arg3 = (unsigned char)i._arg1;
                    return;
                }
                break;
            case _OP_LOAD:
                if(pi.op == _OP_LOAD && i._arg1 < 256)
                {
                    pi.op = _OP_DLOAD;
                    pi._arg2 = i._arg0;
                    pi._arg3 = (unsigned char)i._arg1;
                    return;
                }
                break;
            case _OP_EQ:
            case _OP_NE:
                if(pi.op == _OP_LOAD && pi._arg0 == i._arg1 && (!IsLocal(pi._arg0)))
                {
                    pi.op = i.op;
                    pi._arg0 = i._arg0;
                    pi._arg1 = pi._arg1;
                    pi._arg2 = i._arg2;
                    pi._arg3 = MAX_FUNC_STACKSIZE;
                    return;
                }
                break;
            case _OP_LOADNULLS:
                if((pi.op == _OP_LOADNULLS && pi._arg0+pi._arg1 == i._arg0))
                {
                    pi._arg1 = pi._arg1 + 1;
                    pi.op = _OP_LOADNULLS;
                    return;
                }
                break;
            case _OP_LINE:
                if(pi.op == _OP_LINE)
                {
                    _instructions.pop_back();
                    _lineinfos.pop_back();
                }
                break;
        }
    }
    _optimization = true;
    _instructions.push_back(i);
}

VXRawObj VXFuncState::CreateString(const char *s,VXInteger len)
{
    VXObject ns(VXStringObj::Create(_sharedstate,s,len));
    _table(_strings)->NewSlot(ns,(VXInteger)1);
    return ns;
}

VXRawObj VXFuncState::CreateTable()
{
    VXObject nt(VXTableObj::Create(_sharedstate,0));
    _table(_strings)->NewSlot(nt,(VXInteger)1);
    return nt;
}

VXFuncProtoObj *VXFuncState::BuildProto()
{

    VXFuncProtoObj *f=VXFuncProtoObj::Create(_ss,_instructions.size(),
        _nliterals,_parameters.size(),_functions.size(),_outervalues.size(),
        _lineinfos.size(),_localvarinfos.size(),_defaultparams.size());

    VXObject refidx,key,val;
    VXInteger idx;

    f->_stacksize = _stacksize;
    f->_sourcename = _sourcename;
    f->_bgenerator = _bgenerator;
    f->_name = _name;

    while((idx=_table(_literals)->Next(false,refidx,key,val))!=-1)
    {
        f->_literals[_integer(val)]=key;
        refidx=idx;
    }

    for(VXUnsignedInteger nf = 0; nf < _functions.size(); nf++) f->_functions[nf] = _functions[nf];
    for(VXUnsignedInteger np = 0; np < _parameters.size(); np++) f->_parameters[np] = _parameters[np];
    for(VXUnsignedInteger no = 0; no < _outervalues.size(); no++) f->_outervalues[no] = _outervalues[no];
    for(VXUnsignedInteger nl = 0; nl < _localvarinfos.size(); nl++) f->_localvarinfos[nl] = _localvarinfos[nl];
    for(VXUnsignedInteger ni = 0; ni < _lineinfos.size(); ni++) f->_lineinfos[ni] = _lineinfos[ni];
    for(VXUnsignedInteger nd = 0; nd < _defaultparams.size(); nd++) f->_defaultparams[nd] = _defaultparams[nd];

    memcpy(f->_instructions,&_instructions[0],_instructions.size()*sizeof(VXInstruction));

    f->_varparams = _varparams;

    return f;
}

VXFuncState *VXFuncState::PushChildState(VXSharedState *ss)
{
    VXFuncState *child = (VXFuncState *)vox_malloc(sizeof(VXFuncState));
    new (child) VXFuncState(ss,this,_errfunc,_errtarget);
    _childstates.push_back(child);
    return child;
}

void VXFuncState::PopChildState()
{
    VXFuncState *child = _childstates.back();
    vox_delete(child,VXFuncState);
    _childstates.pop_back();
}

VXFuncState::~VXFuncState()
{
    while(_childstates.size() > 0)
    {
        PopChildState();
    }
}
#endif
