
#ifndef _VXSTATE_H_
#define _VXSTATE_H_

#include "utils.hpp"
#include "object.hpp"

struct VXStringObj;
struct VXTableObj;
//max number of character for a printed number
#define NUMBER_MAX_CHAR 50

struct VXStringObjTable
{
    VXStringObjTable(VXSharedState*ss);
    ~VXStringObjTable();
    VXStringObj *Add(const char *,VXInteger len);
    void Remove(VXStringObj *);
    private:
        void Resize(VXInteger size);
        void AllocNodes(VXInteger size);
        VXStringObj **_strings;
        VXUnsignedInteger _numofslots;
        VXUnsignedInteger _slotused;
        VXSharedState *_sharedstate;
};

struct RefTable
{
    struct RefNode
    {
        VXObject obj;
        VXUnsignedInteger refs;
        struct RefNode *next;
    };
    RefTable();
    ~RefTable();
    void AddRef(VXRawObj &obj);
    bool Release(VXRawObj &obj);
    VXUnsignedInteger GetRefCount(VXRawObj &obj);
    #ifndef NO_GARBAGE_COLLECTOR
    void Mark(VXCollectable **chain);
    #endif
    void Finalize();
    private:
        RefNode *Get(VXRawObj &obj,VXHash &mainpos,RefNode **prev,bool add);
        RefNode *Add(VXHash mainpos,VXRawObj &obj);
        void Resize(VXUnsignedInteger size);
        void AllocNodes(VXUnsignedInteger size);
        VXUnsignedInteger _numofslots;
        VXUnsignedInteger _slotused;
        RefNode *_nodes;
        RefNode *_freelist;
        RefNode **_buckets;
};

#define ADD_STRING(ss,str,len) ss->_stringtable->Add(str,len)
#define REMOVE_STRING(ss,bstr) ss->_stringtable->Remove(bstr)

struct VXObject;

struct VXSharedState
{
    VXSharedState();
    ~VXSharedState();
    void Init();
    public:
        char* GetScratchPad(VXInteger size);
        VXInteger GetMetaMethodIdxByName(const VXObject &name);
    #ifndef NO_GARBAGE_COLLECTOR
        VXInteger CollectGarbage(VXState *vm);
        void RunMark(VXState *vm,VXCollectable **tchain);
        VXInteger ResurrectUnreachable(VXState *vm);
        static void MarkObject(VXObject &o,VXCollectable **chain);
    #endif
        VXObjectVec *_metamethods;
        VXObject _metamethodsmap;
        VXObjectVec *_systemstrings;
        VXObjectVec *_types;
        VXStringObjTable *_stringtable;
        RefTable _refs_table;
        VXObject _registry;
        VXObject _consts;
        VXObject _constructoridx;
    #ifndef NO_GARBAGE_COLLECTOR
        VXCollectable *_gc_chain;
    #endif
        VXObject _root_vm;
        VXObject _table_default_delegate;
        static VXRegFunction _table_default_delegate_funcz[];
        VXObject _array_default_delegate;
        static VXRegFunction _array_default_delegate_funcz[];
        VXObject _string_default_delegate;
        static VXRegFunction _string_default_delegate_funcz[];
        VXObject _number_default_delegate;
        static VXRegFunction _number_default_delegate_funcz[];
        VXObject _generator_default_delegate;
        static VXRegFunction _generator_default_delegate_funcz[];
        VXObject _closure_default_delegate;
        static VXRegFunction _closure_default_delegate_funcz[];
        VXObject _thread_default_delegate;
        static VXRegFunction _thread_default_delegate_funcz[];
        VXObject _class_default_delegate;
        static VXRegFunction _class_default_delegate_funcz[];
        VXObject _instance_default_delegate;
        static VXRegFunction _instance_default_delegate_funcz[];
        VXObject _weakref_default_delegate;
        static VXRegFunction _weakref_default_delegate_funcz[];

        VXCompileError _compilererrorhandler;
        VXPrintFunction _printfunc;
        VXPrintFunction _errorfunc;
        bool _debuginfo;
        bool _notifyallexceptions;
    private:
        char *_scratchpad;
        VXInteger _scratchpadsize;
};

#define _table_ddel     _table(_sharedstate->_table_default_delegate)
#define _array_ddel     _table(_sharedstate->_array_default_delegate)
#define _string_ddel    _table(_sharedstate->_string_default_delegate)
#define _number_ddel    _table(_sharedstate->_number_default_delegate)
#define _generator_ddel _table(_sharedstate->_generator_default_delegate)
#define _closure_ddel   _table(_sharedstate->_closure_default_delegate)
#define _thread_ddel    _table(_sharedstate->_thread_default_delegate)
#define _class_ddel     _table(_sharedstate->_class_default_delegate)
#define _instance_ddel  _table(_sharedstate->_instance_default_delegate)
#define _weakref_ddel   _table(_sharedstate->_weakref_default_delegate)

#define rsl(l) (l)

//extern VXObject _null_;

bool CompileTypemask(VXIntVec &res,const char *typemask);
#endif                           //_VXSTATE_H_
