
#include <cstdarg>
#include <cassert>
#include "pcheader.hpp"
#include "vm.hpp"
#include "string.hpp"
#include "table.hpp"
#include "funcproto.hpp"
#include "closure.hpp"
#include "userdata.hpp"
#include "compiler.hpp"
#include "funcstate.hpp"
#include "class.hpp"
#include "debug.hpp"

VXInteger vox_aux_invalidtype(VXState* v,VXOType type)
{
    return v->ThrowError("unexpected type '%s'", IdType2Name(type));
}

void vox_aux_printcallstack(VXState* v)
{
    const char* fn;
    const char* src;
    VXPrintFunction pf = v->GetErrorFunc();
    if(pf)
    {
        VXStackInfos si;
        //1 is to skip this function that is level 0
        VXInteger level=1;
        while(VX_SUCCEEDED(v->StackInfos(level, &si)))
        {
            fn = get_funcname(si);
            src = get_sourcename(si);
            pf(v, "  At '%s' in [%s:%d]\n", fn, src, si.line, si.column);
            level++;
        }
    }
}

VXInteger vox_aux_printerror(VXState* v)
{
    VXPrintFunction pf = v->GetErrorFunc();
    const char* err;
    if(pf)
    {

        if(v->GetTop() >= 1)
        {
            err = v->LastError();
            pf(v, "\nRuntime Error: %s\n", err);
            vox_aux_printcallstack(v);
        }
    }
    return 0;
}

void vox_aux_compiler_error(VXState* v,
                            const char *sErr,
                            const char *sSource,
                            VXInteger line,
                            VXInteger column)
{
    VXPrintFunction pf = v->GetErrorFunc();
    if(pf)
    {
        pf(v,
            "Syntax Error: in %s, line %d, column %d: %s\n",
            sSource, line, column, sErr);
    }
}

void vox_aux_asserthandler(const char* expr,
                           const char* funct,
                           const char* file,
                           int line)
{
    fprintf(stderr, "vox_assert(%s) failed:\n   At %s:%d, function '%s'\n",
        expr, file, line, funct);
    abort();
}



VXInteger vox_getfunctioninfo(VXState* v,VXInteger level,VXFunctionInfo *fi)
{
    VXInteger cssize = v->_callsstacksize;
    if (cssize > level)
    {
        VXState::CallInfo &ci = v->_callsstack[cssize-level-1];
        if(ci._closure.IsClosure())
        {
            VXForeignClosureObj *c = _closure(ci._closure);
            VXFuncProtoObj *proto = c->_function;
            fi->funcid = proto;
            fi->name = type(proto->_name) == VX_OT_STRING?_stringval(proto->_name):"unknown";
            fi->source = type(proto->_name) == VX_OT_STRING?_stringval(proto->_sourcename):"unknown";
            return VX_OK;
        }
    }
    return v->ThrowError("the object is not a closure");
}

