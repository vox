#define vox_new(__ptr, __type) \
    { \
        __ptr = (__type *)vox_mem_malloc(sizeof(__type)); \
        new(__ptr) __type; \
    }

#define vox_delete(__ptr,__type) \
    { \
        __ptr->~__type(); \
        vox_mem_free(__ptr, sizeof(__type)); \
    }

#define VX_MALLOC(__size) \
    vox_mem_malloc((__size));

#define VX_FREE(__ptr,__size) \
    vox_mem_free((__ptr), (__size));

#define VX_REALLOC(__ptr,__oldsize,__size) \
    vox_mem_realloc((__ptr),(__oldsize),(__size));
