
#ifndef _VXFunction_H_
#define _VXFunction_H_

#include "opcodes.hpp"
#include "object.hpp"
#include "string.hpp"
#include "mem.hpp"

enum VXOuterObjType
{
    otLOCAL = 0,
    otOUTER = 1
};

struct VXOuterObjVar
{

    VXOuterObjVar(){}
    VXOuterObjVar(const VXObject &name,const VXObject &src,VXOuterObjType t)
    {
        _name = name;
        _src=src;
        _type=t;
    }
    VXOuterObjVar(const VXOuterObjVar &ov)
    {
        _type=ov._type;
        _src=ov._src;
        _name=ov._name;
    }
    VXOuterObjType _type;
    VXObject _name;
    VXObject _src;
};

struct VXLocalVarInfo
{
    VXLocalVarInfo():_start_op(0),_end_op(0),_pos(0){}
    VXLocalVarInfo(const VXLocalVarInfo &lvi)
    {
        _name=lvi._name;
        _start_op=lvi._start_op;
        _end_op=lvi._end_op;
        _pos=lvi._pos;
    }
    VXObject _name;
    VXUnsignedInteger _start_op;
    VXUnsignedInteger _end_op;
    VXUnsignedInteger _pos;
};

struct VXLineInfo { VXInteger _line;VXInteger _op; };

typedef VXVector<VXOuterObjVar> VXOuterObjVarVec;
typedef VXVector<VXLocalVarInfo> VXLocalVarInfoVec;
typedef VXVector<VXLineInfo> VXLineInfoVec;

#define _FUNC_SIZE(ni,nl,nparams,nfuncs,nouters,nlineinf,localinf,defparams) (sizeof(VXFuncProtoObj) \
    +((ni-1)*sizeof(VXInstruction))+(nl*sizeof(VXObject)) \
    +(nparams*sizeof(VXObject))+(nfuncs*sizeof(VXObject)) \
    +(nouters*sizeof(VXOuterObjVar))+(nlineinf*sizeof(VXLineInfo)) \
    +(localinf*sizeof(VXLocalVarInfo))+(defparams*sizeof(VXInteger)))

struct VXFuncProtoObj : public CHAINABLE_OBJ
{
    private:
        VXFuncProtoObj(VXSharedState *ss);

        ~VXFuncProtoObj();

    public:
        static VXFuncProtoObj *Create(
                        VXSharedState *ss,
                        VXInteger ninstructions,
                        VXInteger nliterals,
                        VXInteger nparameters,
                        VXInteger nfunctions,
                        VXInteger noutervalues,
                        VXInteger nlineinfos,
                        VXInteger nlocalvarinfos,
                        VXInteger ndefaultparams)
        {
            VXFuncProtoObj *f;
            //I compact the whole class and members in a single memory allocation
            f = (VXFuncProtoObj *)vox_mem_malloc(_FUNC_SIZE(
                    ninstructions,
                    nliterals,
                    nparameters,
                    nfunctions,
                    noutervalues,
                    nlineinfos,
                    nlocalvarinfos,
                    ndefaultparams));
            new (f) VXFuncProtoObj(ss);
            f->_ninstructions = ninstructions;
            f->_literals = (VXObject*)&f->_instructions[ninstructions];
            f->_nliterals = nliterals;
            f->_parameters = (VXObject*)&f->_literals[nliterals];
            f->_nparameters = nparameters;
            f->_functions = (VXObject*)&f->_parameters[nparameters];
            f->_nfunctions = nfunctions;
            f->_outervalues = (VXOuterObjVar*)&f->_functions[nfunctions];
            f->_noutervalues = noutervalues;
            f->_lineinfos = (VXLineInfo *)&f->_outervalues[noutervalues];
            f->_nlineinfos = nlineinfos;
            f->_localvarinfos = (VXLocalVarInfo *)&f->_lineinfos[nlineinfos];
            f->_nlocalvarinfos = nlocalvarinfos;
            f->_defaultparams = (VXInteger *)&f->_localvarinfos[nlocalvarinfos];
            f->_ndefaultparams = ndefaultparams;

            VX_CONSTRUCT_VECTOR(VXObject,f->_nliterals,f->_literals);
            VX_CONSTRUCT_VECTOR(VXObject,f->_nparameters,f->_parameters);
            VX_CONSTRUCT_VECTOR(VXObject,f->_nfunctions,f->_functions);
            VX_CONSTRUCT_VECTOR(VXOuterObjVar,f->_noutervalues,f->_outervalues);
            VX_CONSTRUCT_VECTOR(VXLocalVarInfo,f->_nlocalvarinfos,f->_localvarinfos);
            return f;
        }

        void Release()
        {
            VX_DESTRUCT_VECTOR(VXObject,_nliterals,_literals);
            VX_DESTRUCT_VECTOR(VXObject,_nparameters,_parameters);
            VX_DESTRUCT_VECTOR(VXObject,_nfunctions,_functions);
            VX_DESTRUCT_VECTOR(VXOuterObjVar,_noutervalues,_outervalues);
            VX_DESTRUCT_VECTOR(VXLocalVarInfo,_nlocalvarinfos,_localvarinfos);
            VXInteger size = _FUNC_SIZE(
                                _ninstructions,
                                _nliterals,
                                _nparameters,
                                _nfunctions,
                                _noutervalues,
                                _nlineinfos,
                                _nlocalvarinfos,
                                _ndefaultparams);
            this->~VXFuncProtoObj();
            vox_mem_free(this,size);
        }

        const char* GetLocal(VXState *v,
                             VXUnsignedInteger stackbase,
                             VXUnsignedInteger nseq,
                             VXUnsignedInteger nop,
                             VXObject& dest);

        VXInteger GetLine(VXInstruction *curr);

        bool Save(VXState *v,VXUserPointer up,VXWriteFunc write);

       static bool Load(VXState *v,VXUserPointer up,VXReadFunc read,VXObject &ret);

    #ifndef NO_GARBAGE_COLLECTOR
        void Mark(VXCollectable **chain);

        void Finalize(){ _NULL_VXOBJECT_VECTOR(_literals,_nliterals); }

        VXOType GetType() {return VX_OT_FUNCPROTO;}
    #endif

        VXObject _sourcename;
        VXObject _name;
        VXInteger _stacksize;
        bool _bgenerator;
        VXInteger _varparams;

        VXInteger _nlocalvarinfos;
        VXLocalVarInfo *_localvarinfos;

        VXInteger _nlineinfos;
        VXLineInfo *_lineinfos;

        VXInteger _nliterals;
        VXObject *_literals;

        VXInteger _nparameters;
        VXObject *_parameters;

        VXInteger _nfunctions;
        VXObject *_functions;

        VXInteger _noutervalues;
        VXOuterObjVar *_outervalues;

        VXInteger _ndefaultparams;
        VXInteger *_defaultparams;

        VXInteger _ninstructions;
        VXInstruction _instructions[1];
};
#endif                           //_VXFunction_H_
