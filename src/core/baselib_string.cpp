
#include "baselib.hpp"
#include "string.hpp"
#include <vector>
#include <sstream>
#include <algorithm>
#include <climits>

#define MAX_FORMAT_LEN  20
#define MAX_WFORMAT_LEN 3
#define ADDITIONAL_FORMAT_SPACE (100*sizeof(char))
#define SOUNDEX_TABLE "01230120022455012623010202"
#define SOUNDEX_NULBIT "0000"

int strhelp_find(const std::string& subject, const std::string& findme)
{
    int n = 0;
    std::string ::size_type pos = 0;
    while((pos = subject.find(findme, pos)) != std::string::npos)
    {
        n++;
        pos += findme.size();
    }
    return n;
}


std::string strhelp_soundex(const std::string& subject)
{
    int count;
    char* instr;
    char* outptr;
    std::string result;
    std::string istring;
    std::vector<char> buffer;
    buffer.reserve(16);
    istring = subject;
    instr = (char*)(istring.c_str());
    outptr = &buffer[0];
    count = 0;
    while(!isalpha(instr[0]) && instr[0])
    {
        ++instr;
    }
    if(toupper(instr[0]) == 'P' && toupper(instr[1]) == 'H')
    {
        instr[0] = 'F';
        instr[1] = 'A';
    }
    *outptr++ = toupper(*instr++);
    while(*instr && count < 5)
    {
        if(isalpha(*instr) && *instr != *(instr-1))
        {
            *outptr = SOUNDEX_TABLE[toupper(instr[0]) - 'A'];
            if(*outptr != '0')
            {
                ++outptr;
                ++count;
            }
        }
        ++instr;
    }
    /* the following line is practically pointless,
    but kept for "historical purpose" */
    *outptr = '\0';
    result.append((std::string(&buffer[0]) + SOUNDEX_NULBIT).substr(0, 4));
    return result;
}

std::string strhelp_replace(const std::string& str,
                            const std::string& from,
                            const std::string& to)
{
    std::string tmp = str;
    size_t start_pos = 0;
    while((start_pos = tmp.find(from, start_pos)) != std::string::npos)
    {
        tmp.replace(start_pos, from.length(), to);
        // In case $to contains $from, like replacing 'x' with 'yx'
        start_pos += to.length();
    }
    return tmp;
}

std::string strhelp_quote(const std::string& str, bool escape_only)
{
    int ch;
    int quote = 34;
    std::string res;
    std::string::const_iterator iter;
    if(!escape_only) res.push_back(quote);
    for(iter=str.begin(); iter!=str.end(); iter++)
    {
        ch = *iter;
        switch(ch)
        {
            case '\\':
                res.push_back('\\');
                res.push_back(ch);
                break;
            case '"':
                res.append("\\\"");
                break;
            default:
                if(ch == 0 || iscntrl(ch) || ch > SCHAR_MAX)
                {
                    int fmtres;
                    int maxlen = 30;
                    char cbuf[maxlen+1];
                    fmtres = snprintf(cbuf, maxlen, "\\x%02X", ch);
                    res.append(cbuf, fmtres);
                }
                else
                {
                    res.push_back(ch);
                }
                break;
        }
    }
    if(!escape_only) res.push_back(quote);
    return res;
}



void strhelp_trim_l(const char *str,const char **start)
{
    const char *t = str;
    while(((*t) != '\0') && isspace(*t))
    {
        t++;
    }
    *start = t;
}

void strhelp_trim_r(const char *str,VXInteger len,const char **end)
{
    const char* t;
    if(len == 0)
    {
        *end = str;
        return;
    }
    t = &str[len-1];
    while(t != str && isspace(*t))
    {
        t--;
    }
    *end = t+1;
}

bool strhelp_startswith(const char* subject, VXInteger subject_len,
                        const char* search, VXInteger search_len)
{
    if(subject_len < search_len)
    {
        return false;
    }
    return strncmp(subject, search, search_len) == 0;
}


bool strhelp_endswith(const char* subject, VXInteger subject_len,
                      const char* search, VXInteger search_len)
{
    if(subject_len < search_len)
    {
        return false;
    }
    return strncmp(subject + subject_len - search_len, search, search_len) == 0;
}
std::vector<std::string> strhelp_split(const std::string& s,
                                       const std::string& delim,
                                       const bool keep_empty = true)
{
    // have to use std::vector, because split depends on
    // iterator magic
    std::vector<std::string> result;
    if (delim.empty())
    {
        result.push_back(s);
        return result;
    }
    std::string::const_iterator substart = s.begin(), subend;
    while(true)
    {
        subend = std::search(substart, s.end(), delim.begin(), delim.end());
        std::string temp(substart, subend);
        if (keep_empty || !temp.empty())
        {
            result.push_back(temp);
        }
        if (subend == s.end())
        {
            break;
        }
        substart = subend + delim.size();
    }
    return result;
}

VXInteger stringdeleg_quote(VXState* v)
{
    std::string subject;
    std::string result;
    v->GetString(1, &subject);
    result = strhelp_quote(subject, false);
    v->Push(v->NewString(result.c_str(), result.length()));
    return 1;
}

VXInteger stringdeleg_escape(VXState* v)
{
    std::string subject;
    std::string result;
    v->GetString(1, &subject);
    result = strhelp_quote(subject, true);
    v->Push(v->NewString(result.c_str(), result.length()));
    return 1;
}

VXInteger stringdeleg_replace(VXState* v)
{
    std::string subject;
    std::string search;
    std::string repl;
    std::string result;
    v->GetString(1, &subject);
    v->GetString(2, &search);
    v->GetString(3, &repl);
    result = strhelp_replace(subject, search, repl);
    v->Push(v->NewString(result.c_str(), result.length()));
    return 1;
}

VXInteger stringdeleg_soundex(VXState* v)
{
    std::string subject;
    std::string result;
    v->GetString(1, &subject);
    result = strhelp_soundex(subject);
    v->Push(v->NewString(result.c_str(), result.length()));
    return 1;
}

VXInteger stringdeleg_reverse(VXState* v)
{
    VXInteger subj_len;
    VXInteger count;
    VXInteger index;
    const char* subject;
    char* buf;
    v->GetString(1, &subject, &subj_len);
    buf = new char[subj_len + 1];
    for(index=subj_len-1,count=0;index>=0;index--,count++)
    {
        buf[count] = subject[index];
    }
    buf[count] = '\0';
    v->Push(v->NewString(buf, count));
    delete[] buf;
    return 1;
}

VXInteger stringdeleg_startswith(VXState* v)
{
    VXInteger subject_len;
    VXInteger search_len;
    const char* subject;
    const char* search;
    v->GetString(1, &subject, &subject_len);
    v->GetString(2, &search, &search_len);
    v->Push(strhelp_startswith(subject, subject_len, search, search_len));
    return 1;
}

VXInteger stringdeleg_endswith(VXState* v)
{
    VXInteger subject_len;
    VXInteger search_len;
    const char* subject;
    const char* search;
    v->GetString(1, &subject, &subject_len);
    v->GetString(2, &search, &search_len);
    v->Push(strhelp_endswith(subject, subject_len, search, search_len));
    return 1;
}

VXInteger stringdeleg_trim(VXState* v)
{
    VXInteger len;
    const char* str;
    const char* start;
    const char* end;
    v->GetString(1, &str, &len);
    strhelp_trim_l(str, &start);
    strhelp_trim_r(str, len, &end);
    v->Push(v->NewString(start, end - start));
    return 1;
}

VXInteger stringdeleg_ltrim(VXState* v)
{
    const char* str;
    const char* start;
    v->GetString(1, &str, NULL);
    strhelp_trim_l(str, &start);
    v->Push(v->NewString(start));
    return 1;
}

VXInteger stringdeleg_rtrim(VXState* v)
{
    VXInteger len;
    const char* str;
    const char* end;
    v->GetString(1, &str, &len);
    strhelp_trim_r(str, len, &end);
    v->Push(v->NewString(str, end - str));
    return 1;
}


VXInteger stringdeleg_split(VXState* v)
{
    bool keep_empty;
    std::string str;
    std::string delim;
    std::vector<std::string> values;
    std::vector<std::string>::iterator iter;
    VXArrayObj* arr = v->NewArray();
    v->GetString(1, &str);
    v->GetString(2, &delim);
    if(VX_FAILED(v->GetBool(3, &keep_empty)))
    {
        keep_empty = true;
    }
    values = strhelp_split(str, delim, keep_empty);
    for(iter=values.begin(); iter!=values.end(); iter++)
    {
        std::string item = *iter;
        arr->Append(v->NewString(item.c_str(), item.length()));
    }
    v->Push(arr);
    return 1;
}

VXInteger stringdeleg_chars(VXState* v)
{
    bool as_integers;
    std::string str;
    std::string::const_iterator si;
    VXArrayObj* arr = v->NewArray();
    v->GetString(1, &str);
    if(VX_FAILED(v->GetBool(2, &as_integers)))
    {
        as_integers = false;
    }
    for(si=str.begin(); si!=str.end(); si++)
    {
        VXInteger c = VXInteger(*si);
        if(as_integers)
        {
            arr->Append(c);
        }
        else
        {
            arr->Append(v->NewString((const char*)(&c), 1));
        }
    }
    v->Push(arr);
    return 1;
}

VXInteger stringdeleg_slice(VXState* v)
{
    VXInteger sidx;
    VXInteger eidx;
    VXInteger slen;
    VXObject o;
    if(VX_FAILED(get_slice_params(v,sidx,eidx,o)))
    {
        return -1;
    }
    slen = o.String()->_len;
    if(sidx < 0)
    {
        sidx = slen + sidx;
    }
    if(eidx < 0)
    {
        eidx = slen + eidx;
    }
    if(eidx < sidx)
    {
        return v->ThrowError("wrong indexes");
    }
    if(eidx > slen)
    {
        return v->ThrowError("slice out of range");
    }
    v->Push(v->NewString(&_stringval(o)[sidx], eidx-sidx));
    return 1;
}


VXInteger stringdeleg_substr(VXState* v)
{
    std::string newstr;
    std::string str;
    std::string::size_type offset;
    std::string::size_type count;
    v->GetString(1, &str);
    v->GetInteger(2, (VXInteger*)&offset);
    if(VX_FAILED(v->GetInteger(3, (VXInteger*)&count)))
    {
        count = std::string::npos;
    }
    if(offset > str.length())
    {
        return v->ThrowError("offset is out of range");
    }
    newstr = str.substr(offset, count);
    v->Push(v->NewString(newstr.c_str(), newstr.length()));
    return 1;
}

VXInteger stringdeleg_indexof(VXState* v)
{
    std::string str;
    std::string findme;
    std::string::size_type found;
    v->GetString(1, &str);
    v->GetString(2, &findme);
    found = str.find(findme);
    if(found == std::string::npos)
    {
        return 0;
    }
    v->Push(VXInteger(found));
    return 1;
}

VXInteger stringdeleg_findall(VXState* v)
{
    std::string str;
    std::string findme;
    std::string::size_type found;
    v->GetString(1, &str);
    v->GetString(2, &findme);
    found = str.find(findme);
    if(found == std::string::npos)
    {
        return 0;
    }
    VXArrayObj* arr = v->NewArray();
    std::string::size_type pos = 0;
    while((pos = str.find(findme, pos)) != std::string::npos)
    {
        arr->Append(VXInteger(pos));
        pos += findme.size();
    }
    v->Push(arr);
    return 1;
}

VXInteger stringdeleg_count(VXState* v)
{
    std::string str;
    std::string findme;
    std::string::size_type found;
    v->GetString(1, &str);
    v->GetString(2, &findme);
    found = strhelp_find(str, findme);
    v->Push(VXInteger(found));
    return 1;
}

VXInteger validate_format(VXState* v,
                          char *fmt,
                          const char *src,
                          VXInteger n,
                          VXInteger& width)
{
    char swidth[MAX_WFORMAT_LEN];
    VXInteger wc = 0;
    VXInteger start = n;
    fmt[0] = '%';
    while (strchr("-+ #0", src[n]))
    {
        n++;
    }
    while (isdigit(src[n]))
    {
        swidth[wc] = src[n];
        n++;
        wc++;
        if(wc >= MAX_WFORMAT_LEN)
        {
            return v->ThrowError("width format too long");
        }
    }
    swidth[wc] = '\0';
    if(wc > 0)
    {
        width = atoi(swidth);
    }
    else
    {
        width = 0;
    }
    if (src[n] == '.')
    {
        n++;
        wc = 0;
        while (isdigit(src[n]))
        {
            swidth[wc] = src[n];
            n++;
            wc++;
            if(wc>=MAX_WFORMAT_LEN)
            {
                return v->ThrowError("precision format too long");
            }
        }
        swidth[wc] = '\0';
        if(wc > 0)
        {
            width += atoi(swidth);
        }
    }
    if ((n-start) > MAX_FORMAT_LEN )
    {
        return v->ThrowError("format too long");
    }
    memcpy(&fmt[1], &src[start], ((n-start)+1)*sizeof(char));
    fmt[(n-start)+2] = '\0';
    return n;
}

VXInteger voxstd_format(VXState* v,
                       VXInteger nformatstringidx,
                       VXInteger* outlen,
                       char** output)
{
    VXInteger addlen = 0;
    VXInteger valtype = 0;
    VXInteger ti;
    VXInteger fmt_length;
    VXInteger ts_length;
    VXFloat tf;
    const char *format;
    const char *ts;
    char* dest;
    char fmt[MAX_FORMAT_LEN];
    v->GetString(nformatstringidx, &format, &fmt_length);
    VXInteger allocated = (fmt_length+2)*sizeof(char);
    dest = v->ScratchPad(allocated);
    VXInteger n = 0;
    VXInteger i = 0;
    VXInteger w = 0;
    VXInteger nparam = nformatstringidx+1;
    while(format[n] != '\0')
    {
        if(format[n] != '%')
        {
            vox_assert(i < allocated);
            dest[i++] = format[n];
            n++;
        }
        else if(format[n+1] == '%') //handles %%
        {
            dest[i++] = '%';
            n += 2;
        }
        else
        {
            n++;
            if(nparam > v->GetTop())
            {
                return v->ThrowError("not enough paramters for given format");
            }
            n = validate_format(v,fmt,format,n,w);
            if(n < 0)
            {
                return -1;
            }

            switch(format[n])
            {
                case 's':
                    if(VX_FAILED(v->GetString(nparam, &ts, &ts_length)))
                    {
                        return v->ThrowError("string expected");
                    }
                    addlen = (ts_length * sizeof(char))+((w+1)*sizeof(char));
                    valtype = 's';
                    break;
                case 'i':
                case 'd':
                case 'o':
                case 'u':
                case 'x':
                case 'X':
                case 'c':
                    if(VX_FAILED(v->GetInteger(nparam,&ti)))
                    {
                        return v->ThrowError("integer expected");
                    }
                    addlen = (ADDITIONAL_FORMAT_SPACE)+((w+1)*sizeof(char));
                    valtype = 'i';
                    break;
                case 'f':
                case 'g':
                case 'G':
                case 'e':
                case 'E':
                    if(VX_FAILED(v->GetFloat(nparam,&tf)))
                    {
                        return v->ThrowError("float expected");
                    }
                    addlen = (ADDITIONAL_FORMAT_SPACE)+((w+1)*sizeof(char));
                    valtype = 'f';
                    break;
                default:
                    return v->ThrowError("invalid format");
            }
            n++;
            allocated += addlen + sizeof(char);
            dest = v->ScratchPad(allocated);
            //fprintf(stderr, "string::format(fmt=%s valtype=%c)\n", fmt, valtype);
            switch(valtype)
            {
                case 's':
                    i += sprintf(&dest[i], fmt, ts);
                    break;
                case 'i':
                    i += sprintf(&dest[i], fmt, ti);
                    break;
                case 'f':
                    i += sprintf(&dest[i], fmt, tf);
                    break;
            };
            nparam ++;
        }
    }
    *outlen = i;
    dest[i] = '\0';
    *output = dest;
    return VX_OK;
}

VXInteger stringdeleg_format(VXState* v)
{
    char *dest = NULL;
    VXInteger length = 0;
    if(VX_FAILED(voxstd_format(v, 1, &length, &dest)))
    {
        return -1;
    }
    v->Push(v->NewString(dest,length));
    return 1;
}

VXInteger stringdeleg_hashval(VXState*)
{
    //const char* data_str;
    //VXInteger data_len;
    //v->Push(vox_gethashval(v, 1));
    //return 1;
    return 0;
}

VXInteger stringdeleg_hextoint(VXState* v)
{
    std::string subject;
    VXInteger val;
    v->GetString(1, &subject);
    if(sscanf(subject.c_str(), "%x", ((unsigned int*)&val)) < 1)
    {
        return v->ThrowError("cannot parse string as hexadecimal number");
    }
    v->Push(val);
    return 1;
}

VXInteger stringdeleg_preplace(VXState* v)
{
    std::string subject;
    std::string replacement;
    VXInteger from;
    VXInteger to;
    v->GetString(1, &subject);
    v->GetInteger(2, &from);
    v->GetInteger(3, &to);
    v->GetString(4, &replacement);
    subject.replace(std::string::size_type(from), std::string::size_type(to), replacement);
    v->Push(v->NewString(subject.c_str(), subject.length()));
    return 1;
}

VXInteger stringdeleg_stripset(VXState* v)
{
    std::string self;
    std::string nstr;
    VXInteger i;
    VXArrayObj* arr;
    VXObject arr_ob;
    v->GetString(1, &self);
    v->GetTypedArg(2, VX_OT_ARRAY, arr_ob);
    arr = arr_ob.Array();
    for(i=0; i<arr->Size(); i++)
    {
        VXObject ob;
        arr->Get(i, ob);
        switch(ob.Type())
        {
            case VX_OT_INTEGER:
                fprintf(stderr, "it's a number?\n");
                break;
            default:
                return v->ThrowError("cannot use type '%s' in string::stripset",
                    ob.TypeString());
        }
    }
    v->Push(v->NewString(nstr.c_str(), nstr.length()));
    return 1;
}


#define STRING_TOFUNCZ(func) \
        VXInteger stringdeleg_##func(VXState* v) \
        { \
            VXInteger i; \
            VXStringObj* str = v->StackGet(1).String(); \
            VXInteger len = str->Length(); \
            const char *sThis = str->Value(); \
            char *sNew=(v->ScratchPad(len) + 1); \
            for(i=0; i<len; i++) \
            { \
                sNew[i] = func(sThis[i]); \
            } \
            /*sNew[i] = 0;*/ \
            v->Push(v->NewString(sNew, len)); \
            return 1; \
        }

STRING_TOFUNCZ(tolower)
STRING_TOFUNCZ(toupper)

VXRegFunction VXSharedState::_string_default_delegate_funcz[]=
{
    {"weakref",    obj_delegate_weakref,        1,  NULL},
    {"len",        default_delegate_len,        1,  "s"},
    {"tointeger",  default_delegate_tointeger,  1,  "s"},
    {"tofloat",    default_delegate_tofloat,    1,  "s"},
    {"tostring",   default_delegate_tostring,   -1, ".b"},
    {"repr",       default_delegate_repr,       1,  "."},
    {"slice",      stringdeleg_slice,           -1, " s n  n"},
    {"substr",     stringdeleg_substr,          -1, " s n  n"},
    {"count",      stringdeleg_count,           -2, "s s n "},
    {"findall",    stringdeleg_findall,         2,  ".s"},
    {"indexof",    stringdeleg_indexof,         2,  ".s"},
    {"tolower",    stringdeleg_tolower,         1,  "s"},
    {"toupper",    stringdeleg_toupper,         1,  "s"},
    {"format",     stringdeleg_format,          -2, NULL},
    {"fmt",        stringdeleg_format,          -2, NULL},
    {"hashval",    stringdeleg_hashval,         1,  "s"},
    {"trim",       stringdeleg_trim,            1,  "s"},
    {"ltrim",      stringdeleg_ltrim,           1,  ".s"},
    {"rtrim",      stringdeleg_rtrim,           1,  ".s"},
    {"split",      stringdeleg_split,           -2,  ".sb"},
    {"chars",      stringdeleg_chars,           -1,  NULL},
    {"startswith", stringdeleg_startswith,      2,  ".s"},
    {"endswith",   stringdeleg_endswith,        2,  ".s"},
    {"reverse",    stringdeleg_reverse,         1,  NULL},
    {"soundex",    stringdeleg_soundex,         1,  NULL},
    {"preplace",   stringdeleg_preplace,        4,  ".nns"},
    {"replace",    stringdeleg_replace,         3,  ".ss"},
    {"hextoint",   stringdeleg_hextoint,        1,  ".s"},
    {"quote",      stringdeleg_quote,           1,  NULL},
    {"escape",     stringdeleg_escape,          1,  NULL},
    {"stripset",   stringdeleg_stripset,        2,  ".a"},
    {0, 0, 0, 0}
};

