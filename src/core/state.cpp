
#include "pcheader.hpp"
#include "opcodes.hpp"
#include "vm.hpp"
#include "funcproto.hpp"
#include "closure.hpp"
#include "string.hpp"
#include "table.hpp"
#include "userdata.hpp"
#include "class.hpp"
#include "baselib.hpp"

VXSharedState::VXSharedState()
{
    _compilererrorhandler = NULL;
    _printfunc = NULL;
    _errorfunc = NULL;
    _debuginfo = false;
    _notifyallexceptions = false;
}

#define newsysstring(s) \
    { \
        _systemstrings->push_back(VXStringObj::Create(this,s)); \
    }

#define newmetamethod(s) \
    { \
        _metamethods->push_back(VXStringObj::Create(this,s)); \
        _table(_metamethodsmap)->NewSlot(_metamethods->back(),(VXInteger)(_metamethods->size()-1)); \
    }

bool CompileTypemask(VXIntVec &res,const char *typemask)
{
    VXInteger i = 0;

    VXInteger mask = 0;
    while(typemask[i] != 0)
    {

        switch(typemask[i])
        {
            case 'o': mask |= VX_RT_NULL; break;
            case 'i': mask |= VX_RT_INTEGER; break;
            case 'f': mask |= VX_RT_FLOAT; break;
            case 'n': mask |= (VX_RT_FLOAT | VX_RT_INTEGER); break;
            case 's': mask |= VX_RT_STRING; break;
            case 't': mask |= VX_RT_TABLE; break;
            case 'a': mask |= VX_RT_ARRAY; break;
            case 'u': mask |= VX_RT_USERDATA; break;
            case 'c': mask |= (VX_RT_CLOSURE | VX_RT_NATIVECLOSURE); break;
            case 'b': mask |= VX_RT_BOOL; break;
            case 'g': mask |= VX_RT_GENERATOR; break;
            case 'p': mask |= VX_RT_USERPOINTER; break;
            case 'v': mask |= VX_RT_THREAD; break;
            case 'x': mask |= VX_RT_INSTANCE; break;
            case 'y': mask |= VX_RT_CLASS; break;
            case 'r': mask |= VX_RT_WEAKREF; break;
            case '.': mask = -1; res.push_back(mask); i++; mask = 0; continue;
                                 //ignores spaces
            case ' ': i++; continue;
            default:
                return false;
        }
        i++;
        if(typemask[i] == '|')
        {
            i++;
            if(typemask[i] == 0)
                return false;
            continue;
        }
        res.push_back(mask);
        mask = 0;

    }
    return true;
}

#define ismeta(name) \
    (strhelp_startswith(name, strlen(name), "__", 2) && \
     strhelp_endswith(name, strlen(name), "__", 2))

VXTableObj *CreateDelegate(VXSharedState *ss,VXRegFunction *funcz, bool isdeleg=false)
{
    VXInteger i=0;
    (void)isdeleg;
    VXTableObj* deleg;
    VXTableObj* t =VXTableObj::Create(ss,0);
    if(isdeleg)
    {
        deleg = VXTableObj::Create(ss, 0);
    }
    while(funcz[i].name != 0)
    {
        VXRegFunction& fn = funcz[i];
        VXNativeClosureObj *nc = VXNativeClosureObj::Create(ss, fn.func, 0, VXObject());
        nc->_nparamscheck = fn.nparamscheck;
        nc->_name = VXStringObj::Create(ss, fn.name);
        if(fn.typemask != NULL)
        {
            if(!CompileTypemask(nc->_typecheck, fn.typemask))
            {
                return NULL;
            }
        }
        if(isdeleg && ismeta(fn.name))
        {
            deleg->NewSlot(VXStringObj::Create(ss, fn.name), nc);
        }
        else
        {
            t->NewSlot(VXStringObj::Create(ss, fn.name), nc);
        }
        i++;
    }
    if(isdeleg)
    {
        if(!t->SetDelegate(deleg))
        {
            //fprintf(stderr, "failed to set delegate\n");
        }
    }
    return t;
}

void VXSharedState::Init()
{
    _scratchpad=NULL;
    _scratchpadsize=0;
    _gc_chain=NULL;
    _stringtable = (VXStringObjTable*)VX_MALLOC(sizeof(VXStringObjTable));
    new (_stringtable) VXStringObjTable(this);
    vox_new(_metamethods,VXObjectVec);
    vox_new(_systemstrings,VXObjectVec);
    vox_new(_types,VXObjectVec);
    _metamethodsmap = VXTableObj::Create(this,VX_MT_LAST-1);
    //adding type strings to avoid memory trashing
    //types names
    newsysstring("null");
    newsysstring("table");
    newsysstring("array");
    newsysstring("closure");
    newsysstring("string");
    newsysstring("userdata");
    newsysstring("integer");
    newsysstring("float");
    newsysstring("userpointer");
    newsysstring("function");
    newsysstring("generator");
    newsysstring("thread");
    newsysstring("class");
    newsysstring("instance");
    newsysstring("bool");
    //meta methods
    newmetamethod(VX_MM_ADD);
    newmetamethod(VX_MM_SUB);
    newmetamethod(VX_MM_MUL);
    newmetamethod(VX_MM_DIV);
    newmetamethod(VX_MM_UNM);
    newmetamethod(VX_MM_MODULO);
    newmetamethod(VX_MM_SET);
    newmetamethod(VX_MM_GET);
    newmetamethod(VX_MM_TYPEOF);
    newmetamethod(VX_MM_NEXTI);
    newmetamethod(VX_MM_CMP);
    newmetamethod(VX_MM_CALL);
    newmetamethod(VX_MM_CLONED);
    newmetamethod(VX_MM_NEWSLOT);
    newmetamethod(VX_MM_DELSLOT);
    newmetamethod(VX_MM_TOSTRING);
    newmetamethod(VX_MM_NEWMEMBER);
    newmetamethod(VX_MM_INHERITED);

    _constructoridx = VXStringObj::Create(this,"constructor");
    _registry = VXTableObj::Create(this,0);
    _consts = VXTableObj::Create(this,0);
    _table_default_delegate = CreateDelegate(this,_table_default_delegate_funcz);
    _array_default_delegate = CreateDelegate(this,_array_default_delegate_funcz);
    _string_default_delegate = CreateDelegate(this,_string_default_delegate_funcz, true);
    _number_default_delegate = CreateDelegate(this,_number_default_delegate_funcz);
    _closure_default_delegate = CreateDelegate(this,_closure_default_delegate_funcz);
    _generator_default_delegate = CreateDelegate(this,_generator_default_delegate_funcz);
    _thread_default_delegate = CreateDelegate(this,_thread_default_delegate_funcz);
    _class_default_delegate = CreateDelegate(this,_class_default_delegate_funcz);
    _instance_default_delegate = CreateDelegate(this,_instance_default_delegate_funcz);
    _weakref_default_delegate = CreateDelegate(this,_weakref_default_delegate_funcz);

}

VXSharedState::~VXSharedState()
{
    // and now the rest
    _constructoridx.Null();
    _table(_registry)->Finalize();
    _table(_consts)->Finalize();
    _table(_metamethodsmap)->Finalize();
    _registry.Null();
    _consts.Null();
    _metamethodsmap.Null();
    while(!_systemstrings->empty())
    {
        _systemstrings->back().Null();
        _systemstrings->pop_back();
    }
    _thread(_root_vm)->Finalize();
    _root_vm.Null();
    _table_default_delegate.Null();
    _array_default_delegate.Null();
    _string_default_delegate.Null();
    _number_default_delegate.Null();
    _closure_default_delegate.Null();
    _generator_default_delegate.Null();
    _thread_default_delegate.Null();
    _class_default_delegate.Null();
    _instance_default_delegate.Null();
    _weakref_default_delegate.Null();
    _refs_table.Finalize();
    VXCollectable *t = _gc_chain;
    VXCollectable *nx = NULL;
    while(t)
    {
        t->_uiRef++;
        t->Finalize();
        nx = t->_next;
        if(--t->_uiRef == 0)
            t->Release();
        t=nx;
    }
    vox_assert(_gc_chain==NULL);     //just to prove a theory
    while(_gc_chain)
    {
        _gc_chain->_uiRef++;
        _gc_chain->Release();
    }
    vox_delete(_types,VXObjectVec);
    vox_delete(_systemstrings,VXObjectVec);
    vox_delete(_metamethods,VXObjectVec);
    vox_delete(_stringtable,VXStringObjTable);
    if(_scratchpad)VX_FREE(_scratchpad,_scratchpadsize);
}

VXInteger VXSharedState::GetMetaMethodIdxByName(const VXObject &name)
{
    if(type(name) != VX_OT_STRING)
        return -1;
    VXObject ret;
    if(_table(_metamethodsmap)->Get(name,ret))
    {
        return _integer(ret);
    }
    return -1;
}


void VXSharedState::MarkObject(VXObject &o,VXCollectable **chain)
{
    switch(type(o))
    {
        case VX_OT_TABLE:_table(o)->Mark(chain);break;
        case VX_OT_ARRAY:_array(o)->Mark(chain);break;
        case VX_OT_USERDATA:_userdata(o)->Mark(chain);break;
        case VX_OT_CLOSURE:_closure(o)->Mark(chain);break;
        case VX_OT_NATIVECLOSURE:_nativeclosure(o)->Mark(chain);break;
        case VX_OT_GENERATOR:_generator(o)->Mark(chain);break;
        case VX_OT_THREAD:_thread(o)->Mark(chain);break;
        case VX_OT_CLASS:_class(o)->Mark(chain);break;
        case VX_OT_INSTANCE:_instance(o)->Mark(chain);break;
        case VX_OT_OUTER:_outer(o)->Mark(chain);break;
        case VX_OT_FUNCPROTO:_funcproto(o)->Mark(chain);break;
        default: break;          //shutup compiler
    }
}

void VXSharedState::RunMark(VXState *vm,VXCollectable **tchain)
{
    (void)vm;
    VXState *vms = _thread(_root_vm);
    vms->Mark(tchain);
    _refs_table.Mark(tchain);
    MarkObject(_registry,tchain);
    MarkObject(_consts,tchain);
    MarkObject(_metamethodsmap,tchain);
    MarkObject(_table_default_delegate,tchain);
    MarkObject(_array_default_delegate,tchain);
    MarkObject(_string_default_delegate,tchain);
    MarkObject(_number_default_delegate,tchain);
    MarkObject(_generator_default_delegate,tchain);
    MarkObject(_thread_default_delegate,tchain);
    MarkObject(_closure_default_delegate,tchain);
    MarkObject(_class_default_delegate,tchain);
    MarkObject(_instance_default_delegate,tchain);
    MarkObject(_weakref_default_delegate,tchain);

}

VXInteger VXSharedState::ResurrectUnreachable(VXState *vm)
{
    VXInteger n=0;
    VXCollectable *tchain=NULL;

    RunMark(vm,&tchain);

    VXCollectable *resurrected = _gc_chain;
    VXCollectable *t = resurrected;
    //VXCollectable *nx = NULL;

    _gc_chain = tchain;

    VXArrayObj *ret = NULL;
    if(resurrected)
    {
        ret = VXArrayObj::Create(this,0);
        VXCollectable *rlast = NULL;
        while(t)
        {
            rlast = t;
            VXOType type = t->GetType();
            if(type != VX_OT_FUNCPROTO && type != VX_OT_OUTER)
            {
                VXRawObj sqo;
                sqo._type = type;
                sqo._unVal.pRefCounted = t;
                ret->Append(sqo);
            }
            t = t->_next;
            n++;
        }

        vox_assert(rlast->_next == NULL);
        rlast->_next = _gc_chain;
        if(_gc_chain)
        {
            _gc_chain->_prev = rlast;
        }
        _gc_chain = resurrected;
    }

    t = _gc_chain;
    while(t)
    {
        t->UnMark();
        t = t->_next;
    }

    if(ret)
    {
        VXObject temp = ret;
        vm->Push(temp);
    }
    else
    {
        vm->PushNull();
    }
    return n;
}

VXInteger VXSharedState::CollectGarbage(VXState *vm)
{
    VXInteger n=0;
    VXCollectable *tchain=NULL;

    RunMark(vm,&tchain);

    VXCollectable *t = _gc_chain;
    VXCollectable *nx = NULL;
    while(t)
    {
        t->_uiRef++;
        t->Finalize();
        nx = t->_next;
        if(--t->_uiRef == 0)
            t->Release();
        t = nx;
        n++;
    }

    t = tchain;
    while(t)
    {
        t->UnMark();
        t = t->_next;
    }
    _gc_chain = tchain;

    return n;
}

void VXCollectable::AddToChain(VXCollectable **chain,VXCollectable *c)
{
    c->_prev = NULL;
    c->_next = *chain;
    if(*chain) (*chain)->_prev = c;
    *chain = c;
}

void VXCollectable::RemoveFromChain(VXCollectable **chain,VXCollectable *c)
{
    if(c->_prev) c->_prev->_next = c->_next;
    else *chain = c->_next;
    if(c->_next)
        c->_next->_prev = c->_prev;
    c->_next = NULL;
    c->_prev = NULL;
}


char* VXSharedState::GetScratchPad(VXInteger size)
{
    VXInteger newsize;
    if(size>0)
    {
        if(_scratchpadsize < size)
        {
            newsize = size + (size>>1);
            _scratchpad = (char *)VX_REALLOC(_scratchpad,_scratchpadsize,newsize);
            _scratchpadsize = newsize;

        }
        else if(_scratchpadsize >= (size<<5))
        {
            newsize = _scratchpadsize >> 1;
            _scratchpad = (char *)VX_REALLOC(_scratchpad,_scratchpadsize,newsize);
            _scratchpadsize = newsize;
        }
    }
    return _scratchpad;
}

RefTable::RefTable()
{
    AllocNodes(4);
}

void RefTable::Finalize()
{
    RefNode *nodes = _nodes;
    for(VXUnsignedInteger n = 0; n < _numofslots; n++)
    {
        nodes->obj.Null();
        nodes++;
    }
}

RefTable::~RefTable()
{
    VX_FREE(_buckets,(_numofslots * sizeof(RefNode *)) + (_numofslots * sizeof(RefNode)));
}

void RefTable::Mark(VXCollectable **chain)
{
    RefNode *nodes = (RefNode *)_nodes;
    for(VXUnsignedInteger n = 0; n < _numofslots; n++)
    {
        if(type(nodes->obj) != VX_OT_NULL)
        {
            VXSharedState::MarkObject(nodes->obj,chain);
        }
        nodes++;
    }
}


void RefTable::AddRef(VXRawObj &obj)
{
    VXHash mainpos;
    RefNode *prev;
    RefNode *ref = Get(obj,mainpos,&prev,true);
    ref->refs++;
}

VXUnsignedInteger RefTable::GetRefCount(VXRawObj &obj)
{
    VXHash mainpos;
    RefNode *prev;
    RefNode *ref = Get(obj,mainpos,&prev,true);
    return ref->refs;
}

bool RefTable::Release(VXRawObj &obj)
{
    VXHash mainpos;
    RefNode *prev;
    RefNode *ref = Get(obj,mainpos,&prev,false);
    if(ref)
    {
        if(--ref->refs == 0)
        {
            VXObject o = ref->obj;
            if(prev)
            {
                prev->next = ref->next;
            }
            else
            {
                _buckets[mainpos] = ref->next;
            }
            ref->next = _freelist;
            _freelist = ref;
            _slotused--;
            ref->obj.Null();
            //<<FIXME>>test for shrink?
            return true;
        }
    }
    else
    {
        vox_assert(0);
    }
    return false;
}

void RefTable::Resize(VXUnsignedInteger size)
{
    RefNode **oldbucks = _buckets;
    RefNode *t = _nodes;
    VXUnsignedInteger oldnumofslots = _numofslots;
    AllocNodes(size);
    //rehash
    VXUnsignedInteger nfound = 0;
    for(VXUnsignedInteger n = 0; n < oldnumofslots; n++)
    {
        if(type(t->obj) != VX_OT_NULL)
        {
            //add back;
            vox_assert(t->refs != 0);
            RefNode *nn = Add(::HashObj(t->obj)&(_numofslots-1),t->obj);
            nn->refs = t->refs;
            t->obj.Null();
            nfound++;
        }
        t++;
    }
    vox_assert(nfound == oldnumofslots);
    VX_FREE(oldbucks,(oldnumofslots * sizeof(RefNode *)) + (oldnumofslots * sizeof(RefNode)));
}

RefTable::RefNode *RefTable::Add(VXHash mainpos,VXRawObj &obj)
{
    RefNode *t = _buckets[mainpos];
    RefNode *newnode = _freelist;
    newnode->obj = obj;
    _buckets[mainpos] = newnode;
    _freelist = _freelist->next;
    newnode->next = t;
    vox_assert(newnode->refs == 0);
    _slotused++;
    return newnode;
}

RefTable::RefNode *RefTable::Get(VXRawObj &obj,VXHash &mainpos,RefNode **prev,bool add)
{
    RefNode *ref;
    mainpos = ::HashObj(obj)&(_numofslots-1);
    *prev = NULL;
    for (ref = _buckets[mainpos]; ref; )
    {
        if(_rawval(ref->obj) == _rawval(obj) && type(ref->obj) == type(obj))
            break;
        *prev = ref;
        ref = ref->next;
    }
    if(ref == NULL && add)
    {
        if(_numofslots == _slotused)
        {
            vox_assert(_freelist == 0);
            Resize(_numofslots*2);
            mainpos = ::HashObj(obj)&(_numofslots-1);
        }
        ref = Add(mainpos,obj);
    }
    return ref;
}

void RefTable::AllocNodes(VXUnsignedInteger size)
{
    RefNode **bucks;
    RefNode *nodes;
    bucks = (RefNode **)VX_MALLOC((size * sizeof(RefNode *)) + (size * sizeof(RefNode)));
    nodes = (RefNode *)&bucks[size];
    RefNode *temp = nodes;
    VXUnsignedInteger n;
    for(n = 0; n < size - 1; n++)
    {
        bucks[n] = NULL;
        temp->refs = 0;
        new (&temp->obj) VXObject;
        temp->next = temp+1;
        temp++;
    }
    bucks[n] = NULL;
    temp->refs = 0;
    new (&temp->obj) VXObject;
    temp->next = NULL;
    _freelist = nodes;
    _nodes = nodes;
    _buckets = bucks;
    _slotused = 0;
    _numofslots = size;
}

//////////////////////////////////////////////////////////////////////////
//VXStringObjTable
/*
 * The following code is based on Lua 4.0 (Copyright 1994-2002 Tecgraf, PUC-Rio.)
 * http://www.lua.org/copyright.html#4
 * http://www.lua.org/source/4.0.1/src_lstring.c.html
 */

VXStringObjTable::VXStringObjTable(VXSharedState *ss)
{
    _sharedstate = ss;
    AllocNodes(4);
    _slotused = 0;
}

VXStringObjTable::~VXStringObjTable()
{
    VX_FREE(_strings,sizeof(VXStringObj*)*_numofslots);
    _strings = NULL;
}

void VXStringObjTable::AllocNodes(VXInteger size)
{
    _numofslots = size;
    _strings = (VXStringObj**)VX_MALLOC(sizeof(VXStringObj*)*_numofslots);
    memset(_strings,0,sizeof(VXStringObj*)*_numofslots);
}

VXStringObj *VXStringObjTable::Add(const char *news,VXInteger len)
{
    if(len<0)
    {
        len = (VXInteger)strlen(news);
    }
    VXHash h = ::_hashstr(news,len)&(_numofslots-1);
    VXStringObj *s;
    for (s = _strings[h]; s; s = s->_next)
    {
        if(s->_len == len && (!memcmp(news,s->_val, len)))
        {
            return s;            //found
        }
    }
    VXStringObj *t = (VXStringObj*)VX_MALLOC(len + sizeof(VXStringObj));
    new (t) VXStringObj;
    t->_sharedstate = _sharedstate;
    memcpy(t->_val, news, len);
    t->_val[len] = '\0';
    t->_len = len;
    t->_hash = ::_hashstr(news,len);
    t->_next = _strings[h];
    _strings[h] = t;
    _slotused++;
    if (_slotused > _numofslots) /* too crowded? */
    {
        Resize(_numofslots*2);
    }
    return t;
}

void VXStringObjTable::Resize(VXInteger size)
{
    VXInteger oldsize=_numofslots;
    VXStringObj **oldtable=_strings;
    AllocNodes(size);
    for (VXInteger i=0; i<oldsize; i++)
    {
        VXStringObj *p = oldtable[i];
        while(p)
        {
            VXStringObj *next = p->_next;
            VXHash h = p->_hash&(_numofslots-1);
            p->_next = _strings[h];
            _strings[h] = p;
            p = next;
        }
    }
    VX_FREE(oldtable,oldsize*sizeof(VXStringObj*));
}

void VXStringObjTable::Remove(VXStringObj *bs)
{
    VXStringObj *s;
    VXStringObj *prev=NULL;
    VXHash h = bs->_hash&(_numofslots - 1);

    for (s = _strings[h]; s; )
    {
        if(s == bs)
        {
            if(prev)
                prev->_next = s->_next;
            else
                _strings[h] = s->_next;
            _slotused--;
            VXInteger slen = s->_len;
            s->~VXStringObj();
            VX_FREE(s,sizeof(VXStringObj) + slen);
            return;
        }
        prev = s;
        s = s->_next;
    }
    vox_assert(0); //if this fails then something is wrong
}
