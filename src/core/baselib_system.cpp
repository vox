
#include "baselib.hpp"
#include "string.hpp"

//#define VX_PRINT_REPR

VXInteger vox_printfunc_callback(VXState* v, VXInteger start, VXPrintFunc vpf)
{
    const char* data_str;
    VXInteger data_len;
    VXInteger top;
    VXInteger i;
    if(vpf)
    {
        top = v->GetTop();
        for(i=start; i<=top; i++)
        {
            VXObject res;
            VXObject& ob = v->StackGet(i);
#if defined(VX_PRINT_REPR)
            VXOType tp = ob.Type();
            if(tp != VX_OT_STRING && !ob.IsNumeric())
            {
                std::string s = VXRepr(v, ob).str();
                data_str = s.c_str();
                data_len = s.length();
            }
            else
            {
                v->ToString(ob, res);
                res.String()->Get(&data_str, &data_len);
            }
#else
            v->ToString(ob, res);
            res.String()->Get(&data_str, &data_len);
#endif
            if(VX_FAILED(vpf(v, false, data_str, data_len)))
            {
                goto _printfunc_failed;
            }
        }
        if(VX_FAILED(vpf(v, true, data_str, data_len)))
        {
            goto _printfunc_failed;
        }
    }
    return 0;

_printfunc_failed:
    return v->ThrowError("Internal error: call to printfunc failed");
}


VXInteger _println_callback(VXState* v, bool isend, const char* str, VXInteger len)
{
    if(v->Shared()->_printfunc)
    {
        if(isend)
        {
            v->Shared()->_printfunc(v, "\n");
        }
        else
        {
            v->Shared()->_printfunc(v, "%s", str);
        }
        return VX_OK;
    }
    return VX_ERROR;
}

VXInteger _print_callback(VXState* v, bool isend, const char* str, VXInteger len)
{
    if(_ss(v)->_printfunc)
    {
        if(!isend)
        {
            _ss(v)->_printfunc(v, "%s", str);
        }
        return VX_OK;
    }
    return VX_ERROR;
}

VXInteger system_repr(VXState* v)
{
    VXInteger status;
    VXRepr ser(v, v->StackGet(2));
    status = ser.status();
    if(status == VX_OK)
    {
        const std::string& data = ser.str();
        v->Push(v->NewString(data.c_str(), data.length()));
        return 1;
    }
    return status;
}

VXInteger system_dummy(VXState*)
{
    return 0;
}

VXInteger system_gc_collect(VXState* v)
{
    v->Push(v->CollectGarbage());
    return 1;
}

VXInteger system_gc_resurrect(VXState* v)
{
    v->ResurrectUnreachable();
    return 1;
}

VXInteger system_gc_release(VXState* v)
{
    VXObject& ob = v->StackGet(2);
    ob.Null();
    return 0;
}

VXInteger system_getroottable(VXState* v)
{
    v->Push(v->_roottable);
    return 1;
}

VXInteger system_getconsttable(VXState* v)
{
    v->Push(v->Shared()->_consts);
    return 1;
}

VXInteger system_setroottable(VXState* v)
{
    VXObject oldroot = v->_roottable;
    VXObject newroot = v->StackGet(2);
    if(newroot.Type() == VX_OT_TABLE || newroot.Type() == VX_OT_NULL)
    {
        v->_roottable = newroot;
        v->Push(oldroot);
        return 1;
    }
    return v->ThrowError("wrong type (expected 'table' or 'null')");
}

VXInteger system_setconsttable(VXState* v)
{
    VXObject oldtable = v->Shared()->_consts;
    VXObject newtable = v->StackGet(2);
    if(newtable.Type() == VX_OT_TABLE)
    {
        v->Shared()->_consts = newtable;
        v->Push(oldtable);
        return 1;
    }
    return VX_ERROR;;
}

VXInteger system_seterrorhandler(VXState* v)
{
    v->SetErrorHandler(v->StackGet(2));
    return 0;
}

VXInteger system_setdebughook(VXState* v)
{
    v->SetDebugHook(v->StackGet(2));
    return 0;
}

VXInteger system_enabledebuginfo(VXState* v)
{
    bool val;
    v->GetBool(2, &val);
    v->EnableDebugInfo(val);
    return 0;
}


VXInteger system_getstackinfos(VXState* v)
{
    VXInteger level;
    v->GetInteger(-1, &level);
    return __getcallstackinfos(v,level);
}

VXInteger system_assert(VXState* v)
{
    const char* message;
    if(VXState::IsFalse(v->StackGet(2)))
    {
        if(VX_FAILED(v->GetString(3, &message, NULL)))
        {
            return v->ThrowError("assertion failed");
        }
        else
        {
            return v->ThrowError("assertion failed: %s", message);
        }
    }
    return 0;
}



VXInteger system_print(VXState* v)
{
    return vox_printfunc_callback(v, 2, _print_callback);
}

VXInteger system_println(VXState* v)
{
    return vox_printfunc_callback(v, 2, _println_callback);
}


VXInteger system_compilestring(VXState* v)
{
    const char* src;
    const char* name;
    VXInteger size;
    v->GetString(2, &src, &size);
    if(VX_FAILED(v->GetString(3, &name, NULL)))
    {
        name = "unnamedbuffer";
    }
    if(VX_SUCCEEDED(v->CompileBuffer(src,size,name,false)))
    {
        return 1;
    }
    return VX_ERROR;
}

VXInteger system_newthread(VXState* v)
{
    VXObject &func = v->StackGet(2);
    VXInteger stksize = (func.Closure()->_function->_stacksize << 1) +2;
    VXState* newv = v->NewThread(v, (stksize < MIN_STACK_OVERHEAD + 2)?
                    MIN_STACK_OVERHEAD + 2 : stksize);
    VXState::MoveIndex(newv, v, -2);
    return 1;
}

VXInteger system_suspend(VXState* v)
{
    return v->Suspend();
}

VXInteger system_type(VXState* v)
{
    VXObject &o = v->StackGet(2);
    v->PushString(o.TypeString());
    return 1;
}

VXInteger system_callee(VXState* v)
{
    if(v->_callsstacksize > 1)
    {
        v->Push(v->_callsstack[v->_callsstacksize - 2]._closure);
        return 1;
    }
    return v->ThrowError("no closure in the calls stack");
}

VXInteger system_loadfile(VXState* v)
{
    const char *filename;
    bool printerror = false;
    v->GetString(2, &filename, NULL);
    if(VX_FAILED(v->GetBool(3, &printerror)))
    {
        printerror = false;
    }
    if(VX_SUCCEEDED(v->LoadFile(filename,printerror)))
    {
        return 1;
    }
    return v->ThrowError("loadfile('%s'): %s", filename, v->LastError());
}



VXInteger system_writeclosuretofile(VXState* v)
{
    const char *filename;
    v->GetString(2, &filename, NULL);
    if(VX_SUCCEEDED(v->WriteClosureToFile(filename)))
    {
        return 1;
    }
    return VX_ERROR;
}

VXInteger system_dofile(VXState* v)
{
    const char *filename;
    bool printerror;
    v->GetString(2, &filename, NULL);
    if(VX_FAILED(v->GetBool(3, &printerror)))
    {
        /* by default, do not print the yielded error */
        printerror = false;
    }
    v->Push(v->GetRootTable());
    if(VX_SUCCEEDED(v->DoFile(filename, true, printerror)))
    {
        return 1;
    }
    return v->ThrowError("dofile('%s'): %s", filename, v->LastError());
}

VXInteger system_dostring(VXState* v)
{
    VXInteger ret;
    std::string source;
    const char* buffername;
    bool printerror;
    v->GetString(2, &source);
    if(VX_FAILED(v->GetBool(3, &printerror)))
    {
        /* by default, do not print the yielded error */
        printerror = false;
    }
    if(VX_FAILED(v->GetString(4, &buffername, NULL)))
    {
        buffername = NULL;
    }
    v->Push(v->GetRootTable());
    ret = v->DoString(
        source.c_str(),
        source.length(),
        buffername, true, printerror);
    if(VX_SUCCEEDED(ret))
    {
        return 1;
    }
    /* only throw custom errormessage if buffername is not NULL.
       reason: if system.errorhandler() is called from vox,
       the buffername is not reachable, so we'll just act as if
       it got called natively */
    if(buffername != NULL)
    {
        return v->ThrowError("dostring(%s): %s", buffername , v->LastError());
    }
    return VX_ERROR;
}

VXInteger system_atexit(VXState* v)
{
    VXObject& o = v->StackGet(2);
    return v->AtExit(o);
}

VXInteger system_errorhandler(VXState* v)
{
    v->CallErrorHandler(v->StackGet(2));
    return VX_OK;
}

VXInteger system_info(VXState* v)
{
    VXTableObj* tb;
    VXObject name;
    tb = v->NewTable();
    name = v->StackGet(2);
    tb->Set(v->NewString("platform"), v->NewString(VOX_PLATFORM_NAME));
    tb->Set(v->NewString("platform_group"), v->NewString(VOX_PLATFORM_GROUP_NAME));
    tb->Set(v->NewString("platform_bits"), VXInteger(VOX_PLATFORM_BITS));
    tb->Set(v->NewString("version"), v->NewString(VOX_VERSION));
    tb->Set(v->NewString("version_number"), VXInteger(VOX_VERSION_NUMBER));
    tb->Set(v->NewString("copyright"), v->NewString(VOX_COPYRIGHT));
    if(name.Type() == VX_OT_STRING)
    {
        VXObject res;
        if(!tb->Get(name, res))
        {
            return v->ThrowError("key '%s' not available in the info table",
                name.String()->Value());
        }
        v->Push(res);
    }
    else
    {
        v->Push(tb);
    }
    return 1;
}


VXRegFunction system_funcs[]=
{
    {"info",                    system_info,               -1, ".s"},
    {"seterrorhandler",         system_seterrorhandler,    2,  NULL},
    {"debughook",               system_setdebughook,       2,  NULL},
    {"enabledebug",             system_enabledebuginfo,    2,  NULL},
    {"stackinfos",              system_getstackinfos,      2,  ".n"},
    {"getroot",                 system_getroottable,       1,  NULL},
    {"setroot",                 system_setroottable,       2,  NULL},
    {"getconsttable",           system_getconsttable,      1,  NULL},
    {"setconsttable",           system_setconsttable,      2,  NULL},
    {"compilestring",           system_compilestring,      -2, ".ss"},
    {"newthread",               system_newthread,          2,  ".c"},
    {"suspend",                 system_suspend,            -1, NULL},
    {"type",                    system_type,               2,  NULL},
    {"callee",                  system_callee,             0,  NULL},
    {"dummy",                   system_dummy,              0,  NULL},
    {"loadfile",                system_loadfile,           -2, ".sb"},
    {"loadstring",              system_compilestring,      -2, ".ss"},
    {"dofile",                  system_dofile,             -2, ".sb"},
    {"dostring",                system_dostring,           -2, ".sbs"},
    {"writeclosuretofile",      system_writeclosuretofile, 3,  ".sc"},
    {"atexit",                  system_atexit,             2,  ".c"},
    //{"repr",                    system_repr,               2,  NULL},
    {"errorhandler",            system_errorhandler,       2,  NULL},
    {"gc_collect",              system_gc_collect,         0,  NULL},
    {"gc_resurrect",            system_gc_resurrect,       0,  NULL},
    {"gc_release",              system_gc_release,         2,  "."},

    /** NOTE: these declarations are going to be deprecated in some
              future release. remember to remove them at some point...
              Main reason(s) for removal are usually:
                + overly verbose function name (writeclosuretofile is way too long)
                + ambigious/idiotic function name (gc_collect vs gccollect)
    */
    {"getstackinfos",           system_getstackinfos,      2,  ".n"},
    {"setdebughook",            system_setdebughook,       2,  NULL},
    {"enabledebuginfo",         system_enabledebuginfo,    2,  NULL},
    {"seterrorhandler",         system_seterrorhandler,    2,  NULL},
    {"getroottable",            system_getroottable,       1,  NULL},
    {"setroottable",            system_setroottable,       2,  NULL},
    {"getconsttable",           system_getconsttable,      1,  NULL},
    {"setconsttable",           system_setconsttable,      2,  NULL},
    {0, 0, 0, 0}
};

VXRegFunction system_globalfuncs[] =
{
    {"assert",               system_assert,    -2,  ".bs"},
    {"print",                system_print,     -1,  NULL},
    {"println",              system_println,   -1,  NULL},
    {0, 0, 0, 0}
};


VXInteger voxstd_register_system(VXState* v)
{
    VXTableObj* systb = v->NewTable();
    systb->NewSlot(v->NewString("argv"), v->GetSysArgv());
    v->RegisterLib("system", system_funcs, true, systb);
    v->RegisterLib(NULL, system_globalfuncs, true);
    return VX_OK;
}

