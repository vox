
#include "hash.hpp"
#include <iterator>
#include <iostream>
#include <fstream>
#include <cstring>
#include <cstdio>

// Constants for MD5Transform routine.
enum
{
    __MD5_S11 = 7,
    __MD5_S12 = 12,
    __MD5_S13 = 17,
    __MD5_S14 = 22,
    __MD5_S21 = 5,
    __MD5_S22 = 9,
    __MD5_S23 = 14,
    __MD5_S24 = 20,
    __MD5_S31 = 4,
    __MD5_S32 = 11,
    __MD5_S33 = 16,
    __MD5_S34 = 23,
    __MD5_S41 = 6,
    __MD5_S42 = 10,
    __MD5_S43 = 15,
    __MD5_S44 = 21
};

namespace Hash
{

void Object::updateFromFile(const std::string& path)
{
    int c;
    std::ifstream ifs;
    ifs.open(path.c_str(), std::ios_base::in | std::ios_base::binary);
    while(ifs.good())
    {
        c = ifs.get();
        if(ifs.good())
        {
            this->update(c);
        }
    }
    ifs.close();
}

/*
* SHA1 Impl
*/


std::string SHA1::GenSHA1(const std::string& data, unsigned int length)
{
    SHA1 sh;
    length = (length == 0) ? data.length() : length;
    sh.update(data.c_str(), length);
    sh.finalize();
    return sh.hexDigest();
}


SHA1::SHA1()
{
    reset();
}

SHA1::~SHA1()
{
    delete[] Words;
    delete[] Message_Block;
}

unsigned int SHA1::CircularShift(int bits, unsigned word)
{
    return ((word << bits) & 0xFFFFFFFF) | ((word & 0xFFFFFFFF) >> (32-bits));
}

std::string SHA1::hexDigest()
{
    std::vector<char> outbuffer;
    std::vector<int> digest;
    std::string format = "%08x%08x%08x%08x%08x";
    if(finalized == true)
    {
        digest.resize(6);
        outbuffer.resize(60);
        result(&digest[0]);
        sprintf(&outbuffer[0],
                format.c_str(),
                digest.at(0),
                digest.at(1),
                digest.at(2),
                digest.at(3),
                digest.at(4));
        return (&outbuffer[0]);
    }
    throw Error(
        "SHA1::finalize needs to be called before SHA1::hexDigest");
    return std::string(); // never reached
}

void SHA1::PadMessage()
{
    /*
    *  Check to see if the current message block is too small to hold
    *  the initial padding bits and length.  If so, we will pad the
    *  block, process it, and then continue padding into a second block.
    */
    if (Message_Block_Index > 55)
    {
        Message_Block[Message_Block_Index++] = 0x80;
        while(Message_Block_Index < 64)
        {
            Message_Block[Message_Block_Index++] = 0;
        }
        ProcessMessageBlock();
        while(Message_Block_Index < 56)
        {
            Message_Block[Message_Block_Index++] = 0;
        }
    }
    else
    {
        Message_Block[Message_Block_Index++] = 0x80;
        while(Message_Block_Index < 56)
        {
            Message_Block[Message_Block_Index++] = 0;
        }
    }
    /*
    *  Store the message length as the last 8 octets
    */
    Message_Block[56] = (Length_High >> 24) & 0xFF;
    Message_Block[57] = (Length_High >> 16) & 0xFF;
    Message_Block[58] = (Length_High >> 8) & 0xFF;
    Message_Block[59] = (Length_High) & 0xFF;
    Message_Block[60] = (Length_Low >> 24) & 0xFF;
    Message_Block[61] = (Length_Low >> 16) & 0xFF;
    Message_Block[62] = (Length_Low >> 8) & 0xFF;
    Message_Block[63] = (Length_Low) & 0xFF;
    ProcessMessageBlock();
}

void SHA1::ProcessMessageBlock()
{
    int t;
    unsigned int temp;
    unsigned int A;
    unsigned int B;
    unsigned int C;
    unsigned int D;
    unsigned int E;

    // Constants defined for SHA-1
    const unsigned int K[] = {0x5A827999,
                              0x6ED9EBA1,
                              0x8F1BBCDC,
                              0xCA62C1D6};

    /*
    *  Initialize the first 16 words in the array W
    */
    for(t=0; t<16; t++)
    {
        Words[t]  = ((unsigned int)Message_Block[t * 4])     << 24;
        Words[t] |= ((unsigned int)Message_Block[t * 4 + 1]) << 16;
        Words[t] |= ((unsigned int)Message_Block[t * 4 + 2]) << 8;
        Words[t] |= ((unsigned int)Message_Block[t * 4 + 3]);
    }
    for(t=16; t<80; t++)
    {
       Words[t] = CircularShift(1, Words[t-3] ^ Words[t-8] ^ Words[t-14] ^ Words[t-16]);
    }
    A = H.at(0);
    B = H.at(1);
    C = H.at(2);
    D = H.at(3);
    E = H.at(4);
    for(t=0; t<20; t++)
    {
        temp = CircularShift(5,A) + ((B & C) | ((~B) & D)) + E + Words[t] + K[0];
        temp &= 0xFFFFFFFF;
        E = D;
        D = C;
        C = CircularShift(30,B);
        B = A;
        A = temp;
    }
    for(t = 20; t < 40; t++)
    {
        temp = CircularShift(5,A) + (B ^ C ^ D) + E + Words[t] + K[1];
        temp &= 0xFFFFFFFF;
        E = D;
        D = C;
        C = CircularShift(30,B);
        B = A;
        A = temp;
    }
    for(t = 40; t<60; t++)
    {
        temp = CircularShift(5,A) + ((B & C) | (B & D) | (C & D)) + E + Words[t] + K[2];
        temp &= 0xFFFFFFFF;
        E = D;
        D = C;
        C = CircularShift(30,B);
        B = A;
        A = temp;
    }
    for(t=60; t<80; t++)
    {
        temp = CircularShift(5,A) + (B ^ C ^ D) + E + Words[t] + K[3];
        temp &= 0xFFFFFFFF;
        E = D;
        D = C;
        C = CircularShift(30,B);
        B = A;
        A = temp;
    }
    H[0] = (H[0] + A) & 0xFFFFFFFF;
    H[1] = (H[1] + B) & 0xFFFFFFFF;
    H[2] = (H[2] + C) & 0xFFFFFFFF;
    H[3] = (H[3] + D) & 0xFFFFFFFF;
    H[4] = (H[4] + E) & 0xFFFFFFFF;
    Message_Block_Index = 0;
}


void SHA1::reset()
{
    Message_Block = new unsigned int[64];
    Words = new unsigned int[80];
    H.reserve(5);
    H.push_back(0x67452301);
    H.push_back(0xEFCDAB89);
    H.push_back(0x98BADCFE);
    H.push_back(0x10325476);
    H.push_back(0xC3D2E1F0);
    Length_Low          = 0;
    Length_High         = 0;
    Message_Block_Index = 0;
    Computed    = false;
    Corrupted   = false;
    finalized = false;
}

bool SHA1::result(int* message_digest_array)
{
    int i;
    if (Corrupted)
    {
        return false;
    }
    if (!Computed)
    {
        PadMessage();
        Computed = true;
    }
    for(i=0; i<5; i++)
    {
        message_digest_array[i] = H.at(i);
    }
    return true;
}

void SHA1::update(const char *message_array, unsigned int length)
{
    if (!length)
    {
        return;
    }
    if (Computed || Corrupted)
    {
        Corrupted = true;
        return;
    }
    while((length--) && !Corrupted)
    {
        Message_Block[Message_Block_Index++] = (*message_array & 0xFF);
        Length_Low += 8;
        Length_Low &= 0xFFFFFFFF;               // Force it to 32 bits
        if (Length_Low == 0)
        {
            Length_High++;
            Length_High &= 0xFFFFFFFF;          // Force it to 32 bits
            if (Length_High == 0)
            {
                Corrupted = true;               // Message is too long
            }
        }
        if(Message_Block_Index == 64)
        {
            ProcessMessageBlock();
        }
        message_array++;
    }
}


void SHA1::update(const std::string& data)
{
    update(data.c_str(), data.length());
}

void SHA1::update(char message_element)
{
    update(&message_element, 1);
}

void SHA1::finalize()
{
    finalized = true;
}





/*
* MD5 Impl
*/

std::string MD5::GenMD5(const std::string& str, unsigned int length)
{
    MD5 hs_ctx;
    length = (length == 0) ? str.length() : length;
    hs_ctx.update(str.c_str(), length);
    hs_ctx.finalize();
    return hs_ctx.hexDigest();
}

MD5::MD5()
{
    presetup();
    init();
}

MD5::~MD5()
{
    cleanup();
}

void MD5::presetup()
{
    cleanedup = false;
    finalized = false;
    buffer = new uint1[blocksize+1];
    count = new uint4[2+1];
    state = new uint4[4+1];
    digest = new uint1[16+1];
    digest_buffer = new char[33+1];
}


void MD5::cleanup()
{
    if(cleanedup == false)
    {
        delete[] buffer;
        delete[] count;
        delete[] state;
        delete[] digest;
        delete[] digest_buffer;
        cleanedup = true;
    }
}

void MD5::decode(uint4 output[], const uint1 input[], size_type len)
{
    for (unsigned int i = 0, j = 0; j < len; i++, j += 4)
    {
        output[i] =
        (
            ((uint4)input[j])           |
            (((uint4)input[j + 1]) << 8)  |
            (((uint4)input[j + 2]) << 16) |
            (((uint4)input[j + 3]) << 24)
        );
    }
}

void MD5::encode(uint1 output[], const uint4 input[], size_type len)
{
    size_type i;
    size_type j;
    for (i=0, j=0; j<len; i++, j+=4)
    {
        output[j] = input[i] & 0xff;
        output[j+1] = (input[i] >> 8) & 0xff;
        output[j+2] = (input[i] >> 16) & 0xff;
        output[j+3] = (input[i] >> 24) & 0xff;
    }
}

void MD5::finalize()
{
    size_type index;
    size_type padLen;
    unsigned char bits[8];
    static unsigned char padding[64] =
    {
        0x80, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0,
        0, 0, 0, 0, 0, 0, 0, 0, 0, 0
    };

    if (!finalized)
    {
        /* Save number of bits */
        encode(bits, count, 8);

        /* pad out to 56 mod 64. */
        index = count[0] / 8 % 64;
        padLen = (index < 56) ? (56 - index) : (120 - index);
        update(padding, padLen);

        /* Append length (before padding) */
        update(bits, 8);

        /* Store state in digest */
        encode(digest, state, 16);

        /* Zeroize sensitive information. */
        //memset(buffer, 0, sizeof(buffer));
        memset(buffer, 0, blocksize);
        //memset(count, 0, sizeof(count));
        memset(buffer, 0, 2);
        finalized = true;
    }
}

std::string MD5::hexDigest()
{
    const char* format = "%02x%02x%02x%02x%02x%02x%02x%02x"
                         "%02x%02x%02x%02x%02x%02x%02x%02x";
    if(finalized == true)
    {
        sprintf(digest_buffer, format,
            digest[0],  digest[1],  digest[2],
            digest[3],  digest[4],  digest[5],
            digest[6],  digest[7],  digest[8],
            digest[9],  digest[10], digest[11],
            digest[12], digest[13], digest[14],
            digest[15], digest[16]);
        return std::string(digest_buffer);
    }
    throw Error(
        "MD5::finalize needs to be called before MD5::hexDigest");
    return std::string(); //never reached
}

void MD5::init()
{
    finalized=false;

    count[0] = 0;
    count[1] = 0;

    // load magic initialization constants.
    state[0] = 0x67452301;
    state[1] = 0xefcdab89;
    state[2] = 0x98badcfe;
    state[3] = 0x10325476;
}

MD5::uint4 MD5::rotate_left(uint4 x, int n)
{
    return (((x << n) | (x >> (32 - n))));
}

MD5::uint4 MD5::logic_F(uint4 x, uint4 y, uint4 z)
{
    return (((x & y) | (~x & z)));
}

MD5::uint4 MD5::logic_G(uint4 x, uint4 y, uint4 z)
{
    return (((x & z) | (y & ~z)));
}

MD5::uint4 MD5::logic_H(uint4 x, uint4 y, uint4 z)
{
    return ((x ^ y ^ z));
}

MD5::uint4 MD5::logic_I(uint4 x, uint4 y, uint4 z)
{
    return ((y ^ (x | ~z)));
}

void MD5::rot_round1(uint4 &a, uint4 b, uint4 c, uint4 d, uint4 x, uint4 s, uint4 ac)
{
    a = rotate_left(a + logic_F(b, c, d) + x + ac, s) + b;
}

void MD5::rot_round2(uint4 &a, uint4 b, uint4 c, uint4 d, uint4 x, uint4 s, uint4 ac)
{
    a = rotate_left(a + logic_G(b, c, d) + x + ac, s) + b;
}

void MD5::rot_round3(uint4 &a, uint4 b, uint4 c, uint4 d, uint4 x, uint4 s, uint4 ac)
{
    a = rotate_left(a + logic_H(b, c, d) + x + ac, s) + b;
}

void MD5::rot_round4(uint4 &a, uint4 b, uint4 c, uint4 d, uint4 x, uint4 s, uint4 ac)
{
    a = rotate_left(a + logic_I(b,c,d) + x + ac, s) + b;
}


void MD5::transform(const uint1 block[blocksize])
{
    uint4 a = state[0];
    uint4 b = state[1];
    uint4 c = state[2];
    uint4 d = state[3];
    uint4 x[16];
    decode(x, block, blocksize);

    /* Round 1 */
    rot_round1(a, b, c, d, x[ 0], __MD5_S11, 0xd76aa478); /* 1 */
    rot_round1(d, a, b, c, x[ 1], __MD5_S12, 0xe8c7b756); /* 2 */
    rot_round1(c, d, a, b, x[ 2], __MD5_S13, 0x242070db); /* 3 */
    rot_round1(b, c, d, a, x[ 3], __MD5_S14, 0xc1bdceee); /* 4 */
    rot_round1(a, b, c, d, x[ 4], __MD5_S11, 0xf57c0faf); /* 5 */
    rot_round1(d, a, b, c, x[ 5], __MD5_S12, 0x4787c62a); /* 6 */
    rot_round1(c, d, a, b, x[ 6], __MD5_S13, 0xa8304613); /* 7 */
    rot_round1(b, c, d, a, x[ 7], __MD5_S14, 0xfd469501); /* 8 */
    rot_round1(a, b, c, d, x[ 8], __MD5_S11, 0x698098d8); /* 9 */
    rot_round1(d, a, b, c, x[ 9], __MD5_S12, 0x8b44f7af); /* 10 */
    rot_round1(c, d, a, b, x[10], __MD5_S13, 0xffff5bb1); /* 11 */
    rot_round1(b, c, d, a, x[11], __MD5_S14, 0x895cd7be); /* 12 */
    rot_round1(a, b, c, d, x[12], __MD5_S11, 0x6b901122); /* 13 */
    rot_round1(d, a, b, c, x[13], __MD5_S12, 0xfd987193); /* 14 */
    rot_round1(c, d, a, b, x[14], __MD5_S13, 0xa679438e); /* 15 */
    rot_round1(b, c, d, a, x[15], __MD5_S14, 0x49b40821); /* 16 */

    /* Round 2 */
    rot_round2(a, b, c, d, x[ 1], __MD5_S21, 0xf61e2562); /* 17 */
    rot_round2(d, a, b, c, x[ 6], __MD5_S22, 0xc040b340); /* 18 */
    rot_round2(c, d, a, b, x[11], __MD5_S23, 0x265e5a51); /* 19 */
    rot_round2(b, c, d, a, x[ 0], __MD5_S24, 0xe9b6c7aa); /* 20 */
    rot_round2(a, b, c, d, x[ 5], __MD5_S21, 0xd62f105d); /* 21 */
    rot_round2(d, a, b, c, x[10], __MD5_S22,  0x2441453); /* 22 */
    rot_round2(c, d, a, b, x[15], __MD5_S23, 0xd8a1e681); /* 23 */
    rot_round2(b, c, d, a, x[ 4], __MD5_S24, 0xe7d3fbc8); /* 24 */
    rot_round2(a, b, c, d, x[ 9], __MD5_S21, 0x21e1cde6); /* 25 */
    rot_round2(d, a, b, c, x[14], __MD5_S22, 0xc33707d6); /* 26 */
    rot_round2(c, d, a, b, x[ 3], __MD5_S23, 0xf4d50d87); /* 27 */
    rot_round2(b, c, d, a, x[ 8], __MD5_S24, 0x455a14ed); /* 28 */
    rot_round2(a, b, c, d, x[13], __MD5_S21, 0xa9e3e905); /* 29 */
    rot_round2(d, a, b, c, x[ 2], __MD5_S22, 0xfcefa3f8); /* 30 */
    rot_round2(c, d, a, b, x[ 7], __MD5_S23, 0x676f02d9); /* 31 */
    rot_round2(b, c, d, a, x[12], __MD5_S24, 0x8d2a4c8a); /* 32 */

    /* Round 3 */
    rot_round3(a, b, c, d, x[ 5], __MD5_S31, 0xfffa3942); /* 33 */
    rot_round3(d, a, b, c, x[ 8], __MD5_S32, 0x8771f681); /* 34 */
    rot_round3(c, d, a, b, x[11], __MD5_S33, 0x6d9d6122); /* 35 */
    rot_round3(b, c, d, a, x[14], __MD5_S34, 0xfde5380c); /* 36 */
    rot_round3(a, b, c, d, x[ 1], __MD5_S31, 0xa4beea44); /* 37 */
    rot_round3(d, a, b, c, x[ 4], __MD5_S32, 0x4bdecfa9); /* 38 */
    rot_round3(c, d, a, b, x[ 7], __MD5_S33, 0xf6bb4b60); /* 39 */
    rot_round3(b, c, d, a, x[10], __MD5_S34, 0xbebfbc70); /* 40 */
    rot_round3(a, b, c, d, x[13], __MD5_S31, 0x289b7ec6); /* 41 */
    rot_round3(d, a, b, c, x[ 0], __MD5_S32, 0xeaa127fa); /* 42 */
    rot_round3(c, d, a, b, x[ 3], __MD5_S33, 0xd4ef3085); /* 43 */
    rot_round3(b, c, d, a, x[ 6], __MD5_S34,  0x4881d05); /* 44 */
    rot_round3(a, b, c, d, x[ 9], __MD5_S31, 0xd9d4d039); /* 45 */
    rot_round3(d, a, b, c, x[12], __MD5_S32, 0xe6db99e5); /* 46 */
    rot_round3(c, d, a, b, x[15], __MD5_S33, 0x1fa27cf8); /* 47 */
    rot_round3(b, c, d, a, x[ 2], __MD5_S34, 0xc4ac5665); /* 48 */

    /* Round 4 */
    rot_round4(a, b, c, d, x[ 0], __MD5_S41, 0xf4292244); /* 49 */
    rot_round4(d, a, b, c, x[ 7], __MD5_S42, 0x432aff97); /* 50 */
    rot_round4(c, d, a, b, x[14], __MD5_S43, 0xab9423a7); /* 51 */
    rot_round4(b, c, d, a, x[ 5], __MD5_S44, 0xfc93a039); /* 52 */
    rot_round4(a, b, c, d, x[12], __MD5_S41, 0x655b59c3); /* 53 */
    rot_round4(d, a, b, c, x[ 3], __MD5_S42, 0x8f0ccc92); /* 54 */
    rot_round4(c, d, a, b, x[10], __MD5_S43, 0xffeff47d); /* 55 */
    rot_round4(b, c, d, a, x[ 1], __MD5_S44, 0x85845dd1); /* 56 */
    rot_round4(a, b, c, d, x[ 8], __MD5_S41, 0x6fa87e4f); /* 57 */
    rot_round4(d, a, b, c, x[15], __MD5_S42, 0xfe2ce6e0); /* 58 */
    rot_round4(c, d, a, b, x[ 6], __MD5_S43, 0xa3014314); /* 59 */
    rot_round4(b, c, d, a, x[13], __MD5_S44, 0x4e0811a1); /* 60 */
    rot_round4(a, b, c, d, x[ 4], __MD5_S41, 0xf7537e82); /* 61 */
    rot_round4(d, a, b, c, x[11], __MD5_S42, 0xbd3af235); /* 62 */
    rot_round4(c, d, a, b, x[ 2], __MD5_S43, 0x2ad7d2bb); /* 63 */
    rot_round4(b, c, d, a, x[ 9], __MD5_S44, 0xeb86d391); /* 64 */

    state[0] += a;
    state[1] += b;
    state[2] += c;
    state[3] += d;

    // Zeroize sensitive information.
    //memset(x, 0, sizeof(x));
}

void MD5::update(const unsigned char input[], size_type length)
{
    MD5::size_type index;
    MD5::size_type i;
    MD5::size_type firstpart;
    // compute number of bytes mod 64
    index = count[0] / 8 % blocksize;

    // Update number of bits
    if ((count[0] += (length << 3)) < (length << 3))
    {
        count[1]++;
    }
    count[1] += (length >> 29);

    // number of bytes we need to fill in buffer
    firstpart = 64 - index;

    // transform as many times as possible.
    if (length >= firstpart)
    {
        // fill buffer first, transform
        memcpy(&buffer[index], input, firstpart);
        transform(buffer);

        // transform chunks of blocksize (64 bytes)
        for (i = firstpart; i + blocksize <= length; i += blocksize)
        {
            transform(&input[i]);
        }
        index = 0;
    }
    else
    {
        i = 0;
    }
    // buffer remaining input
    memcpy(&buffer[index], &input[i], length-i);
}


void MD5::update(const char* input, size_type length)
{
    update((const unsigned char*)input, length);
}

void MD5::update(const std::string& input)
{
    update(input.c_str(), input.length());
}

void MD5::update(char input)
{
    update(&input, 1);
}

} // namespace Hash

