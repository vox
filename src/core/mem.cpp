
#include "pcheader.hpp"

void* vox_mem_malloc(VXUnsignedInteger size)
{
    return malloc(size);
}

void* vox_mem_realloc(void *p, VXUnsignedInteger oldsize, VXUnsignedInteger size)
{
    (void)oldsize;
    return realloc(p, size);
}

void vox_mem_free(void *p, VXUnsignedInteger size)
{
    (void)size;
    free(p);
}

void *vox_malloc(VXUnsignedInteger size)
{
    return VX_MALLOC(size);
}

void *vox_realloc(void* p,VXUnsignedInteger oldsize,VXUnsignedInteger newsize)
{
    return VX_REALLOC(p,oldsize,newsize);
}

void vox_free(void *p,VXUnsignedInteger size)
{
    VX_FREE(p,size);
}
