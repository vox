
#include "baselib.hpp"


VXInteger closure_pcall(VXState* v)
{
    //return VX_SUCCEEDED(vox_call(v,vox_gettop(v)-1,true,false))?1:VX_ERROR;
    return v->CallStack(v->GetTop()-1, true, false);
}

VXInteger closure_call(VXState* v)
{
    bool raise;
    if(VX_FAILED(v->GetBool(3, &raise)))
    {
        raise = true;
    }
    if(VX_FAILED(v->CallStack(v->GetTop()-1, true, raise)))
    {
        return VX_ERROR;
    }
    return 1;
}

VXInteger _closure_acall(VXState* v,bool raiseerror)
{
    VXInteger i;
    VXArrayObj *aparams = v->StackGet(2).Array();
    VXInteger nparams = aparams->Size();
    v->Push(v->StackGet(1));
    for(i=0; i<nparams; i++)
    {
        v->Push(aparams->_values[i]);
    }
    return v->CallStack(nparams, true, raiseerror);
}

VXInteger closure_acall(VXState* v)
{
    return _closure_acall(v,true);
}

VXInteger closure_pacall(VXState* v)
{
    return _closure_acall(v,false);
}

VXInteger closure_bindenv(VXState* v)
{
    VXObject res;
    if(VX_FAILED(v->BindEnv(v->StackGet(1), v->StackGet(2), res)))
    {
        return VX_ERROR;
    }
    v->Push(res);
    return 1;
}

VXInteger closure_getinfos(VXState* v)
{
    VXInteger n;
    bool native;
    VXObject filename;
    VXObject funcname;
    VXObject sourcename;
    VXRawObj o = v->StackGet(1);
    VXTableObj* res = v->NewTable();
    if(type(o) == VX_OT_CLOSURE)
    {
        VXFuncProtoObj* f = _closure(o)->_function;
        VXInteger nparams = f->_nparameters + (f->_varparams?1:0);
        VXObject params = v->NewArray(nparams);
        for(n=0; n<(f->_nparameters); n++)
        {
            _array(params)->Set(n, f->_parameters[n]);
        }
        if(f->_varparams)
        {
            _array(params)->Set(nparams-1, v->NewString("..."));
        }
        native = false;
        funcname = f->_name;
        filename = f->_sourcename;
        res->NewSlot(v->NewString("parameters"), params);
        res->NewSlot(v->NewString("varargs"),    f->_varparams);
    }
    else                         //VX_OT_NATIVECLOSURE
    {
        VXNativeClosureObj *nc = _nativeclosure(o);
        native = true;
        funcname = nc->_name.String();
        filename = v->NewString("[native]");
        res->NewSlot(v->NewString("paramscheck"), nc->_nparamscheck);
        VXObject typecheck;
        if(nc->_typecheck.size() > 0)
        {
            typecheck = v->NewArray(nc->_typecheck.size());
            for(VXUnsignedInteger n = 0; n<nc->_typecheck.size(); n++)
            {
                _array(typecheck)->Set((VXInteger)n, nc->_typecheck[n]);
            }
        }
        res->NewSlot(v->NewString("typecheck"), typecheck);
    }
    res->NewSlot(v->NewString("native"), native);
    res->NewSlot(v->NewString("name"), funcname);
    res->NewSlot(v->NewString("src"), filename);
    v->Push(res);
    return 1;
}

VXRegFunction VXSharedState::_closure_default_delegate_funcz[]=
{
    {"call",     closure_call,              -1, "c"},
    {"pcall",    closure_pcall,             -1, "c"},
    {"acall",    closure_acall,             2,  "ca"},
    {"pacall",   closure_pacall,            2,  "ca"},
    {"weakref",  obj_delegate_weakref,      1,  NULL },
    {"tostring", default_delegate_tostring, -1,  ".b"},
    {"bindenv",  closure_bindenv,           2,  "c x|y|t"},
    {"getinfos", closure_getinfos,          1,  "c"},
    {0, 0, 0, 0}
};


