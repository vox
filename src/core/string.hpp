
#ifndef _VXSTRING_H_
#define _VXSTRING_H_

#include "pcheader.hpp"
#include "object.hpp"
#include "state.hpp"
#include <string>

std::string strhelp_quote(const std::string& str, bool escape_only=false);

inline VXHash _hashstr(const char* s, size_t l)
{
    VXHash h = (VXHash)l;        /* seed */
    size_t step = (l>>5)|1;      /* if string is too long, don't hash all its chars */
    for (; l>=step; l-=step)
    {
        h = h ^ ((h<<5)+(h>>2)+(unsigned short)*(s++));
    }
    return h;
}


#endif                           //_VXSTRING_H_
