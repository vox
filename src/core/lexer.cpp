
#include <ctype.h>
#include <stdlib.h>

#include "pcheader.hpp"
#include "table.hpp"
#include "string.hpp"
#include "compiler.hpp"
#include "lexer.hpp"
#include "names.hpp"

#define CUR_CHAR (_currdata)

#define RETURN_TOKEN(t) \
    { \
        _prevtoken = _curtoken; \
        _curtoken = t; \
        return t; \
    }

#define IS_EOB() \
    (CUR_CHAR <= VOX_EOB)

#define NEXT() \
    { \
        Next(); \
        _currentcolumn++; \
    }

#define INIT_TEMP_STRING() \
    { \
        _longstr.resize(0); \
    }

#define APPEND_CHAR(c) \
    { \
        _longstr.push_back(c); \
    }

#define TERMINATE_BUFFER() \
    { \
        _longstr.push_back('\0'); \
    }

#define ADD_KEYWORD(key, id) \
    _keywords->NewSlot(VXStringObj::Create(ss, key), VXInteger(id))

VXLexer::VXLexer(){}
VXLexer::~VXLexer()
{
    _keywords->Release();
}

void VXLexer::Init(VXSharedState *ss, VXLexReadFunc rg, VXUserPointer up,CompilerErrorFunc efunc,void *ed)
{
    _errfunc = efunc;
    _errtarget = ed;
    _sharedstate = ss;
    _keywords = VXTableObj::Create(ss, 27);
    ADD_KEYWORD(NAMES_TK_THISVAR,  TK_THIS);
    ADD_KEYWORD(NAMES_TK_CTOR,     TK_CONSTRUCTOR);
    ADD_KEYWORD("while",           TK_WHILE);
    ADD_KEYWORD("do",              TK_DO);
    ADD_KEYWORD("if",              TK_IF);
    ADD_KEYWORD("else",            TK_ELSE);
    ADD_KEYWORD("break",           TK_BREAK);
    ADD_KEYWORD("continue",        TK_CONTINUE);
    ADD_KEYWORD("return",          TK_RETURN);
    ADD_KEYWORD("null",            TK_NULL);
    ADD_KEYWORD("function",        TK_FUNCTION);
    ADD_KEYWORD("local",           TK_LOCAL);
    ADD_KEYWORD("for",             TK_FOR);
    ADD_KEYWORD("foreach",         TK_FOREACH);
    ADD_KEYWORD("in",              TK_IN);
    ADD_KEYWORD("typeof",          TK_TYPEOF);
    ADD_KEYWORD("base",            TK_BASE);
    ADD_KEYWORD("delete",          TK_DELETE);
    ADD_KEYWORD("try",             TK_TRY);
    ADD_KEYWORD("catch",           TK_CATCH);
    ADD_KEYWORD("throw",           TK_THROW);
    ADD_KEYWORD("clone",           TK_CLONE);
    ADD_KEYWORD("yield",           TK_YIELD);
    ADD_KEYWORD("resume",          TK_RESUME);
    ADD_KEYWORD("switch",          TK_SWITCH);
    ADD_KEYWORD("case",            TK_CASE);
    ADD_KEYWORD("default",         TK_DEFAULT);
    ADD_KEYWORD("class",           TK_CLASS);
    ADD_KEYWORD("extends",         TK_EXTENDS);
    ADD_KEYWORD("instanceof",      TK_INSTANCEOF);
    ADD_KEYWORD("true",            TK_TRUE);
    ADD_KEYWORD("false",           TK_FALSE);
    ADD_KEYWORD("static",          TK_STATIC);
    ADD_KEYWORD("enum",            TK_ENUM);
    ADD_KEYWORD("const",           TK_CONST);
    //ADD_KEYWORD("import",        TK_IMPORT);

    _readf = rg;
    _up = up;
    _lasttokenline = _currentline = 1;
    _currentcolumn = 0;
    _prevtoken = -1;
    _reached_eof = false;
    Next();
}

void VXLexer::Error(const char *err)
{
    _errfunc(_errtarget,err);
}

void VXLexer::Next()
{
    VXInteger t = _readf(_up);
    if(t > VOX_MAX_CHAR)
    {
        Error("Invalid character");
    }
    if(t != 0)
    {
        _currdata = (LexChar)t;
        return;
    }
    _currdata = VOX_EOB;
    _reached_eof = true;
}

const char *VXLexer::Tok2Str(VXInteger tok)
{
    VXObject itr, key, val;
    VXInteger nitr;
    while((nitr = _keywords->Next(false,itr, key, val)) != -1)
    {
        itr = (VXInteger)nitr;
        if(((VXInteger)_integer(val)) == tok)
        {
            return _stringval(key);
        }
    }
    return NULL;
}

void VXLexer::LexBlockComment()
{
    bool done = false;
    while(!done)
    {
        switch(CUR_CHAR)
        {
            case '*':
            {
                NEXT();
                if(CUR_CHAR == '/')
                {
                    done = true;
                    NEXT();
                }
            };
            continue;
            case '\n':
                _currentline++;
                NEXT();
                continue;
            case
                VOX_EOB: Error("missing \"*/\" in comment");
            default:
                NEXT();
        }
    }
}

void VXLexer::LexLineComment()
{
    do
    {
        NEXT();
    } while (CUR_CHAR != '\n' && (!IS_EOB()));
}

VXInteger VXLexer::Lex()
{
    _lasttokenline = _currentline;
    while(CUR_CHAR != VOX_EOB)
    {
        switch(CUR_CHAR)
        {
            case '\t':
            case '\r':
            case ' ':
                NEXT();
                continue;
            case '\n':
                _currentline++;
                _prevtoken=_curtoken;
                _curtoken='\n';
                NEXT();
                _currentcolumn=1;
                continue;
            case '#':
                LexLineComment();
                continue;
            case '/':
                NEXT();
                switch(CUR_CHAR)
                {
                    case '*':
                        NEXT();
                        LexBlockComment();
                        continue;
                    case '/':
                        LexLineComment();
                        continue;
                    case '=':
                        NEXT();
                        RETURN_TOKEN(TK_DIVEQ);
                        continue;
                    case '>':
                        NEXT();
                        RETURN_TOKEN(TK_ATTR_CLOSE);
                        continue;
                    default:
                        RETURN_TOKEN('/');
                }
            case '=':
                NEXT();
                if (CUR_CHAR != '=')
                {
                    RETURN_TOKEN('=')
                }
                else
                {
                    NEXT();
                    RETURN_TOKEN(TK_EQ);
                }
            case '<':
                NEXT();
                switch(CUR_CHAR)
                {
                    case '=':
                        NEXT();
                        if(CUR_CHAR == '>')
                        {
                            NEXT();
                            RETURN_TOKEN(TK_3WAYSCMP);
                        }
                        RETURN_TOKEN(TK_LE)
                        break;
                    case '<':
                        NEXT();
                        RETURN_TOKEN(TK_SHIFTL);
                        break;
                    case '/':
                        NEXT();
                        RETURN_TOKEN(TK_ATTR_OPEN);
                        break;
                }
                RETURN_TOKEN('<');

            case '>':
                NEXT();
                if (CUR_CHAR == '=')
                {
                    NEXT();
                    RETURN_TOKEN(TK_GE);
                }
                else if(CUR_CHAR == '>')
                {
                    NEXT();
                    if(CUR_CHAR == '>')
                    {
                        NEXT();
                        RETURN_TOKEN(TK_USHIFTR);
                    }
                    RETURN_TOKEN(TK_SHIFTR);
                }
                else
                {
                    RETURN_TOKEN('>')
                }
            case '!':
                NEXT();
                if (CUR_CHAR != '=')
                {
                    RETURN_TOKEN('!')
                }
                else
                {
                    NEXT();
                    RETURN_TOKEN(TK_NE);
                }
            case '@':
            {
                VXInteger stype;
                VXInteger until = '"';
                NEXT();
                if(CUR_CHAR != '"')
                {
                    Error("Expected '\"'");
                }
                if((stype=ReadString(until, true)) != -1)
                {
                    RETURN_TOKEN(stype);
                }
                Error("error parsing the string literal (missing quote?)");
            }
            case '"':
            case '\'':
            {
                VXInteger stype;
                if((stype=ReadString(CUR_CHAR,false))!=-1)
                {
                    RETURN_TOKEN(stype);
                }
                Error("error parsing the string literal");
            }
            case '{':
            case '}':
            case '(':
            case ')':
            case '[':
            case ']':
            case ';':
            case ',':
            case '?':
            case '^':
            case '~':
            {
                VXInteger ret = CUR_CHAR;
                NEXT();
                RETURN_TOKEN(ret);
            }
            case '.':
                NEXT();
                if (CUR_CHAR != '.')
                {
                    RETURN_TOKEN('.')
                }
                NEXT();
                if(CUR_CHAR != '.')
                {
                    Error("invalid token '..'");
                }
                NEXT();
                RETURN_TOKEN(TK_VARPARAMS);
            case '&':
                NEXT();
                if (CUR_CHAR != '&')
                {
                    RETURN_TOKEN('&')
                }
                else
                {
                    NEXT();
                    RETURN_TOKEN(TK_AND);
                }
            case '|':
                NEXT();
                if(CUR_CHAR != '|')
                {
                    RETURN_TOKEN('|');
                }
                else
                {
                    NEXT();
                    RETURN_TOKEN(TK_OR);
                }
            case ':':
                NEXT();
                /*if(CUR_CHAR == '=')
                {
                    NEXT();
                    RETURN_TOKEN(TK_NEWSLOT);
                    //break;
                }
                */
                if (CUR_CHAR != ':')
                {
                    RETURN_TOKEN(':')
                }
                else
                {
                    NEXT();
                    RETURN_TOKEN(TK_DOUBLE_COLON);
                }
            case '*':
                NEXT();
                if (CUR_CHAR == '=')
                {
                    NEXT();
                    RETURN_TOKEN(TK_MULEQ);
                }
                else
                {
                    RETURN_TOKEN('*');
                }
            case '%':
                NEXT();
                if(CUR_CHAR == '=')
                {
                    NEXT(); RETURN_TOKEN(TK_MODEQ);
                }
                else
                {
                    RETURN_TOKEN('%');
                }
            case '-':
                NEXT();
                if (CUR_CHAR == '=')
                {
                    NEXT();
                    RETURN_TOKEN(TK_MINUSEQ);
                }
                else if(CUR_CHAR == '-')
                {
                    NEXT();
                    RETURN_TOKEN(TK_MINUSMINUS);
                }
                else
                {
                    RETURN_TOKEN('-');
                }
            case '+':
                NEXT();
                if(CUR_CHAR == '=')
                {
                    NEXT();
                    RETURN_TOKEN(TK_PLUSEQ);
                }
                else if(CUR_CHAR == '+')
                {
                    NEXT();
                    RETURN_TOKEN(TK_PLUSPLUS);
                }
                else
                {
                    RETURN_TOKEN('+');
                }
            case VOX_EOB:
                return 0;
            default:
            {
                if (isdigit(CUR_CHAR))
                {
                    VXInteger ret = ReadNumber();
                    RETURN_TOKEN(ret);
                }
                else if (isalpha(CUR_CHAR) || CUR_CHAR == '_')
                {
                    VXInteger t = ReadID();
                    RETURN_TOKEN(t);
                }
                else
                {
                    VXInteger c = CUR_CHAR;
                    if (iscntrl((int)c))
                    {
                        Error("unexpected character (ctrl)");
                    }
                    NEXT();
                    RETURN_TOKEN(c);
                }
                RETURN_TOKEN(0);
            }
        }
    }
    return 0;
}

VXInteger VXLexer::GetIDType(char *s)
{
    VXObject t;
    if(_keywords->Get(VXStringObj::Create(_sharedstate, s), t))
    {
        return VXInteger(_integer(t));
    }
    return TK_IDENTIFIER;
}

#define ADD_ESCAPE(find_this, add_this) \
    case find_this: \
        APPEND_CHAR(add_this); \
        NEXT(); \
        break;

VXInteger VXLexer::ReadString(VXInteger ndelim, bool verbatim)
{
    INIT_TEMP_STRING();
    NEXT();
    if(IS_EOB())
    {
        return -1;
    }
    for(;;)
    {
        while(CUR_CHAR != ndelim)
        {
            switch(CUR_CHAR)
            {
                case VOX_EOB:
                    Error("unfinished string");
                    return -1;
                case '\n':
                    APPEND_CHAR(CUR_CHAR); NEXT();
                    _currentline++;
                    break;
                case '\\':
                    if(verbatim == false)
                    {
                        NEXT();
                        switch(CUR_CHAR)
                        {
                            case 'x':
                                {
                                    VXInteger n = 0;
                                    const VXInteger maxdigits = 4;
                                    char* sTemp;
                                    char temp[maxdigits+1];
                                    NEXT();
                                    if(!isxdigit(CUR_CHAR))
                                    {
                                        Error("hexadecimal number expected");
                                    }
                                    while(isxdigit(CUR_CHAR) && n < maxdigits)
                                    {
                                        temp[n] = CUR_CHAR;
                                        n++;
                                        NEXT();
                                    }
                                    temp[n] = 0;
                                    APPEND_CHAR((char)strtoul(temp,&sTemp,16));
                                }
                                break;
                            ADD_ESCAPE('t', '\t')
                            ADD_ESCAPE('a', '\a')
                            ADD_ESCAPE('b', '\b')
                            ADD_ESCAPE('n', '\n')
                            ADD_ESCAPE('r', '\r')
                            ADD_ESCAPE('v', '\v')
                            ADD_ESCAPE('e', 033)
                            ADD_ESCAPE('0', '\0')
                            ADD_ESCAPE('\\', '\\')
                            ADD_ESCAPE('"', '\"')
                            ADD_ESCAPE('\'', '\'')
                            default:
                                Error("unrecognised escape char");
                                break;
                        }
                    }
                    else
                    {
                        APPEND_CHAR('\\');
                        NEXT();
                    }
                    break;
                default:
                    APPEND_CHAR(CUR_CHAR);
                    NEXT();
            }
        }
        NEXT();
        //double quotation
        if(verbatim && CUR_CHAR == '"')
        {
            APPEND_CHAR(CUR_CHAR);
            NEXT();
        }
        else
        {
            break;
        }
    }
    TERMINATE_BUFFER();
    VXInteger len = _longstr.size()-1;
    if(ndelim == '\'')
    {
        if(len == 0) Error("empty constant");
        if(len > 1) Error("constant too long");
        _nvalue = _longstr[0];
        return TK_INTEGER;
    }
    _svalue = &_longstr[0];
    return TK_STRING_LITERAL;
}

void LexHexadecimal(const char *s,VXInteger *res)
{
    *res = 0;
    while(*s != 0)
    {
        if(isdigit(*s))
        {
            *res = (*res)*16+((*s++)-'0');
        }
        else if(isxdigit(*s))
        {
            *res = (*res)*16+(toupper(*s++)-'A'+10);
        }
        else
        {
            vox_assert(0);
        }
    }
}

void LexInteger(const char *s, VXInteger* res)
{
    (*res) = atol(s);
}

VXInteger scisodigit(VXInteger c)
{
    return c >= '0' && c <= '7';
}

void LexOctal(const char *s,VXInteger *res)
{
    *res = 0;
    while(*s != 0)
    {
        if(scisodigit(*s))
        {
            *res = (*res)*8+((*s++)-'0');
        }
        else
        {
            vox_assert(0);
        }
    }
}

VXInteger isexponent(VXInteger c)
{
    return c == 'e' || c=='E';
}

#define MAX_HEX_DIGITS (sizeof(VXInteger)*2)
VXInteger VXLexer::ReadNumber()
{
    #define TINT 1
    #define TFLOAT 2
    #define THEX 3
    #define TSCIENTIFIC 4
    #define TOCTAL 5
    VXInteger type = TINT;
    VXInteger firstchar = CUR_CHAR;
    char *sTemp;
    INIT_TEMP_STRING();
    NEXT();
    if(firstchar == '0' && (toupper(CUR_CHAR) == 'X' || scisodigit(CUR_CHAR)))
    {
        if(scisodigit(CUR_CHAR))
        {
            type = TOCTAL;
            while(scisodigit(CUR_CHAR))
            {
                APPEND_CHAR(CUR_CHAR);
                NEXT();
            }
            if(isdigit(CUR_CHAR))
            {
                Error("invalid octal number");
            }
        }
        else
        {
            NEXT();
            type = THEX;
            while(isxdigit(CUR_CHAR))
            {
                APPEND_CHAR(CUR_CHAR);
                NEXT();
            }
            if(_longstr.size() > MAX_HEX_DIGITS) Error("too many digits for an Hex number");
        }
    }
    else
    {
        APPEND_CHAR((int)firstchar);
        while (CUR_CHAR == '.' || isdigit(CUR_CHAR) || isexponent(CUR_CHAR))
        {
            if(CUR_CHAR == '.') type = TFLOAT;
            if(isexponent(CUR_CHAR))
            {
                if(type != TFLOAT) Error("invalid numeric format");
                type = TSCIENTIFIC;
                APPEND_CHAR(CUR_CHAR);
                NEXT();
                if(CUR_CHAR == '+' || CUR_CHAR == '-')
                {
                    APPEND_CHAR(CUR_CHAR);
                    NEXT();
                }
                if(!isdigit(CUR_CHAR)) Error("exponent expected");
            }

            APPEND_CHAR(CUR_CHAR);
            NEXT();
        }
    }
    TERMINATE_BUFFER();
    switch(type)
    {
        case TSCIENTIFIC:
        case TFLOAT:
            _fvalue = (VXFloat)strtod(&_longstr[0],&sTemp);
            return TK_FLOAT;
        case TINT:
            LexInteger(&_longstr[0], (VXInteger*)&_nvalue);
            return TK_INTEGER;
        case THEX:
            LexHexadecimal(&_longstr[0], (VXInteger*)&_nvalue);
            return TK_INTEGER;
        case TOCTAL:
            LexOctal(&_longstr[0], (VXInteger*)&_nvalue);
            return TK_INTEGER;
    }
    return 0;
}

VXInteger VXLexer::ReadID()
{
    VXInteger res;
    INIT_TEMP_STRING();
    do
    {
        APPEND_CHAR(CUR_CHAR);
        NEXT();
    } while(isalnum(CUR_CHAR) || CUR_CHAR == '_');
    TERMINATE_BUFFER();
    res = GetIDType(&_longstr[0]);
    if(res == TK_IDENTIFIER || res == TK_CONSTRUCTOR)
    {
        _svalue = &_longstr[0];
    }
    return res;
}
