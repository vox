
#ifndef _VXUTILS_H_
#define _VXUTILS_H_

#include <vector>
#include "pcheader.hpp"
#include "mem.hpp"

#define _ss(_vm_) (_vm_)->_sharedstate

#ifndef NO_GARBAGE_COLLECTOR
#   define _opt_ss(_vm_) (_vm_)->_sharedstate
#else
#   define _opt_ss(_vm_) NULL
#endif

#define _GETSAFE_OBJ(v,idx,type,o) \
    { \
        if(!vox_aux_gettypedarg(v,idx,type,&o)) \
        { \
            return VX_ERROR; \
        } \
    }

#define vox_aux_paramscheck(v,count) \
    { \
        if(vox_gettop(v) < count) \
        { \
            v->ThrowError("not enough params in the stack"); \
            return VX_ERROR; \
        } \
    }

struct BufState
{
    const char *buf;
    VXInteger ptr;
    VXInteger size;
};

VXInteger buf_lexfeed(VXUserPointer file);
VXInteger file_read(VXUserPointer file,VXUserPointer buf,VXInteger size);
VXInteger file_write(VXUserPointer file, VXUserPointer p, VXInteger size);
VXInteger _io_file_lexfeed_PLAIN(VXUserPointer file);
VXInteger _io_file_lexfeed_UCS2_LE(VXUserPointer file);
VXInteger _io_file_lexfeed_UCS2_BE(VXUserPointer file);

#endif //_VXUTILS_H_
