
/* here be dragons
   you have been warned */

#include <ctime>
#include <cstdlib>
#include <cstdio>
#include <cstring>
#include <cerrno>
#include <vector>
#include "baselib.hpp"

#define IOLIB_STDOUT_PTRNAME "__HSTDOUT"
#define IOLIB_STDERR_PTRNAME "__HSTDERR"
#define IOLIB_STDIN_PTRNAME  "__HSTDIN"
#define IOLIB_PTRCLASS_NAME "__ioclass_fileptr"

#define VXIO_CHECKVALID(v, self) \
    if((self)->isvalid == false) \
    { \
        return ioclass_fn_invalidhandle(v, self); \
    }

#define IOTYPETAG (('I' << 24) | ('O' << 16) | ('S' << 8) | 'C')



struct VXIOStream: VXIOBase
{
    FILE* fn_base_open(const char* path, const char* mode)
    {
        return fopen(path, mode);
    }

    void fn_base_close(FILE* handle)
    {
        fclose(handle);
    }
};


void decl_fileptr(VXState* v, const char* name, const char* ptrname)
{
    v->DoStringFmt(true, false, false, "::io.%s = ::io.%s(::io.%s);",
        name, IOLIB_PTRCLASS_NAME, ptrname);
}

void del_member(VXState* v, const char* name)
{
    v->DoStringFmt(true, false, false, "delete ::io.%s;", name);
}

VXInteger iolib_readfile(VXState* v)
{
    VXInteger c;
    VXIOStream stream;
    std::vector<char> buffer;
    const char* filename;
    v->GetString(2, &filename, NULL);
    if(!stream.open(filename, "rb"))
    {
        return v->ThrowError(stream.error());
    }
    while((c = stream.readchar()) != EOF)
    {
        buffer.push_back(c);
    }
    stream.close();
    v->PushString(&buffer[0], buffer.size());
    return 1;
}

VXInteger iolib_writefile(VXState* v)
{
    VXIOStream stream;
    VXInteger written;
    VXInteger len;
    bool append;
    const char* filename;
    const char* data;
    std::vector<char> buffer;
    v->GetString(2, &filename, NULL);
    v->GetString(3, &data, &len);
    if(VX_FAILED(v->GetBool(4, &append)))
    {
        append = false;
    }
    if(!stream.open(filename, append ? "a" : "w"))
    {
        return v->ThrowError(stream.error());
    }
    if(stream.write(data, len, &written) == false)
    {
        return v->ThrowError(stream.error());
    }
    stream.close();
    v->Push(written);
    return 1;
}



VXInteger iolib_read(VXState* v)
{
    VXInteger actual;
    VXInteger howmuch;
    VXIOStream stream;
    char* buf;
    stream.open(stdin);
    if(!VX_FAILED(v->GetInteger(2, &howmuch)))
    {
        howmuch = 1024;
    }
    buf = new char[howmuch+1];
    actual = stream.readbuf(buf, howmuch);
    v->Push(v->NewString(buf, actual));
    delete[] buf;
    return 1;
}

VXInteger iolib_readline(VXState* v)
{
    std::string buffer;
    VXIOStream stream;
    stream.open(stdin);
    stream.readline(buffer);
    if(buffer.size() == 0)
    {
        return 0;
    }
    v->Push(v->NewString(buffer.c_str(), buffer.length()));
    return 1;
}

VXInteger iolib_readchar(VXState* v)
{
    char ch;
    VXIOStream hnd;
    hnd.open(stdin);
    ch = hnd.readchar();
    if(ch == EOF)
    {
        v->PushNull();
    }
    else
    {
        v->Push(VXInteger(ch));
    }
    return 1;
}

VXInteger ioclass_fn_invalidhandle(VXState* v, VXIOStream* self)
{
    (void)self;
    return v->ThrowError("invalid i/o handle (%f)");
}

VXInteger ioclass_fn_releasehook(VXUserPointer p, VXInteger size)
{
    (void)size;
    VXIOStream* self = (VXIOStream*)p;
    if(self->isclosed == false)
    {
        self->close();
    }
    delete self;
    return 1;
}

VXInteger ioclass_fn_read(VXState* v)
{
    char* buffer;
    VXInteger howmuch;
    VXInteger haveread;
    VXIOStream* self;
    v->GetInstanceUp(1, (VXUserPointer*)&self, 0);
    VXIO_CHECKVALID(v, self)
    if(VX_FAILED(v->GetInteger(2, &howmuch)))
    {
        howmuch = 1024;
    }
    buffer = new char[howmuch+1];
    haveread = self->readbuf(buffer, howmuch);
    if(haveread > 0)
    {
        buffer[haveread] = 0;
        v->PushString(buffer, haveread);
    }
    else
    {
        v->PushNull();
    }
    delete[] buffer;
    return 1;
}

VXInteger ioclass_fn_flush(VXState* v)
{
    VXIOStream* self;
    v->GetInstanceUp(1, (VXUserPointer*)&self, 0);
    VXIO_CHECKVALID(v, self)
    v->Push(self->flushstream());
    return 1;
}

VXInteger ioclass_fn_name(VXState* v)
{
    VXIOStream* self;
    v->GetInstanceUp(1, (VXUserPointer*)&self, 0);
    VXIO_CHECKVALID(v, self)
    v->PushString(self->name);
    return 1;
}

VXInteger ioclass_fn_readline(VXState* v)
{
    VXIOStream* self;
    std::string buffer;
    v->GetInstanceUp(1, (VXUserPointer*)&self, 0);
    VXIO_CHECKVALID(v, self)
    if(self->eof())
    {
        return 0;
    }
    self->readline(buffer);
    v->Push(v->NewString(buffer.c_str(), buffer.length()));
    return 1;
}

VXInteger ioclass_fn_readchar(VXState* v)
{
    VXIOStream* self;
    VXInteger c;
    v->GetInstanceUp(1, (VXUserPointer*)&self, 0);
    VXIO_CHECKVALID(v, self)
    c = self->readchar();
    /* make use of the fact that vox has a nulltype/nullvalue,
       and push null when EOF is reached*/
    if(c == EOF)
    {
        v->PushNull();
    }
    else
    {
        v->Push(c);
    }
    return 1;
}


VXInteger ioclass_fn_close(VXState* v)
{
    VXIOStream* self;
    v->GetInstanceUp(1, (VXUserPointer*)&self, 0);
    VXIO_CHECKVALID(v, self)
    v->Push(self->close());
    return 1;
}

VXInteger ioclass_fn_write(VXState* v)
{
    const char* data;
    VXInteger len;
    VXInteger written;
    VXIOStream* self;
    v->GetInstanceUp(1, (VXUserPointer*)&self, 0);
    VXIO_CHECKVALID(v, self)
    v->GetString(2, &data, &len);
    if(self->write(data, len, &written) == true)
    {
        v->Push(written);
        return 0;
    }
    /* when the file got opened in readonly mode, write()
       will fail, so don't just ignore it, but handle it */
    return v->ThrowError(self->error());
}

VXInteger ioclass_fn_eof(VXState* v)
{
    VXIOStream* self;
    v->GetInstanceUp(1, (VXUserPointer*)&self, 0);
    VXIO_CHECKVALID(v, self)
    v->Push(bool(self->eof()));
    return 1;
}

VXInteger ioclass_fn_constructor(VXState* v)
{
    const char* path;
    const char* mode;
    VXIOStream* self;
    self = new VXIOStream;
    v->GetString(2, &path, NULL);
    if(VX_FAILED(v->GetString(3, &mode, NULL)))
    {
        mode = "r";
    }
    if(!self->open(path, mode))
    {
        return v->ThrowError("%s: %s", self->error(), path);
    }
    v->SetInstanceUp(1, (VXUserPointer*)&self);
    v->SetReleaseHook(1, ioclass_fn_releasehook);
    return 0;
}

VXInteger ioclass_fn_constructor_ptr(VXState* v)
{
    VXIOStream* self;
    self = new VXIOStream;
    v->GetUserPointer(2, (VXUserPointer*)&(self->stream));
    if(self->stream == NULL)
    {
        return v->ThrowError("invalid i/o pointer");
    }
    self->isvalid = true;
    v->SetInstanceUp(1, (VXUserPointer*)&self);
    v->SetReleaseHook(1, ioclass_fn_releasehook);
    return 0;
}

VXInteger ioclass_fn_typeof(VXState* v)
{
    v->PushString("file");
    return 1;
}

VXInteger ioclass_fn_cloned(VXState* v)
{
    return v->ThrowError("cannot clone a file descriptor");
}

VXRegFunction mkreg(const char* name, VXFunction func,
                     VXInteger nparamscheck, const char* typemask)
{
    VXRegFunction fn;
    fn.name = name;
    fn.func = func;
    fn.nparamscheck = nparamscheck;
    fn.typemask = typemask;
    return fn;
}

/* .console functions {{{ */

bool conhelper_kbhit()
{
#if defined(VOX_PLATFORM_MSWINDOWS)
    bool hb = false;
    INPUT_RECORD *ir = NULL;
    HANDLE hin;
    DWORD i;
    DWORD count = 0;
    hin = GetStdHandle(STD_INPUT_HANDLE);
    GetNumberOfConsoleInputEvents(hin, &count);
    if(count)
    {
        ir = (INPUT_RECORD*)malloc(count * sizeof(INPUT_RECORD));
        if(ir && PeekConsoleInput(hin, ir, count, &count))
        {
            for(i=0; i<(count-1); i++)
            {
                if (ir[i].EventType == KEY_EVENT &&
                    ir[i].Event.KeyEvent.bKeyDown &&
                    ir[i].Event.KeyEvent.uChar.AsciiChar)
                {
                    hb = true;
                    break;
                }
            }
        }
    }
    if(ir != NULL)
    {
        free(ir);
    }
    return hb;
#else /* just assume POSIX */
    struct timeval tv;
    fd_set fds;
    tv.tv_sec = 0;
    tv.tv_usec = 0;
    FD_ZERO(&fds);
    FD_SET(STDIN_FILENO, &fds);
    select(STDIN_FILENO+1, &fds, NULL, NULL, &tv);
    return (FD_ISSET(0, &fds));
#endif
}

VXInteger ioconsole_kbhit(VXState* v)
{
    v->Push(conhelper_kbhit());
    return 1;
}

/* .console functions }}} */

VXVector<VXRegFunction> get_funcs(bool isptrclass)
{
    VXVector<VXRegFunction> funcs;
    VXFunction ctor = ioclass_fn_constructor;
    VXInteger ctor_args = -2;
    const char* ctor_tm = ".ss";
    if(isptrclass)
    {
        ctor_args = 2;
        ctor = ioclass_fn_constructor_ptr;
        ctor_tm = NULL;
    }
    funcs.push_back(mkreg("constructor", ctor, ctor_args, ctor_tm));
    funcs.push_back(mkreg("name", ioclass_fn_name, -1, ".n"));
    funcs.push_back(mkreg("read", ioclass_fn_read, -1, ".n"));
    funcs.push_back(mkreg("readchar", ioclass_fn_readchar, 1,  NULL));
    funcs.push_back(mkreg("readline", ioclass_fn_readline, -1, NULL));
    funcs.push_back(mkreg("write", ioclass_fn_write, 2,  ".s"));
    funcs.push_back(mkreg("close", ioclass_fn_close, 1,  NULL));
    funcs.push_back(mkreg("flush", ioclass_fn_flush, 1,  "x"));
    funcs.push_back(mkreg("eof", ioclass_fn_eof, 1,  "x"));
    funcs.push_back(mkreg("__typeof__", ioclass_fn_typeof, 1,  "x"));
    funcs.push_back(mkreg("__cloned__", ioclass_fn_cloned, -1,  NULL));
    return funcs;
}

static VXRegFunction iolib_funcs[]=
{
    {"readfile",  iolib_readfile,   2,  ".s"},
    {"writefile", iolib_writefile,  -1, ".ssb"},
    {"read",      iolib_read,       -1, NULL},
    {"readline",  iolib_readline,   -1, NULL},
    {"readchar",  iolib_readchar,   -1, NULL},
    {0, 0, 0, 0},
};


VXInteger voxstd_register_iolib(VXState* v)
{
    VXTableObj* tb;
    VXTableObj* contb;
    tb = v->NewTable();
    tb->NewSlot(v->NewString("open"), v->RegClass(get_funcs(false)));
    tb->NewSlot(v->NewString(IOLIB_PTRCLASS_NAME), v->RegClass(get_funcs(true)));
    tb->NewSlot(v->NewString(IOLIB_STDOUT_PTRNAME), VXUserPointer(stdout));
    tb->NewSlot(v->NewString(IOLIB_STDERR_PTRNAME), VXUserPointer(stderr));
    tb->NewSlot(v->NewString(IOLIB_STDIN_PTRNAME), VXUserPointer(stdin));
    v->RegisterLib("io", iolib_funcs, true, tb);


    // create 'console' subtable
    contb = v->NewTable();
    contb->NewSlot(v->NewString("kbhit"), v->NewClosure(ioconsole_kbhit));
    tb->NewSlot(v->NewString("console"), contb);

    // declare filehandle members
    decl_fileptr(v, "stdout", IOLIB_STDOUT_PTRNAME);
    decl_fileptr(v, "stderr", IOLIB_STDERR_PTRNAME);
    decl_fileptr(v, "stdin", IOLIB_STDIN_PTRNAME);

    // cleanup the ungodly mess we created
    del_member(v, IOLIB_STDOUT_PTRNAME);
    del_member(v, IOLIB_STDERR_PTRNAME);
    del_member(v, IOLIB_STDIN_PTRNAME);
    del_member(v, IOLIB_PTRCLASS_NAME);
    return 1;
}


