
#ifndef __standalone_hashpp_header_defined__
#define __standalone_hashpp_header_defined__

#include <exception>
#include <stdexcept>
#include <string>
#include <vector>

namespace Hash
{

class Error: public std::runtime_error
{
    public:
        Error(const std::string& msg): std::runtime_error(msg)
        {
        }

        virtual ~Error() throw()
        {
        }
};


/**
* The HashObject "superclass".
* For maximum portability, rely -only- on functions provided in HashObject.
*/
class Object
{
    public:
        typedef unsigned int  size_type;

    public:
        virtual ~Object()
        {}

        virtual void update(const std::string& input) = 0;
        virtual void update(const char* input, size_type length) = 0;
        virtual void update(char input) = 0;
        virtual void updateFromFile(const std::string& path);
        virtual void finalize() = 0;
        virtual std::string hexDigest() = 0;

};


class MD5: public Object
{
    public: // types
        typedef unsigned char uint1; //  8bit
        typedef unsigned int  uint4;  // 32bit

    public: // static
        static std::string GenMD5(const std::string& str, unsigned int length=0);

    private: // enums
        enum
        {
            blocksize = 64
        };

    private: // variables
        bool finalized;
        bool cleanedup;
        char* digest_buffer;
        MD5::uint1* buffer;
        MD5::uint4* count;
        MD5::uint4* state;
        MD5::uint1* digest;

    private:
        void presetup();

        void init();

        // apply MD5 algo on a block
        void transform(const uint1 block[blocksize]);

        // decodes input (unsigned char) into output (uint4).
        // Assumes len is a multiple of 4.
        void decode(uint4 output[], const uint1 input[], size_type len);


        // encodes input (uint4) into output (unsigned char). Assumes len is
        // a multiple of 4.
        void encode(uint1 output[], const uint4 input[], size_type len);

        // low level logic operations
        uint4 logic_F(uint4 x, uint4 y, uint4 z);

        uint4 logic_G(uint4 x, uint4 y, uint4 z);

        uint4 logic_H(uint4 x, uint4 y, uint4 z);

        uint4 logic_I(uint4 x, uint4 y, uint4 z);

        uint4 rotate_left(uint4 x, int n);

        void rot_round1(uint4 &a, uint4 b, uint4 c, uint4 d,
                        uint4 x, uint4 s, uint4 ac);

        void rot_round2(uint4 &a, uint4 b, uint4 c, uint4 d,
                        uint4 x, uint4 s, uint4 ac);

        void rot_round3(uint4 &a, uint4 b, uint4 c, uint4 d,
                        uint4 x, uint4 s, uint4 ac);

        void rot_round4(uint4 &a, uint4 b, uint4 c, uint4 d,
                        uint4 x, uint4 s, uint4 ac);

    public: // functions
        MD5();
        virtual ~MD5();
        void update(const std::string& input);
        void update(const unsigned char* input, size_type length);
        void update(const char* input, size_type length);
        void update(char input);
        void finalize();
        void cleanup();
        std::string hexDigest();
};

class SHA1: public Object
{
    public:
        static std::string GenSHA1(const std::string& data, unsigned int length=0);

    private: // vars
        // Message digest buffers
        std::vector<unsigned int> H;

        // 512-bit message blocks
        unsigned int* Message_Block;

        // words block
        unsigned int* Words;

        // Message length in bits
        unsigned int Length_Low;

        // Message length in bits
        unsigned int Length_High;

        // Index into message block array
        int Message_Block_Index;

        // Is the digest computed?
        bool Computed;

        // Is the message digest corruped?
        bool Corrupted;

        // is the digest finalized?
        bool finalized;

    private:
        void ProcessMessageBlock();
        void PadMessage();
        unsigned int CircularShift(int bits, unsigned int word);

    public:
        SHA1();
        virtual ~SHA1();
        void reset();
        void update(const std::string& data);
        void update(const char* message_array, unsigned int length);
        void update(char message_element);
        bool result(int* message_digest_array);
        void finalize();
        std::string hexDigest();
};

} // namespace Hash

#endif /* __standalone_hashpp_header_defined__ */
