
#include "baselib.hpp"

VXInteger thread_call(VXState* v)
{
    VXInteger i;
    VXObject o = v->StackGet(1);
    if(o.Type() == VX_OT_THREAD)
    {
        VXInteger nparams = v->GetTop();
        _thread(o)->Push(o.Thread()->_roottable);
        for(i = 2; i<(nparams+1); i++)
        {
            VXState::MoveIndex(o.Thread(), v, i);
        }
        if(VX_SUCCEEDED(o.Thread()->CallStack(nparams, true, false)))
        {
            VXState::MoveIndex(v, o.Thread(), -1);
            o.Thread()->Pop(1);
            return 1;
        }
        v->_lasterror = o.Thread()->_lasterror;
        return VX_ERROR;
    }
    return v->ThrowError("wrong parameter");
}

VXInteger thread_wakeup(VXState* v)
{
    VXObject o = v->StackGet(1);
    if(o.Type() == VX_OT_THREAD)
    {
        VXState *thread = o.Thread();
        VXInteger state = thread->GetState();
        if(state != VX_VMSTATE_SUSPENDED)
        {
            switch(state)
            {
                case VX_VMSTATE_IDLE:
                    return v->ThrowError("cannot wakeup a idle thread");
                    break;
                case VX_VMSTATE_RUNNING:
                    return v->ThrowError("cannot wakeup a running thread");
                    break;
            }
        }

        VXInteger wakeupret = v->GetTop() > 1 ? 1 : 0;
        if(wakeupret)
        {
            VXState::MoveIndex(thread, v, 2);
        }
        if(VX_SUCCEEDED(thread->WakeUp(wakeupret,true,true,false)))
        {
            VXState::MoveIndex(v, thread, -1);
            thread->Pop(1);   //pop retval
            if(thread->GetState() == VX_VMSTATE_IDLE)
            {
                                 //pop roottable
                thread->SetTop(1);
            }
            return 1;
        }
        thread->SetTop(1);
        v->_lasterror = thread->_lasterror;
        return VX_ERROR;
    }
    return v->ThrowError("wrong parameter");
}

VXInteger thread_getstatus(VXState* v)
{
    VXObject& o = v->StackGet(1);
    //switch(vox_getvmstate(_thread(o)))
    VXInteger status = o.Thread()->GetState();
    switch(status)
    {
        case VX_VMSTATE_IDLE:
            v->PushString("idle");
            break;
        case VX_VMSTATE_RUNNING:
            v->PushString("running");
            break;
        case VX_VMSTATE_SUSPENDED:
            v->PushString("suspended");
            break;
        default:
            return v->ThrowError("internal VM error: unknown status (%d)!", status);
    }
    return 1;
}

VXInteger thread_getstackinfos(VXState* v)
{
    VXObject o = v->StackGet(1);
    if(o.Type() == VX_OT_THREAD)
    {
        VXState *thread = o.Thread();
        VXInteger threadtop = thread->GetTop();
        VXInteger level;
        //vox_getinteger(v,-1,&level);
        v->GetInteger(-1, &level);
        VXInteger res = __getcallstackinfos(thread,level);
        if(VX_FAILED(res))
        {
            //vox_settop(thread,threadtop);
            thread->SetTop(threadtop);
            if(thread->_lasterror.Type() == VX_OT_STRING)
            {
                v->ThrowError(thread->_lasterror.String()->Value());
            }
            else
            {
                v->ThrowError("unknown error");
            }
        }
        if(res > 0)
        {
            //some result
            VXState::MoveIndex(v, thread, -1);
            thread->SetTop(threadtop);
            return 1;
        }
        //no result
        thread->SetTop(threadtop);
        return 0;

    }
    return v->ThrowError("wrong parameter");
}

VXRegFunction VXSharedState::_thread_default_delegate_funcz[] =
{
    {"call", thread_call, -1, "v"},
    {"wakeup", thread_wakeup, -1, "v"},
    {"getstatus", thread_getstatus, 1, "v"},
    {"weakref",obj_delegate_weakref,1, NULL },
    {"getstackinfos",thread_getstackinfos,2, "vn"},
    {"tostring",default_delegate_tostring,1, "."},
    {0, 0, 0, 0},
};


