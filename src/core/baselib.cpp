
#include <iomanip>
#include <fstream>
#include <algorithm>
#include "baselib.hpp"
#include "names.hpp"
#include "debug.hpp"

bool str2num(const char *s,VXObject &res)
{
    char *end;
    if(strstr(s, "."))
    {
        VXFloat r = VXFloat(strtod(s, &end));
        if(s == end)
        {
            return false;
        }
        res = r;
        return true;
    }
    else
    {
        /*
        VXInteger r = VXInteger(strtol(s,&end,10));
        if(s == end)
        {
            return false;
        }
        */
        VXInteger r = VXInteger(atol(s));
        res = r;
        return true;
    }
}

template<typename Type>
std::string num2hex(Type ivalue, bool prefix, VXInteger width, char fill)
{
    std::stringstream stream;
    if(prefix)
    {
        stream << "0x";
    }
    stream << std::setfill(fill) << std::setw(width) << std::hex << ivalue;
    return stream.str();
}

VXInteger __getcallstackinfos(VXState* v,VXInteger level)
{
    VXTableObj* main_tb;
    VXTableObj* sub_tb;
    VXStackInfos si;
    VXInteger seq = 0;
    const char* name;
    const char* fn;
    const char* src;
    if (VX_SUCCEEDED(v->StackInfos(level, &si)))
    {
        VXObject value;
        fn = get_funcname(si);
        src = get_sourcename(si);
        main_tb = v->NewTable();
        sub_tb = v->NewTable();
        main_tb->NewSlot(v->NewString("func"), v->NewString(fn));
        main_tb->NewSlot(v->NewString("src"), v->NewString(src));
        main_tb->NewSlot(v->NewString("line"), si.line);
        while((name = v->GetLocal(level, seq, value)))
        {
            sub_tb->NewSlot(v->NewString(name), value);
            seq++;
        }
        main_tb->NewSlot(v->NewString("locals"), sub_tb);
        v->Push(main_tb);

        return 1;
    }

    return 0;
}

VXInteger default_delegate_repr(VXState* v)
{
    VXInteger status;
    VXObject& o = v->StackGet(1);
    VXRepr ser(v, o);
    if(VX_FAILED(status = ser.status()))
    {
        return status;
    }
    const std::string& str = ser.str();
    v->Push(v->NewString(str.c_str(), str.length()));
    return 1;
}

VXInteger default_delegate_tostring(VXState* v)
{
    bool do_serialize;
    VXInteger status;
    VXObject res;
    if(VX_FAILED(v->GetBool(2, &do_serialize)))
    {
        do_serialize = false;
    }
    if(VX_SUCCEEDED(status = v->ToStringAt(1, res)))
    {
        v->Push(res);
    }
    return status;
}

VXInteger obj_delegate_weakref(VXState* v)
{
    VXRawObj &o = v->StackGet(1);
    if(ISREFCOUNTED(type(o)))
    {
        v->Push(_refcounted(o)->GetWeakRef(type(o)));
    }
    else
    {
        v->Push(o);
    }
    return 1;
}

VXInteger obj_clear(VXState* v)
{
    return v->ClearAt(-1);
}

VXInteger get_slice_params(VXState* v,VXInteger &sidx,VXInteger &eidx,VXObject &o)
{
    VXInteger top = v->GetTop();
    sidx=0;
    eidx=0;
    o= v->StackGet(1);
    VXObject &start = v->StackGet(2);
    if(type(start) != VX_OT_NULL && start.IsNumeric())
    {
        sidx = start.Integer();
    }
    if(top>2)
    {
        VXObject &end = v->StackGet(3);
        if(end.IsNumeric())
        {
            eidx=end.Integer();
        }
    }
    else
    {
        eidx = v->GetSizeAt(1);
    }
    return 1;
}


VXInteger default_delegate_len(VXState* v)
{
    v->Push(VXInteger(v->GetSize(v->StackGet(1))));
    return 1;
}

VXInteger default_delegate_tofloat(VXState* v)
{
    VXObject& o = v->StackGet(1);
    switch(type(o))
    {
        case VX_OT_STRING:
            {
                VXObject res;
                if(str2num(_stringval(o),res))
                {
                    v->Push(VXObject(tofloat(res)));
                    break;
                }
            }
            return v->ThrowError("cannot convert the string to float");
            break;
        case VX_OT_INTEGER:
        case VX_OT_FLOAT:
            v->Push(VXObject(tofloat(o)));
            break;
        case VX_OT_BOOL:
            v->Push(VXObject((VXFloat)(_integer(o)?1:0)));
            break;
        default:
            v->PushNull();
            break;
    }
    return 1;
}

VXInteger default_delegate_tointeger(VXState* v)
{
    VXObject& o = v->StackGet(1);
    switch(type(o))
    {
        case VX_OT_STRING:
            {
                VXObject res;
                if(str2num(_stringval(o),res))
                {
                    v->Push(VXObject(tointeger(res)));
                    break;
                }
            }
            return v->ThrowError("cannot convert the string to integer");
            break;
        case VX_OT_INTEGER:
        case VX_OT_FLOAT:
            v->Push(VXObject(tointeger(o)));
            break;
        case VX_OT_BOOL:
            v->Push(VXObject(_integer(o)?(VXInteger)1:(VXInteger)0));
            break;
        default:
            v->PushNull();
            break;
    }
    return 1;
}


VXInteger container_rawset(VXState* v)
{
    return v->RawSetStack(-3);
}

VXInteger container_rawget(VXState* v)
{
    return v->RawGetStack(-2);
}


VXInteger number_delegate_tochar(VXState* v)
{
    VXRawObj &o=stack_get(v,1);
    char c = (char)tointeger(o);
    v->Push(v->NewString((const char*)&c, 1));
    return 1;
}

VXInteger number_delegate_tohex(VXState* v)
{
    VXInteger i;
    VXInteger width;
    bool prefix;
    std::string val;
    v->GetInteger(1, &i);
    if(VX_FAILED(v->GetBool(2, &prefix)))
    {
        prefix = false;
    }
    if(VX_FAILED(v->GetInteger(3, &width)))
    {
        width = 0;
    }
    val = num2hex(i, prefix, width, '0');
    v->PushString(val.c_str(), val.length());
    return 1;
}

/*
VXInteger weakref_ref(VXState* v)
{
    if(VX_FAILED(vox_getweakrefval(v,1)))
    {
        return VX_ERROR;
    }
    return 1;
}
*/

VXRegFunction VXSharedState::_number_default_delegate_funcz[]=
{
    {"tointeger", default_delegate_tointeger, 1,  "n|b"},
    {"tofloat",   default_delegate_tofloat,   1,  "n|b"},
    {"tostring",  default_delegate_tostring, -1, ".b"},
    {"tohex",     number_delegate_tohex,     -1,  ".bn"},
    {"tochar",    number_delegate_tochar,     1,  "n|b"},
    {"weakref",   obj_delegate_weakref,       1,  NULL},
    {"repr",      default_delegate_repr,      1,  "."},
    {0, 0, 0, 0}
};


VXRegFunction VXSharedState::_weakref_default_delegate_funcz[] =
{
    /*{"ref",      weakref_ref,               1, "r"},*/
    {"weakref",  obj_delegate_weakref,      1,  NULL },
    {"tostring", default_delegate_tostring, -1, ".b"},
    {"repr",     default_delegate_repr,     1,  "."},
    {0, 0, 0, 0}
};


/* --- VXIOBase ---- */

VXIOBase::VXIOBase()
{
    this->stream = NULL;
    this->isvalid = false;
    this->isclosed = false;
}

bool VXIOBase::open(const char* path, const char* mode)
{
    this->name = path;
    if((this->stream = fn_base_open(path, mode)) == NULL)
    {
        return false;
    }
    this->isvalid = true;
    return true;
}

bool VXIOBase::open(FILE* strm)
{
    this->stream = strm;
    this->isclosed = true;
    this->isvalid = true;
    return true;
}

const char* VXIOBase::error()
{
    return strerror(errno);
}

bool VXIOBase::close()
{
    if(this->isclosed == false)
    {
        fn_base_close(this->stream);
        this->isclosed = true;
    }
    return true;
}

size_t VXIOBase::readbuf(char* buffer, size_t howmuch, size_t nmemb)
{
    size_t haveread;
    haveread = fread(buffer,  nmemb, howmuch, this->stream);
    return haveread;
}

void VXIOBase::readline(std::string& dest)
{
    std::string buffer;
    std::string::iterator pos;
    while ((pos = std::find(buffer.begin(), buffer.end(), '\n')) == buffer.end())
    {
        char buf[1025];
        int n = read(fileno(this->stream), buf, 1024);
        if (n == -1)
        {
            dest = buffer;
            buffer = "";
            return;
        }
        buffer.append(buf, n);
    }
    dest = std::string(buffer.begin(), pos);
    buffer = std::string(pos+1, buffer.end());
}

bool VXIOBase::eof()
{
    return feof(this->stream) != 0;
}


bool VXIOBase::flushstream()
{
    if(fflush(this->stream) == EOF)
    {
        return false;
    }
    return true;
}

int VXIOBase::readchar()
{
    return fgetc(this->stream);
}

