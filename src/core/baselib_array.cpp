
#include <sstream>
#include "baselib.hpp"

//ARRAY DEFAULT DELEGATE///////////////////////////////////////

VXInteger array_append(VXState* v)
{
    VXObject arr = v->StackGet(1);
    arr.Array()->Append(v->StackGet(2));
    return VX_OK;
}

VXInteger array_extend(VXState* v)
{
    VXArrayObj* self = v->StackGet(1).Array();
    self->Extend(v->StackGet(2).Array());
    v->Push(self);
    return 1;
}

VXInteger array_reverse(VXState* v)
{
    VXInteger i;
    VXInteger n;
    VXInteger size;
    VXArrayObj* arr = v->StackGet(1).Array();
    VXArrayObj* narr = arr->Clone();
    if(arr->Size() > 0)
    {
        VXObject t;
        size = arr->Size();
        n = (size >> 1);
        size -= 1;
        for(i=0; i < n; i++)
        {
            t = arr->_values[i];
            narr->_values[i] = arr->_values[size-i];
            narr->_values[size-i] = t;
        }
    }
    v->Push(narr);
    return 1;
}

VXInteger array_pop(VXState* v)
{
    VXArrayObj* arr = v->StackGet(1).Array();
    if(arr->Size() > 0)
    {
        v->Push(arr->Top());
        arr->Pop();
        return 1;
    }
    return v->ThrowError("pop() on an empty array");
}

VXInteger array_top(VXState* v)
{
    VXArrayObj* arr = v->StackGet(1).Array();
    if(arr->Size() > 0)
    {
        v->Push(arr->Top());
        return 1;
    }
    return v->ThrowError("top() on a empty array");
}

VXInteger array_insert(VXState* v)
{
    VXArrayObj* arr = v->StackGet(1).Array();
    VXObject& idx = v->StackGet(2);
    VXObject& val = v->StackGet(3);
    if(!arr->Insert(idx.Integer(), val))
    {
        return v->ThrowError("insert() failed: index out of range");
    }
    return 0;
}

VXInteger array_remove(VXState* v)
{
    VXObject val;
    VXObject& o = v->StackGet(1);
    VXObject& idx = v->StackGet(2);
    if(!idx.IsNumeric())
    {
        return v->ThrowError("wrong type");
    }
    if(o.Array()->Get(idx.Integer(), val))
    {
        o.Array()->Remove(idx.Integer());
        v->Push(val);
        return 1;
    }
    return v->ThrowError("index out of range");
}

VXInteger array_resize(VXState* v)
{
    VXObject &o = v->StackGet(1);
    VXObject &nsize = v->StackGet(2);
    VXObject fill;
    if(nsize.IsNumeric())
    {
        if(v->GetTop() > 2)
        {
            fill = v->StackGet(3);
        }
        o.Array()->Resize(nsize.Integer(), fill);
        return 0;
    }
    return v->ThrowError("size must be a number");
}

VXInteger __map_array(VXState* v, VXArrayObj *dest,VXArrayObj *src)
{
    VXInteger n;
    VXInteger size;
    VXObject temp;
    size = src->Size();
    for(n=0; n<size; n++)
    {
        src->Get(n,temp);
        v->Push(src);
        v->Push(temp);
        if(VX_FAILED(v->StackCall(2, true, false)))
        {
            return VX_ERROR;
        }
        dest->Set(n, v->GetUp(-1));
        v->Pop();
    }
    return 0;
}


VXInteger array_map(VXState* v)
{
    VXObject& o = v->StackGet(1);
    VXInteger size = o.Array()->Size();
    VXObject ret = v->NewArray(size);
    if(VX_FAILED(__map_array(v, ret.Array(), o.Array())))
        return VX_ERROR;
    v->Push(ret);
    return 1;
}


VXInteger array_apply(VXState* v)
{
    VXObject& o = v->StackGet(1);
    if(VX_FAILED(__map_array(v, o.Array(), o.Array())))
    {
        return VX_ERROR;
    }
    return 0;
}

VXInteger array_reduce(VXState* v)
{
    VXObject& o = stack_get(v,1);
    VXArrayObj *a = o.Array();
    VXInteger size = a->Size();
    if(size == 0)
    {
        return 0;
    }
    VXObject res;
    a->Get(0,res);
    if(size > 1)
    {
        VXObject other;
        for(VXInteger n = 1; n < size; n++)
        {
            a->Get(n,other);
            v->Push(o);
            v->Push(res);
            v->Push(other);
            if(VX_FAILED(v->StackCall(3,true,false)))
            {
                return VX_ERROR;
            }
            res = v->GetUp(-1);
            v->Pop();
        }
    }
    v->Push(res);
    return 1;
}

VXInteger array_filter(VXState* v)
{
    VXObject& o = v->StackGet(1);
    VXArrayObj* a = o.Array();
    VXObject ret = v->NewArray();
    VXInteger size = a->Size();
    VXObject val;
    for(VXInteger n = 0; n < size; n++)
    {
        a->Get(n,val);
        v->Push(o);
        v->Push(n);
        v->Push(val);
        if(VX_FAILED(v->StackCall(3, true, false)))
        {
            return VX_ERROR;
        }
        if(!VXState::IsFalse(v->GetUp(-1)))
        {
            ret.Array()->Append(val);
        }
        v->Pop();
    }
    v->Push(ret);
    return 1;
}

VXInteger array_find(VXState* v)
{
    bool res;
    VXObject temp;
    VXInteger found_elements;
    VXInteger n;
    VXObject& o = v->StackGet(1);
    VXObject& val = v->StackGet(2);
    VXArrayObj* a = o.Array();
    VXInteger size = a->Size();
    found_elements = 0;
    res = false;
    for(n=0; n<size; n++)
    {
        a->Get(n, temp);
        if(VXState::IsEqual(temp, val, res) && res)
        {
            found_elements++;
        }
    }
    v->Push(found_elements);
    return 1;
}

//QSORT ala Sedgewick
bool _qsort_compare(VXState* v,VXObject &arr,VXObject &a,VXObject &b,VXInteger func,VXInteger &ret)
{
    (void)arr;
    if(func < 0)
    {
        if(!v->ObjCmp(a,b,ret))
        {
            return false;
        }
    }
    else
    {
        VXInteger top = v->GetTop();
        v->Repush(func);
        v->Push(v->GetRootTable());
        v->Push(a);
        v->Push(b);
        if(VX_FAILED(v->CallStack(3, true, false)))
        {
            if(!v->_lasterror.IsString())
            {
                v->ThrowError("compare func failed");
            }
            return false;
        }
        v->GetInteger(-1, &ret);
        v->SetTop(top);
        return true;
    }
    return true;
}

//QSORT ala Sedgewick
bool _qsort(VXState* v,VXObject &arr, VXInteger l, VXInteger r,VXInteger func)
{
    VXInteger i;
    VXInteger j;
    VXArrayObj *a= arr.Array();
    VXObject pivot,t;
    if( l < r )
    {
        pivot = a->_values[l];
        i = l;
        j = r + 1;
        while(1)
        {
            VXInteger ret;
            do
            {
                ++i;
                if(i > r)
                {
                    break;
                }
                if(!_qsort_compare(v,arr,a->_values[i],pivot,func,ret))
                {
                    return false;
                }
            } while( ret <= 0);
            do
            {
                --j;
                if (j < 0)
                {
                    v->ThrowError("Invalid qsort, probably compare function defect");
                    return false;
                }
                if(!_qsort_compare(v,arr,a->_values[j],pivot,func,ret))
                {
                    return false;
                }
            }
            while(ret > 0);
            if(i >= j)
            {
                break;
            }
            t = a->_values[i];
            a->_values[i] = a->_values[j];
            a->_values[j] = t;
        }
        t = a->_values[l];
        a->_values[l] = a->_values[j];
        a->_values[j] = t;
        if(!_qsort( v, arr, l, j-1,func))
        {
            return false;
        }
        if(!_qsort( v, arr, j+1, r,func))
        {
            return false;
        }
    }
    v->Push(arr);
    return true;
}

VXInteger array_sort(VXState* v)
{
    VXInteger func = -1;
    VXObject &o = v->StackGet(1);
    if(_array(o)->Size() > 1)
    {
        VXObject clone = o.Array()->Clone();
        if(v->GetTop() == 2)
        {
            func = 2;
        }
        if(!_qsort(v, clone, 0, clone.Array()->Size()-1, func))
        {
            return VX_ERROR;
        }
    }
    return 1;
}

VXInteger array_slice(VXState* v)
{
    VXInteger sidx;
    VXInteger eidx;
    VXObject o;
    if(get_slice_params(v, sidx, eidx, o) == -1)
    {
        return -1;
    }
    VXInteger alen = o.Array()->Size();
    if(sidx < 0)
    {
        sidx = alen + sidx;
    }
    if(eidx < 0)
    {
        eidx = alen + eidx;
    }
    if(eidx < sidx)
    {
        return v->ThrowError("wrong indexes");
    }
    if(eidx > alen)
    {
        return v->ThrowError("slice out of range");
    }
    VXArrayObj* arr = v->NewArray(eidx - sidx);
    VXObject t;
    VXInteger count=0;
    for(VXInteger i=sidx;i<eidx;i++)
    {
        _array(o)->Get(i,t);
        arr->Set(count++,t);
    }
    v->Push(arr);
    return 1;

}

VXInteger array_join(VXState* v)
{
    std::string delim;
    std::stringstream strm;
    VXObject& o = v->StackGet(1);
    VXArrayObj *a = o.Array();
    VXInteger size = a->Size();
    VXInteger n;
    VXObject temp;
    v->GetString(2, &delim);
    for(n=0; n<size; n++)
    {
        const char* stringval;
        VXInteger stringlen;
        if(a->Get(n, temp))
        {
            VXObject res;
            if(v->ToString(temp, res))
            {
                res.String()->Get(&stringval, &stringlen);
            }
            else
            {
                goto cannot_convert;
            }
            strm.write(stringval, stringlen);
            if((n+1) < size)
            {
                strm << delim;
            }
        }
    }
    v->Push(v->NewString(strm.str().c_str(), strm.str().length()));
    return 1;

cannot_convert:
    return v->ThrowError("join(): cannot convert element to string");
}

VXInteger array_isset(VXState* v)
{
    bool status;
    VXInteger index;
    VXObject val;
    VXObject& o = v->StackGet(1);
    VXArrayObj *a = o.Array();
    v->GetInteger(2, &index);
    if(a->Get(index, val))
    {
        status = true;
    }
    else
    {
        status = false;
    }
    v->Push(status);
    return 1;

    //v->Push(v->StackGet(1).Array()->IsSet(v->StackGet(2)));
    //return 1;
}

VXInteger array_first(VXState* v)
{
    VXObject val;
    VXObject& o = v->StackGet(1);
    VXArrayObj* a = o.Array();
    a->Get(0, val);
    v->Push(val);
    return 1;
}

VXInteger array_last(VXState* v)
{
    VXObject val;
    VXObject& o = v->StackGet(1);
    VXArrayObj* a = o.Array();
    a->Get(a->Size()-1, val);
    v->Push(val);
    return 1;
}

VXRegFunction VXSharedState::_array_default_delegate_funcz[]=
{
    {"weakref",  obj_delegate_weakref,      1,  NULL},
    {"tostring", default_delegate_tostring, -1,  ".b"},
    {"len",      default_delegate_len,      1,  "a"},
    {"clear",    obj_clear,                 1,  "."},
    {"first",    array_first,               1,  "a"},
    {"last",     array_last,                1,  "a"},
    {"append",   array_append,              2,  "a"},
    {"push",     array_append,              2,  "a"},
    {"extend",   array_extend,              2,  "aa"},
    {"pop",      array_pop,                 1,  "a"},
    {"top",      array_top,                 1,  "a"},
    {"insert",   array_insert,              3,  "an"},
    {"remove",   array_remove,              2,  "an"},
    {"resize",   array_resize,              -2, "an"},
    {"reverse",  array_reverse,             1,  "a"},
    {"sort",     array_sort,                -1, "ac"},
    {"slice",    array_slice,               -1, "ann"},
    {"map",      array_map,                 2,  "ac"},
    {"apply",    array_apply,               2,  "ac"},
    {"reduce",   array_reduce,              2,  "ac"},
    {"filter",   array_filter,              2,  "ac"},
    {"find",     array_find,                2,  "a."},
    {"join",     array_join,                2,  "as"},
    {"isset",    array_isset,               2,  "an"},
    {"repr",     default_delegate_repr,     1,  "."},
    {0, 0, 0, 0}
};


