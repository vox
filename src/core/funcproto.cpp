
#include "funcproto.hpp"

void VXFuncProtoObj::Mark(VXCollectable **chain)
{
    START_MARK()
        for(VXInteger i = 0; i < _nliterals; i++) VXSharedState::MarkObject(_literals[i], chain);
    for(VXInteger k = 0; k < _nfunctions; k++) VXSharedState::MarkObject(_functions[k], chain);
    END_MARK()
}

const char* VXFuncProtoObj::GetLocal(
    VXState *vm,
    VXUnsignedInteger stackbase,
    VXUnsignedInteger nseq,
    VXUnsignedInteger nop,
    VXObject& valuedest)
{
    VXUnsignedInteger nvars=_nlocalvarinfos;
    const char *res=NULL;
    if(nvars>=nseq)
    {
        for(VXUnsignedInteger i=0;i<nvars;i++)
        {
            if(_localvarinfos[i]._start_op<=nop && _localvarinfos[i]._end_op>=nop)
            {
                if(nseq==0)
                {
                    //vm->Push(vm->_stack[stackbase+_localvarinfos[i]._pos]);
                    valuedest = vm->_stack[stackbase+_localvarinfos[i]._pos];
                    res=_stringval(_localvarinfos[i]._name);
                    break;
                }
                nseq--;
            }
        }
    }
    return res;
}

VXInteger VXFuncProtoObj::GetLine(VXInstruction *curr)
{
    VXInteger op = (VXInteger)(curr-_instructions);
    VXInteger line=_lineinfos[0]._line;
    VXInteger low = 0;
    VXInteger high = _nlineinfos - 1;
    VXInteger mid = 0;
    while(low <= high)
    {
        mid = low + ((high - low) >> 1);
        VXInteger curop = _lineinfos[mid]._op;
        if(curop > op)
        {
            high = mid - 1;
        }
        else if(curop < op)
        {
            if((mid < (_nlineinfos - 1)) && (_lineinfos[mid + 1]._op >= op))
            {
                break;
            }
            low = mid + 1;
        }
        else                     //equal
        {
            break;
        }
    }
    line = _lineinfos[mid]._line;
    return line;
}


VXFuncProtoObj::VXFuncProtoObj(VXSharedState *ss)
{
    _stacksize=0;
    _bgenerator=false;
    INIT_CHAIN();ADD_TO_CHAIN(&_ss(this)->_gc_chain,this);
}

VXFuncProtoObj::~VXFuncProtoObj()
{
    REMOVE_FROM_CHAIN(&_ss(this)->_gc_chain,this);
}

bool VXFuncProtoObj::Save(VXState *v,VXUserPointer up,VXWriteFunc write)
{
    VXInteger i,nliterals = _nliterals,nparameters = _nparameters;
    VXInteger noutervalues = _noutervalues,nlocalvarinfos = _nlocalvarinfos;
    VXInteger nlineinfos=_nlineinfos,ninstructions = _ninstructions,nfunctions=_nfunctions;
    VXInteger ndefaultparams = _ndefaultparams;
    _CHECK_IO(WriteTag(v,write,up,VX_CLOSURESTREAM_PART));
    _CHECK_IO(WriteObject(v,up,write,_sourcename));
    _CHECK_IO(WriteObject(v,up,write,_name));
    _CHECK_IO(WriteTag(v,write,up,VX_CLOSURESTREAM_PART));
    _CHECK_IO(SafeWrite(v,write,up,&nliterals,sizeof(nliterals)));
    _CHECK_IO(SafeWrite(v,write,up,&nparameters,sizeof(nparameters)));
    _CHECK_IO(SafeWrite(v,write,up,&noutervalues,sizeof(noutervalues)));
    _CHECK_IO(SafeWrite(v,write,up,&nlocalvarinfos,sizeof(nlocalvarinfos)));
    _CHECK_IO(SafeWrite(v,write,up,&nlineinfos,sizeof(nlineinfos)));
    _CHECK_IO(SafeWrite(v,write,up,&ndefaultparams,sizeof(ndefaultparams)));
    _CHECK_IO(SafeWrite(v,write,up,&ninstructions,sizeof(ninstructions)));
    _CHECK_IO(SafeWrite(v,write,up,&nfunctions,sizeof(nfunctions)));
    _CHECK_IO(WriteTag(v,write,up,VX_CLOSURESTREAM_PART));
    for(i=0;i<nliterals;i++)
    {
        _CHECK_IO(WriteObject(v,up,write,_literals[i]));
    }

    _CHECK_IO(WriteTag(v,write,up,VX_CLOSURESTREAM_PART));
    for(i=0;i<nparameters;i++)
    {
        _CHECK_IO(WriteObject(v,up,write,_parameters[i]));
    }

    _CHECK_IO(WriteTag(v,write,up,VX_CLOSURESTREAM_PART));
    for(i=0;i<noutervalues;i++)
    {
        _CHECK_IO(SafeWrite(v,write,up,&_outervalues[i]._type,sizeof(VXUnsignedInteger)));
        _CHECK_IO(WriteObject(v,up,write,_outervalues[i]._src));
        _CHECK_IO(WriteObject(v,up,write,_outervalues[i]._name));
    }

    _CHECK_IO(WriteTag(v,write,up,VX_CLOSURESTREAM_PART));
    for(i=0;i<nlocalvarinfos;i++)
    {
        VXLocalVarInfo &lvi=_localvarinfos[i];
        _CHECK_IO(WriteObject(v,up,write,lvi._name));
        _CHECK_IO(SafeWrite(v,write,up,&lvi._pos,sizeof(VXUnsignedInteger)));
        _CHECK_IO(SafeWrite(v,write,up,&lvi._start_op,sizeof(VXUnsignedInteger)));
        _CHECK_IO(SafeWrite(v,write,up,&lvi._end_op,sizeof(VXUnsignedInteger)));
    }

    _CHECK_IO(WriteTag(v,write,up,VX_CLOSURESTREAM_PART));
    _CHECK_IO(SafeWrite(v,write,up,_lineinfos,sizeof(VXLineInfo)*nlineinfos));

    _CHECK_IO(WriteTag(v,write,up,VX_CLOSURESTREAM_PART));
    _CHECK_IO(SafeWrite(v,write,up,_defaultparams,sizeof(VXInteger)*ndefaultparams));

    _CHECK_IO(WriteTag(v,write,up,VX_CLOSURESTREAM_PART));
    _CHECK_IO(SafeWrite(v,write,up,_instructions,sizeof(VXInstruction)*ninstructions));

    _CHECK_IO(WriteTag(v,write,up,VX_CLOSURESTREAM_PART));
    for(i=0;i<nfunctions;i++)
    {
        _CHECK_IO(_funcproto(_functions[i])->Save(v,up,write));
    }
    _CHECK_IO(SafeWrite(v,write,up,&_stacksize,sizeof(_stacksize)));
    _CHECK_IO(SafeWrite(v,write,up,&_bgenerator,sizeof(_bgenerator)));
    _CHECK_IO(SafeWrite(v,write,up,&_varparams,sizeof(_varparams)));
    return true;
}

bool VXFuncProtoObj::Load(VXState *v,VXUserPointer up,VXReadFunc read,VXObject &ret)
{
    VXInteger i, nliterals,nparameters;
    VXInteger noutervalues ,nlocalvarinfos ;
    VXInteger nlineinfos,ninstructions ,nfunctions,ndefaultparams ;
    VXObject sourcename, name;
    VXObject o;
    _CHECK_IO(CheckTag(v,read,up,VX_CLOSURESTREAM_PART));
    _CHECK_IO(ReadObject(v, up, read, sourcename));
    _CHECK_IO(ReadObject(v, up, read, name));

    _CHECK_IO(CheckTag(v,read,up,VX_CLOSURESTREAM_PART));
    _CHECK_IO(SafeRead(v,read,up, &nliterals, sizeof(nliterals)));
    _CHECK_IO(SafeRead(v,read,up, &nparameters, sizeof(nparameters)));
    _CHECK_IO(SafeRead(v,read,up, &noutervalues, sizeof(noutervalues)));
    _CHECK_IO(SafeRead(v,read,up, &nlocalvarinfos, sizeof(nlocalvarinfos)));
    _CHECK_IO(SafeRead(v,read,up, &nlineinfos, sizeof(nlineinfos)));
    _CHECK_IO(SafeRead(v,read,up, &ndefaultparams, sizeof(ndefaultparams)));
    _CHECK_IO(SafeRead(v,read,up, &ninstructions, sizeof(ninstructions)));
    _CHECK_IO(SafeRead(v,read,up, &nfunctions, sizeof(nfunctions)));

    VXFuncProtoObj *f = VXFuncProtoObj::Create(_opt_ss(v),ninstructions,nliterals,nparameters,
        nfunctions,noutervalues,nlineinfos,nlocalvarinfos,ndefaultparams);
    VXObject proto = f;       //gets a ref in case of failure
    f->_sourcename = sourcename;
    f->_name = name;

    _CHECK_IO(CheckTag(v,read,up,VX_CLOSURESTREAM_PART));

    for(i = 0;i < nliterals; i++)
    {
        _CHECK_IO(ReadObject(v, up, read, o));
        f->_literals[i] = o;
    }
    _CHECK_IO(CheckTag(v,read,up,VX_CLOSURESTREAM_PART));

    for(i = 0; i < nparameters; i++)
    {
        _CHECK_IO(ReadObject(v, up, read, o));
        f->_parameters[i] = o;
    }
    _CHECK_IO(CheckTag(v,read,up,VX_CLOSURESTREAM_PART));

    for(i = 0; i < noutervalues; i++)
    {
        VXUnsignedInteger type;
        VXObject name;
        _CHECK_IO(SafeRead(v,read,up, &type, sizeof(VXUnsignedInteger)));
        _CHECK_IO(ReadObject(v, up, read, o));
        _CHECK_IO(ReadObject(v, up, read, name));
        f->_outervalues[i] = VXOuterObjVar(name,o, (VXOuterObjType)type);
    }
    _CHECK_IO(CheckTag(v,read,up,VX_CLOSURESTREAM_PART));

    for(i = 0; i < nlocalvarinfos; i++)
    {
        VXLocalVarInfo lvi;
        _CHECK_IO(ReadObject(v, up, read, lvi._name));
        _CHECK_IO(SafeRead(v,read,up, &lvi._pos, sizeof(VXUnsignedInteger)));
        _CHECK_IO(SafeRead(v,read,up, &lvi._start_op, sizeof(VXUnsignedInteger)));
        _CHECK_IO(SafeRead(v,read,up, &lvi._end_op, sizeof(VXUnsignedInteger)));
        f->_localvarinfos[i] = lvi;
    }
    _CHECK_IO(CheckTag(v,read,up,VX_CLOSURESTREAM_PART));
    _CHECK_IO(SafeRead(v,read,up, f->_lineinfos, sizeof(VXLineInfo)*nlineinfos));

    _CHECK_IO(CheckTag(v,read,up,VX_CLOSURESTREAM_PART));
    _CHECK_IO(SafeRead(v,read,up, f->_defaultparams, sizeof(VXInteger)*ndefaultparams));

    _CHECK_IO(CheckTag(v,read,up,VX_CLOSURESTREAM_PART));
    _CHECK_IO(SafeRead(v,read,up, f->_instructions, sizeof(VXInstruction)*ninstructions));

    _CHECK_IO(CheckTag(v,read,up,VX_CLOSURESTREAM_PART));
    for(i = 0; i < nfunctions; i++)
    {
        _CHECK_IO(_funcproto(o)->Load(v, up, read, o));
        f->_functions[i] = o;
    }
    _CHECK_IO(SafeRead(v,read,up, &f->_stacksize, sizeof(f->_stacksize)));
    _CHECK_IO(SafeRead(v,read,up, &f->_bgenerator, sizeof(f->_bgenerator)));
    _CHECK_IO(SafeRead(v,read,up, &f->_varparams, sizeof(f->_varparams)));

    ret = f;
    return true;
}
