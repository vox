
#include "object.hpp"
#include "closure.hpp"

void VXNativeClosureObj::Mark(VXCollectable **chain)
{
    START_MARK()
    for(VXUnsignedInteger i = 0; i < _noutervalues; i++)
    {
        VXSharedState::MarkObject(_outervalues[i], chain);
    }
    END_MARK()
}

void VXForeignClosureObj::Mark(VXCollectable **chain)
{
    START_MARK()
    if(_base)
    {
        _base->Mark(chain);
    }
    VXFuncProtoObj *fp = _function;
    fp->Mark(chain);
    for(VXInteger i = 0; i < fp->_noutervalues; i++)
    {
        VXSharedState::MarkObject(_outervalues[i], chain);
    }
    for(VXInteger k = 0; k < fp->_ndefaultparams; k++)
    {
        VXSharedState::MarkObject(_defaultparams[k], chain);
    }
    END_MARK()
}

bool VXForeignClosureObj::Save(VXState *v,VXUserPointer up,VXWriteFunc write)
{
    _CHECK_IO(WriteTag(v,write,up,VX_CLOSURESTREAM_HEAD));
    _CHECK_IO(WriteTag(v,write,up,sizeof(char)));
    _CHECK_IO(WriteTag(v,write,up,sizeof(VXInteger)));
    _CHECK_IO(WriteTag(v,write,up,sizeof(VXFloat)));
    _CHECK_IO(_function->Save(v,up,write));
    _CHECK_IO(WriteTag(v,write,up,VX_CLOSURESTREAM_TAIL));
    return true;
}

bool VXForeignClosureObj::Load(VXState *v,VXUserPointer up,VXReadFunc read,VXObject &ret)
{
    VXObject func;
    if(!CheckTag(v,read,up,VX_CLOSURESTREAM_HEAD))
    {
        // FIXME: technically speaking, this should be
        // handled by vox_readclosure
        v->Irrecoverable_Error("Invalid Bytecode signature!");
        return false;
    }
    _CHECK_IO(CheckTag(v,read,up,sizeof(char)));
    _CHECK_IO(CheckTag(v,read,up,sizeof(VXInteger)));
    _CHECK_IO(CheckTag(v,read,up,sizeof(VXFloat)));
    _CHECK_IO(VXFuncProtoObj::Load(v,up,read,func));
    _CHECK_IO(CheckTag(v,read,up,VX_CLOSURESTREAM_TAIL));
    ret = VXForeignClosureObj::Create(_ss(v),_funcproto(func));
    return true;
}


VXForeignClosureObj::~VXForeignClosureObj()
{
    __ObjRelease(_env);
    __ObjRelease(_base);
    REMOVE_FROM_CHAIN(&_ss(this)->_gc_chain,this);
}

