
#ifndef _VXCLASS_H_
#define _VXCLASS_H_


#define MEMBER_TYPE_METHOD 0x01000000
#define MEMBER_TYPE_FIELD 0x02000000

#define _ismethod(o) (_integer(o)&MEMBER_TYPE_METHOD)
#define _isfield(o) (_integer(o)&MEMBER_TYPE_FIELD)
#define _make_method_idx(i) ((VXInteger)(MEMBER_TYPE_METHOD|i))
#define _make_field_idx(i) ((VXInteger)(MEMBER_TYPE_FIELD|i))
#define _member_type(o) (_integer(o)&0xFF000000)
#define _member_idx(o) (_integer(o)&0x00FFFFFF)

#define calcinstancesize(_theclass_) \
    (_theclass_->_udsize + vox_aligning(sizeof(VXInstanceObj) +\
    (sizeof(VXObject) * \
    (_theclass_->_defaultvalues.size()>0?_theclass_->_defaultvalues.size()-1:0))))



#endif                           //_VXCLASS_H_
