

#include "utils.hpp"

VXInteger buf_lexfeed(VXUserPointer file)
{
    BufState *buf=(BufState*)file;
    if(buf->size<(buf->ptr+1))
        return 0;
    return buf->buf[buf->ptr++];
}

VXInteger file_read(VXUserPointer file,VXUserPointer buf,VXInteger size)
{
    VXInteger ret;
    if((ret = fread(buf, 1, size, (FILE*)file)) != 0)
    {
        return ret;
    }
    return -1;
}

VXInteger file_write(VXUserPointer file, VXUserPointer p, VXInteger size)
{
    return  fwrite(p, 1, size,(FILE*)file);
}

VXInteger _io_file_lexfeed_PLAIN(VXUserPointer file)
{
    VXInteger ret;
    char c;
    if((ret = fread(&c, sizeof(c), 1, (FILE*)file) > 0))
    {
        return c;
    }
    return 0;
}

VXInteger _io_file_lexfeed_UCS2_LE(VXUserPointer file)
{
    VXInteger ret;
    wchar_t c;
    if((ret = fread(&c, sizeof(c), 1, (FILE *)file) > 0))
    {
        return (char)c;
    }
    return 0;
}

VXInteger _io_file_lexfeed_UCS2_BE(VXUserPointer file)
{
    VXInteger ret;
    unsigned short c;
    if((ret = fread(&c, sizeof(c), 1, (FILE*)file) > 0))
    {
        c = ((c >> 8) & 0x00FF) | ((c << 8) & 0xFF00);
        return (char)c;
    }
    return 0;
}


