
#ifndef __vox_baselib_header_defined__
#define __vox_baselib_header_defined__

#include "pcheader.hpp"
#include "vm.hpp"
#include "string.hpp"
#include "table.hpp"
#include "funcproto.hpp"
#include "closure.hpp"
#include "class.hpp"

#include <stdlib.h>
#include <stdarg.h>
#include <ctype.h>
#if defined(VOX_PLATFORM_MSWINDOWS)
#   include <windows.h>
#endif

bool str2num(const char *s,VXObject &res);
VXInteger __getcallstackinfos(VXState* v,VXInteger level);
VXInteger default_delegate_tostring(VXState* v);
VXInteger obj_delegate_weakref(VXState* v);
VXInteger obj_clear(VXState* v);
VXInteger get_slice_params(VXState* v,VXInteger &sidx,VXInteger &eidx,VXObject &o);
VXInteger default_delegate_len(VXState* v);
VXInteger default_delegate_tofloat(VXState* v);
VXInteger default_delegate_tointeger(VXState* v);
VXInteger default_delegate_repr(VXState* v);
VXInteger container_rawset(VXState* v);
VXInteger container_rawget(VXState* v);

bool strhelp_startswith(const char* subject, VXInteger subject_len,
                        const char* search, VXInteger search_len);


bool strhelp_endswith(const char* subject, VXInteger subject_len,
                      const char* search, VXInteger search_len);


struct VXIOBase
{
    FILE* stream;
    bool isvalid;
    bool isclosed;
    const char* name;

    virtual FILE* fn_base_open(const char* path, const char* mode) = 0;
    virtual void fn_base_close(FILE* handle) = 0;

    VXIOBase();

    bool open(const char* path, const char* mode);

    bool open(FILE* strm);

    const char* error();

    bool close();

    size_t readbuf(char* buffer, size_t howmuch, size_t nmemb=1);

    void readline(std::string& buffer);

    bool eof();

    bool flushstream();

    int readchar();

    template<typename CharT>
    bool write(const CharT* data, size_t length, VXInteger* written)
    {
        size_t actually_written;
        *written = 0;
        actually_written = fwrite(data, sizeof(CharT), length, this->stream);
        if(actually_written < length)
        {
            return false;
        }
        *written = actually_written;
        return true;
    }
};

#endif /* __vox_baselib_header_defined__ */

