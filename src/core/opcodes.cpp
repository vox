

#include <cmath>
#include "opcodes.hpp"
#include "vm.hpp"

#define __VSTK(_vm_, a) (_vm_->_stack.values()[_vm_->_stackbase+(a)])
#define VSTK(vm, a) (((vm->_stack).values())[(vm->_stackbase)+(a)])


void FindOuter(VXState* v, VXObject &target, VXObject *stackindex)
{
    VXOuterObj **pp = &v->_openouters;
    VXOuterObj *p;
    VXOuterObj *otr;
    while ((p = *pp) != NULL && p->_valptr >= stackindex)
    {
        if (p->_valptr == stackindex)
        {
            target = VXObject(p);
            return;
        }
        pp = &p->_next;
    }
    otr = VXOuterObj::Create(_ss(v), stackindex);
    otr->_next = *pp;
    otr->_idx  = (stackindex - v->_stack.values());
    __ObjAddRef(otr);
    *pp = otr;
    target = VXObject(otr);
}

void RelocateOuters(VXState* v)
{
    VXOuterObj *p = v->_openouters;
    while (p)
    {
        p->_valptr = v->_stack.values() + p->_idx;
        p = p->_next;
    }
}

void CloseOuters(VXState* v, VXObject *stackindex)
{
    VXOuterObj *p;
    while ((p = v->_openouters) != NULL && p->_valptr >= stackindex)
    {
        p->_value = *(p->_valptr);
        p->_valptr = &p->_value;
        v->_openouters = p->_next;
        __ObjRelease(p);
    }
}


bool CallMetaMethod(VXState* v, VXObject &closure,
                             VXMetaMethod mm,
                             VXInteger nparams,
                             VXObject &outres)
{
    (void)mm;
    v->_nmetamethodscall++;
    if(v->Call(closure, nparams, v->_top - nparams, outres, false))
    {
        v->_nmetamethodscall--;
        v->Pop(nparams);
        return true;
    }
    v->_nmetamethodscall--;
    v->Pop(nparams);
    return false;
}



bool ArithMetaMethod(VXState* v,
                     VXInteger op,
                     const VXObject &o1,
                     const VXObject &o2,
                     VXObject &dest)
{
    VXMetaMethod mm;
    bool isdelegable = (is_delegable(o1) && _delegable(o1)->_delegate);
    bool usemeta = true;
    switch(op)
    {
        case '+':
            mm=VX_MT_ADD;
            break;
        case '-':
            mm=VX_MT_SUB;
            break;
        case '/':
            mm=VX_MT_DIV;
            break;
        case '*':
            mm=VX_MT_MUL;
            break;
        case '%':
            mm=VX_MT_MODULO;
            break;
        default:
            mm = VX_MT_ADD;
            vox_assert(0);
            break;
    }
    if(isdelegable)
    {
        VXObject closure;
        if(_delegable(o1)->GetMetaMethod(v, mm, closure))
        {
            v->Push(o1);
            v->Push(o2);
            return CallMetaMethod(v, closure,mm,2,dest);
        }
    }
    v->ThrowError("arith op '%c' on between '%s' and '%s'",
            op, o1.TypeString(), o2.TypeString());
    return false;
}

bool NEG_OP(VXState* v, VXObject &trg, const VXObject &o)
{

    switch(type(o))
    {
        case VX_OT_INTEGER:
            trg = -_integer(o);
            return true;
        case VX_OT_FLOAT:
            trg = -_float(o);
            return true;
        case VX_OT_TABLE:
        case VX_OT_USERDATA:
        case VX_OT_INSTANCE:
            if(_delegable(o)->_delegate)
            {
                VXObject closure;
                if(_delegable(o)->GetMetaMethod(v, VX_MT_UNM, closure))
                {
                    v->Push(o);
                    if(!CallMetaMethod(v, closure, VX_MT_UNM, 1, v->temp_reg))
                    {
                        return false;
                    }
                    _Swap(trg, v->temp_reg);
                    return true;

                }
            }
        default:
            break; //shutup compiler
    }
    v->ThrowError("attempt to negate a %s", GetTypeName(o));
    return false;
}

bool PLOCAL_INC(VXState* v, VXInteger op,
                       VXObject &target,
                       VXObject &a,
                       VXObject &incr)
{
    VXObject trg;
    _RET_ON_FAIL(ARITH_OP(v, op , trg, a, incr));
    target = a;
    a = trg;
    return true;
}

bool CMP_OP(VXState* v,
            VXInteger op,
            const VXObject &o1,
            const VXObject &o2,
            VXObject &res)
{
    VXInteger r;
    if(v->ObjCmp(o1, o2, r))
    {
        switch(op)
        {
            case CMP_G:
                res = (r > 0);
                return true;
            case CMP_GE:
                res = (r >= 0);
                return true;
            case CMP_L:
                res = (r < 0);
                return true;
            case CMP_LE:
                res = (r <= 0);
                return true;
            case CMP_3W:
                res = r;
                return true;
        }
        vox_assert(0);
    }
    return false;
}


bool Return(VXState* v, VXInteger _arg0,
                     VXInteger _arg1,
                     VXObject &retval)
{
    VXObject *dest;
    bool _isroot = v->ci->_root;
    VXInteger callerbase = v->_stackbase - v->ci->_prevstkbase;
    if (v->_debughook)
    {
        for(VXInteger i=0; i<v->ci->_ncalls; i++)
        {
            v->CallDebugHook('r');
        }
    }
    if (_isroot)
    {
        dest = &(retval);
    }
    else if (v->ci->_target == -1)
    {
        dest = NULL;
    }
    else
    {
        dest = &v->_stack.values()[callerbase + v->ci->_target];
    }
    if (dest)
    {
        if(_arg0 != 0xFF)
        {
            *dest = v->_stack.values()[v->_stackbase+_arg1];
        }
        else
        {
            dest->Null();
        }
    }
    v->LeaveFrame();
    return _isroot ? true : false;
}

bool ARITH_OP(VXState* v, VXUnsignedInteger op,
                       VXObject &trg,
                       const VXObject &o1,
                       const VXObject &o2)
{
    VXInteger tmask = o1.Type() | o2.Type();
    /*
    if(tmask & VX_RT_STRING)
    {
        if(op == '+')
        {
            if(!v->StringCat(o1, o2, trg))
            {
                return false;
            }
            return true;
        }
    }
    */

    if((tmask & VX_RT_STRING) && (op == '+'))
    {
        if(v->StringCat(o1, o2, trg))
        {
            return true;
        }
    }

    switch(tmask)
    {
        case VX_OT_INTEGER:
            {
                VXInteger res;
                VXInteger i1 = _integer(o1);
                VXInteger i2 = _integer(o2);
                switch(op)
                {
                    case '+':
                        res = i1 + i2;
                        break;
                    case '-':
                        res = i1 - i2;
                        break;
                    case '/':
                        if(i2 == 0)
                        {
                            v->ThrowError("division by zero");
                            return false;
                        }
                        res = i1 / i2;
                        break;
                    case '*':
                        res = i1 * i2;
                        break;
                    case '%':
                        if(i2 == 0)
                        {
                            v->ThrowError("modulo by zero");
                            return false;
                        }
                        res = i1 % i2;
                        break;
                    default:
                        res = 0xDEADBEEF;
                }
                trg = res;
            }
            break;
        case (VX_OT_FLOAT|VX_OT_INTEGER):
        case (VX_OT_FLOAT):
            {
                VXFloat res;
                VXFloat f1 = tofloat(o1);
                VXFloat f2 = tofloat(o2);
                switch(op)
                {
                    case '+':
                        res = f1 + f2;
                        break;
                    case '-':
                        res = f1 - f2;
                        break;
                    case '/':
                        res = f1 / f2;
                        break;
                    case '*':
                        res = f1 * f2;
                        break;
                    case '%':
                        res = VXFloat(fmod((double)f1,(double)f2));
                        break;
                    default:
                        res = 0x0f;
                }
                trg = res;
            }
            break;
        default:
            if(!ArithMetaMethod(v, op, o1, o2, trg))
            {
                return false;
            }
    }
    return true;
}

bool BW_OP(VXState* v,
           VXUnsignedInteger op,
           VXObject& trg,
           const VXObject& o1,
           const VXObject& o2)
{
    VXInteger res;
    if((type(o1) | type(o2)) == VX_OT_INTEGER)
    {
        VXInteger i1 = _integer(o1);
        VXInteger i2 = _integer(o2);
        switch(op)
        {
            case BW_AND:
                res = i1 & i2;
                break;
            case BW_OR:
                res = i1 | i2;
                break;
            case BW_XOR:
                res = i1 ^ i2;
                break;
            case BW_SHIFTL:
                res = i1 << i2;
                break;
            case BW_SHIFTR:
                res = i1 >> i2;
                break;
            case BW_USHIFTR:
                res = (VXInteger)(*((VXUnsignedInteger*)&i1) >> i2);
                break;
            default:
            {
                v->ThrowError("internal vm error: bitwise op failed");
                return false;
            }
        }
    }
    else
    {
        v->ThrowError("bitwise op between '%s' and '%s'",GetTypeName(o1),GetTypeName(o2));
        return false;
    }
    trg = res;
    return true;
}

bool CLOSURE_OP(VXState* v, VXObject &target,
                         VXFuncProtoObj *func)
{
    VXInteger nouters;
    VXForeignClosureObj *closure = VXForeignClosureObj::Create(_ss(v), func);
    if((nouters = func->_noutervalues))
    {
        for(VXInteger i = 0; i<nouters; i++)
        {
            VXOuterObjVar &outer = func->_outervalues[i];
            switch(outer._type)
            {
                case otLOCAL:

                    FindOuter(v,
                        closure->_outervalues[i],
                        &VSTK(v, _integer(outer._src))
                    );
                    break;
                case otOUTER:
                    closure->_outervalues[i] =
                        _closure(v->ci->_closure)->_outervalues[_integer(outer._src)];
                    break;
            }
        }
    }
    VXInteger ndefparams;
    if((ndefparams = func->_ndefaultparams))
    {
        for(VXInteger i = 0; i < ndefparams; i++)
        {
            VXInteger spos = func->_defaultparams[i];
            closure->_defaultparams[i] = v->_stack.values()[v->_stackbase + spos];
        }
    }
    target = closure;
    return true;
}

bool CLASS_OP(VXState* v,
                VXObject &target,
                VXInteger baseclass,
                VXInteger attributes,
                VXInteger name)
{
    VXClassObj *base = NULL;
    VXObject nob;
    VXObject attrs;
    if(baseclass != -1)
    {
        if(type(v->_stack.values()[v->_stackbase+baseclass]) != VX_OT_CLASS)
        {
            v->ThrowError("cannot inherit from type %s",
                GetTypeName(v->_stack.values()[v->_stackbase+baseclass]));
            return false;
        }
        base = _class(v->_stack.values()[v->_stackbase + baseclass]);
    }
    if(attributes != MAX_FUNC_STACKSIZE)
    {
        attrs = v->_stack.values()[v->_stackbase+attributes];
    }
    target = VXClassObj::Create(_ss(v), base);
    if(type(_class(target)->_metamethods[VX_MT_INHERITED]) != VX_OT_NULL)
    {
        int nparams = 2;
        VXObject ret;
        v->Push(target);
        v->Push(attrs);
        v->Call(_class(target)->_metamethods[VX_MT_INHERITED],
             nparams, (v->_top - nparams), ret, false);
        v->Pop(nparams);
    }

    
    if((v->_stackbase+name) < v->_stack.size())
    {
        nob = v->_stack.values()[v->_stackbase+name];
        if(nob.Type() == VX_OT_STRING)
        {
            _class(target)->_name = nob;
        }
    }
    else
    {
        _class(target)->_name = v->NewString("<anonymous class>");
    }
    _class(target)->_attributes = attrs;
    return true;
}

bool FOREACH_OP(VXState* v,
        VXObject &o1,
        VXObject &o2,
        VXObject &o3,
        VXObject &o4,
        VXInteger arg_2,
        int exitpos,
        int &jump)
{
    VXInteger nrefidx;
    (void)arg_2;
    switch(type(o1))
    {
        case VX_OT_TABLE:
            if((nrefidx = _table(o1)->Next(false,o4, o2, o3)) == -1)
            {
                _FINISH(exitpos);
            }
            o4 = (VXInteger)nrefidx;
            _FINISH(1);
        case VX_OT_ARRAY:
            if((nrefidx = _array(o1)->Next(o4, o2, o3)) == -1)
            {
                _FINISH(exitpos);
            }
            o4 = (VXInteger) nrefidx; _FINISH(1);
        case VX_OT_STRING:
            if((nrefidx = _string(o1)->Next(o4, o2, o3)) == -1)
            {
                _FINISH(exitpos);
            }
            o4 = (VXInteger)nrefidx;
            _FINISH(1);
        case VX_OT_CLASS:
            if((nrefidx = _class(o1)->Next(o4, o2, o3)) == -1)
            {
                _FINISH(exitpos);
            }
            o4 = (VXInteger)nrefidx;
            _FINISH(1);
        case VX_OT_USERDATA:
        case VX_OT_INSTANCE:
            if(_delegable(o1)->_delegate)
            {
                VXObject itr;
                VXObject closure;
                if(_delegable(o1)->GetMetaMethod(v, VX_MT_NEXTI, closure))
                {
                    v->Push(o1);
                    v->Push(o4);
                    if(CallMetaMethod(v, closure, VX_MT_NEXTI, 2, itr))
                    {
                        o4 = o2 = itr;
                        if(type(itr) == VX_OT_NULL)
                        {
                            _FINISH(exitpos);
                        }
                        if(!v->Get(o1, itr, o3, false, DONT_FALL_BACK))
                        {
                            // could be changed
                            v->ThrowError("__nexti__ returned an invalid index");
                            return false;
                        }
                        _FINISH(1);
                    }
                    else
                    {
                        return false;
                    }
                }
                v->ThrowError("__nexti__ failed");
                return false;
            }
            break;
        case VX_OT_GENERATOR:
            if(_generator(o1)->_state == VXGeneratorObj::eDead)
            {
                _FINISH(exitpos);
            }
            if(_generator(o1)->_state == VXGeneratorObj::eSuspended)
            {
                VXInteger idx = 0;
                if(type(o4) == VX_OT_INTEGER)
                {
                    idx = _integer(o4) + 1;
                }
                o2 = idx;
                o4 = idx;
                _generator(o1)->Resume(v, o3);
                _FINISH(0);
            }
        default:
            v->ThrowError("cannot iterate %s", GetTypeName(o1));
    }
    return false; //cannot be hit(just to avoid warnings)
}

bool DerefInc(VXState* v, VXInteger op,
                       VXObject &target,
                       VXObject &self,
                       VXObject &key,
                       VXObject &incr,
                       bool postfix,
                       VXInteger selfidx)
{
    VXObject tmp;
    VXObject tself = self;
    VXObject tkey = key;
    if(!v->Get(tself, tkey, tmp, false, selfidx))
    {
        return false;
    }
    if(!ARITH_OP(v, op , target, tmp, incr))
    {
        return false;
    }
    if(!v->Set(tself, tkey, target,selfidx))
    {
        return false;
    }
    if(postfix)
    {
        target = tmp;
    }
    return true;
}

