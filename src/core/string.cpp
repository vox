
#include "string.hpp"

VXStringObj* VXStringObj::Create(VXSharedState* ss, const char* str)
{
    VXStringObj* pstr = VXStringObj::Create(ss, str, -1);
    return pstr;
}

VXStringObj *VXStringObj::Create(VXSharedState *ss,const char *s,VXInteger len)
{
    VXStringObj* pstr= ADD_STRING(ss,s,len);
    return pstr;
}

void VXStringObj::Release()
{
    REMOVE_STRING(_sharedstate,this);
}

VXInteger VXStringObj::Next(const VXObject &refpos, VXObject &outkey, VXObject &outval)
{
    VXInteger idx = (VXInteger)TranslateIndex(refpos);
    while(idx < _len)
    {
        outkey = (VXInteger)idx;
        outval = VXInteger(_val[idx]);
        //return idx for the next iteration
        return ++idx;
    }
    //nothing to iterate anymore
    return -1;
}

void VXStringObj::Get(const char** dest_str, VXInteger* dest_len)
{
    (*dest_str) = _val;
    if(dest_len != NULL)
    {
        (*dest_len) = _len;
    }
}
