
#include "pcheader.hpp"
#include "utils.hpp"
#include "object.hpp"
#include "vm.hpp"
#include "state.hpp"

VXArrayObj::VXArrayObj(VXSharedState *ss,VXInteger nsize)
{
    _values.resize(nsize);
    INIT_CHAIN();
    ADD_TO_CHAIN(&_ss(this)->_gc_chain,this);
}

VXArrayObj::~VXArrayObj()
{
    REMOVE_FROM_CHAIN(&_ss(this)->_gc_chain,this);
}

VXArrayObj* VXArrayObj::Create(VXSharedState *ss,VXInteger nInitialSize)
{
    VXArrayObj *newarray = (VXArrayObj*)VX_MALLOC(sizeof(VXArrayObj));
    new (newarray) VXArrayObj(ss,nInitialSize);
    return newarray;
}

void VXArrayObj::Mark(VXCollectable **chain)
{
    START_MARK()
        VXInteger len = _values.size();
    for(VXInteger i = 0;i < len; i++)
    {
        VXSharedState::MarkObject(_values[i], chain);
    }
    END_MARK()
}

VXOType VXArrayObj::GetType()
{
    return VX_OT_ARRAY;
}

void VXArrayObj::Finalize()
{
    _values.resize(0);
}
bool VXArrayObj::Get(const VXInteger nidx,VXObject &val)
{
    if((nidx >= 0) && (nidx < ((VXInteger)_values.size())))
    {
        VXObject &o = _values[nidx];
        val = VX_REALVAL(o);
        return true;
    }
    return false;
}

bool VXArrayObj::Set(const VXInteger nidx,const VXObject &val)
{
    if(nidx>=0 && nidx < (VXInteger)_values.size())
    {
        _values[nidx] = val;
        return true;
    }
    else
    {
        VXObject fill;
        Resize(nidx + 1, fill);
        return Set(nidx, val);
    }
    return false;
}

VXInteger VXArrayObj::Next(const VXObject &refpos, VXObject &outkey, VXObject &outval)
{
    VXUnsignedInteger idx=TranslateIndex(refpos);
    while(idx < _values.size())
    {
        //first found
        outkey=(VXInteger)idx;
        VXObject &o = _values[idx];
        outval = VX_REALVAL(o);
        //return idx for the next iteration
        return ++idx;
    }
    //nothing to iterate anymore
    return -1;
}

VXArrayObj* VXArrayObj::Clone()
{
    VXArrayObj *anew=Create(_opt_ss(this),0);
    anew->_values.copy(_values);
    return anew;
}

VXInteger VXArrayObj::Size() const
{
    return _values.size();
}

void VXArrayObj::Resize(VXInteger size)
{
    VXObject _null;
    Resize(size,_null);
}

void VXArrayObj::Resize(VXInteger size,VXObject &fill)
{
    _values.resize(size,fill);
    ShrinkIfNeeded();
}

void VXArrayObj::Reserve(VXInteger size)
{
    _values.reserve(size);
}

void VXArrayObj::Append(const VXObject &o)
{
    _values.push_back(o);
}

void VXArrayObj::Extend(const VXArrayObj *a)
{
    VXInteger xlen;
    if((xlen=a->Size()))
        for(VXInteger i=0;i<xlen;i++)
            Append(a->_values[i]);
}


VXObject& VXArrayObj::Top()
{
    return _values.top();
}

void VXArrayObj::Pop()
{
    _values.pop_back();
    ShrinkIfNeeded();
}

bool VXArrayObj::Insert(VXInteger idx,const VXRawObj &val)
{
    if(idx < 0 || idx > (VXInteger)_values.size())
    {
        return false;
    }
    _values.insert(idx,val);
    return true;
}

void VXArrayObj::ShrinkIfNeeded()
{
                            //shrink the array
    if(_values.size() <= _values.capacity()>>2)
    {
        _values.shrinktofit();
    }
}

bool VXArrayObj::Remove(VXInteger idx)
{
    if(idx < 0 || idx >= (VXInteger)_values.size())
    {
        return false;
    }
    _values.remove(idx);
    ShrinkIfNeeded();
    return true;
}

void VXArrayObj::Release()
{
    vox_delete(this,VXArrayObj);
}
