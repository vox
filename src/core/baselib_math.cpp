
#include "baselib.hpp"
#include <cmath>
#include <ctime>


#ifndef M_PI
#   define M_PI (3.14159265358979323846)
#endif


#define single_arg_func(InType, OutType, v, fn_cb) \
    do \
    { \
        InType arg; \
        VXObject& ob = v->StackGet(2); \
        arg = ob.As<InType>(); \
        v->Push((OutType)(fn_cb(arg))); \
    } while(0);

#define float_func(v, fn_cb) \
    single_arg_func(VXFloat, VXFloat, v, fn_cb)

#define int_func(v, fn_cb) \
    single_arg_func(VXInteger, VXInteger, v, fn_cb)

VXInteger math_srand(VXState* v)
{
    VXInteger seed;
    if(VX_FAILED(v->GetInteger(2, &seed)))
    {
        seed = time(NULL);
    }
    srand((unsigned int)seed);
    return 0;
}

VXInteger math_rand(VXState* v)
{
    v->Push(VXInteger(rand()));
    return 1;
}

VXInteger math_abs(VXState* v)
{
    int_func(v, cos);
    return 1;
}

VXInteger math_sqrt(VXState* v)
{
    float_func(v, sqrt);
    return 1;
}

VXInteger math_fabs(VXState* v)
{
    float_func(v, fabs);
    return 1;
}

VXInteger math_sin(VXState* v)
{
    float_func(v, sin);
    return 1;
}

VXInteger math_cos(VXState* v)
{
    float_func(v, cos);
    return 1;
}


VXInteger math_asin(VXState* v)
{
    float_func(v, asin);
    return 1;
}


VXInteger math_acos(VXState* v)
{
    float_func(v, acos)
    return 1;
}


VXInteger math_log(VXState* v)
{
    float_func(v, log)
    return 1;
}


VXInteger math_log10(VXState* v)
{
    float_func(v, log10)
    return 1;
}


VXInteger math_tan(VXState* v)
{
    float_func(v, tan)
    return 1;
}


VXInteger math_atan(VXState* v)
{
    float_func(v, atan)
    return 1;
}


VXInteger math_floor(VXState* v)
{
    float_func(v, floor)
    return 1;
}


VXInteger math_ceil(VXState* v)
{
    float_func(v, ceil)
    return 1;
}


VXInteger math_exp(VXState* v)
{
    float_func(v, ceil)
    return 1;
}


VXInteger math_atan2(VXState* v)
{
    VXFloat p1;
    VXFloat p2;
    v->GetFloat(2, &p1);
    v->GetFloat(3, &p2);
    v->Push(VXFloat(atan2(p1, p2)));
    return 1;
}


VXInteger math_pow(VXState* v)
{
    VXFloat p1, p2;
    v->GetFloat(2, &p1);
    v->GetFloat(3, &p2);
    v->Push(VXFloat(pow(p1, p2)));
    return 1;
}


static VXRegFunction vox_mathlib_funcs[] =
{

    {"sqrt",  math_sqrt,  2,  ".n"},
    {"sin",   math_sin,   2,  ".n"},
    {"cos",   math_cos,   2,  ".n"},
    {"asin",  math_asin,  2,  ".n"},
    {"acos",  math_acos,  2,  ".n"},
    {"log",   math_log,   2,  ".n"},
    {"log10", math_log10, 2,  ".n"},
    {"tan",   math_tan,   2,  ".n"},
    {"atan",  math_atan,  2,  ".n"},
    {"atan2", math_atan2, 3,  ".nn"},
    {"pow",   math_pow,   3,  ".nn"},
    {"floor", math_floor, 2,  ".n"},
    {"ceil",  math_ceil,  2,  ".n"},
    {"exp",   math_exp,   2,  ".n"},
    {"srand", math_srand, -1, ".n"},
    {"rand",  math_rand,  1,  NULL},
    {"fabs",  math_fabs,  2,  ".n"},
    {"abs",   math_abs,   2,  ".n"},
    {0, 0, 0, 0},
};

VXInteger voxstd_register_mathlib(VXState* v)
{
    VXTableObj* tb = v->RegisterLib("math", vox_mathlib_funcs, true);
    tb->NewSlot(v->NewString("PI"), VXFloat(M_PI));
    v->Push(tb);
    return 1;
}


