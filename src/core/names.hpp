
#ifndef __vox_names_hpp__
#define __vox_names_hpp__

// names
#define NAMES_VARGV    "vargv"
#define NAMES_MAINFUNC "<main>"
#define NAMES_ANONFUNC "<anonymous>"
#define NAMES_ANONFILE "<unknown>"


// token names
#define NAMES_TK_THISVAR  "this"
#define NAMES_TK_CTOR     "constructor"

// generator statuses
#define GENERATOR_STATUS_SUSPENDED "suspended"
#define GENERATOR_STATUS_RUNNING   "running"
#define GENERATOR_STATUS_DEAD      "dead"

// types names and other yadda yadda
#define S_TYPE_NULL         "null"
#define S_TYPE_STRING       "string"
#define S_TYPE_INTEGER      "integer"
#define S_TYPE_FLOAT        "float"
#define S_TYPE_USERPOINTER  "userpointer"
#define S_TYPE_TABLE        "table"
#define S_TYPE_ARRAY        "array"
#define S_TYPE_CLOSURE      "closure"
#define S_TYPE_ARRAY        "array"
#define S_TYPE_ARRAY        "array"
#define S_TYPE_ARRAY        "array"


#endif /* __vox_names_hpp__ */
